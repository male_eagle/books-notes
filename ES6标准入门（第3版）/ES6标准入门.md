# 第 1 章		ECMAScript 6 简介

ECMAScript6（以下简称 ES6）的目标是使 JavaScript 语言可以用于编写复杂的大型应用程序，成为企业级开发语言。

## ECMAScript 和 JavaScript 的关系

ECMAScript 和 JavaScript 的关系是：前者是后者的规格，后者是前者的一种实现。另外的 ECMAScript 方言还有 JScript 和 ActionScript。日常场合中两个词是可以互换的。

## ES6 与 ECMAScript 2015 的关系

ES6 既是一个历史名词，也是一个泛指，含义是 5.1 版本后的 JavaScript 的下一代标准，涵盖了 ES2015、ES2016、ES2017 等，而 ES2015 则是正式名称，特指当年发布的正式版本的语言标准。本书中的 ES6 一般是指 ES2015 标准。

## 语法提案的批准流程

## ECMAScript 的历史

## 部署进度

使用 `es-checker` 可以检查各种运行环境对 ES6 的支持情况。

```shell
npm install -g es-checker
es-checker
```

## Babel 转码器

`Babel` 是一个广为使用的 ES6 转码器，可将 ES6 代码转为 ES5 代码，从而在浏览器或其他环境执行，而不必担心环境是否支持。

### 配置文件 .babelrc

`Babel` 的配置文件是 `.babelrc`，存放在项目根目录下。使用 Babel 的第一步就是配置这个文件。

该文件用来设置转码规则和插件，基本格式如下：

```json
{
  "presets": [],
  "plugins": []
}
```

`presets` 字段设定转码规则，官方提供以下的规则集，可以根据需要进行安装。

```shell
# 最新转码规则
npm install --save-dev babel-preset-latest
# react 转码规则
npm install --save-dev babel-preset-react
# 不同阶段语法提案的转码规则，4个阶段选装一个
npm install --save-dev babel-preset-stage-0
npm install --save-dev babel-preset-stage-1
npm install --save-dev babel-preset-stage-2
npm install --save-dev babel-preset-stage-3
```

然后将规则加入 .babelrc 中。

```json
{
  "presets": [
    "latest",
    "react",
    "stage-2"
  ],
  "plugins": []
}
```

要注意，想使用以下所有 Babel 工具和模块，必须先写好 .babelrc 文件。

### 命令行转码 babel-cli

Babel 提供 babel-cli 工具，用于命令行转码。书中 p6 有在全局环境下进行 Babel 转码的示例。

将 babel-cli 安装在项目之中，可以使不同项目使用不同版本的 Babel。

```shell
npm install --save-dev babel-cli
```

然后改写 `package.json` 文件。

```json
{
  // ...
  "devDependencies": {
    "babel-cli": "^6.0.0"
  },
  "scripts": {
    "bulid": "babel src -d lib"
  },
}
```

转码的时候需要执行以下命令。

```shell
npm run build
```

### babel-node

babel-cli 工具自带一个 babel-node 命令，提供一个支持 ES6 的 REPL 环境。babel-node 可以直接运行 ES6 脚本。

### babel-register

babel-register 模块改写了 require 命令。此后，每当使用 require 加载后缀为 .js、.jsx、.es 和 .es6 的文件时，就会先用 Babel 进行转码，然后再引入文件中。由于是实时转码，所以只适合在开发环境中使用。

```shell
npm install --save-dev babel-register
```

使用时必须先加载 babel-register。

```js
require('babel-register');
require('./index.js');	// 如此就不用手动对 index.js 进行转码了
```

### babel-core

如果某些代码需要调用 Babel 的 API 进行转码，就要使用 babel-core 模块。使用方法查阅 p8。

### babel-polyfill

Babel 默认只转换新的 JavaScript 句法，而不转换新的 API 以及一些定义在全局对象上的方法，这些都不会转码。例如 ES6 在 Array 对象上新增了 Array.from 方法，Babel 就不会转码这个方法，如果想让该方法运行，必须使用 babel-polyfill 为当前环境提供一个垫片。具体使用方法翻阅 p9。

### 浏览器环境

Babel 也可以用于浏览器环境。以 Babel 配合 Browserify，具体方法翻阅 p10。

### 在线转换

Babel 提供一个 REPL 在线编辑器（babeljs.io/repl/），可以在线将 ES6 代码转为 ES5 代码。转换后的代码可以直接作为 ES5 代码插入网页并运行。

### 与其他工具的配合

许多工具需要使用 Babel 进行前置转码，此处举两个例子：ESLint 和 Mocha。Mocha 是一个测试框架。

## Traceur 转码器

Google 公司的 Traceur 转码器也可以将 ES6 代码转为 ES5 代码。

### 直接插入网页

本节介绍如何直接在网页中使用 Traceur。

### 在线转换

Traceur 也提供了一个在线编辑器，可以在线将 ES6 代码转为 ES5 代码。

### 命令行转换

本节介绍如何把 Traceur 当作命令行工具来使用。

### Node 环境的用法

本节介绍如何在 Node 中使用 Traceur。







# 第 2 章		let 和 const 命令

## let 命令

### 基本用法

ES6 新增了 `let` 命令，用于声明变量，用法类似于 var，但所声明的变量只在 let 命令所在的代码块中有效。

```js
{
  let a = 10;
  var b = 1;
}
console.log(a);	// ReferenceError: a is not defined
console.log(b);	// 1
```

下面的代码如果使用 var，最后将输出 10。

```js
var a = [];
for (var i = 0; i < 10; i++) {
  a[i] = function () {
    console.log(i);
  };
}
a[6]();	// 10
```

上面的代码中，变量 i 是 var 声明的，在全局范围内都有效，所以全局只有一个变量 i。每次循环变量 i 的值都会发生改变，而循环内被赋给数组 a 的成员的函数内部的 console.log(i) 的 i 指向全局的 i。所以，所有数组 a 的成员中的 i 指向的都是同一个 i，导致运行时输出的是最后一轮的 i 值，也就是 10。

如果使用 let，声明的变量仅在块级作用域内有效，最后将输出 6。

```js
var a = [];
for (let i = 0; i < 10; i++) {
	a[i] = function () {
    console.log(i);
  };
}
a[6]();	// 6
```

上面的代码中，变量 i 是 let 声明的，当前 i 只在本轮循环内有效。所以每次循环的 i 都是一个新的变量，所以最后输出的是 6。但这样有个问题，那就是如果每一轮循环的变量 i 都是重新声明的，那它怎么知道上一轮循环中 i 的值从而计算出本轮循环中的 i 值呢？这是因为 JavaScript 引擎内部会记住上一轮循环的值，初始化本轮循环变量 i 时，就在上一轮循环的基础上进行计算。

另外，for 循环中还有个特别之处，就是设置循环变量的那部分是一个父作用域，而循环体内部是一个单独的子作用域。

```js
for (let i = 0; i < 3; i++) {
  let i = 'abc';
  console.log(i);
}
// abc
// abc
// abc
```

正确运行上述代码将输出 3 次 abc。这表明循环体内部的变量 i 与循环变量 i 不在同一个作用域，而是有各自单独的作用域。

### 不存在变量提升

var 命令存在变量提升现象，即变量可以在声明之前使用，值为 undefined。

```js
console.log(foo);	// undefined
var foo = 2;
// 上述代码是这样执行的，脚本运行时 foo 变量就已经存在
var foo;
console.log(foo);
foo = 2;
```

为了纠正这种现象，let 命令改变了语法行为，它所声明的变量一定要在声明后使用，否则会报错。

```js
console.log(bar);	// 报错 ReferenceError
let bar = 2;
```

### 暂时性死区

只要块级作用域内存在 let（还有 const）命令，它所声明的变量就绑定到这个作用域中，不再受外部的影响。

```js
var tmp = 123;
if (true) {
  tmp = 'abc';	// ReferenceError
  let tmp;
}
```

上述代码中存在全局变量 tmp，但是块级作用域内 let 又声明了一个局部变量 tmp，导致局部变量 tmp 绑定了块级作用域，所以在 let 声明变量 tmp 前为 tmp 赋值会报错。ES6 明确规定，如果区块中存在 let 和 const 命令，则这个区块对这些命令声明的变量从一开始就形成封闭作用域。只要在声明之前就使用这些变量，就会报错。总之，在代码块中，使用 let 命令声明变量之前该变量都是不可用的。这在语法上称为 “暂时性死区”，简称 TDZ。

```js
if (true) {
  // TDZ 开始
  tmp = 'abc';	// ReferenceError
  console.log(tmp);	// ReferenceError
  let tmp;	// TDZ 结束
  console.log(tmp);	// undefined
  tmp = 123;
  console.log(tmp);	// 123
}
```

上面代码中，let 命令声明变量 tmp 之前，都属于变量 tmp 的死区。暂时性死区也意味着 typeof 不再是百分百安全的操作。

```js
typeof x;	// ReferenceError
let x;
```

作为比较，如果一个变量根本没有被声明，使用 typeof 反而不会报错。

```js
typeof y;	// "undefined"
```

使用 let 声明变量时，只要变量在还没有声明完成前使用，也是暂时性死区报错。

```js
var x = x;	// 不报错
let x = x;	// ReferenceError：x is not defined
```

### 不允许重复声明

let 不允许在相同作用域内重复声明同一个变量。

```js
// 报错
function () {
  let a = 10;
  var a = 1;
}
// 报错
function () {
  let a = 10;
  let a = 1;
}
```

所以也不能在函数内部重新声明参数。

```js
function func (arg) {
  let arg;	// 报错
}
function func (arg) {
  {
    let arg;	// 不报错
  }
}
```

## 块级作用域

### 为什么需要块级作用域

ES5 只有全局作用域和函数作用域，没有块级作用域，这导致很多场景不合理。

```js
// 第一种场景，内层变量可能覆盖外层变量
var tmp = new Date();
function f () {
  console.log(tmp);
  if (false) {
    var tmp = 'hello world';
  }
}
f();	// undefined
```

上面的代码因为进行了变量提升，导致内层的 tmp 变量覆盖了外层的 tmp 变量。

```js
var tmp = new Date();
function f () {
 	var tmp;	// 声明变量提升到函数作用域最前面 
  console.log(tmp);
  if (false) {
    tmp = 'hello world';
  }
}
f();	// undefined
```

```js
// 第二种场景，用来计数的循环变量泄露为全局变量
var s = 'hello';
for (var i = 0; i < s.length; i++) {
  console.log(s[i]);
}
console.log(i);	// 5
```

上面代码中，变量 i 只用来控制循环，循环结束后并没有消失，而是泄露成了全局变量。

### ES6 的块级作用域

let 实际上为 JavaScript 新增了块级作用域。

```js
function f1 () {
  let n = 5;
  if (true) {
    let n = 10;
  }
  console.log(n);	// 5，如果使用 var 声明 n，那么将输出 10
}
```

上述代码表示外层代码块不受内层代码块的影响。ES6 允许块级作用域的任意嵌套，外层作用域无法读取内层作用域的变量。内层作用域可以定义外层作用域的同名变量。

```js
{{{{
  { let insane = 'hello world' }
	console.log(insane);	// 报错
}}}};
{{{{
  let insane = 'hello world';
  { let insane = 'hello world' }
}}}}
```

### 块级作用域与函数声明

ES5 规定，函数只能在顶层作用域和函数作用域中声明，不能在块级作用域声明。

```js
// 情况一
if (true) {
  function f () {}
}
// 情况二
try {
  function f () {}
} catch (e) {}
```

上面两种函数声明在 ES5 中都是非法的。但是浏览器为了兼容以前的旧代码，还是支持在块级作用域中声明函数，所以上面两种情况实际上都能运行，并不会报错。ES6 引入了块级作用域，明确允许在块级作用域中声明函数。ES6 规定，在块级作用域之中，函数声明语句的行为类似于 let，在块级作用域之外不可引用。

```js
function f () { console.log('i am outside') }
(function () {
  if (false) {
    // 重复声明一次函数 f
    function f () { console.log('i am inside') }
  }
  f();
})();
```

以上代码在 ES5 中运行会得到 i am inside。因为在 if 内声明的函数 f 会被提升到函数头部，实际运行的代码如下。

```js
// ES5 环境
function f () { console.log('i am outside') }
(function () {
  function f () { console.log('i am inside') }
  if (false) {
  }
  f();
})();
```

在 ES6 中运行理论上会得到 i am outside，但如果真的在 ES6 浏览器中运行上面代码，是会报错的，这是因为浏览器 ES6 环境中，块级作用域内声明函数的行为实际上会在顶层作用域或函数作用域中使用 var 声明一个与函数同名的变量，而不是像规定一样类似于 let 把函数声明在块级作用域中。

```js
// ES6 环境
function f () { console.log('i am outside') }
(function () {
  var f = undefined;
  if (false) {
    function f () { console.log('i am inside') }
  }
  f();	// Uncaught TypeError: f is not a function
})();
```

简而言之，就是 ES5 块级作用域中声明函数会提升，ES6 块级作用域中声明函数会使用 var 声明一个同名变量。考虑到环境导致的行为差异大，应该避免在块级作用域内声明函数，如果确实需要，应该写成函数表达式的形式。另外，ES6 的块级作用域规定声明函数的规则只能在使用大括号的情况下成立，如果没有使用大括号，会报错。

```js
// 不报错
'use strict';
if (true) {
  function f () {}
}
// 报错
'use strict';
if (true)
  function f () {}
```

### do 表达式

本质上，块级作用域是将多个操作封装在一起的语句，没有返回值。可以使用 do 表达式来获取块级作用域的返回值。

```js
let x = do {
  let t = f();
  t * t + 1;
};
```

实际测试后会报错，可能是因为没有转为 ES5 语法。

## const 命令

### 基本用法

`const` 声明一个只读的常量，一旦声明，常量的值就不能改变。而这也意味着 const 一旦声明常量，就必须立即初始化，不能留到以后赋值。对于 const 而言，只声明不赋值就会报错。

```js
const PI = 3.1415926;
PI = 3;	// TypeError: Assignment to constant variable.

const foo;	// SyntaxError: Missing initializer in const declaration.
```

const 的作用域与 let 命令相同，只在声明所在的块级作用域内有效。const 声明的变量也存在暂时性死区，只能在声明后使用。const 声明变量也不可重复声明。

```js
if (true) {
  console.log(MAX);	// ReferenceError
  const MAX = 5;
}
console.log(MAX);	// Uncaught ReferenceError: MAX is not defined

var message = 'Hello';
const message = 'World';	// 报错
```

### 本质

const 实际上保证的并不是变量的值不得改动，而是变量保存的那个内存地址不得改动。对于简单类型的数据，值就保存在变量保存的内存地址中，因此等同于常量。但对于复合类型的数据，变量保存的内存地址保存的是一个指向该复合类型数据的指针，const 保证的是变量的内存地址不变，而指针指向的复合类型是可变的，也就是说 const 声明的变量如果是数组或对象等复合类型数据，那么不能将变量重新声明为新的数组或对象等其他复合类型数据，但变量指向的数组或对象可以添加自己的成员和属性等。

```js
const foo = {};
// 可以添加属性
foo.prop = 123;
// 将 foo 指向另一个对象，就会报错
foo = {};	// TypeError: "foo" is read-only
```

如果真的想将对象冻结，不能添加属性，应该使用 `Object.freeze` 方法。

```js
const foo = Object.freeze({});
// 常规模式下，下面这一行不起作用；严格模式下，会报错
foo.prop = 123;
```

### ES6 声明变量的 6 种方法

ES5 只有 var 和 function 两种命令。ES6 新增了 let、const 和后面介绍的 import 和 class 四种命令。

## 顶层对象的属性

顶层对象在浏览器环境中指的是 window 对象，在 Node 环境中指的是 global 对象。ES5 中，顶层对象的属性与全局变量是等价的，这被认为是 JavaScript 语言中最大的设计败笔之一（原因翻阅 p30）。ES6 为了改变这一点，规定 var 和 function 命令声明的全局变量依旧是顶层对象的属性，而 let、const 和 class 命令声明的全局变量不属于顶层对象的属性。

```js
var a = 1;
window.a;	// 1
let b = 1;
window.b;	// undefined
```

## global 对象

ES5 的顶层对象具有在各种实现中不统一的问题。浏览器中，顶层对象是 window，Node 中顶层对象是 global。同一段代码为了能够在各种环境中都取得顶层对象，一般使用 this 变量，但也有局限性。p31 记载了两种勉强可以使用的方法，方法会返回不同环境下的顶层对象。







# 第 3 章		变量的解构赋值

## 数组的解构赋值

### 基本用法

ES6 允许按照一定模式从数组和对象中提取值，然后对变量进行赋值，这被称为解构赋值。

```js
// 以前写法
let a = 1;	let b = 2;	let c = 3;
// ES6 允许这样写
let [a, b, c] = [1, 2, 3];
```

上面的代码表示，可以从数组中提取值，按照对应位置对变量赋值。本质上这种写法属于模式匹配，只要等号两边的模式相同，右边的值就会赋给左边对应的变量。下面是一些例子。

```js
let [foo, [[bar], baz]] = [1, [[2], 3]];	// foo 1;	bar 2;	baz 3;

let [x, , y] = [1, 2, 3];	// x 1;	y 3;

let [head, ...tail] = [1, 2, 3, 4];	// head 1;	tail [2, 3, 4];
```

如果解构不成功（变量没有对应的值），变量的值就等于 undefined。

```js
let [foo] = [];					// foo undefined;
let [bar, foo] = [1];		// foo undefined;	bar 1;
```

还有一种情况是不完全结构，即等号左边的模式只匹配一部分等号右边的数组。

```js
let [x, y] = [1, 2, 3];	// x 1;	y 2;

let [a, [b], d] = [1, [2, 3], 4];	// a 1;	b 2; d 4;
```

如果等号右边不是数组（严格来说是不可遍历的结构，详见第 15 章），那么将会报错。

```js
// 以下代码都会报错
let [foo] = 1;
let [foo] = false;
let [foo] = NaN;
let [foo] = undefined;
let [foo] = null;
let [foo] = {};
```

前五个表达式等号右边转为对象后的值不具备 `Iterator` 接口，最后一个表达式等号右边的值本身就不具备 Iterator 接口，所以上面的语句都会报错。

对于 Set 结构，也可以使用数组的解构赋值。

```js
let [x, y, z] = new Set(['a', 'b', 'c']);	// x 'a';	y 'b'; z 'c';
```

事实上，只要某种数据结构具有 Iterator 接口，都可以采用数组形式的解构赋值。

```js
funciton* fn () {
  ...
}
let [first, second, third] = fn();
```

上面的代码中，fn 函数是一个 `Generator` 函数（参见第 16 章），原生具有 Iterator 接口。解构赋值依次从这个接口中获取值。

### 默认值

解构赋值允许指定默认值。

```js
let [foo = true] = [];	// foo true;

let [x, y = 'b'] = ['a', undefined];	// x 'a'; y 'b';
```

ES6 内部使用严格相等运算符 `===` 判断一个位置是否有对应的值，如果位置上对应的值不是 undefined 或值为空的话，默认值是不会生效的。

```js
let [x = 1] = [undefined];	// 默认值生效，x 等于 1

let [x = 1] = [null];	// 默认值失效，因为 x 对应的值不为 undefined 或空，所以 x 等于 null
```

默认值可以引用解构赋值的其他变量，前提是变量已经声明。

```js
let [x = 1, y = x] = [];	// x 1; y 1;
let [x = 1, y = x] = [2];	// x 2; y 2;
let [x = 1, y = x] = [1, 2];	// x 1; y 2;
let [x = y, y = 1] = [];	// ReferenceError
```

## 对象的解构赋值

对象的解构与数组有一个重要的不同。数组的元素是按顺序排列，变量的取值是由元素的位置决定的。而对象的属性没有顺序，变量必须与属性同名才能取到正确的值。

```js
let { bar, foo } = { foo: 'aaa', bar: 'bbb' };	// foo 'aaa';	bar 'bbb';
```

实际上，对象的解构赋值是下面形式的简写。

```js
let { foo: foo, bar: bar } = { foo: 'aaa', bar: 'bbb' };
```

如果变量名与属性名不一致，应该写成下面这样。

```js
let { foo: baz } = { foo: 'aaa', bar: 'bbb' };	// baz 'aaa';
```

对象的解构赋值的内部机制是先找到同名属性，然后再赋值给对应的变量。就像上面的例子中，先找到左边对象 foo 对应的右边对象的同名 foo 属性，然后把右边 foo 属性的值赋给左边同名 foo 属性的属性值变量 baz。左边对象中，foo 是匹配模式，baz 才是变量，真正被赋值的是后者，而不是前者。与数组一样，解构也可以用于嵌套结构的对象。

```js
let obj = {
  p: [
    'Hello',
    { y: 'World' }
  ]
};
let { p: [x, { y } ] } = obj;	// x 'Hello';	y 'World';
```

上例中的 p 是匹配模式，而不是变量，所以不会被赋值。如果 p 也要作为变量赋值，可以写成下面这样。

```js
let { p, p: [x, { y } ] } = obj;	// p ['Hello', { y: 'World' } ];	x 'Hello';	y 'World';
```

嵌套赋值的例子如下：

```js
let obj = {};
let arr = [];
({ foo: obj.prop, bar: arr[0] } = { foo: 123, bar: true });	// obj { prop: 123 };	arr [true];
```

上面的解构赋值包含在括号中是因为如果不包含在括号中，那么 JavaScript 引擎会将 { foo: obj.prop, bar: arr[0] } 理解成一个代码块，从而发生语法错误。

对象的解构也可以指定默认值。默认值生效的条件是对象的属性值严格等于 undefined 或为空。

```js
let { x = 3 } = {};	// x 3;
let { x = 3 } = { x: undefined };	// x 3;
let { x = 3 } = { x: null };	// x null;
```

如果变量没有对应的同名属性或解构失败，那么取不到值，最后等于 undefined。

```js
let { baz } = { foo: 'aaa', bar: 'bbb' };	// baz undefined;
```

所以如果解构模式是嵌套的对象，且子对象所在的父属性不存在，那么将会报错。

```js
let { foo: { bar } } = { baz: 'baz' };	// undefined.bar 报错
```

对象的解构赋值可以很方便的将现有对象的方法赋值到某个变量。下面的代码把 Math 对象的对数、正弦、余弦三个方法赋值到对应的变量上。

```js
let { log, sin, cos } = Math;
```

因为数组本质是特殊的对象，所以可以对数组进行对象属性的解构。

```js
let arr = [1, 2, 3];
let { 0: first } = arr;	// first 1;
```

## 字符串的解构赋值

字符串也可以解构赋值，因为此时字符串被转换成了一个类似数组的对象。类似数组的对象都有一个 length 属性，所以还可以对这个属性进行解构赋值。

```js
let str = 'hello';
let [a, b, c, d, e] = str;	// a 'h';	b 'e'; c 'l';	d 'l'; e 'o';
let { length: len } = str;	// len 5;
```

## 数值和布尔值的解构赋值

解构赋值的规则是，只要等号右边的值不是对象或数组，就先将其转为对象。而 undefined 和 null 无法转为对象，所以对它们进行解构赋值时都会报错。

```js
let { prop: x } = undefined;	// TypeError
let { prop: y } = null;	// TypeError

let { toString: s } = 123;
let { toString: b } = true;
```

上面例子中，数值和布尔值的包装对象都有 toString 属性，所以变量 s 和 b 都能取到值。

## 函数参数的解构赋值

函数的参数也可以使用解构赋值。

```js
function add ([x, y]) {
  return x + y;
}
add([1, 2]);	// 3
```

函数的解构赋值没有这么简单，具体翻阅 p42。

## 圆括号问题

只要有可能，就不要在模式中放置圆括号。

### 不能使用圆括号的情况

#### 变量声明语句

```js
// 全部报错
let [(a)] = [1];
let { x: (c) } = {};
let { o: ({ p: p}) } = { o: { p: 2} };
```

#### 函数参数

```js
// 全部报错
function f([(z)]) = { return z; }
function f([z, (x)]) = { return x; }
```

#### 赋值语句的模式

```js
// 全部报错
({ p: a }) = { p: 42 };
([a]) = [5];
```

### 可以使用圆括号的情况

可以使用圆括号的情况只有一种：赋值语句的非模式部分可以使用圆括号。

```js
// 全部正确
[(b)] = [3];	// 如果使用 let，如 let [(b)] = [3] 就会报错
({ p: (d) } = {});
[( parseInt.prop )] = [3];
```

## 用途

### 交换变量的值

```js
let x = 1;
let y = 2;
[x, y] = [y, x];
```

上面的代码交换 x 和 y 的值，写法简洁易读。

### 从函数返回多个值

```js
// 返回一个数组
function example () {
  return [1, 2, 3];
}
let [a, b, c] = example();

// 返回一个对象
function example () {
  return {
    foo: 1,
    bar: 2
  };
}
let { foo, bar } = example();
```

### 函数参数的定义

```js
// 参数是一组有次序的值
function f ([x, y, z]) { ... }
f([1, 2, 3]);
// 参数是一组无次序的值
function f ({x, y, z}) { ... }
f({z: 3, y: 2, x: 1});
```

### 提取 JSON 数据

解构赋值对提取 JSON 对象中的数据尤其有用。

```js
let jsonData = {
  id: 42,
  status: "OK",
  data: [123, 456]
}
let { id, status, data: number } = jsonData;
console.log(id, status, number);	// 42, "OK", [123, 456]
```

### 函数参数的默认值

```js
function fn (data, {
  name = 'zs',
  age = 21
})
```

### 遍历 Map 结构

任何部署了 Iterator 接口的对象都可以用 `for ... of` 循环遍历。Map 结构原生支持 Iterator 接口，配合变量的解构赋值获取键名和键值十分方便。

```js
var map = new Map();
map.set('first', 'hello');
map.set('second', 'world');
for (let [key, value] of map) {
  console.log(key + ' is ' + value);	// first is hello, second is world
}

// 单独获取键名
for (let [key] of map) { ... }

// 单独获取键值
for (let [, value] of map) { ... }
```

### 输入模块的指定方法

加载模块时，需要指定输入的方法，即要使用的方法。引入 source-map 模块中的 SourceMapConsumer 和 SourceNode 两个方法。

```js
const { SourceMapConsumer, SourceNode } = require("source-map");
```







# 第 4 章		字符串的扩展

## 字符的 Unicode 表示法

JavaScript 允许采用 `\uxxxx` 表示一个字符，其中 `xxxx` 表示字符的 Unicode 码点，`\u0061` 表示字符 a。但这种表示方法只限于码点在 0000 ~ FFFF 之间的字符，超出这个范围的字符必须用 2 个双字节的形式表达。ES6 对这一点做出了改进，只要将码点放入大括号，就能正确解读该字符。

```js
"\uD842\uDFB7" === "𠮷"
"\u20BB7" === "7"	// JavaScript 会理解为 \u20BB + 7，\u20BB 是不可打印字符，只显示一个空格
"\u{20BB7}" === "𠮷"
```

有了这种表示法后，JavaScript 共有 6 种方法可以表示一个字符。

```js
"\z" === "\172" === "\x7A" === "\u007A" === "\u{7A}" === "z" 
```

## codePointAt()

JavaScript 内部，字符以 UTF-16 的格式存储，每个字符固定为 2 个字节。对于 4 个字节存储的字符（Unicode 码点大于 0xFFFF 的字符），JavaScript 会认为它们是 2 个字符。

```js
var s = "𠮷";
s.length;	// 2
s.charAt(0);	// ''
s.charAt(1);	// ''
s.charCodeAt(0);	// 55362
s.charCodeAt(1);	// 57271
```

对于 4 个字节的字符，JavaScript 不能正确处理，字符长度会被误判为 2，而且 charAt 方法无法读取整个字符，charCodeAt 方法只能分别返回前 2 个字节和后 2 个字节的值。ES6 提供了 codePointAt 方法，能够正确处理 4 个字节存储的字符，返回字符的码点。

```js
var s = "𠮷a";
s.codePointAt(0);	// 134071	十六进制的 20BB7
s.codePointAt(1);	// 57271
s.codePointAt(2);	// 97
```

codePointAt 方法的参数仍然是不正确的，a 的位置应该是 1，但传入的是 2。解决问题的方法是使用 for ... of 循环，因为它会正确识别 32 位的 UTF-16 字符。

```js
var s = "𠮷a";
for (let ch of s) {
  console.log(ch.codePointAt(0).toString(16));	// 20bb7 61
}
```

`codePointAt` 方法是测试一个字符是由 2 个字节还是 4 个字节组成的最简单方法。

```js
function is32Bit (str) {
  return str.codePointAt(0) > 0xFFFF;
}
```

## String.fromCodePoint()

ES5 提供了 String.fromCharCode 方法，用于从码点返回对应字符，但这个方法不能识别 32 位的 UTF-16 字符。ES6 提供了 `String.fromCodePoint` 方法，可以识别大于 0xFFFF 的字符，弥补 fromCharCode 方法的不足。作用上，正好与 codePointAt 方法相反。`String.fromCodePoint(0x20BB7) === '𠮷'`。

注意：fromCodePoint 方法定义在 String 对象上，而 codePointAt 方法定义在字符串的实例对象上。

## 字符串的遍历器接口

ES6 为字符串添加了遍历器接口（详见第 15 章），使得字符串可以使用 for ... of 循环遍历。除了遍历字符串，这个遍历器的最大优点是可以识别大于 0xFFFF 的码点，传统 for 循环无法识别。

```js
var text = String.fromCodePoint(0x20BB7);
for (let i = 0; i < text.length; i++) {
  console.log(text[i]);	// 输出 "" 和	""
}
for (let i of text) {
  console.log(i);	// "𠮷"
}
```

字符串 text 只有一个字符，但 for 循环会认为它包含 2 个字符，且都不可打印。而 for ... of 循环会正确识别出字符。

## at()

ES5 对字符串对象提供了 charAt 方法，返回字符串给定位置的字符，但不能识别码点大于 0xFFFF 的字符。目前有一个提案，提出字符串实例的 at 方法，可以识别 Unicode 编号大于 0xFFFF 的字符，返回正确的字符。at 方法可以通过垫片库（p53）实现。

## normalize()

ES6 为字符串实例提供了 normalize 方法，用来将字符的不同表示方法统一为同样的形式，这称为 Unicode 正规化。

## includes()、startsWith()、endsWith()

之前，JavaScript 只有 indexOf 方法来确定一个字符串是否包含在另一个字符串中。ES6 又提供了下列方法。

- includes()：返回布尔值，表示是否找到了参数字符串。
- startsWith()：返回布尔值，表示参数字符串是否在源字符串的头部。
- endsWith()：返回布尔值，表示参数字符串是否在源字符串的尾部。

这 3 个方法的第一个参数为源字符串，也都支持第二个参数，表示开始搜索的位置。其中 endsWith 方法的第二个参数针对前 n 个字符，其他两个方法针对从第 n 个位置到字符结束位置之间的字符。

```js
var s = 'Hello World!';
s.startsWith('Hello');	// true
s.startsWith('World', 6);	// true
s.endsWith('!');	// true
s.endsWith('Hello', 5);	// true
s.includes('o');	// true
s.includes('Hello', 6);	// false
```

## repeat()

repeat 方法接收一个参数 n，表示将原字符串重复 n 次，返回重复后的字符串。参数如果是小数，正数会被向下取整，负数会被向上取整，如果负数取整后小于 0，那么会报错。如果参数是 Infinity，会报错。如果参数是 NaN，会被转为 0。如果参数是字符串，会先转为数字。

```js
'hello'.repeat(2);	// 'hellohello'
'na'.repeat(0);	// ''
'na'.repeat(2.8);	// 'nana'
'na'.repeat(-0.8);	// ''
'na'.repeat(-1);	// RangeError
'na'.repeat(Infinity);	// RangeError
'na'.repeat(NaN);	// ''
'na'.repeat('3');	// 'nanana'
'na'.repeat('ss');	// ''
```

## padStart()、padEnd()

ES2017 引入了字符串补全长度的功能。如果某个字符串不够指定长度，可以使用 `padStart` 方法补全头部，`padEnd` 方法补全尾部。方法接收两个参数，第一个参数用来指定字符串的最小长度，第二个参数是用来补全的字符串，默认为空格。如果原字符串长度等于或大于指定的最小长度，返回原字符串。

```js
'x'.padStart(5, 'ab');	// 'ababx'
'x'.padEnd(5, 'ad');	// 'xabab'
'x'.padStart(4, 'ab');	// 'abax'
'x'.padEnd(4);	// 'x   '
'xxx'.padStart(2, 'ab');	// 'xxx'
'xxx'.padEnd(3);	// 'xxx'
'abcd'.padEnd(7, 'efghijk');	// 'abcdefg'
```

padStart 的常见用途是为数值补全指定位数。

```js
'123456'.padStart(10, '0');	// '0000123456'
```

另一个用途是提示字符串格式。

```js
'12'.padStart(10, 'YYYY-MM-DD');	// 'YYYY-MM-12'
'09-12'.padStart(10, 'YYYY-MM-DD');	// 'YYYY-09-12'
```

## 模板字符串

ES6 引入模板字符串来解决 JavaScript 中字符串拼接繁琐的问题。模板字符串是增强版的字符串，用反引号 \` 标识。模板字符串中的所有空格和缩进都会被保留在输出中。模板字符串中可以使用 `${}` 来嵌入变量或执行任意的 JavaScript 表达式，可以计算，亦可以引用对象属性，然后把结果替换到字符串中。

```js
let str = `
	<html>
		<head></head>
		<body></body>
	</html>
`
console.log(str);
//'
//	<html>
//		<head></head>
//		<body></body>
//	</html>
//'

console.log(`hello ${'eagle'}`);	// 'hello eagle'

let [x, y] = [1, 2];
console.log(`${x} + ${y} = ${x + y}`);	// '1 + 2 = 3'

let message = 'Hello World';
console.log(`${message}, eagle`);	// 'Hello World, eagle'

function fn () {
  return 'Hello World'
}
console.log(`${fn()}, eagle`);	// 'Hello World, eagle'
```

如果模板字符串中的变量没有声明，将报错。

```js
console.log(`${test}`);	// test 未声明，报错
```

## 实例：模板编译

## 标签模板

模板字符串可以紧跟在一个函数名后面，该函数将被调用来处理这个模板字符串。这就是标签模板功能。

```js
alert`123`;	// 等同于 alert(123)
```

标签模板一个重要应用是过滤 HTML 字符串，防止用户输入恶意内容，具体翻阅 p65。另一个应用是多语言转换，国际化处理，具体翻阅 p66。

## String.raw()

ES6 为原生 String 对象提供了一个 `raw` 方法。String.raw 方法往往用来充当模板字符串的处理函数。

```js
String.raw`Hi\n${2+3}!`;	// "Hi\\n5!"
```

也可以作为正常的函数使用。第一个参数为具有 raw 属性的对象，且 raw 属性的值应该是一个数组，否则会报错。从第二个参数开始可以有 0 到数组 length - 1 个参数（超出的参数不起作用），这些参数将穿插在数组的成员中，返回数组成员与后面参数穿插后拼接成的字符串。

```js
String.raw({ raw: 'test' }, 0, 1, 2, 3, 4);	// 't0e1s2t'	'test' 可以认为是 ['t', 'e', 's', 't']
String.raw({ raw: [0, 1, 2] }, 'a', 'b');	// '0a1b2'
```

## 模板字符串的限制

模板字符串默认会将字符串转义。







# 第 5 章		正则的扩展

## RegExp 构造函数

ES5 中，RegExp 构造函数的参数有两种情况。第一种情况，第一个参数是字符串，第二个参数为正则表达式的修饰符。

```js
var regex = new RegExp('xyz', 'i');	// 等价于 var regex = /xyz/i;
```

第二种情况，参数是正则表达式，此时返回原有正则表达式的拷贝。但 ES5 不允许此时使用第二个参数添加修饰符，否则会报错。

```js
var regex = new RegExp(/xyz/i);	// 等价于 var regex = /xyz/i;
var regex2 = new RegExp(/xyz/, 'i');	// 报错
```

ES6 修改了这种行为，如果 RegExp 的第一个参数是一个正则对象，那么第二个参数依旧可以指定修饰符，且指定的修饰符会覆盖原有正则表达式的修饰符。

```js
new RegExp(/abc/ig, 'i').flags;	// 'i'
```

## 字符串的正则方法

字符串对象共有 4 个方法可以使用正则表达式：`match()` `replace()` `search()` `split()`。ES6 使这 4 个方法在语言内部调用 RegExp 的实例方法，从而达到所有与正则相关的方法都定义在 RegExp 对象上。

- String.prototype.match 调用 RegExp.prototype[Symbol.match]
- String.prototype.replace 调用 RegExp.prototype[Symbol.replace]
- String.prototype.search 调用 RegExp.prototype[Symbol.search]
- String.prototype.split 调用 RegExp.prototype[Symbol.split]

## u 修饰符

ES6 对正则表达式添加了含义为 “Unicode 模式” 的修饰符 `u`，用于正确处理大于 \uFFFF 的 Unicode 字符。也就是可以正确处理 4 个字节的 UTF-16 编码。

```js
/^\uD83D/u.test('\uD83D\uDC2A');	// false
/^\uD83D/.test('\uD83D\uDC2A');	// true
```

上面的例子中，\uD83D\uDC2A 是一个 4 字节的 UTF-16 编码，代表一个字符。ES5 不支持 4 字节的 UTF-16 编码，将其识别为 2 个字符，所以结果为 true。加了修饰符后，ES6 将其识别为一个字符，所以结果为 false。

一旦加上 u 修饰符，下面这些正则表达式的行为就会被修改。

### 点字符

点字符 `.` 在正则表达式中的含义是除换行符以外的任意单个字符。对于码点大于 0xFFFF 的 Unicode 字符，点字符不能识别，必须加上 u 修饰符。如果不添加 u 修饰符，遇到码点大于 0xFFFF 的字符，正则表达式就会认为字符串为 2 个字符，从而匹配失败。

```js
var s = '𠮷';
/^.$/.test(s);	// false
/^.$/u.test(s);	// true
```

### Unicode 字符表示法

ES6 新增了使用大括号表示 Unicode 字符的表示法，这种表示法在正则表达式中必须加上 u 修饰符才能识别大括号，否则会被解读为量词。

```js
/\u{61}/.test('a');	// false 解读为匹配 61 个连续的 u
/\u{61}/u.test('a');	// true
/\u{20BB7}/u.test('𠮷');	// true
```

### 量词

使用 u 修饰符后，所有量词都会正确识别码点大于 0xFFFF 的 Unicode 字符。

```js
/𠮷{2}/.test('𠮷𠮷');	// false
/𠮷{2}/u.test('𠮷𠮷');	// true
```

### 预定义模式

u 修饰符也影响到预定义模式能否正确识别码点大于 0xFFFF 的 Unicode 字符。

```js
/^\S$/.test('𠮷');	// false
/^\S$/u.test('𠮷');	// true
```

\S 是预定义模式，匹配所有不是空格的字符。只有加了 u 修饰符才能正确识别大于 0xFFFF 的字符。利用这一点，可以写出正确返回字符串长度的函数。

```js
function codePointLength (text) {
  var result = text.match(/[\s\S]/gu);
  return result ? result.length : 0;
}
var s = '𠮷𠮷';
s.length;	// 4
codePointLength(s);	// 2
```

### i 修饰符

`i` 修饰符表示不区分大小写。有些 Unicode 字符的编码不同，但是字型很相近，例如 \u004B 和 \u212A 都表示大写的 K。如果不加上 u 修饰符就无法识别非规范的 K 字符。

```js
/[a-z]/i.test('\u212A');	// false
/[a-z]/iu.test('\u212A');	// true
```

## y 修饰符

除了 u 修饰符，ES6 还添加了 `y` 修饰符，叫做粘连修饰符。作用与 g 修饰符类似，也是全局匹配，后一次匹配都从上一次匹配成功的下一个位置开始。不同的是，g 修饰符只要剩余位置中存在匹配就行，y 修饰符会确保匹配必须从剩余的第一个位置开始。y 修饰符的设计本意就是让头部匹配的标志 ^ 在全局匹配中都有效。

```js
var s = 'aaa_aa_a';
var r1 = /a+/g;
var r2 = /a+/y;
r1.exec(s);	// ['aaa']
r2.exec(s);	// ['aaa']
r1.exec(s);	// ['aa']
r2.exec(s);	// null
```

上面例子中，第一次执行时 g 修饰符和 y 修饰符两者行为相同，剩余字符串都是 '_aa_a'。但 g 修饰符没有位置要求，所以第二次返回结果 'aa'，而 y 修饰符有位置要求，剩余位置第一个是 '\_'，所以不匹配返回 null。

更多关于 y 修饰符的内容，包括 split、replace、match 方法的使用请翻阅 p74。y 修饰符的一个应用是从字符串中提取 token（词元），y 修饰符确保了匹配之间不会有漏掉的字符，具体翻阅 p76。

## sticky 属性

与 y 修饰符相匹配，ES6 的正则对象多了 `sticky` 属性，表示是否设置了 y 修饰符。

```js
var r = /hello\d/y;
r.sticky;	// true
```

## flags 属性

ES6 为正则表达式新增了 `flags` 属性，会返回正则表达式的修饰符。

```js
// ES5 的 source 属性返回正则表达式的正文
/abc/ig.source;	// 'abc'
/abc/ig.flags;	// 'gi'
```

## s 修饰符：dotAll 模式

正则表达式中 `.` 点是一个特殊字符，代表任意的单个字符，但行终止符除外，以下 4 个字符属于行终止符。

- U+000A 换行符 \n
- U+000D 回车符 \r
- U+2028 行分隔符
- U+2029 段分隔符

```js
/foo.bar/.test('foo\nbar');	// false
```

上面代码中，因为 . 不匹配 \n，所以返回 false。但很多时候希望 . 能够匹配任意单个字符，此时有种变通的写法。

```js
/foo[^]bar/.test('foo\nbar');	// true
```

这种写法虽然可以实现效果，但是不符合直觉，所以有了一个提案：引入 `s` 修饰符，使得 . 可以匹配任意单个字符。

```js
/foo.bar/s.test('foo\nbar');	// true
```

这称之为 dotAll 模式，即点 dot 匹配一切字符。所以正则表达式还引入了一个 `dotAll` 属性，返回一个布尔值，表示该正则表达式是否处在 dotAll 模式下。

```js
const re = /foo.bar/s;
re.dotAll;	// true
```

s 修饰符和多行修饰符 `m` 不冲突，两者一起使用的情况下，. 匹配所有字符，而 ^ 和 $ 匹配每一行的行首和行尾。

## 后行断言

JavaScript 语言的正则表达式只支持先行断言和先行否定断言，但不支持后行断言和后行否定断言。先行断言指的是 x 只有在 y 前面才匹配，必须写成 /x(?=y)/ 的形式。例如匹配百分号前的数字，要写成 /\d+(?=%)/。先行否定断言指的是 x 只有不在 y 前面才匹配，必须写成 /x(?!y)/ 的形式。例如匹配不在百分号之前的数字，要写成 /\d+(?!%)/。

```js
/\d+(?=%)/.exec('100%');	// ['100']
/\d+(?!%)/.exec('44');	// ['44']
```

现在有提案引入后行断言。后行断言正好与先行断言相反，x 只有在 y 后面才匹配，必须写成 /(?<=y)x/ 的形式。例如只匹配美元符号之后的数字，要写成 /(?<=\\$)\d+/。后行否定断言则与先行否定断言相反，x 只有不在 y 后面才匹配，必须写成 /(?<!y)x/ 的形式。例如只匹配不在美元符号后面的数字，要写成 /(?<!\\$)\d+/。

```js
/(?<=\$)\d+/.exec('99$100');	// ['100']
/(?<!\$)\d+/.exec('99$100');	// ['99']
```

## Unicode 属性类

提案引入了一种新的类的写法：`\p{ ... }` 和 `\P{ ... }`，允许正则表达式匹配符合 Unicode 某种属性的所有字符。这两种类只对 Unicode 有效，所以使用的时候一定要加上 u 修饰符，如果不加上 u 修饰符，正则表达式使用 \p 和 \P 便会报错。Unicode 属性类要指定属性名和属性值 `\p{UnicodePropertyName=UnicodePropertyValue}`。对于某些属性，可以只写属性名 `\p{UnicodePropertyName}`。\P{ ... } 是 \p{ ... } 的反向匹配，即匹配不满足条件的字符。 

```js
const regexGreekSymbol = /\p{Script=Greek}/u;	// \p{Script=Greek} 指定匹配一个希腊文字母
regexGreekSymbol.test('Π');	// true
```

`\p{Decimal_Number}` 匹配所有十进制字符。`\p{Number}` 匹配罗马数字。

## 具名组匹配

### 简介

正则表达式使用圆括号进行组匹配。

```js
const re_date = /(\d{4})-(\d{2})-(\d{2})/;
const matchObj = re_date.exec('1999-12-31');
const matchobj = matchObj[0];	// '1999-12-31'
const year = matchObj[1];	// 1999
const month = matchObj[2];	// 12
const day = matchObj[3];	// 31
const groups = matchObj.groups;	// undefined
```

组匹配有一个问题，每一组的匹配含义不明确，且只能用数字序号引用，要是组的顺序改变了，就必须改变引用的组序号。此时可以使用具名组匹配。具名组匹配允许为每一个组匹配指定一个名字，指定方式为在模式头部添加 `?<name>`。

```js
const re_date = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/;
const matchObj = re_date.exec('1999-12-31');
const Y = matchObj.groups.year;	// 1999
const m = matchObj.groups.month;	// 12
const d = matchObj.groups.day;	// 31
const matchobj = matchObj[0];	// '1999-12-31'
const year = matchObj[1];	// 1999
const month = matchObj[2];	// 12
const day = matchObj[3];	// 31
```

可以在 exec 方法返回结果的 groups 属性上引用组名，同时数字序号 matchObj[1] 依旧有效。

### 解构赋值和替换

有了具名组匹配后，可以使用解构赋值直接从匹配结果上为变量赋值。

```js
let { groups: {one, two} } = /^(?<one>.*):(?<two>.*)$/u.exec('foo:bar');
one;	// foo
two;	// bar
```

字符串替换时，使用 `$<name>` 引用具名组。

```js
let re = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/u;
'2015-01-02'.replace(re, '$<day>/$<month>/$<year>');	// 02/01/2015
```

replace 的第二个参数还可以是函数，函数的参数序列翻阅 p83。

### 引用

如果要在正则表达式内部引用某个具名组匹配，可以使用 `\k<name>` 的写法。

```js
const re_twice = /^(?<word>[a-z]+)!\k<word>$/;
re_twice.test('abc!abc');	// true
re_twice.test('abc!ab');	// false
re_twice.test('ab!cab');	// false
re_twice.test('ab!ab');	// true
```







# 第 6 章		数值的扩展

## 二进制和八进制表示法

ES6 提供了二进制和八进制数值的新写法，分别使用前缀 `0b / 0B` 和 `0o / 0O` 表示。

```js
0b111110111 === 503	// true
0o767 === 503	// true
```

从 ES5 开始，严格模式下八进制数值就不再允许使用前缀 0 表示，ES6 进一步明确要使用前缀 0o 表示。

```js
(function () {
  console.log(0o11 === 011);
})()	// true
(function () {
  'use strict';
  console.log(0o11 === 011);
})()	// Uncaught SyntaxError
```

如果要将使用 0b 和 0o 前缀的字符串数值转为十进制数值，要使用 `Number` 方法。

```js
Number('0b111');	// 7
Number('0o10');	// 8
```

## Number.isFinite()、Number.isNaN()

ES6 在 Number 对象上新提供了 `Number.isFinite()` 和 `Number.isNaN()` 方法。Number.isFinite() 方法用来检查一个数值是否为有限的，只对数值有效，对于非数值一律返回 false。Number.isNaN() 方法用来检查一个值是否为 NaN，只对于 NaN 才返回 true，非 NaN 一律返回 false。

```js
Number.isFinite(15);	// true
Number.isFinite(0.8);	// true
Number.isFinite('2');	// true
Number.isFinite(NaN);	// false
Number.isFinite(Infinity);	// false

Number.isNaN(NaN);	// true
Number.isNaN(15);	// false
Number.isNaN(true);	// false
Number.isNaN(9 / NaN);	// true
Number.isNaN('true' / 0);	// true
```

## Number.parseInt()、Number.parseFloat()

ES6 将全局方法 `parseInt()` 和 `parseFloat()` 方法转移到了 Number 对象上，行为完全保持不变。目的是为了逐步减少全局性方法，使得语言逐步模块化。

```js
// ES5 写法
parseInt('12.34');	// 12
parseFloat('12.34#');	// 12.34

// ES6 写法
Number.parseInt('12.34');	// 12
Number.parseFloat('12.34#');	// 12.34

Number.parseInt === parseInt	// true
Number.parseFloat === Number.parseFloat	// true
```

## Number.isInteger()

`Number.isInteger()` 用来判断一个值是否为整数。注意，JavaScript 中整数和浮点数是同样的存储方法，所以 3 和 3.0 被视为同一个值。

```js
Number.isInteger(3);	// true
Number.isInteger(3.0);	// true
Number.isInteger(3.1);	// false
Number.isInteger('3');	// false
Number.isInteger(true);	// false
```

## Number.EPSILON

ES6 在 Number 对象上新增了一个极小的常量 `Number.EPSILON`。引入该常量的原因是为浮点数计算设置一个误差范围，因为浮点数计算是不精确的，但如果误差小于 Number.EPSILON，就可以认为得到了正确的结果。

```js
Number.EPSILON;	// 2.220446049250313e-16
Number.EPSILON.toFixed(20);	// "0.00000000000000022204"
0.1 + 0.2;	// 0.30000000000000004
0.1 + 0.2 - 0.3;	// 5.551115123125783e-17
0.1 + 0.2 - 0.3 === 0;	// false
0.1 + 0.2 - 0.3 < Number.EPSILON;	// true
```

## 安全整数和 Number.isSafeInteger()

JavaScript 能够准确表示的整数范围在 -2⁵³ 到 2⁵³ 之间，不包括 -2⁵³ 和 2⁵³，超过这个范围就无法精确表示。

```js
Math.pow(2, 53);	// 9007199254740992
9007199254740992;	// 9007199254740992
9007199254740993;	// 9007199254740992
Math.pow(2, 53) === Math.pow(2, 53) + 1;	// true
```

ES6 引入了 `Number.MAX_SAFE_INTEGER` 和 `Number.MIN_SAFE_INTEGER` 两个常量，用来表示范围的上下限。

```js
Number.MAX_SAFE_INTEGER === Math.pow(2. 53)	- 1;	// true
Number.MAX_SAFE_INTEGER === 9007199254740991;	// true
Number.MIN_SAFE_INTEGER === -Number.MAX_SAFE_INTEGER;	// true
Number.MIN_SAFE_INTEGER === -9007199254740991;	// true
```

`Number.isSafeInteger()` 则是用来判断一个整数是否落在这个范围之内。

```js
Number.isSafeInteger(12);	// true
Number.isSafeInteger(12.34);	// false
Number.isSafeInteger(9007199254740991);	// true
Number.isSafeInteger(9007199254740992);	// false
Number.isSafeInteger(Infinity);	// false
```

## Math 对象的扩展

### Math.trunc()

`Math.trunc()` 方法用于去除一个数的小数部分，返回整数部分。对于非数值，内部调用 Number 方法将其先转为数值。对于空值和无法截取整数的值，返回 NaN。

```js
Math.trunc(4.1);	// 4
Math.trunc(-0.2);	// -0
Math.trunc(-4.1);	// -4
Math.trunc('123.456');	// 123
Math.trunc(NaN);	// NaN
Math.trunc('foo');	// NaN
Math.trunc();	// NaN
```

### Math.sign()

`Math.sign()` 方法用来判断一个数到底是正数、负数、零还是非数值数。对于非数值数，会先试着将其转为数值。返回值有以下 5 种情况：

- 参数为正数，返回 1；
- 参数为负数，返回 -1；
- 参数为 0，返回 0；
- 参数为 -0，返回 -0；
- 参数为非数值，返回 NaN。

```js
Math.sign(5);	// 1
Math.sign(-5);	// -1
Math.sign(0);	// 0
Math.sign(-0);	// -0
Math.sign('5');	// 1
Math.sign('foo');	// NaN
Math.sign();	// NaN
```

### Math.cbrt()

`Math.cbrt()` 用于计算一个数的立方根，对于非数值也是方法内部调用 Number 方法转为数值。

```js
Math.cbrt(-1);	// -1
Math.cbrt('8');	// 2
Math.cbrt(0);	// 0
Math.cbrt('hello');	// NaN
```

### Math.clz32()

JavaScript 的整数使用 32 位二进制形式表示，`Math.clz32()` 方法返回一个数的 32 位无符号整数形式有多少个前导 0。对于小数，方法只考虑整数部分。对于空值或其他类型的值，方法会先将他们转为数值，然后再计算。

```js
Math.clz32(0);	// 32
Math.clz32(1);	// 31
Math.clz32(0b01000000000000000000000000000000);	// 1
Math.clz(3.2);	// 30
Math.clz();	// 32
Math.clz(NaN);	// 32
Math.clz(Infinity);	// 32
Math.clz('foo');	// 32
```

左移运算符 `<<` 与 Math.clz32() 方法直接相关。

```js
Math.clz32(1 << 1);	// 30
Math.clz32(1 << 29);	// 2
```

### Math.imul()

`Math.imul()` 方法返回两个数以 32 位带符号整数形式相乘的结果，返回的也是一个 32 位的带符号的整数。

```js
Math.imul(2, 4);	// 8
Math.imul(-1, 8);	// -8
```

如果只考虑最后 32 位，大多数情况下，Math.imul(a, b) 与 a * b 的结果是相同的。

### Math.fround()

`Math.fround()` 方法返回一个数的单精度浮点数形式。对于整数来说，该方法的返回结果与原来的数一样，区别主要在于那些无法用 64 个二进制位精确表示的小数，该方法会返回最接近这个小数的单精度浮点数。

```js
Math.fround(0);	// 0
Math.fround(1.5);	// 1.5
Math.fround(1.337);	// 1.3370000123977661
```

### Math.hypot()

`Math.hypot()` 方法返回所有参数的平方和的平方根。如果参数不是数值，会先将其转为数值。只要转换后有一个参数不是数值，就会返回 NaN。

```js
Math.hypot(-3);	// 3
Math.hypot(3, 4);	// 5
Math.hypot();	// 0
Math.hypot(3, 4, '5');	// 7.0710678118654755
Math.hypot(3, 4, 'foo');	// NaN
```

### 对数方法

#### Math.expm1()

`Math.expm1(x)` 返回 e 的 x 次方减一，即 `Math.exp(x) - 1`。

#### Math.log1p()

`Math.log1p(x)` 方法返回 `ln(1+x)`，即 `Math.log(1+x)`。如果 x 小于 -1，则返回 NaN。

#### Math.log10()

`Math.log10(x)` 返回以 10 为底的 x 的对数。如果 x 小于 0，则返回 NaN。

#### Math.log2()

`Math.log2(x)` 返回以 2 为底的 x 的对数。如果 x 小于 0，则返回 NaN。

### 双曲函数方法

- `Math.sinh(x)` 返回 x 的双曲正弦。
- `Math.cosh(x)` 返回 x 的双曲余弦。
- `Math.tanh(x)` 返回 x 的双曲正切。
- `Math.asinh(x)` 返回 x 的反双曲正弦。
- `Math.acosh(x)` 返回 x 的反双曲余弦。
- `Math.atanh(x)` 返回 x 的反双曲正切。

## Math.signbit()

`Math.sign()` 用来判断一个值的正负，但如果参数为 -0，会返回 -0 而不是 0。

```js
Math.sign(-0);	// -0
```

JavaScript 内部使用 64 位浮点数表示数值，第一位为符号位，0 表示正数，1 表示负数。所以会有两种零，+0 是符号位为 0 时的零值，-0 是符号位为 1 时的零值。而实际编程中判断一个值是 +0 还是 -0 非常麻烦，因为它们是相等的。

```js
+0 === -0;	// true
```

但有一个提案引入了 `Math.signbit()` 方法来判断一个数的符号位是否已经设置，node 中测试报错。符号位为 1 则表示已设置返回 true，否则返回 false。也就是说如果参数是 -0 或其他负值返回 true，NaN 和其他情况返回 false。

```js
Math.signbit(0);	// false
Math.signbit(-0);	 // true
```

## 指数运算符

ES2016 新增了指数运算符 `**`。指数运算符可以与等号结合，形成新的赋值运算符 `**=`。

```js
2 ** 3;	// 8
let b = 4;
b **= 3;	// 64	b = b * b * b
```

注意：V8 引擎中，指数运算符和 Math.pow 方法的实现不同，对于特别大的运算结果会有细微的差异。

```js
Math.pow(99, 99); // 3.697296376497263e+197
99 ** 99;					// 3.697296376497268e+197
```

## Integer 数据类型

### 简介

JavaScript 所有数字都保存成 64 位浮点数，所以整数的精确程度只能到 53 个二进制位。大于这个范围的整数，JavaScript 无法精确表示，这也是 JavaScript 不适合进行科学和金融方面精确计算的原因。但现在有个提案：引入新的数据类型 `Integer` 整数类型来解决这一问题。整数类型的数据只用来表示整数，没有位数的限制，任何位数的整数都可以精确表示。而为了与 Number 类型区别，Integer 类型的数据必须使用后缀 `n` 来表示，无论是二进制、八进制、十进制亦或是十六进制表示法都需要加上后缀 n。

```js
1n + 2n;	// 3n
0b1101n;
0o777n;
0xFFn;
```

对于 Integer 类型的数据，typeof 运算符返回 integer。

```js
typeof 123n;	// integer
```

JavaScript 原生提供 Integer 对象，用来生成 Integer 类型的数值。转换规则基本与 Number() 一致。

```js
Integer(123);	// 123n
Integer('123');	// 123n
Integer(false);	// 0n
Integer(true);	// 1n
```

以下用法会报错。

```js
new Integer();	// TypeError
Integer(undefined);	// TypeError
Integer(null);	// TypeError
Integer('123n');	// SyntaxError
Integer('abc');	// SyntaxError
```

### 运算

数学运算方面，Integer 类型的 +、-、*、** 这四个运算符与 Number 类型的行为一致。/ 运算符会舍去小数部分，返回一个整数。

```js
9n / 5n;	// 1n
```

几乎所有的 Number 运算符都可以用在 Integer 中，有两个例外。如果使用了不带符号的右位移运算符 `>>>` 和一元的求正运算符 `+`，将会报错。

```js
9n >>> 2;	// TypeError: Cannot mix BigInt and other types, use explicit conversions
+9n;	// TypeError: Cannot convert a BigInt value to a number
```

Integer 类型不能与 Number 类型进行混合运算。

```js
1n + 1;	// 报错 TypeError: Cannot mix BigInt and other types, use explicit conversions
```

相等运算符 `==` 会改变数据类型，所以最好不要混合使用。全等运算符 `===` 不会改变数据类型，所以可以混合使用。

```js
0n == 0;	// true
0n == false;	// true
0n === 0;	// false
0n === false;	// false
```







# 第 7 章		函数的扩展

## 函数参数的默认值

### 基本用法

ES6 允许为函数的参数设置默认值，直接写在参数定义的后面。

```js
function log (x, y = 'World') {
  console.log(x, y);
}
log('Hello');	// Hello World
log('Hello', 'China');	// Hello China
log('Hello', '');	// Hello
```

参数变量是默认声明的，不能用 let 或 const 再次声明。

```js
function foo (x = 5) {
  let x = 1;	// error
  const x = 2;	// error
}
```

参数默认值是惰性求值的，每次都重新计算默认值表达式的值。

```js
let x = 99;
function foo (p = x + 1) {
  console.log(p);
}
foo();	// 100
x = 100;
foo();	// 101
```

### 与解构赋值默认值结合使用

参数默认值可以与解构赋值的默认值结合起来使用。

```js
function fetch (url, { method = 'GET' } = {}) {
  console.log(method);
}
fetch('http://example.com');	// GET
fetch('http://example.com', {});	// GET
fetch('http://example.com', { method: 'POST' });	// POST
fetch('http://example.com', { header: {} });	// GET
```

### 参数默认值的位置

定义了默认值的参数应该放在函数参数的尾部，如果非尾部的参数设置了默认值，实际上这个参数是无法省略的。如果传入的参数为 undefined，而参数又有默认值，则触发该参数等于默认值。如果传入的参数为 null，将不会触发参数等于默认值。

```js
function f (x, y = 5, z) {
  return [x, y, z];
}
f();	// [undefined, 5, undefined]
f(1);	// [1, 5, undefined]
f(1, , 2);	// 报错
f(1, undefined, 2);	// [1, 5, 2]
f(1, null, 2);	// [1, null, 2]
```

### 函数的 length 属性

指定了默认值的参数将不会包含函数的 `length` 属性中，如果设置了默认值的参数不在函数参数的尾部，那么后面的参数也不会计入 length 属性中。`rest` 参数也不会计入 length 属性。

```js
(function (a, b) {}).length;	// 2
(function (a, b, c = 3) {}).length;	// 2
(function (...args) {}).length;	// 0
(function (a, b = 2, c) {}).length;	// 1
```

### 作用域

一旦设置了参数的默认值，函数进行声明初始化时，参数会形成一个单独的作用域。等到初始化结束后，这个作用域就会消失。这种语法行为在不设置参数默认值时是不会出现的。

```js
var x = 1;
function f (x, y = x) {
  console.log(y);
}
f();	// undefined
f(2);	// 2
```

上面代码中，参数 y 的默认值等于变量 x。调用函数 f 时，参数形成一个单独的作用域。该作用域里面，y 的默认值变量 x 指向第一个参数 x，而不是全局变量 x。如果没有参数 x，那么 y 的默认值变量 x 在当前作用域找不到，就会在上一级作用域也就是全局作用域中寻找，也就是全局变量 x，如果此时全局变量 x 不存在，那么会报错。如果参数的默认值是一个函数，那么该函数的作用域也遵守这个规则。

### 应用

利用参数默认值可以指定某一个参数不得省略，如果省略就抛出一个错误。

```js
function throwIfMissing () {
  throw new Error('Missing parameter');
}
function foo (mustBeProvided = throwIfMissing()) {
  return mustBeProvided;
}
foo();	// Error: Missing parameter
```

也可以将参数默认值设为 undefined，表示这个参数是可以省略的。

```js
function foo (optional = undefined) { ... }
```

## rest 参数

ES6 引入了 rest 参数，形式为 `...propertyName`，用于获取函数的多余参数，这样就不需要 arguments 对象了。注意，rest 参数只能是最后一个参数，其后不能再有其他参数，否则会报错。rest 参数搭配的变量是一个数组，多余的参数都会被放入其中。

```js
function add (...values) {
  let sum = 0;
  for (let value of values) {
    sum += value;
  }
  return sum;
}
add(2, 5, 3);	// 10

// 报错
function f (a, ...b, c) { ... }
```

函数的 length 属性不包括 rest 参数。

```js
(function (...a) {}).length;	// 0
```

## 严格模式

从 ES5 开始，函数内部可以设定为严格模式。但 ES2016 规定了只要函数参数使用了默认值、解构赋值或 rest 参数，那么函数内部就不能显示的设定严格模式，否则会报错。

```js
function fn1 (a, b) {
  'use strict';
  ...
}
// 报错
function fn2 (a, b = 5) {
  'use strict';
  ...
}
// 报错
function fn3 ({a, b}) {
  'use strict';
  ...
}
// 报错
function fn4 (...a) {
  'use strict';
  ...
}
```

## name 属性

函数的 name 属性返回该函数的函数名，该属性早就被浏览器广泛支持，但直到 ES6 才写入规范。如果将匿名函数赋给一个变量，ES5 的 name 属性返回空字符串，ES6 的 name 属性返回实际的函数名，也就是变量名。

```js
var f = function () {}
// ES5
f.name;	// ''
// ES6
f.name;	// 'f'
```

如果将一个具名函数赋值给一个变量，ES5 和 ES6 的 name 属性都返回这个具名函数原本的名字。

```js
const bar = function fn () {}
// ES5 与 ES6
bar.name;	// 'fn'
```

Function 构造函数返回的函数实例，name 属性的值为 anonymous。

```js
(new Function).name;	// 'anonymous'
```

bind 返回的函数，name 属性值会加上 bound 前缀。

```js
function foo () {};
foo.bind({}).name;	// 'bound foo'
```

## 箭头函数

### 基本用法

ES6 允许使用箭头 `=>` 定义函数，形式为 `参数 => 函数体`。箭头函数如果没有参数，那么使用圆括号代替参数部分；如果只有一个参数，可以不用圆括号包裹，直接写一个参数就好；如果有多个参数，就放入圆括号中。如果函数体只有一行代码，那么可以不用大括号，且这一行代码的执行结果还会被返回，如果这一行代码会返回一个对象，则必须在对象外面加上括号；如果有多行代码，必须放在大括号中。

```js
var f = v => v;
var f = function (v) { return v };

var sum = (num1, num2) => num1 + num2;
var sum = function (num1, num2) { return num1 + num2 };

var getTempItem = id => ({ id: id, name: 'Temp' });

var multilineFunction = (name) => {
  switch (name) {
    case 'zs':
      return 'hello world';
      break;
    case 'li':
      return 'world hello';
      break;
  }
}
```

### 注意事项

1. 箭头函数体内的 this 对象是定义箭头函数时所在的对象，而不是使用时所在的对象。箭头函数没有自己的 this，导致内部的 this 就是外层代码块的 this。因为没有自己的 this，所以无法使用 call、apply 和 bind 这些方法改变 this 的指向，如果使用了会忽略。

   ```js
   // ES6
   function foo () {
     setTimeout(() => {
       console.log('id', this.id);
     }, 100);
   }
   //  ES5
   function foo () {
     var _this = this;
     setTimeout(function () {
       console.log('id', _this.id);
     }, 100);
   }
   
   function foo () {
     return () => {
       return () => {
         return () => {
           console.log('id', this.id);
         };
       };
     };
   }
   var f = foo.call({id: 1});
   var t1 = f.call({id: 2})()();	// id1
   var t2 = f().call({id: 3})();	// id1
   var t3 = f()().call({id: 4});	// id1
   ```

2. 因为没有自己的 this，所以不可以当作构造函数。不能使用 new 命令，否则会抛出一个错误。

3. 不可以使用 arguments、super、new.target 对象，这些对象在函数体内不存在。要使用 arguments，可以使用 rest 参数。

4. 不可以使用 yield 命令，因此箭头函数不能用作 Generator 函数。

### 嵌套的箭头函数

箭头函数内部可以再使用箭头函数。

```js
function insert (value) {
  return {into: function (array) {
    return {after: function (afterValue) {
      array.splice(array.indexOf(afterValue) + 1, 0, value);
      return array;
    }};
  }};
}
let insert = (value) => ({into: (array) => ({after: (afterValue) => {
  array.splice(array.indexOf(afterValue) + 1, 0, value);
  return array;
}})});
insert(2).into([1, 3]).after(1);	// [1, 2, 3]
```

```js
const pipeline = (...funcs) => val => funcs.reduce((a, b) => b(a), val);
const plus1 = a => a + 1;
const mult2 = a => a * 2;
const addThenMult = pipeline(plus1, mult2);
addThenMult(5);	// 12
```

## 绑定 this

箭头函数可以绑定 this 对象，指向外层代码块的 this，大大减少了显示绑定 this 对象的写法（call、apply、bind）。但箭头函数并非适用于所有场合。ES7 提出了函数绑定运算符 `::` 用来取代 call、apply、bind 调用。目前 Babel 转码器已经支持。
双冒号左边是一个对象，右边是一个函数。该运算符会自动将左边的对象作为上下文环境（this 对象）绑定到右边的函数上。

```js
foo::bar;
// 等同于
bar.bind(foo);

foo::bar(...arguments);
// 等同于
bar.apply(foo, arguments);
```

## 尾调用优化

### 什么是尾调用

尾调用是函数式编程的一个重要概念，是指某个函数的最后一步是调用另一个函数。尾调用不一定出现在函数尾部，只要是最后一步就可以了。

```js
function f (x) {
  if (x > 0) {
    return m(x);
  }
  return n(x);
}
```

上面代码中，函数 m 和 n 都属于尾调用，因为都是函数 f 的最后一步操作。以下情况不属于尾调用。

```js
function f (x) {
  let y = g(x);
  return y;
}
function f (x) {
  return g(x) + 1;
}
function f (x) {
  g(x);
}
```

### 尾调用优化

如果内层函数没有用到外层函数的内部变量，可以进行尾调用优化。

### 尾递归

```js
function factorial (n) {
  if (n === 1) return 1;
  return n * factorial(n - 1);
}
factorial(5);	// 120
```

上面代码是一个阶乘函数，计算 n 的阶乘，最多需要保存 n 个调用记录，复杂度为 O(n)。如果改写成尾递归，只保留一个调用记录，那么复杂度为 O(1)。

```js
function factorial (n, total) {
  if (n === 1) return total;
  return factorial(n - 1, n * total);
}
factorial(5, 1);	// 120
```

还有一个著名的例子 —— 计算 Fibonacci 数列，也能充分说明尾递归优化的重要性。

```js
// 非尾递归
function Fibonacci (n) {
  if (n <= 1) {
    return 1;
  }
  return Fibonacci(n - 1) + Fibonacci(n - 2);
}
Fibonacci(10);	// 89
Fibonacci(100);	// 堆栈溢出
Fibonacci(500);	// 堆栈溢出

// 尾递归
function Fibonacci2 (n, ac1 = 1, ac2 = 1) {
  if (n <= 1) {
    return ac2;
  }
  return Fibonacci2(n - 1, ac2, ac1 + ac2);
}
Fibonacci2(100);	// 573147844013817200000
Fibonacci2(1000);	// 7.0330367711422765e+208
Fibonacci2(10000);	// Infinity
```

由上面代码可以知道尾调用优化对递归操作意义重大。ES6 第一次明确规定，所有 ECMAScript 的实现都必须部署尾调用优化。ES6 中，只要使用尾递归，就不会发生栈溢出，相对节省内存。

### 递归函数的改写

尾递归的实现往往需要改写递归函数，确保最后一步只调用自身。做到这一点的方法就是把所有用到的内部变量改写成函数的参数。例如上面的阶乘函数用到一个中间变量 total，但这样做的确定是不直观。有两个方法可以解决这个问题。最简单的就是采用 ES6 的函数默认值。

```js
function factorial (n, total = 1) {
  if (n === 1) {
    return total;
  }
  return factorial(n - 1, n * total);
}
factorial(5);	// 120
```

另一种方法是尾递归函数之外再提供一个正常形式的函数。

```js
function tailFactorial (n, total) {
  if (n === 1) {
    return total;
  }
  return tailFactorial(n - 1, n * total);
}
function factorial (n) {
  return tailFactorial(n, 1);
}
factorial(5);	// 120
```

函数式编程有一个重要的概念，叫做柯里化 currying。意思是将多参数的函数转换成单参数的形式。

```js
function currying (fn, n) {
  return function (m) {
    return fn.call(this, m, n);	// 这里用不用 call 指定 this 都可以
  };
}
function tailFactorial (n, total) {
  if (n === 1) {
    return total;
  }
  return tailFactorial(n - 1, n * total);
}
const factorial = currying(tailFactorial, 1);
factorial(5);	// 120
```

### 严格模式

ES6 的尾调用优化只在严格模式下开启，正常模式下是无效的。上面的代码没有使用严格模式也可以运行可能是浏览器自动优化了，或者 JS 运行环境不同，栈的容量不同。

正常模式下函数内部有两个变量，可以跟踪函数的调用栈。function.arguments 返回调用函数的参数，funciton.caller 返回调用当前函数的那个函数。尾调用优化发生时，函数的调用栈会重写，所以这两个变量会失真。严格模式禁用这两个变量，所以尾调用模式仅在严格模式下生效。

### 尾递归优化的实现

尾递归优化只在严格模式下生效，那么在正常模式下，或者在不支持该功能的环境中，怎么自己实现尾递归优化呢？原理很简单。尾递归之所以需要优化，原因是调用栈太多造成溢出，那么只要减少调用栈就不会溢出。可以采用循环替换递归。具体方法翻阅 p130。

## 函数参数的尾逗号

ES2017 有一个提案：允许函数的最后一个参数有尾逗号，不管是定义函数还是调用函数都可以。这样规定使得函数参数与数组和对象的尾逗号规则可以保持一致。







# 第 8 章		数组的扩展

## 扩展运算符

### 含义

扩展运算符是三个点 `...`，就如同 rest 参数的逆运算，将一个数组转为用逗号分隔的参数序列。

```js
console.log(...[1, 2, 3]);	// 1 2 3
console.log(1, ...[2, 3, 4], 5);	// 1 2 3 4 5
[...document.querySelectorAll('div')];	// [<div>, <div>, <div>]
```

该运算符主要用于函数调用。

```js
function push (array, ...items) {
  array.push(...items);
}
```

扩展运算符后面还可以放置表达式。

```js
const arr = [
  ...(x > 0 ? ['a'] : []),
  'b',
];
```

如果扩展运算符后面是一个空数组，则不产生任何效果。

```js
[...[], 1];	// [1]
```

### 替代数组的 apply 方法

由于扩展运算符可以展开数组，所以不再需要使用 apply 方法将数组转换为函数的参数或为一个数组插入另一个数组。

```js
function f (x, y, z) { ... }
var args = [0, 1, 2];

// ES5
f.apply(null, args);
                      
// ES6
f(...args);
```

```js
var arr1 = [0, 1, 2];
var arr2 = [3, 4, 5];

// ES5
Array.prototype.push.apply(arr1, arr2);

// ES6
arr1.push(...arr2);
```

### 扩展运算符的应用

#### 合并数组

扩展运算符提供了数组合并的新写法。

```js
var arr1 = ['a', 'b']
var arr2 = ['c', 'd', 'e'];
var arr3 = ['f'];
// ES5
arr1.concat(arr2, arr3);
// ES6
[...arr1, ...arr2, ...arr3];
```

#### 与解构赋值结合

```js
const [first, ...rest] = [1, 2, 3];
first;	// 1
rest;	// [2, 3]
```

如果将扩展运算符用于数组赋值，则只能将其放在参数的最后一位，否则会报错。

```js
const [...butLast, last] = [1, 2, 3];	// 报错
```

#### 函数的返回值

JavaScript 的函数只能返回一个值，如果需要返回多个值，只能返回数组或对象。扩展运算符提供了一种变通方法，也就是返回数组然后扩展出来。

#### 字符串

扩展运算符还可以将字符串转为真正的数组。

```js
[...'hello'];	// ['h', 'e', 'l', 'l', 'o']
```

这种写法有一个重要的好处，就是能够正确识别 32 位 Unicode 字符。

```js
'x\uD83D\uDE80y'.length;	// 4
[...'x\uD83D\uDE80y'].length;	// 3
```

利用这一点可以写出正确返回字符串长度的函数。

```js
function length (str) {
  return [...str].length;
}
```

凡是涉及操作 32 位 Unicode 字符的函数都会将 32 位 Unicode 字符识别为 2 个字符，所以最好都采用扩展运算符改写。

```js
let str = 'x\uD83D\uDE80y';
str.split('').reverse().join('');	// 'y\uDE80\uD83Dx';
[...str].reverse().join('');	// 'y\uD83D\uDE80x'
```

#### 实现了 Iterator 接口的对象

任何 Iterator 接口的对象都可以用扩展运算符转为真正的数组。

```js
var nodeList = document.querySelectorAll('div');
var array = [...nodeList];
```

上面代码中，querySelectorAll 方法返回一个 nodeList 对象。该对象是一个类似数组的对象，而 NodeList 对象实现了 Iterator，所以可以直接使用扩展运算符将其转为真正的数组。如果没有部署 Iterator 接口的类似数组的对象，就无法使用扩展运算符将其转为真正的数组。

```js
let arrayLike = {
  '0': 'a',
  '1': 'b',
  length: 2
};
let arr = [...arrayLike];	// 报错
```

可以使用 Array.from() 方法将 arrayLike 转为真正的数组。

#### Map 和 Set 结构、Generator 函数

Map 结构具有 Iterator 接口，所以可以使用扩展运算符。

```js
let map = new Map([
  [1, 'one'],
  [2, 'two'],
  [3, 'three'],
]);
let arr = [...map.keys()];	// [1, 2, 3]
```

Generator 函数运行后会返回一个遍历器对象，因此可以使用扩展运算符。

```js
var go = function* () {
  yield 1;
  yield 2;
  yield 3;
};
[...go()];	// [1, 2, 3]
```

## Array.from()

`Array.from()` 方法用于将两类对象转为真正的数组：类似数组的对象和可遍历的对象。实际应用中常见的类似数组的对象是 DOM 操作返回的 NodeList 集合和函数内部的 arguments 对象。所谓类似数组的对象，本质特征只有一点，即必须有 length 属性。所以任何有 length 属性的对象，都可以通过 Array.from() 方法转为数组，而扩展运算符无法转换。

```js
let arrayLike = {
  '0': 'a',
  '1': 'b',
  length: 2
};
// ES5
var arr1 = [].slice.call(arrayLike);	// ['a', 'b']
// ES6
var arr2 = Array.from(arrayLike);	// ['a', 'b']

Array.from({ length: 3 });	// [undefined, undefined, undefined]
```

Array.from() 方法还接受第二个参数，作用类似于数组的 map 方法，用来对每个元素进行处理，将处理后的值放入返回的数组中。

```js
Array.from(arrayLike, x => x * x);
// 等同于
Array.from(arrayLike).map(x => x * x);

Array.from([1, 2, 3], x => x * x);	// [1, 4, 9]
```

如果 map 函数里面用到了 this 关键字，还可以传入 Array.from() 第三个参数，用来绑定 this。

Array.from() 的另一个应用是将字符串转为数组，然后返回字符串的长度。因为它能正确处理各种 Unicode 字符，避免 JavaScript 将大于 \uFFFF 的 Unicode 字符算作 2 个字符的 bug。

```js
function countSymbols (string) {
  return Array.from(string).length;
}
```

## Array.of()

`Array.of()` 方法用于将一组值转换为数组。这个方法的主要目的是弥补数组构造函数 Array() 的不足。因为参数个数的不同会导致 Array() 的行为差异。Array.of() 基本上可以用来替代 Array() 或 new Array()。

```js
Array();	// []
Array(3);	// [, , ,]
Array(3, 4, 7);	// [3, 4, 7]

Array.of();	// []
Array.of(3);	// [3]
Array.of(3, 4, 7);	// [3, 4, 7]
```

## 数组实例的 copyWithin()

数组实例的 `copyWithin` 方法会在当前数组内部将指定位置的成员复制到其他位置（会覆盖原有成员），然后返回当前数组。也就是说该方法会修改当前数组。

```js
Array.prototype.copyWithin(target, start = 0, end = this.length);
```

方法接收三个参数，三个参数都应该是数值，如果不是会自动转换为数值。

- target：必选参数，表示从该位置开始替换数据。
- start：可选参数，表示从该位置开始读取数据，默认为 0。如果为负值，表示倒数，-1 表示数组最后一位。
- end：可选参数，表示到该位置前停止读取数据，默认为数组长度。如果为负值，表示倒数，-1 表示数组最后一位。

```js
[1, 2, 3, 4, 5].copyWithin(0, 3);	// [4, 5, 3, 4, 5]
[1, 2, 3, 4, 5].copyWithin(0, 3, 4);	// [4, 2, 3, 4, 5]
[1, 2, 3, 4, 5].copyWithin(0, -2, -1);	// [4, 2, 3, 4, 5]
```

## 数组实例的 find() 和 findIndex()

数组实例的 `find()` 方法用于找出第一个符合条件的数组成员。它的参数是一个回调函数，所有数组成员依次执行该回调函数，直到找出第一个返回值为 true 的成员，然后返回该成员。如果没有符合条件的成员，返回 undefined。

```js
[1, 3, -5, 7].find(n => n < 0);	// -5
```

回调函数可以接收 3 个参数，依次为当前的值、当前的位置和原数组。

数组实例的 `findIndex()` 方法的用法与 find() 方法非常类似，返回第一个符合条件的数组成员的位置，如果所有成员都不符合条件，返回 -1。

```js
[1, 3, -5, 7].findIndex(n => n < 0);	// 2
```

这两个方法都可以接受第二个参数，用来绑定回调函数的 this 对象。另外这两个方法都可以发现 NaN，弥补了数组的 indexOf() 方法的不足。

```js
[NaN].indexOf(NaN);	// -1
[NaN].findIndex(v => Number.isNaN(v));	// 0
```

## 数组实例的 fill()

`fill()` 方法用于填充一个数组，该方法用于空数组的初始化时非常方便。第一个参数是要填充的值，数组中已有的元素会被全部抹去。第二个参数和第三个参数用于指定填充的起始位置和结束位置。

```js
['a', 'b', 'c'].fill(7);	// [7, 7, 7]
new Array(3).fill(7);	// [7, 7, 7]
['a', 'b', 'c'].fill(7, 1, 2);	// ['a', 7, 'c']
```

## 数组实例的 entries()、keys() 和 values()

ES6 提供了 `entries()` `keys()` `values()` 三个新方法用于遍历数组。它们都返回一个遍历器对象，可以用 for ... of 循环遍历。唯一的区别在于，keys() 方法是对键名的遍历，values() 方法是对键值的遍历，entries() 方法是对键值对的遍历。

```js
for (let index of ['a', 'b'].keys()) {
  console.log(index);
  // 0
  // 1
}
for (let elem of ['a', 'b'].values()) {
  console.log(elem);
  // 'a'
  // 'b'
}
for (let [index, elem] of ['a', 'b'].entries()) {
  console.log(index, elem);
  // 0 'a'
  // 1 'b'
}
```

如果不使用 for ... of 循环，可以手动调用遍历器对象的 `next()` 方法进行遍历。

```js
let letter = ['a', 'b', 'c'];
let entries = letter.entries();
console.log(entries.next().value);	// [0, 'a']
console.log(entries.next().value);	// [1, 'b']
console.log(entries.next().value);	// [2, 'c']
```

## 数组实例的 includes()

ES2016 引入了 `Array.prototype.includes()` 方法，该方法返回一个布尔值，表示某个数组是否包含给定的值，与字符串的 includes 方法类似。该方法的第二个参数表示搜索的起始位置，默认为 0。如果第二个参数为负数，则表示倒数，-1 表示数组的最后一位。如果大于数组的长度，则重置为 0。

```js
[1, 2, 3].includes(3);	// true
[1, 2, NaN].includes(NaN);	// true
[1, 2, 3].includes(3, 3);	// false
[1, 2, 3].includes(3, -1);	// true
```

## 数组的空位

数组的空位指数组的某一个位置没有任何值，注意是没有值，如果值为 undefined 依然是有值的，空位是没有任何值的。Array 构造函数返回的数组都是空位。

```js
Array(3);	// [, , ,]
0 in [undefined, undefined];	// true
0 in [, ,];	// false
```

ES5 对空位的处理很不一致，大多数情况下会忽略空位。

- forEach()、filter()、every() 和 some() 都会跳过空位。
- map() 会跳过空位，但会保留这个值。
- join() 和 toString() 会将空位视为 undefined，而 undefined 和 null 会被处理成空字符串。

ES6 则是明确将空位转为 undefined。由于空位的处理规则非常不统一，所以建议避免出现空位。







# 第 9 章		对象的扩展

## 属性的简洁表示法

ES6 允许直接写入变量和函数作为对象的属性和方法。在对象中只写属性名，不写属性值，这时属性值等于属性名所代表的变量。当然，方法也可以简写。

```js
var name = 'zs';
var o = {
  name,
  method () {
    return 'hello';
  }
}
// 等同于
var o = {
  name: name,
  method: function () {
    return 'hello';
  }
}
```

CommonJS 模块输出变量就非常适合使用简洁写法。

```js
var ms = {};
function getItem (key) {
  return key in ms ? ms[key] : null;
}
function setItem (key, value) {
  ms[key] = value;
}
function clear () {
  ms = {};
}
module.exports = { getItem, setItem, clear };
// 等同于
module.exports = {
  getItem: getItem,
  setItem, setItem,
  clear: clear
}
```

注意，简洁写法中属性名总是字符串，所以属于关键字的属性名不会导致语法解析报错。

```js
var obj = {
  class () {}
}
// 等同于
var obj = {
  'class': function () {}
}
```

如果某个方法是一个 Generator 函数，则其前面需要加上星号。

```js
var obj = {
  * m () {
    yield 'hello world';
  }
}
```

## 属性名表达式

JavaScript 语言定义对象的属性有两种方法。

```js
// 标识符作为属性名
obj.foo = true;
// 用表达式作为属性名，表达式需要放在方括号内
obj['a' + 'bc'] = 123;
```

如果使用字面量方式定义对象，ES5 中只能使用标识符作为属性名定义属性，ES6 允许使用表达式作为属性名来定义属性。

```js
let propKey = 'foo';
var obj = {
 	[propKey]: true,
  abc: 123
}
```

表达式还可以用于定义方法名。

```js
var obj = {
  ['h' + 'ello'] () {
    return 'hi';
  }
}
obj.hello();	// 'hi'
```

注意，属性名表达式和简洁表示法不能同时使用，否则会报错。

```js
var foo = 'bar';
var bar = 'abc';
var baz = { [foo] };	// 报错

var foo = 'bar';
var baz = { [foo]: 'abc' }	// 正确
```

属性名表达式如果是一个对象，默认情况下会自动将对象转为字符串 `[object Object]`。

```js
const keyA = { a: 1 };
const keyB = { b: 2 };
const myObject = {
  [keyA]: 'valueA',
  [keyB]: 'valueB'
}
console.log(myObject);	// { '[object Object]': 'valueB' }
```

上面代码中，[keyA] 和 [keyB] 得到的都是 [object Object]，所以 [keyB] 覆盖了 [keyA]。

## 方法的 name 属性

函数的 name 属性返回函数名。对象方法也是函数，因此也有 name 属性。

```js
const person = {
  sayName () {}
}
person.sayName.name;	// 'sayName'
```

如果对象的方法使用了取值函数 getter 和存值函数 setter，则 name 属性不是在该方法上面，而是在该方法属性的描述对象的 get 和 set 属性上面，返回值是方法名前面加上 get 和 set。

```js
const obj = {
  get foo () {},
  set foo (x) {}
}
obj.foo.name;	// TypeError
const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
descriptor.get.name;	// 'get foo'
descriptor.set.name;	// 'set foo'
```

bind 方法创造的函数，name 属性返回 'bound' 加上原函数名字。Function 构造函数创造的函数，name 属性返回 'anonymous'。如果对象的方法是一个 Symbol 值，那么 name 属性返回的是这个 Symbol 值的描述。

## Object.is()

ES5 比较两个值是否相等，只有两个运算符：`==` 和 `===`。它们都有缺点，前者会自动转换数据类型，两者的 NaN 都不等于自身以及 +0 等于 -0。JavaScript 缺乏一种在所有环境中，只要两个值是一样的，就会相等的运算。ES6 提出了同值相等算法来解决这个问题。`Object.is()` 方法就是基于这个算法的新方法，它用来比较两个值是否严格相等，与 === 的行为基本一致，只有两个不同之处，一是 +0 不等于 -0，二是 NaN 等于自身。

```js
+0 === -0;	// true
Object.is(+0, -0);	// false

NaN === NaN;	// false
Object.is(NaN, NaN);	// true

Object.is({}, {});	// false
```

## Object.assign()

### 基本用法

`Object.assign()` 方法用于将源对象的所有可枚举属性复制到目标对象。方法的第一个参数是目标对象，后面的参数都是源对象。如果目标对象与源对象有同名属性，或多个源对象有同名属性，后面的属性会覆盖前面的属性。

```js
var target = { a: 1, c: 3 };
var source1 = { b: 2 };
var source2 = { c:33, d: 4 };
Object.assign(target, source1, source2);
target;	// { a: 1, c: 33, b: 2, d: 4 }
```

如果只有一个参数，方法会直接返回该参数。

```js
var obj = { a: 1 };
Object.assign(obj) === obj;	// true
```

如果参数不是对象，会先转为对象，然后返回。如果复制源对象的值给非对象，那么想要接收需要声明一个变量，否则非对象是无法接收这些复制的值的。

```js
let num = 2;
typeof Object.assign(num);	// 'object'
Object.assign(num);	// [Number: 2]
Object.assign(num, { a: 1 });	// {[Number: 2] a: 1 }
num;	// 2
let myObject = Object.assign(num, { a: 1 });
myObject;	// { [Number: 2] a: 1 }
num;	// 2
```

由于 undefined 和 null 无法转成对象，所以如果将它们作为参数，就会报错。

```js
Object.assign(undefined);	// 报错
Object.assign(null);	// 报错
```

如果非对象参数出现在源对象的位置，即不是第一个目标对象参数，那么处理规则将有所不同。这些参数都会转成对象，无法转换的将会跳过，这意味着 undefined 和 null 如果不在首参数就不会报错。

```js
let obj = { a: 1 };
Object.assign(obj, undefined) === obj;	// true
Object.assign(obj, null) === obj;	// true
```

其他类型的值如果不在首参数，那么除了字符串会以数组形式复制到目标对象，其他值都不会产生效果。这是因为只有字符串的包装对象会产生可枚举属性。

```js
var v1 = 'ab';
var v2 = true;
var v3 = 1000;
var obj = Object.assign({}, v1, v2, v3);
console.log(obj);	// { '0': 'a', '1': 'b' }
```

Object.assign 复制的属性是有限制的，只复制源对象的自身属性以及属性名为 Symbol 值的属性，不复制继承属性，也不复制不可枚举的属性。

### 注意点

Object.assign() 方法实行的是浅复制，而不是深复制。也就是说如果源对象某个属性的值是对象，那么目标对象复制得到的是这个对象的引用，而不是这个对象的副本。所以如果源对象的那个对象属性值改变，目标对象复制来的对象属性值也会改变。

```js
var obj1 = { a: { b: 1 }};
var obj2 = Object.assign({}, obj1);
obj1.a.b = 2;
obj2.a.b;	// 2
```

对于这种嵌套的对象，一旦遇到同名属性，将会替换而不是添加。

```js
var target = { a: { b: 'c', d: 'e' }};
var source = { a: { b: 'hello' }};
Object.assign(target, source);
target;	// { a: { b: 'hello' }}
```

Object.assign 可以用来处理数组，但是会把数组视为对象来处理。

```js
Object.assign([1, 2, 3], [4, 5]);	// [4, 5, 3]
```

### 常见用途

#### 为对象添加属性

```js
class Point {
  constructor (x, y) {
    Object.assign(this, {x, y});
  }
}
```

#### 为对象添加方法

```js
Object.assign(SomeClass.prototype, {
  someMethod (arg1, arg2) { ... },
  anotherMethod () { ... }
})
// 等同于
SomeClass.prototype.someMethod = function (arg1, arg2) { ... }
SomeClass.prototype.anotherMethod = function () { ... }
```

#### 克隆对象

```js
function clone (origin) {
  return Object.assign({}, origin);
}
```

上面这种方法只能克隆原始对象自身的值，如果想连继承的值一起克隆，需要这样定义方法。

```js
function clone (origin) {
  let originProto = Object.getPrototypeOf(origin);
  return Object.assign(Object.create(originProto), origin);
}
```

#### 合并多个对象

```js
const merge = (target, ...sources) => Object.assign(target, ...sources);
```

#### 为属性指定默认值

## 属性的可枚举性

对象的每一个属性都有一个描述对象，用于控制该属性的行为。`Object.getOwnPropertyDescriptor` 方法可以获取属性的描述对象。其中，描述对象的 `enumerable` 属性称为 “可枚举性”，如果该属性为 false，就表示某些操作会忽略当前属性。ES5 中有 3 个操作会忽略 enumerable 为 false 的属性。

- for ... in 循环：只遍历对象自身的和继承的可枚举属性。
- Object.keys()：返回对象自身的所有可枚举属性的键名。
- JSON.stringify()：只串行化对象自身的可枚举属性。

ES6 新增了 1 个会忽略 enumerable 为 false 的属性的操作，它就是 Object.assign()。以上 4 个操作中，只有 for ... in 会返回继承的属性。而操作中引入继承的属性会让问题复杂化，尽量不要用 for ... in 循环，而是使用 Object.keys() 代替。

## 属性的遍历

ES6 一共有 5 种方法可以遍历对象的属性。

1. for ... in：循环遍历对象自身的和继承的可枚举属性，不包含 Symbol 属性。
2. Object.keys(obj)：返回一个数组，包含对象自身的所有可枚举属性，不含继承来的属性和 Symbol 属性。
3. Object.getOwnPropertyNames(obj)：返回一个数组，包含对象自身的所有属性，不含 Symbol 属性和不可枚举属性。
4. Object.getOwnPropertySymbols(obj)：返回一个数组，包含对象自身的所有 Symbol 属性。
5. Reflect.ownKeys(obj)：返回一个数组，包含对象自身的所有属性，不管属性名是 Symbol 还是字符串，也不管是否可枚举。

以上 5 种方法遍历对象的属性时都遵守同样的属性遍历次序规则。

- 首先遍历所有属性名为数值的属性，按照数字排序。
- 其次遍历所有属性名为字符串的属性，按照生成时间排序。
- 最后遍历所有属性名为 Symbol 值的属性，按照生成时间排序。

```js
Reflect.ownKeys({ [Symbol()]: 0, b: 0, 10: 0, 2: 0, a: 0 });	// ['2', '10', 'b', 'a', Symbol]
```

## \_\_proto\_\_ 属性、Object.setPrototypeOf()、Object.getPrototypeOf()

### \_\_proto\_\_ 属性

`__proto__` 属性用来读取或设置当前对象的 prototype 对象，目前所有浏览器都部署了这个属性。

```js
// ES6
var obj = {
  method () { ... }
};
obj.__proto__ = someOtherObj;
  
// ES5
var obj = Object.create(someOtherObj);
obj.method = function () { ... }
```

该属性并没有写入 ES6 的正文，而是写入了附录，因为前后的双下划线说明它本质上是一个内部属性，而不是一个正式的对外 API，只是因为浏览器的广泛支持，才加入了 ES6。标准明确规定只有浏览器必须部署这个属性，而其他运行环境不一定要部署。所以无论从语义的角度还是兼容性的角度，都不要使用这个属性，而是使用 Object.getPrototypeOf()（读操作）、Object.setPrototypeOf()（写操作）和 Object.create()（生成）代替。

### Object.setPrototypeOf()

`Object.setPrototypeOf()` 方法的作用与 \_\_proto\_\_ 相同，用来设置一个对象的 prototype 对象，返回参数对象本身。它是 ES6 正式推荐的设置原型对象的方法。格式为 `Object.setPrototypeOf(object, prototype)`。

```js
let proto = {};
let obj = { x: 10 };
Object.setPrototypeOf(obj, proto);
proto.y = 20;
proto.z = 40;
obj.x;	// 10
obj.y;	// 20
obj.z;	// 40
obj;	// { x: 10 }
obj.__proto__;	// { y: 20, z: 40 }
```

如果第一个参数不是对象，则会自动转为对象，但由于返回的还是第一个参数，所以这个操作不会产生任何效果。但如果第一个参数为 undefined 或 null，就会报错，因为 undefined 和 null 无法转为对象。

### Object.getPrototypeOf()

该方法与 setPrototypeOf 方法配套，用于读取一个对象的 prototype 对象。`Object.getPrototypeOf(obj)` 如果参数不是对象，则会被自动转为对象。如果参数是 undefined 或 null，由于无法转换所以会报错。

```js
function Rectangle () { ... }
var rec = new Rectangle();
Object.getPrototypeOf(rec) === Rectangle.prototype;	// true
Object.setPrototypeOf(rec, Object.prototype);
Object.getPrototypeOf(rec) === Rectangle.prototype;	// false
```

## Object.keys()、Object.values()、Object.entries()

### Object.keys()

ES5 引入了 `Object.keys()` 方法，返回一个数组，成员是参数对象自身的所有可遍历属性的键名，不包含继承的属性。

```js
var obj = { foo: 'bar', baz: 42 };
Object.keys(obj);	// ['foo', 'baz']
```

### Object.values()

`Object.values()` 方法返回一个数组，成员是参数对象自身的所有可遍历属性的键值，不包含继承的属性值，不包含属性名为 Symbol 值的属性值。数组成员的顺序，与本章的 “属性的遍历” 部分介绍的排列规则一样。

```js
var obj = { foo: 'bar', baz: 42 };
Object.values(obj);	// ['bar', 42]
```

如果参数不是对象，会先转为对象，而数值和布尔值的包装对象都不会为实例添加非继承的属性，所以方法会返回空数组。而字符串参数则会返回各个字符组成的一个数组。

```js
Object.values('foo');	// ['f', 'o', 'o']
Object.values(1);	// []
Object.values(true);	// []
```

### Object.entries

`Object.entries()` 方法返回一个数组，成员是参数对象自身的所有可遍历属性的键值对数组，不包含继承的。除了返回值不一样，该方法的行为与 Object.values 基本一致。该方法的基本用途是遍历对象的属性。

```js
let obj = { one: 1, two: 2 };
for (let [k, v] of Object.entries(obj)) {
  console.log(`${JSON.stringify(k)}: ${JSON.stringify(v)}`);
  // "one": 1
  // "two": 2
}
```

另一个用处就是将对象转为真正的 Map 结构。

```js
var obj = { foo: 'bar', baz: 42 };
var map = new Map(Object.entries(obj));
map;	// Map { foo: 'bar', baz: 42 }
```

## 对象的扩展运算符

ES2017 为对象引入了扩展运算符。

### 解构赋值

对象的解构赋值用于从一个对象取值，相当于将所有可遍历的但尚未被读取的属性分配到指定的对象上面。所有的键和它们的值都会复制到新对象上面。注意是浅复制，即如果一个键的值是复合类型的值，那么解构赋值复制的是这个值的引用，而不是这个值的副本。另外解构赋值要求等号右边是一个对象，如果等号右边是 undefined 或 null，将会报错，因为这两个值无法转为对象。扩展运算必须是最后一个参数，否则会报错。

```js
let { x, y, ...z } = { x: 1, a: 2, b: 3, y: 4 };
x;	// 1
y;	// 4
z;	// { a: 2, b: 3 }

let obj = { a: { b: 1 }};
let { ...x } = obj;
obj.a.b = 2;
x.a.b;	// 2

let { x, y, ...z } = null;	// 运行时错误
let { x, y, ...z } = undefined;	// 运行时错误

let { ...x, y, z } = obj;	// 语法错误
```

解构赋值不会复制继承自原型对象的属性。

```js
let o1 = { a: 1 };
let o2 = { b: 2 };
o2.__proto__ = o1;
let { ...o3 } = o2;
o3;	// { b: 2 }
o3.a;	// undefined
```

### 扩展运算符

扩展运算符 `...` 用于取出参数对象的所有可遍历属性，并将其复制到当前对象之中。等同于使用 Object.assign() 方法。

```js
let z = { a: 3, b: 4 };
let n = { ...z };
n;	// { a: 3, b: 4 }
// 等同于
let n = Object.assign({}, z);
```

上面的例子只是复制了对象实例的属性，如果想完整克隆一个对象，还要复制对象原型的属性，可以采用下列方法。写法一中的 \_\_proto\_\_ 属性在非浏览器环境中不一定部署，所以推荐第二种方法。

```js
// 方法一
const clone1 = {
  __proto__: Object.getPrototypeOf(obj),
  ...obj
};
// 写法二
const clone2 = {
  Object.create(Object.getPrototypeOf(obj)),
  obj
}
```

扩展运算符的参数对象中如果有取值函数 get，这个函数会执行。

```js
// 抛出错误
let runtimeError = {
  ...a,
  ...{
    get x () {
      throw new Error('throw now');
    }
  }
}
```

如果用户自定义的属性放在扩展运算符后面，扩展运算符内部的同名属性会被覆盖。如果自定义属性放在扩展运算符前面，则自定义属性会被扩展运算符参数对象的同名属性覆盖。与数组的扩展运算符一样，对象的扩展运算符后面可以带有表达式。如果扩展运算符后面是一个空对象，则没有任何效果。如果扩展运算符的参数是 null 或 undefined，则这两个值会被忽略，不会报错。

```js
...(x > 1 ? { a: 1 } : {});
...null;	// 不报错
{ ...{}, a: 1 };	// { a: 1 }
```

## Object.getOwnPropertyDescriptors()

ES5 的 Object.getOwnPropertyDescriptor 方法用来返回某个对象属性的描述对象。ES2017 引入了 Object.getOwnPropertyDescriptors 方法，返回指定对象所有自身属性的描述对象，不包含继承属性的描述对象。

```js
const obj = {
  foo: 123,
  get bar () { return 'abc' }
};
Object.getOwnPropertyDescriptors(obj);
// {
//   foo:
//     { value: 123,
//       writable: true,
//       enumerable: true,
//       configurable: true },
//   bar:
//     { get: [Function: get bar],
//       set: undefined,
//       enumerable: true,
//       configurable: true } 
// }
```

该方法的引入主要是为了解决 Object.assign() 方法无法正确复制 get 属性和 set 属性的问题。可以使用 Object.defineProperties 方法配合 Object.getOwnPropertyDescriptors 方法实现正确的复制。

```js
const source = {
  set foo (value) {
    console.log(value);
  }
}
const target1 = {}
Object.assign(target1, source);
Object.getOwnPropertyDescriptor(target1, 'foo');
// { value: undefined, ... }

const target2 = {}
Object.defineProperties(target2, Object.getOwnPropertyDescriptors(source));
Object.getOwnPropertyDescriptor(target2, 'foo');
// { set: [Function: foo], ... }
```

上诉代码中，source 对象的 foo 属性值是一个赋值函数，Object.assign 方法将这个属性复制给了 target1 对象，结果该属性的值变成了 undefined。这是因为 Object.assign 方法总是复制一个属性的值，而不会复制它背后的复制方法或取值方法。

该方法的另一个用处是配合 Object.create 方法将对象属性克隆到一个新对象，这属于浅复制。

```js
const clone = Object.create(Object.getPrototypeOf(obj), Object.getOwnPropertyDescriptors(obj));	// 克隆 obj
```

该方法可以实现一个对象继承另一个对象。

```js
// 以前的方法
const obj = {
  __proto__: prot,
  foo: 123
}

// 使用 Object.getOwnPropertyDescriptors
const obj = Object.create(
	prot,
  Object.getOwnPropertyDescriptor({
    foo: 123
  })
)
```

该方法也可以用来实现 Mixin 混入模式。具体翻阅 p180。

## Null 传导运算符

如果读取对象内部的某个属性，往往需要判断该对象是否存在，例如读取 message.body.user.firstName，安全的写法如下：

```js
const firstName = (message && message.body && message.body.user && message.body.user.firstName) || 'default'
```

上面这样层层判断非常麻烦，所以现在有一个提案引入了 Null 传导运算符 `?.`，可以简化上面的写法，实际测试报错。

```js
const firstName = message?.body?.user?.firstName || 'default'
```

上面代码有 3 个 ?. 运算符，只要其中一个返回 null 或 undefined，就不再继续运算，而是返回 undefined。

Null 运算符有 4 种用法。

- obj?.prop：读取对象属性。
- obj?.[expr]：读取对象属性。
- func?.(...args)：函数或对象方法的调用。
- new C?.(...args)：构造函数的调用。

```js
a?.b.c().d;	// 如果 a 是 null 或 undefined，返回 undefined，否则返回 a.b.c().d
a?.b = 42;	// 如果 a 是 null 或 undefined，这条的语句不产生任何效果，否则执行 a.b = 42
delete a?.b;	// 如果 a 是 null 或 undefined，这条的语句不产生任何效果
```







# 第 10 章		Symbol

## 概述

ES5 的对象属性名都是字符串，容易造成属性名冲突。如使用一个他人提供的对象，为这个对象添加一个新属性，这时可能会与对象原有的属性发生属性名相同的冲突。ES6 为了解决这一问题，引入了一种新的原始数据类型 `Symbol`。这是 JavaScript 的第 7 种数据类型，表示独一无二的值。Symbol 值通过 Symbol 函数生成，所以现在对象属性名可以有两种类型，一种原有的字符串，一种新增的 Symbol 类型。只要属性名属于 Symbol 类型，就是独一无二的，保证不会与其他属性名产生冲突。

```js
let s = Symbol();
typeof s;	// 'symbol'
```

Symbol 函数前不能使用 new 操作符，否则会报错。因为 Symbol 是原始类型，而不是对象，所以 Symbol 值也不能添加属性，基本上，是一种与字符串类似的数据类型。

Symbol 函数可以接受一个字符串作为参数，表示对 Symbol 实例的描述，主要是为了在控制台显示或转为字符串时比较容易区分。通过 String 函数或 toString 方法可以将 Symbol 值显示转为字符串。如果 Symbol 的参数是一个对象，会调用该对象的 toString 方法，返回值作为参数。

```js
var s1 = Symbol('foo');
s1;	// Symbol(foo)
s1.toString();	// 'Symbol(foo)'
String(s1);	// 'Symbol(foo)'
var s2 = Symbol({
  toString () {
    return 'abc';
  }
})
s2;	// Symbol(abc)
```

因为 Symbol 函数的参数只表示对当前 Symbol 值的描述，所以相同参数的 Symbol 函数的返回值是不相等的。

```js
var s1 = Symbol();
var s2 = Symbol();
s1 === s2;	// false
var s3 = Symbol('false');
var s4 = Symbol('false');
s3 === s4;	// false
```

Symbol 值不能与其他类型的值进行运算，否则会报错。

```js
var sym = Symbol('My Symbol');
"your symbol is " + sym;	// TypeError
```

Symbol 值还可以转为布尔值，但不能转为数值。

```js
var sym = Symbol();
Boolean(sym);	// true
!sym;	// false
Number(sym);	// TypeError
```

## 作为属性名的 Symbol

因为 Symbol 值是独一无二的，所以 Symbol 值作为标识符用于对象的属性名可以保证不会出现同名的属性，这样能防止某一个键被不小心改写或覆盖，对于由多个模块构成的对象非常有用。注意，使用 Symbol 值定义属性时，Symbol 值必须放在方括号中，不能使用点运算符。因为点运算符后面总是字符串，所以不会读取 Symbol 值作为标识名所指代的属性。

```js
var mySym = Symbol();
// 第一种写法
var a = {};
a[mySym] = 'hello';
// 第二种写法
var a = {
  [mySym]: 'hello'
};
// 第三种写法
var a = {};
Object.defineProperty(a, mySym, { value: 'hello' });
// 以上写法都得到相同的结果
a[mySym];	// 'hello'

a.mySym = 'world';
a[mySym];	// hello
a['mySym'];	// world
```

Symbol 类型还可以用于定义一组常量，保证这组常量的值都是不相等的。注意，Symbol 值作为属性名时，该属性是公开属性，而不是私有属性。

## 实例：消除魔术字符串

魔术字符串指在代码中多次出现、与代码形成强耦合的某一个具体的字符串或数值。风格良好的代码，应该尽量消除魔术字符串，而由含义清晰的变量代替。常用的消除魔术字符串的方法，就是把它写成一个变量。

```js
function getArea (shape, options) {
  var area = 0;
  switch (shape) {
    case 'Triangle':	// 魔术字符串
      area = .5 * options.width * options.height;
      break;
    ...
  }
  return area;
}
getArea('Triangle', { width: 100, height: 100 });	// 魔术字符串
// 可以把 Triangle 封装为一个对象的属性，可以把 switch 中所有值都封装进对象来消除魔术字符串
var shapeType = {
  triangle: 'Triangle',
  ...
};
function getArea (shape, options) {
  var area = 0;
  switch (shape) {
    case shapeType.triangle:
      area = .5 * options.width * options.height;
      break;
    ...
  }
  return area;
}
getArea(shapeType.triangle, { width: 100, height: 100 });
```

上面的代码中，将 ‘Triangle’ 写成 shapeType 对象的 triangle 属性，这样消除了强耦合。但仔细分析后，发现 shapeType.triangle 等于什么值并不重要，只要确保不会与其他 shapeType 对象属性的值冲突即可，所以这里就很适合使用 Symbol 值。

```js
const shapeType = {
  triangle: Symbol()
}
```

## 属性名的遍历

Symbol 作为属性名，该属性不会出现在 for ... in、for ... of 循环中，也不会被 Object.keys()、Object.getOwnPropertyNames() 返回。但它又不是私有属性，有一个 `Object.getOwnPropertySymbols` 方法可以获取指定对象的所有 Symbol 属性名。另一个新的 API —— `Reflect.ownKeys()` 方法可以返回所有类型的键名，包括常规键名和 Symbol 键名。

## Symbol.for()、Symbol.keyFor()

有时希望重新使用同一个 Symbol 值，`Symbol.for()` 方法可以做到这一点。它接收一个字符串作为参数，然后搜索有没有登记以该参数作为名字的 Symbol 值，如果有就返回，没有就在全局登记新建并返回一个以参数字符串作为名称的 Symbol 值。

```js
var s1 = Symbol.for('foo');
var s2 = Symbol.for('foo');
s1 === s2;	// true
```

Symbol.for 方法创建的 Symbol 值是有登记在全局环境中供搜索的，而 Symbol 函数不会。所以 Symbol 函数创建的 Symbol 值，就算参数字符串相同，Symbol 值也不同。

```js
var s1 = Symbol('foo');	
var s2 = Symbol('foo');
s1 === s2;	// false
var s3 = Symbol.for('foo');
s1 === s3;	// false
```

`Symbol.keyFor()` 方法返回一个已经登记的 Symbol 类型值的参数 key。

```js
var s1 = Symbol('foo');
var s2 = Symbol.for('foo');
Symbol.keyFor(s1);	// undefined
Symbol.keyFor(s2);	// 'foo'
```

## 实例：模块的 Singleton 模式

Singleton 模式指的是，调用一个类并且在任何时候都返回同一个实例。对于 Node 来说，模块文件可以看成一个类，如果要保证每次执行这个模块文件返回的都是同一个实例，可以把实例放到顶层对象 global 中。

```js
// ./mod.js
function A () {
  this.foo = 'hello';
}
if (!global._foo) {
  global._foo = new A();
}
module.exports = global._foo;
// ./test.js
var a = require('./mod.js');
console.log(a.foo);	// 'hello'
```

上面代码中，变量 a 任何时候加载的都是 A 的同一个实例。这有一个问题，全局变量 global._foo 是可写的，任何文件都可修改。

```js
var a = require('./mod.js');
global._foo = 123;
```

上面的代码会使别的脚本在加载 mod.js 时都产生失真。为了防止这种情况，可以使用 Symbol。

```js
// ./mod.js
const FOO_KEY = Symbol('foo');
function A () {
  this.foo = 'hello';
}
if (!global[FOO_KEY]) {
  global[FOO_KEY] = new A();
}
module.exports = global[FOO_KEY];
```

上面的代码中其他脚本都无法引用 FOO_KEY，但这样多次执行脚本时，每次得到的 FOO_KEY 都是不一样的。虽然 Node 会将脚本的执行结果缓存，一般不会多次执行同一个脚本，但禁不住用户手动清除缓存，所以也不是完全可靠。

## 内置的 Symbol 值

除了自定义的 Symbol 值，ES6 还提供了 11 个内置的 Symbol 值，指向语言内部使用的方法。

### Symbol.hasInstance

对象的 `Symbol.hasInstance` 属性指向一个内部方法，对象使用 instanceof 运算符时会调用这个方法。

```js
class Event {
  static [Symbol.hasInstance] (obj) {
    return Number(obj) % 2 === 0;
  }
}
1 instanceof Even;	// false
2 instanceof Even;	// true

class MyClass {
  [Symbol.hasInstance] (foo) {
    return foo instanceof Array;
  }
}
[1, 2, 3] instanceof new MyClass();	// true
```

### Symbol.isConcatSpreadable

对象的 `Symbol.isConcatSpreadable` 属性等于一个布尔值，表示该对象使用 Array.prototype.concat() 时是否可以展开。Symbol.isConcatSpreadable 为 true 或 undefined 时都可以展开，数组的默认行为就是可以展开。

```js
let arr1 = [1, 2, 3];
[4, 5].concat(arr1, 6);	// [4, 5, 1, 2, 3, 6]
arr1[Symbol.isConcatSpreadable];	// undefined
let arr2 = [7, 8];
arr2[Symbol.isConcatSpreadable] = false;
[4, 5].concat(arr2, 9);	// [4, 5, [7, 8], 9]
```

类似数组的对象也可以展开，但它的 Symbol.isConcatSpreadable 默认为 false，必须手动打开。

```js
let obj = { length: 2, 0: 1, 1: 2 };
[3, 4].concat(obj, 5);	// [3, 4, obj, 5]
obj[Symbol.isConcatSpreadable] = true;
[3, 4].concat(obj, 5);	// [3, 4, 1, 2, 5]
```

对于一个类而言，Symbol.isConcatSpreadable 属性必须写成实例的属性。

```js
class A1 extends Array {
  constructor (args) {
    super(args);
    this[Symbol.isConcatSpreadable] = true;
  }
}
class A2 extends Array {
  constructor (args) {
    super(args);
    this[Symbol.isConcatSpreadable] = false;
  }
}
let a1 = new A1();
a1[0] = 3;
a1[1] = 4;
let a2 = new A2();
a2[0] = 5;
a2[1] = 6;
[1, 2].concat(a1).concat(a2);	// [1, 2, 3, 4, [5, 6]]
```

### Symbol.species

对象的 `Symbol.species` 属性指向当前对象的构造函数，会返回一个函数。创建实例时默认会把这个返回的方法当作构造函数来创造新的实例对象。定义 Symbol.species 属性要采用 get 读取器。

```js
class MyArray extends Array {
  static get [Symbol.species] () {
    return Array;
  }
}
var a = new MyArray(1, 2, 3);
var mapped = a.map(x => x * x);
mapped instanceof MyArray;	// false
mapped instanceof Array;	// true
```

### Symbol.match

对象的 `Symbol.match` 属性指向一个函数，执行 string.match(object) 时，如果该属性存在，会调用指向的函数返回函数返回值。

```js
class MyMatcher {
  [Symbol.match] (string) {
    return 'hello'.indexOf(string);
  }
}
'e'.match(new MyMatcher());	// 1
```

### Symbol.replace

对象的 `Symbol.replace` 属性指向一个方法，当对象被 String.prototype.replace 方法调用时会返回该方法的返回值。

```js
const x = {};
x[Symbol.replace] = (...s) => console.log(s);
'Hello'.replace(x, 'World');	// ['Hello', 'World']
```

Symbol.replace 会接收到两个参数，第一个参数是 replace 方法正在作用的对象，也就是上面的 ‘Hello’，第二个参数是替换后的值，也就是上面的 ‘World’。

### Symbol.search

对象的 `Symbol.search` 属性指向一个方法，当对象被 String.prototype.search 方法调用时会返回该方法的返回值。

```js
class MySearch {
  constructor (value) {
    this.value = value;
  }
  [Symbol.search] (string) {
    return string.indexOf(this.value);
  }
}
'foobar'.search(new MySearch('foo'));	// 0
```

### Symbol.split

对象的 `Symbol.split` 属性指向一个方法，当对象被 String.prototype.split 方法调用时会返回该方法的返回值。

```js
class MySplit {
  constructor (value) {
    this.value = value;
  }
  [Symbol.split] (string) {
    var index = string.indexOf(this.value);
    if (index === -1) {
      return string;
    }
    return [
      string.substr(0, index),
      string.substr(index + this.value.length);
    ]
  }
}
'foobar'.split(new MySplit('foo'));	// ['', 'bar']
'foobar'.split(new MySplit('bar'));	// ['foo', '']
```

### Symbol.iterator

对象的 `Symbol.iterator` 属性指向该对象的默认遍历器方法。对象进行 for ... of 循环时，会调用 Symbol.iterator 方法返回该对象的默认遍历器。

### Symbol.toPrimitive

对象的 `Symbol.toPrimitive` 属性指向一个方法，对象转为原始类型的值时会调用这个方法，返回该对象对应的原始类型值。Symbol.toPrimitive 被调用时会接受一个表示当前运算模式的字符串，一共有 3 种运算模式。

- number：该场合需要转成数值。
- string：该场合需要转成字符串。
- default：该场合可以转成数值也可以转成字符串。

```js
let obj = {
  [Symbol.toPrimitive] (hint) {
    switch (hint) {
      case 'number':
        return 123;
      case 'string':
        return 'string';
      case 'default':
        return 'default';
      default:
        throw new Error();
    }
  }
}
2 * obj;	// 246
String(obj);	// string
3 + obj;	// '3default'
```

### Symbol.toStringTag

对象的 `Symbol.toStringTag` 属性指向一个方法，在对象上调用 Object.prototype.toString 方法时，如果这个属性存在，其返回值会出现在 toString 方法返回的字符串中，表示对象的类型。也就是说这个属性可以定制 [object Object] 或 [object Array] 中 object 后面的字符串。

```js
({ [Symbol.toStringTag]: 'Foo' }.toString());	// [object Foo]
```

ES6 新增内置对象的 Symbol.toStringTag 属性值在 p201。

### Symbol.unscopables

对象的 `Symbol.unscopables` 属性指向一个对象，指定了使用 with 关键字时哪些属性会被 with 环境排除。

```js
Array.prototype[Symbol.unscopables];
// { copyWithin: true,
//   entries: true,
//   fill: true,
//   find: true,
//   findIndex: true,
//   includes: true,
//   keys: true,
//   values: true }
```

上面代码说明，数组有 7 个属性会被 with 命令排除。







# 第 11 章		Set 和 Map 数据结构

## Set

### 基本用法

ES6 提供了新的数据结构 `Set`。它类似于数组，区别在于成员的值都是唯一的，没有重复。向 Set 加入值时不会发生类型转换，所以 5 和 '5' 是不同的值。Set 内部判断两个值是否相等的算法叫做同值相等算法，类似于精确相等运算符，和 Object.is 方法一样，NaN 等于自身。Set 本身是一个构造函数，用来生成 Set 数据结构。构造函数可以接收一个数组或具有 iterable 接口的其他数据结构作为参数，用来初始化。

```js
const s = new Set();
[2, 3, 4, 5, 5, 2, 2].forEach(x => s.add(x));
for (let i of s) {
  console.log(i);	// 2 3 4 5
}

const set = new Set([1, 2, 3, 4, 4]);
[...set];	// [1, 2, 3, 4]

// 利用 Set 内部成员值唯一的特性，可以去除数组重复的成员
let arr = [1, 1, 2, 2, 3, 3, 4, 4];
[...new Set(arr)];	// [1, 2, 3, 4]
```

### Set 实例的属性和方法

Set 结构的实例有以下属性。

- Set.prototype.constructor：构造函数，默认就是 Set 函数。
- Set.prototype.size：返回 Set 实例的成员总数。

Set 实例的方法有用于操作数据的操作方法，用于遍历成员的遍历方法，下面介绍 4 个操作方法。

- add(value)：添加某个值，返回 Set 结构本身。
- delete(value)：删除某个值，返回一个布尔值，表示删除是否成功。
- has(value)：返回一个布尔值，表示参数是否为 Set 的成员。
- clear()：清除所有成员，没有返回值。

```js
const s = new Set();
s.add(1).add(2).add(2);
s.size;	// 2
s.has(1);	// true
s.has(2);	// true
s.has(3);	// false
s.delete(2);	// true
s.has(2);	// false
s.size;	// 1
```

Array.from 方法可以将 Set 结构转为数组，利用这一点可以封装一个去除数组重复元素的方法。

```js
const items = new Set([1, 2, 3, 4, 5]);
items;	// Set { 1, 2, 3, 4, 5 }
Array.from(items);	// [1, 2, 3, 4, 5]

function dedupe (array) {
  return Array.from(new Set(array));
}
let arr = dedupe([1, 1, 2, 2, 3, 3, 4, 4, 5]);
arr;	// [1, 2, 3, 4, 5]
```

### 遍历操作

Set 结构的实例有 4 个遍历方法，可用于遍历成员。Set 的遍历顺序就是插入顺序。

- keys()：返回键名的遍历器。
- values()：返回键值的遍历器。
- entries()：返回键值对的遍历器。
- forEach()：使用回调函数遍历每个成员。

#### keys()、values()、entries()

这三个方法都返回遍历器对象。因为 Set 结构没有键名，只有键值（或说键名键值都是同一个值），所以 keys 方法和 values 方法的行为完全一致。而 entries 返回的遍历器同时包括键名和键值，所以每次输出一个数组，其两个成员键名和键值完全相等。

```js
let set = new Set(['red', 'green', 'blue']);
for (let item of set.keys()) {
  console.log(item);	// 'red' 'green' 'blue'
}
for (let item of set.values()) {
  console.log(item);	// 'red' 'green' 'blue'
}
for (let item of set.entries()) {
  console.log(item);	// ['red', 'red'] ['green', 'green'] ['blue', 'blue']
}
```

Set 结构的实例默认可遍历，其默认遍历器生成函数就是它的 values 方法。

```js
Set.prototype[Symbol.iterator] === Set.prototype.values;	// true
```

这意味着使用 for ... of 循环时可以省略 values 方法，直接 for ... of 循环遍历 Set。

```js
let set = new Set(['red', 'green', 'blue']);
for (let item of set) {
  console.log(item);	// 'red' 'green' 'blue'
}
```

#### forEach()

Set 结构实例的 forEach 方法接收两个参数，没有返回值。第一个是回调函数，用于对每个成员执行某种操作，函数有三个参数，依次为键值、键名、集合本身。第二个参数是表示绑定的 this 对象。

```js
let set = new Set([1, 2, 3]);
set.forEach(value => console.log(value * 2));	// 2, 4, 6
```

#### 遍历的应用

扩展运算符 ... 内部使用 for ... of 循环，所以也可以用于 Set 结构。扩展运算符和 Set 结构相结合还可以去除数组重复成员。

```js
let arr = [1, 1, 2, 2, 3, 3, 4, 4];
let unique = [...new Set(arr)];	// [1, 2, 3, 4]
```

数组的 map 和 filter 方法也可以用于 Set。

```js
let set = new Set([1, 2, 3]);
set = new Set([...set].map(x => x * 2));	// Set { 2, 4, 6 }

set = new Set([...set].filter(x => x % 2 === 0));	// Set { 2 }
```

使用 Set 可以很容易的实现并集、交集和差集。

```js
let a = new Set([1, 2, 3]);
let b = new Set([2, 3, 4]);
// 并集
let union = new Set([...a, ...b]);	// Set { 1, 2, 3, 4 }
// 交集
let intersect = new Set([...a].filter(x => b.has(x)));	// Set { 2, 3 }
// 差集
let difference = new Set([...a].filter(x => !b.has(x)));	// Set { 1 }
```

## WeakSet

### 含义

`WeakSet` 结构与 Set 类似，也是不重复的值的集合，但与 Set 有两个区别。第一，WeakSet 的成员只能是对象，而不能是其他类型的值。第二，WeakSet 中的对象都是弱引用，即垃圾回收机制不考虑 WeakSet 对该对象的引用，如果其他对象都不再引用该对象，那么垃圾回收机制不考虑该对象是否还存在于 WeakSet 中，会自动回收该对象所占用的内存。所以 WeakSet 适合临时存放一组对象，以及存放跟对象绑定的信息。ES6 规定 WeakSet 不可遍历。以上特点同样适用后面介绍的 WeakMap 结构。

```js
const ws = new WeakSet();
ws.add(1);	// TypeError
ws.add(Symbol());	// TypeError
```

### 语法

WeakSet 是一个构造函数，可以使用 new 命令创建 WeakSet 数据结构。作为构造函数，WeakSet 接收一个数组或类似数组的对象作为参数。实际上，任何具有 iterable 接口的对象都可以作为 WeakSet 的参数。该数组的所有成员都会自动成为 WeakSet 实例对象的成员，这意味着参数数组的成员只能是对象。

```js
const a = [3, 4];
const b = [[1, 2], [3, 4]];
const wsa = new WeakSet(a);	// Uncaught TypeError
const wsb = new WeakSet(b);	// WeakSet { [1, 2], [3, 4] }
```

WeakSet 结构有以下 3 个方法。

- WeakSet.prototype.add(value)：向 WeakSet 实例添加一个新成员。
- WeakSet.prototype.delete(value)：清除 WeakSet 实例的指定成员。
- WeakSet.prototype.has(value)：返回一个布尔值，表示某个值是否在 WeakSet 实例中。

WeakSet 没有 size 属性，没有办法遍历其成员。

```js
ws.size;	// undefined
ws.forEach;	// undefined
```

## Map

### 含义和基本用法

JavaScript 的 Object 对象本质上是键值对的 Hash 结构集合，但只能用字符串作为键为使用带来了很大的限制。ES6 提供了 `Map` 数据结构来解决这一问题。它类似于对象，也是键值对的集合，但是键的范围不限于字符串，各种类型的值（包括对象）都可以当作键。也就是说，Object 结构提供了 “字符串 —— 值” 的对应，而 Map 结构提供了 “值 —— 值” 的对应，是一种更完善的 Hash 结构实现。作为构造函数，Map 可以接受一个数组作为参数，该数组的成员是一个个表示键值对的数组。

```js
const map = new Map([
  ['name', 'zs'],
  ['title', 'Author']
]);
map.size;	// 2
map.has('name');	// true
map.get('name');	// 'zs'
map.get('title');	// 'Author'
```

事实上，不仅仅是数组，任何具有 Iterator 接口且每个成员都是一个双元素数组的数据结构都可以当作 Map 构造函数参数。所以 Set 和 Map 都可以当作参数来生成新的 Map。

```js
const set = new Set([
  ['one', 1],
  ['two', 2]
]);
const m1 = new Map(set);
m1.get('one');	// 1
const m2 = new Map([['three', 3]]);
const m3 = new Map(m2);
m3.get('three');	// 3
```

如果读取一个未知的键，会返回 undefined。如果对同一个键多次赋值，后面的值将覆盖前面的值，而只有对同一个对象的引用，Map 结构才将其视为同一个键。

```js
const map = new Map();
map.get('jkfjasklfdj');	// undefined
map.set(1, 'a').set(1, 'b');
map.get(1);	// 'b'
map.set(['a'], 555);
map.get(['a']);	// undefined
```

如果 Map 的键是一个简单类型的值（数字、布尔值、字符串），则只要两个值严格相等，Map 就将其视为一个键，包括 0 和 -0。另外，NaN 虽然不严格等于自身，但是 Map 将其视为同一个键。

```js
let map = new Map();
map.set(-0, 333);
map.get(+0);	// 333
map.set(true, 1);
map.set('true', 2);
map.get(true);	// 1
map.set(undefined, 3);
map.set(null, 4);
map.get(undefined);	// 3
map.set(NaN, 111);
map.get(NaN);	// 111
```

由上可知，Map 的键实际上是和内存地址绑定的，只要内存地址不一样，就是为两个键。这就解决了同名属性碰撞的问题。

### 实例的属性和操作方法

#### size 属性

`size` 属性返回 Map 结构的成员总数。

#### set(key, value)

`set` 方法设置键 key 所对应的键值 value，然后返回整个当前的 Map 对象，所以可以采用链式写法。如果 key 已经有值，则 value 会被更新，否则就生成新的 key。

#### get(key)

`get` 方法读取 key 对应的键值，如果找不到 key 返回 undefined。

#### has(key)

`has` 方法返回一个布尔值，表示某个键是否在 Map 数据结构中。

#### delete(key)

`delete` 方法删除指定 key 键，删除成功返回 true，删除失败返回 false。

#### clear()

`clear` 方法清除所有成员，没有返回值。

### 遍历方法

Map 原生提供 3 个遍历器生成函数和 1 个遍历方法。Map 的遍历顺序就是插入顺序。

- keys()：返回键名的遍历器。
- values()：返回键值的遍历器。
- entries()：返回所有成员的遍历器。
- forEach()：遍历 Map 的所有成员。

Map 结构的默认遍历器接口 Symbol.iterator 属性指向 entries 方法。这意味着 Map 结构如果使用扩展运算符 ... 或者 for ... of 循环遍历将默认调用 entries 方法。

```js
let map = new Map().set(1, 'a').set(2, 'b').set(3, 'c');
map[Symbol.iterator] === map.entries;	// true
for (let [key, value] of map) {
  console.log(key, value);	// 1 'a', 2 'b', 3 'c'
}
// 等同于
for (let [key, value] of map.entries()) {
  console.log(key, value);	// 1 'a', 2 'b', 3 'c'
}
```

Map 结构转为数组结构比较快速的方法是结合扩展运算符。

```js
let map = new Map().set(1, 'a').set(2, 'b').set(3, 'c');
[...map.keys()];	// [1, 2, 3]
[...map.values()];	// ['a', 'b', 'c']
[...map.entries()];	// [[1, 'a'], [2, 'b'], [3, 'c']]
[...map];	// [[1, 'a'], [2, 'b'], [3, 'c']]
```

Map 本身并没有 map 和 filter 方法，所以要想实现 Map 的遍历和过滤，可以转为数组后结合数组的 map 和 filter 方法。

Map 还有一个 forEach 方法，与数组的 forEach 方法类似，也可以实现遍历。

### 与其他数据结构的互相转换

#### Map 转为数组

Map 转为数组最方便的方法就是使用扩展运算符。

#### 数组转为 Map

将数组传入 Map 构造函数就可以转为 Map，当然数组的成员也得是数组。

#### Map 转为对象

如果 Map 的所有键都是字符串，那么可以转为对象。

```js
function strMapToObj (strMap) {
  let obj = Object.create(null);
  for (let [k, v] of strMap) {
    obj[k] = v;
  }
  return obj;
}
const strMap = new Map().set('yes', true).set('no', false);
strMapToObj(strMap);	// { yes: true, no: false }
```

#### 对象转为 Map

```js
function objToStrMap (obj) {
  let strMap = new Map();
  for (let k of Object.keys(obj)) {
    strMap.set(k, obj[k]);
  }
  return strMap;
}
objToStrMap({ yes: true, no: false });	// Map {'yes' => true, 'no' => false }
```

#### Map 转为 JSON

Map 转为 JSON 要区分两种情况。一种情况是 Map 的键名都是字符串，这时可以选择转为对象 JSON。

```js
function strMapToJSON (strMap) {
  return JSON.stringify(strMapToObj(strMap));
}
let myMap = new Map().set('yes', true).set('no', false);
strMapToJSON(myMap);	// '{"yes": true, "no": false}'
```

另一种情况是，Map 的键名有非字符串，这时可以选择转为数组 JSON。

```js
function mapToArrayJSON (map) {
  return JSON.stringify([...map]);
}
let myMap = new Map().set(true, 7).set({foo: 3}, ['abc']);
mapToArrayJSON(myMap);	// '[[true: 7], [{"foo": 3}, ["abc"]]]'
```

#### JSON 转为 Map

JSON 转为 Map，正常情况下所有键名都是字符串。

```js
function JSONToStrMap (jsonStr) {
  return objToStrMap(JSON.parse(jsonStr));
}
JSONToStrMap('{"yes": true, "no": false}');	// Map { 'yes' => true, 'no' => false }
```

但当整个 JSON 就是一个数组，且每个数组成员本身又是一个具有两个成员的数组，这时可以一一对应的转为 Map。

```js
function JSONToMap (jsonStr) {
  return new Map(JSON.parse(jsonStr));
}
JSONToMap('[[true: 7], [{"foo": 3}, ["abc"]]]');	// Map { true => 7, Object {foo: 3} => ['abc'] }
```

## WeakMap

### 含义

WeakMap 结构与 Map 结构类似，也用于生成键值对的集合。区别有以下两点。第一，WeakMap 只接受对象作为键名（null 除外），不接受其他类型的值作为键名。第二，WeakMap 的键名所指向的对象（只有键，键值还是正常引用的）不计入垃圾回收机制。垃圾回收机制与 WeakSet 差不多，可以查看 [WeakSet](#WeakSet) 一节。基本上，如果要向对象中添加数据又不想干扰垃圾回收机制，就可以使用 WeakMap。

### WeakMap 的语法

WeakMap 与 Map 在 API 上的区别主要有：没有 keys()、values()、entries() 这些遍历操作方法和 clear() 方法，也没有 size 属性。

### WeakMap 示例

这节展示了 WeakMap 的例子，具体翻阅 p227。

### WeakMap 的用途

WeakMap 应用的典型场景就是以 DOM 节点作为键名的场景，一旦这个 DOM 节点删除，状态就会消失，不存在内存泄漏风险。

```js
let myElement = document.getElementById('logo');
let myWeakMap = new WeakMap();
myWeakMap.set(myElement, { timesClicked: 0 });
myElement.addEventListener('click', function () {
  let logoData = myWeakMap.get(myElement);
  logoData.timesClicked++;
}, false)
```

WeakMap 的另一个用处是部署私有属性，如何部署翻阅 p230。







# 第 12 章		Proxy

## 概述

`Proxy` 用于修改某些操作的默认行为，在语言层面做出修改，属于一种 “元编程”。Proxy 可以理解为在目标对象前架设一个拦截层，外界对该对象的访问都必须先通过这层拦截，所以提供了一种机制可以对外界的访问进行过滤和改写。ES6 原生提供 Proxy 构造函数，用于生成 Proxy 实例。

```js
var proxy = new Proxy(target, handler);
```

Proxy 对象的所有用法都是这样，不同的只是 handler 参数的写法。target 参数表示所要拦截的目标对象；handler 参数是一个配置对象，用于定制拦截行为，对每一个代理的操作都要提供一个对应的处理函数，该函数将拦截对应的操作，如果 handler 没有设置任何拦截，那么等同于直接通向目标对象。

```js
var proxy = new Proxy({}, {
  get: function (target, property) {
    return 35;
  }
})
proxy.time;	// 35
proxy.name;	// 35
```

上面这个例子中，构造函数 Proxy 接受两个参数，第一个参数是代理的 target 目标对象（上例中是空对象），Proxy 实例就是该对象的副本。通过目标对象直接访问内部的属性，那么会直接返回属性的值，但通过 Proxy 实例来访问目标对象中的属性，就会执行 handler 定义的拦截行为 get 函数，这就是 Proxy 代理操作。	[case](code\第12章-Proxy\12.1-概述.js)

下面是 Proxy 支持的所有拦截操作，所有操作都在 [Proxy 实例的方法](# Proxy 实例的方法) 详细介绍。

### get(target, propKey, receiver)

拦截对象属性的读取。最后一个参数 receiver 是可选对象，参见下面的 [Reflect.get](# Reflect.get(target, name, receiver)) 的部分。

### set(target, propKey, value, receiver)

拦截对象属性的设置。返回一个布尔值。

### has(target, propKey)

拦截 propKey in proxy 的操作，返回一个布尔值。

### deleteProperty(target, propKey)

拦截 delete proxy[propKey] 的操作，返回一个布尔值。

### ownKeys(target)

拦截 Object.getOwnPropertyNames(proxy)、Object.getOwnPropertySymbols(proxy)、Object.keys(proxy)，返回一个数组。该方法返回目标对象所有自身属性的属性名，而 Object.keys() 的返回结果仅包含目标对象自身的可遍历属性。

### getOwnPropertyDescriptor(target, propKey)

拦截 Object.getOwnPropertyDescriptor(proxy, propKey)，返回属性的描述对象。

### defineProperty(target, propKey, propDesc)

拦截 Object.defineProperty(proxy, propKey, propDesc)、Object.defineProperties(proxy, propDescs)，返回一个布尔值。

### preventExtensions(target)

拦截 Object.preventExtensions(proxy)，返回一个布尔值。

### getPrototypeOf(target)

拦截 Object.getPrototypeOf(proxy)，返回一个对象。

### isExtensible(target)

拦截 Object.isExtensible(proxy)，返回一个布尔值。

### setPrototypeOf(target, proto)

拦截 Object.setPrototypeOf(proxy, proto)，返回一个布尔值。如果目标对象是函数，那么还有两种额外操作可以拦截。

### apply(target, object, args)

拦截 Proxy 实例，并将其作为函数调用的操作。例如 proxy(...args)、proxy.call(Object, ...args) 和 proxy.apply(...)。

```js
var proxy = new Proxy(function (x, y) {
  return x + y;
}, {
  apply (target, object, args) {
    return args[0];
  }
})
proxy(1, 2);	// 1
```

### construct(target, args)

拦截 Proxy 实例作为构造函数调用的操作。例如 new proxy(...args)。

## Proxy 实例的方法

### get()

get() 方法前面已有例子，下面介绍 get 方法的继承。

```js
let proto = new Proxy({}, {
  get (target, propertyKey, receiver) {
    console.log('GET ' + propertyKey);
    return target[propertyKey];
  }
})
let obj = Object.create(proto);
obj.xxx;	// 'GET xxx'
```

使用 get 拦截实现数组读取负数索引例子 p238。

将读取属性的操作 get 转变为执行某个函数，从而实现属性的链式操作 p239。

利用 get 拦截实现一个生成各种 DOM 节点的通用函数例子 p239。

如果一个属性不可配置（[configurable] 为 false）或不可写（[writable] 为 false），则该属性不能被代理，通过 Proxy 对象访问该属性将会报错。

```js
const traget = Object.defineProperties({}, {
  foo: {
    value: 123,
    writable: false,
    configurable: false
  }
})
const handler {
  get (target, propKey) {
    return 'abc';
  }
}
const proxy = new Proxy(target, handler);
proxy.foo;	// TypeErro
```

### set()

set 用于拦截某个属性的赋值操作。如果目标对象自身的某个属性不可写也不可配置，那么 set 不得改变这个属性的值，只能返回同样的值，否则报错。下例利用 Proxy 对象保证 age 属性是一个不大于 200 的整数。

```js
let validator = {
  set (obj, prop, value) {
    if (prop === 'age') {
      if (!Number.isInteger(value)) {
        throw new TypeError('The age is not an integer');
      } else if (value > 200) {
        throw new RangeError('The age seems invalid');
      }
    }
    obj[prop] = value;
  }
}
let person = new Proxy({}, validator);
person.age = 100;
person.age = 300;	// 报错
person.age = 100.5;	// 报错
```

有时在对象上设置内部属性，属性名的第一个字符使用下划线开头，表示属性不应该被外部所使用，结合 get 和 set 防止内部属性被外部读和写的例子在 p242。

### apply()

apply 方法拦截函数的调用、call 和 apply 操作。该方法接收 3 个参数，分别是目标对象、目标对象的上下文 this 和目标对象的参数数组。静态方法 `Reflect.apply()` 通过指定的参数列表发起对目标(target)函数的调用。

```js
var twice = {
  apply (target, ctx, args) {
    return Reflect.apply(...arguments) * 2;
  }
}
function sum (left, right) {
  return left + right;
}
var proxy = new Proxy(sum, twice);
proxy(1, 2);	// 6
```

### has()

has 方法用来拦截 HasProperty 操作，即判断对象是否具有某个属性时，这个方法会生效。has 方法不判断一个属性是对象自身属性还是继承属性。典型的操作就是 in 运算符。而 for ... in 循环也用到了 in 运算符，但是 has 拦截对 for ... in 循环不生效。另外，如果原对象 target 某个属性不可配置或 target 不可扩展，那么这时 has 拦截会报错。

```js
// 使内部属性不被 in 运算符返回
var handler = {
  has (target, key) {
    if (key[0] === '_') {
      return false;
    }
    return key in target;
  }
}
var target = { _prop: 'foo', prop: 'foo' };
var proxy = new Proxy(target, handler);
'_prop' in proxy;	// false
```

```js
var obj = { a: 10 };
Object.preventExtensions(obj);	// 禁止 obj 扩展
var p = new Proxy(obj, {
  has (target, prop) {
    return false;
  }
})
'a' in p;	// TypeError
```

### construct()

construct 方法用于拦截 new 命令。方法接收两个参数：target 目标对象，args 构建函数的参数对象。方法返回的必须是一个对象，否则会报错。

```js
var p1 = new Proxy(function () {}, {
  construct: function (target, args) {
    console.log('called: ' + args.join(', '));
    return { value: args[0] * 10 };
  }
})
(new p1(1)).value;
// 'called: 1'
// 10

var p = new Proxy(function () {}, {
  construct (target, args) {
    return 1;
  }
})
new p();	// 报错
```

### deleteProperty()

deleteProperty 方法用于拦截 delete 操作，如果这个方法抛出错误或者返回 false，当前属性就无法被 delete 命令删除。目标对象自身的不可配置属性不能被 deleteProperty 方法删除，否则会报错。

### defineProperty()

defineProperty 方法拦截了 Object.defineProperty 操作。如果目标对象不可扩展，则 defineProperty 不能增加目标对象中不存在的属性，否则会报错。另外，如果目标对象的某个属性不可写或不可配置，则 defineProperty 方法不得改变这两个设置。

### getOwnPropertyDescriptor()

getOwnPropertyDescriptor 方法拦截 Object.getOwnPropertyDescriptor()，返回一个属性描述对象或者 undefined。

### getPrototypeOf()

getPrototypeOf 方法主要用来拦截获取对象原型，具体来说是拦截下列操作：

- Object.prototype.\_\_proto\_\_
- Object.prototype.isPrototypeOf()
- Object.getPrototypeOf()
- Reflect.getPrototypeOf()
- instanceof

getPrototypeOf 方法的返回值必须是对象或 null，否则会报错。另外如果目标对象不可扩展，那么必须返回目标对象的原型对象。

### isExtensible()

isExtensible 方法拦截 Object.isExtensible 操作。方法只能返回布尔值，否则返回值会被自动转为布尔值。方法有一个强限制，就是返回值必须与目标对象的 isExtensible 属性保持一致，否则就会抛出错误。`Object.isExtensible()` 方法判断一个对象是否是可扩展的（是否可以在它上面添加新的属性）。

### ownKeys()

ownKeys 方法用来拦截对象自身属性的读取操作，具体来说拦截以下操作：

- Object.getOwnPropertyNames()
- Object.getOwnPropertySymbols()
- Object.keys()

拦截 Object.keys() 方法时，有三类属性会被 ownKeys 方法自动过滤，不会返回。分别是：目标对象上不存在的属性，属性名为 Symbol 值，不可遍历的属性。

ownkeys 方法返回的数组成员只能是字符串或 Symbol 值，如果有其他类型的值，或者返回的根本不是数组，会报错。如果目标对象自身包含不可配置的属性，则该属性必须被 ownKeys 方法返回，否则会报错。如果目标对象是不可扩展的，这时 ownKeys 方法返回的数组之中必须包含原对象的所有属性，且不能包含多余的属性，否则会报错。

### preventExtensions()

preventExtensions 方法拦截 Object.preventExtensions()（该方法使对象不可扩展）。该方法必须返回一个布尔值，否则会被自动转为布尔值。方法有一个限制，只有目标对象不可扩展时，即 Object.isExtensible(proxy) 为 false 时，proxy.preventExtensions 才能返回 true，否则会报错。

### setPrototypeOf()

setPrototypeOf 方法主要用于拦截 Object.setPrototypeOf 方法。该方法只能返回布尔值，否则会被自动转为布尔值。另外，如果目标对象不可扩展，setPrototypeOf 方法不得改变目标对象的原型。

## Proxy.revocable()

Proxy.revocable 方法返回一个对象，其 proxy 属性是 Proxy 实例，revoke 属性是一个函数，可以取消 Proxy 实例。当执行 revoke 函数后再访问 Proxy 实例，就会抛出一个错误。Proxy.revocable 的一个使用场景是，目标对象不允许直接访问，必须通过代理访问，一旦访问结束，就收回代理权，不允许再次访问。

```js
let target = {};
let handler = {};
let { proxy, revoke } = Proxy.revocable(target, handler);
proxy.foo = 123;
proxy.foo;	// 123
revoke();
proxy.foo;	// TypeError: Revoked
```

## this 问题

Proxy 不是目标对象的透明代理，即不做任何拦截的情况下也无法保证与目标对象的行为一致。这是因为 Proxy 代理的情况下，目标对象内部的 this 关键字会指向 Proxy 代理。

```js
const target = {
  m () {
    console.log(this === proxy);
  }
}
const handler = {};
const proxy = new Proxy(target, handler);
target.m();	// false
proxy.m();	// true
```

下面这个例子展示了由于 this 指向的变化导致 Proxy 无法代理目标对象。

```js
const _name = new WeakMap();
class Person {
  constructor (name) {
    _name.set(this, name);
  }
  get name () {
    return _name.get(this);
  }
}
const jane = new Person('Jane');
jane.name;	// 'Jane'
const proxy = new Proxy(jane, {});
proxy.name;	// undefined
```

有些原生对象的内部属性只有通过正确的 this 才能获取，所以 Proxy 也无法代理这些原生对象的属性。

```js
const target = new Date();
const handler = {};
const proxy = new Proxy(target, handler);
proxy.getDate();	// TypeError: this is not a Date object
```

可以通过 this 绑定原始对象解决这个问题。

```js
const target = new Date('2020-05-01');
cosnt handler = {
  get (target, prop) {
    if (prop === 'getDate') {
      return target.getDate.bind(target);
    }
    return Reflect.get(target, prop);
  }
}
const proxy = new Proxy(target, handler);
proxy.getDate();	// 1
```

## 实例：Web 服务的客户端

Proxy 对象可以拦截目标对象的任意属性，这使得它很适合用来编写 Web 服务的客户端。

```js
const service = createWebService('http://example.com/data');
service.employees().then(json => {
  const employees = JSON.parse(json);
  // ...
})
```

上面的代码新建一个 Web 服务的接口，这个接口返回各种数据。Proxy 可以拦截这个对象的任意属性，所以不用为每一种数据写一个适配方法，只要写一个 Proxy 拦截即可。

```js
function createWebService (baseUrl) {
  return new Proxy({}, {
    get (target, propKey, receiver) {
      return () => httpGet(baseUrl + '/' + propKey);
    }
  })
}
```

同理，Proxy 也可以用来实现数据库的 ORM 层。







# 第 13 章		Reflect

## 概述

Reflect 对象与 Proxy 对象一样，也是 ES6 为了操作对象而提供的新的 API。Reflect 对象设计目的有以下几个。

1. 将 Object 对象的一些明显属于语言内部的方法（如 Object.defineProperty）放到 Reflect 对象上。现阶段，某些方法同时在 Object 和 Reflect 对象上部署，未来的新方法将只在 Reflect 对象上部署。也就是从 Reflect 对象上可以获得语言内部的方法。

2. 修改某些 Object 方法的返回结果，让其变得更合理。如 Object.defineProperty(obj, name, desc) 在无法定义属性时会抛出一个错误，而 Reflect.defineProperty(obj, name, desc) 则会返回 false。

   ```js
   // 旧写法
   try {
     Object.defineProperty(target, property, attributes);
     // success
   } catch (e) {
     // failure
   }
   
   // 新写法
   if (Reflect.defineProperty(target, property, attributes)) {
     // success
   } else {
     // failure
   }
   ```

3. 让 Object 操作都变成函数行为。某些 Object 操作是命令式，比如 name in obj 和 delete obj[name]，而 Reflect.has(obj, name) 和 Reflect.deleteProperty(obj, name) 让它们变成了函数行为。

   ```js
   // 旧写法
   'assign' in Object;	// true
   // 新写法
   Reflect.has(Object, 'assign');	// true
   ```

4. Reflect 对象的方法与 Proxy 对象的方法一一对应，只要是 Proxy 对象的方法，就能在 Reflect 对象上找到对应的方法。这使 Proxy 对象可以方便的调用对应的 Reflect 方法来完成默认行为，作为修改行为的基础。

   ```js
   // Proxy 方法拦截 target 对象的属性赋值行为，采用 Reflect.set 方法确保完成原有行为，然后部署新功能
   Proxy(target, {
     set (target, name, value, receiver) {
       var success = Reflect.set(target, name, value, receiver);
       if (success) {
         log('property ' + name + ' on ' + target + ' set to ' + value)''
       }
       return value;
     }
   })
   ```

有了 Reflect 对象以后，很多操作会更易读。

```js
// 旧写法
Function.prototype.apply.call(Math.floor, undefined, [1.75]);	//1
// 新写法
Reflect.apply(Math.floor, undefined, [1.75]);	// 1
```

## 静态方法

Reflect 对象一共有 13 个静态方法，这些方法的作用大部分与 Object 对象的同名方法的作用是相同的，而且与 Proxy 对象的方法是一一对应的，具体解释如下。

### Reflect.get(target, name, receiver)

Reflect.get 方法查找并返回 target 对象的 name 属性，如果没有该属性，则返回 undefined。如果 target 不是对象，会报错。如果 name 属性部署了读取函数 getter，则读取函数的 this 绑定 receiver 对象。

```js
var myObject = {
  foo: 1,
  bar: 2,
  get baz () {
    return this.foo + this.bar;
  }
}
var myReceiverObject = {
  foo: 4,
  bar: 4
}
Reflect.get(myObject, 'baz');	// 3
Reflect.get(myObject, 'baz', myReceiverObject);	// 8
Reflect.get(1, 'foo');	// 报错
```

### Reflect.set(target, name, value, receiver)

Reflect.set 方法设置 target 对象的 name 属性等于 value。如果 name 属性设置了赋值函数，则赋值函数的 this 绑定 receiver 对象。如果 target 参数不是对象，Reflect.set 会报错。

```js
var myObject = {
  foo: 4,
  set bar (value) {
    return this.foo = value;
  }
}
var myReceiverObject = {
  foo: 0
}
Reflect.set(myObject, foo, 5);
myObject.foo;	// 5
Reflect.set(myObject, bar, 10, myReceiverObject);
myObject.foo;	// 5
myReceiverObject.foo;	// 10
Reflect.set(1, 'foo', {});	// 报错
```

注意，Reflect.set 会触发 Proxy.defineProperty 拦截。

```js
let p = {
  a: 'a'
}
let handler = {
  set (target, key, value, receiver) {
    console.log('set');
    Reflect.set(target, key, value, receiver);	// 触发 defineProperty 拦截
  },
  defineProperty (target, key, attribute) {
    console.log('defineProperty');
    Reflect.defineProperty(target, key, attribute);
  }
}
let obj = new Proxy(p, handler);
obj.a = 'A';
// set
// defineProperty
```

### Reflect.has(obj, name)

Reflect.has 方法对应 name in obj 中的 in 运算符。如果 obj 参数不是对象，会报错。

### Reflect.deleteProperty(obj, name)

Reflect.deleteProperty 方法等同于 delete obj[name]，用于删除对象的属性。该方法返回一个布尔值。如果删除成功或者被删除的属性不存在，就返回 true；如果删除失败或者被删除的属性依然存在，则返回 false。

### Reflect.construct(target, args)

Reflect.construct 方法等同于 new target(...args)，提供一种不使用 new 来调用构造函数的方法。

### Reflect.getPrototypeOf(obj)

Reflect.getPrototypeOf 方法用于读取对象的 \_\_proto\_\_ 属性，对应 Object.getPrototypeOf(obj)。Reflect.getPrototypeOf 和 Object.getPrototypeOf 的一个区别是，如果参数不是对象，Object.getPrototypeOf 会先将这个参数转为对象，然后再运行，而 Reflect.getPrototypeOf 会报错。

### Reflect.setPrototypeOf(obj, newProto)

Reflect.setPrototypeOf 方法用于设置对象的 \_\_proto\_\_ 属性，返回第一个参数对象，对应 Object.setPrototypeOf(obj, newProto)。如果第一个参数不是对象，Object.setPrototypeOf 会返回第一个参数本身，Reflect.setPrototypeOf 会报错。如果第一个参数是 undefined 或 null，Object.setPrototypeOf 和 Reflect.setPrototypeOf 都会报错。

### Reflect.apply(func, thisArg, args)

Reflect.apply 方法等同于 Function.prototype.apply.call(func, thisArg, args)，用于绑定 this 对象后执行给定函数。

### Reflect.defineProperty(target, propertyKey, attributes)

Reflect.defineProperty 方法基本等同于 Object.defineProperty，用来为对象定义属性。今后后者会被逐渐废除，因此从现在开始使用 Reflect.defineProperty。方法的 target 参数如果不是对象，会报错。

### Reflect.getOwnPropertyDescriptor(target, propertyKey)

Reflect.getOwnPropertyDescriptor 基本等同于 Object.getOwnPropertyDescriptor，用于获取指定属性的描述对象，将来会替代后者。两者的区别在于如果第一个参数不是对象，Object.getOwnPropertyDescriptor(1, 'foo') 不会报错，而是返回 undefined。但 Reflect.getOwnPropertyDescriptor(1, 'foo') 会抛出错误，表示参数非法。

### Reflect.isExtensible(target)

Reflect.isExtensible 方法对应 Object.isExtensible，返回一个布尔值，表示当前对象是否可扩展。如果参数不是对象，Object.isExtensible 会返回 false，因为非对象本来就是不可扩展的，而 Reflect.isExtensible 会报错。

### Reflect.preventExtensions(target)

Reflect.preventExtensions 对应 Object.preventExtensions 方法，用于使一个对象变为不可扩展的。它返回一个布尔值，表示是否操作成功。如果 target 参数不是对象，Object.preventExtensions 在 ES5 环境下将报错，在 ES6 环境下将返回传入的参数，而 Reflect.preventExtensions 会报错。

### Reflect.ownKeys(target)

Reflect.ownKeys 方法用于返回对象的所有属性，基本等同于 Object.getPropertyNames 与 Object.getOwnPropertySymbols 之和。

## 实例：使用 Proxy 实现观察者模式

观察者模式指的是函数自动观察数据对象的模式，一旦对象有变化，函数就会自动执行。

```js
const queuedObservers = new Set();
const observe = fn => queuedObservers.add(fn);
const observable = obj => new Proxy(obj, { set });
function set (target, key, value, receiver) {
  const result = Reflect.set(target, key, value, receiver);
  queuedObservers.forEach(observer => observer());
  return result;
}

const person = observable({
  name: 'zs',
  age: 21
})
function print () {
  console.log(`${ person.name }, ${ person.age }`);
}
observe(print);
person.name = 'ls';	// ls, 21
```

上面代码中，person 数据对象是观察目标，print 函数是观察者。一旦数据对象发生变化，print 就会自动执行。







# 第 14 章		Promise 对象

## Promise 的含义

Promise 是异步编程的一种解决方案，比传统的解决方案 —— 回调函数和事件更加合理且更加强大。最早由社区提出并实现，ES6 将其写入语言标准，统一用法，并原生提供了 Promise 对象。

Promise 简单来说就是一个容器，里面保存着某个未来才会结束的事件（通常是一个异步操作）的结果状态。从语法上来说，Promise 是一个对象，从它可以获取异步操作的消息。Promise 提供统一的 API，各种异步操作都可以用同样的方法处理。

Promise 对象有以下两个特点：

1. 对象的状态不受外界影响。Promise 对象代表一个异步操作，有 3 种状态：Pending 进行中，Fulfilled 已成功，Rejected 已失败。只有异步操作的结果可以决定当前是哪一种状态，任何其他操作都无法改变这个状态。
2. 一旦状态改变后就不会再变，任何时候都可以得到状态结果。Promise 对象的状态改变只有两种可能：从 Pending 变为 Fulfilled 或从 Pending 变为 Rejected。只要这两种情况中的一种发生，状态改变后就凝固了，不会再变，而是一直保持这个结果，这时就称为 Resolved 已定型。

为了讨论方便，后面的已定型 Resolved 状态都是指 Fulfilled 状态，而不包含 Rejected 状态。

Promise 对象可以将异步操作以同步操作的流程表达出来，避免了层层嵌套的回调函数。Promise 对象提供统一的接口，使得控制异步操作更加容易。

Promise 也有缺点。首先，无法取消 Promise，一旦新建它就会立即执行，无法中途取消。其次，如果不设置回调函数，Promise 内部抛出的错误不会反应到外部。再者，当处于 Pending 状态时，无法得知目前的进展，是刚开始还是即将完成。

## 基本用法

ES6 规定，Promise 对象是一个构造函数，用来生成 Promise 实例。Promise 构造函数接受一个函数作为参数，该函数又接收两个参数，分别是 resolve 函数参数和 reject 函数参数。resolve 函数的作用是将 Promise 对象的状态从未完成变为成功，即从 Pending 变为 Resolved。reject 函数的作用是将 Promise 对象的状态从未完成变为失败，即从 Pending 变为 Rejected。

Promise 实例生成后，可以用 then 方法分别指定 Resolved 状态和 Rejected 状态的回调函数。then 方法可以接收两个回调函数作为参数，第一个回调函数是 Promise 对象的状态变为 Resolved 时调用的，即 resolve 函数。第二个回调函数是 Promise 对象的状态变为 Rejected 时调用的，即 reject 函数。其中，第二个回调函数 reject 是可选的。这两个回调函数都接受从 Promise 对象传出的值作为参数。

Promise 构造函数接收的函数参数，如果执行 resolve 函数，表示异步操作成功，传入的参数将被作为参数传递出去，被 then 方法的第一个函数参数接收。如果执行 reject 函数，表示异步操作失败，传入的参数将被作为参数传递出去，被 then 方法的第二个函数参数接收。

```js
function timeout (ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve('done'), ms);
  })
}
timeout(100).then(value => console.log(value));	// 'done'
```

Promise 新建后会立即执行。then 方法指定的回调函数将在当前脚本所有同步任务执行完成后才会执行。

```js
let promise = new Promise((resolve, reject) => {
  console.log('Promise');
  resolve();
})
promise.then(() => {
  console.log('Resolved');
})
console.log('Hi!');
// Promise
// Hi!
// Resolved
```

reject 函数参数通常是 Error 对象实例，表示抛出的错误。而 resolve 函数的参数除了正常的值外，还可能是另一个 Promise 实例。

```js
var p1 = new Promise((resolve, reject) => { ... })
var p2 = new Promise((resolve, reject) => {
  resolve(p1);
})
```

p1 和 p2 都是 Promise 的实例，但是 p2 的 resolve 方法将 p1 作为参数，即一个异步操作的返回结果是另一个异步操作。此时 p1 的状态会传递给 p2，也就是说，p1 的状态决定了 p2 的状态。如果 p1 的状态还是 Pending，还没有成功或失败，而此时就算 p2 调用了 resolve 或 reject，方法也不会执行，p2 状态不会改变，而是等待 p1 的状态改变，只有 p1 的状态变为已完成或失败时， p2 的状态才会真正改变，也就是 p2.then 的回调函数才会执行。如果 p1 的状态已经改变，而 p2 的状态还没有改变，那么 p2 状态改变时，p2.then 回调函数就会立即执行。总而言之，只有 p1 的状态变为已完成或失败时，p2 的状态才能真正改变，p2.then 回调函数也才能执行。且 p2.then 方法的回调函数的参数是从 p1 传出来的。	[case](code\第14章-Promise对象\14.2-基本用法.js)

```js
var p1 = new Promise((resolve, reject) => {
  setTimeout(() => reject(new Error('fail')), 3000);
})
var p2 = new Promise((resolve, reject) => {
  setTimeout(() => resolve(p1), 1000);
})
p2.then(result => console.log(result)).catch(error => console.log(error));	// 3s 后报错：Error: fail
```

注意，调用 resolve 或 reject 并不会终结 Promise 的参数函数的执行。resolve 和 reject 方法会等待 Promise 函数参数中的同步操作执行完毕后才真正的执行。一般来说，调用 resolve 和 reject 后，Promise 的使命就应该完成了，后续操作应该放在 then 方法里面，而不应该写在 resolve 或 reject 的前后面。所以最后在 resolve 和 reject 前面加上 return 语句，保证不会产生意外。

```js
new Promise((resolve, reject) => {
  resolve(1);
  console.log(2);
}).then(value => console.log(value));
// 2
// 1

new Promise((resolve, reject) => {
  return resolve(1);
  console.log(2);
}).then(value => console.log(value));	// 1
```

## Promise.prototype.then()

Promise 实例具有 then 方法，即 then 方法是定义在原型对象 Promise.prototype 上的。then 方法的作用是为 Promise 实例添加状态改变时的回调函数。第一个参数是 Resolved 状态的回调函数，第二个参数可选，是 Rejected 状态的回调函数。then 方法返回的是一个新的 Promise 实例，注意不是原来那个调用 then 方法的 Promise 实例。所以可以采用链式的写法。

```js
new Promise((resolve, reject) => { 
  resolve(2);
})
	.then(valueA => { return valueA })
	.then(valueB => console.log(valueB))	// 2
```

上面的代码中，then 方法依次指定了两个回调函数，第一个回调函数完成以后，会将返回结果 valueA 作为参数传入第二个回调函数。而如果前面的 then 方法返回的是一个 Promise 实例对象，那么后面的 then 方法指定的回调函数就会等待这个新的 Promise 对象状态发生变化。如果变为 Resolved，则调用 funcA；如果状态变为 Rejected，则调用 funcB。	[case](code\第14章-Promise对象\14.3-Promise.prototype.then().js)

```js
new Promise((resolve, reject) => {
  resolve(2);
})
	.then(value => {
  	console.log(value);
  	return new Promise();
	})
	.then(function funcA (success) {
  	// resolve
	}, function funcB (err) {
  	// reject
	})
```

## Promise.prototype.catch()

Promise.prototype.catch 方法是 .then(null, rejection) 的别名，用于指定发生错误时的回调函数。	[case](code\第14章-Promise对象\14.4-Promise.prototype.catch().js)

```js
new Promise((resolve, reject) => {
  reject(new Error('test'));
}).then(() => {
 	console.log('success')
}).catch(err => {
 	console.log(err);	// Error: test
})
```

如果 Promise 对象的状态变为 Resolved，则会调用 then 方法指定的回调函数；如果异步操作抛出错误，也就是状态变为 Rejected，就会调用 catch 方法指定的回调函数处理这个错误。另外，then 方法指定的回调函数如果在运行中抛出错误，也会被 catch 方法捕获。	[case](code\第14章-Promise对象\14.4-Promise.prototype.catch().js)

如果 Promise 状态已经变成了 Resolved，再抛出错误是无效的，因为 Promise 的状态一旦改变，就会永久保持该状态，不再改变。

```js
new Promise((resolve, reject) => {
  resolve('ok');
  throw new Error('test');
}).then(value => {
  console.log(value);	// 'ok'
}).catch(err => {
 	console.log(err);
})
```

一般来说，不要在 then 方法中定义 Rejected 状态的回调函数，即 then 方法的第二个参数，而是应该使用 catch 方法。catch 方法返回的还是一个 Promise 对象，所以可以接着调用 then 方法，而 catch 方法会捕获前面的 Promise 对象和 then 方法的错误，所以定义在 catch 方法后面的 then 方法如果抛出错误是不会被接收到的。

Node 中有一个 `unhandledRejection` 事件，专门用来监听未捕获的 reject 错误。

## Promise.all()

Promise.all 方法用于将多个 Promise 实例包装成一个新的 Promise 实例。

```js
var p = Promise.all([p1, p2, p3]);
```

方法接收一个数组作为参数，数组成员都是 Promise 实例，如果不是就会调用下面介绍的 Promise.resolve 方法，将参数转为 Promise 实例，再进一步处理。其实方法接受的参数不一定是数组，只要具有 Iterator 接口且返回的每个成员都是 Promise 实例即可。方法的返回值 p 的状态由 p1，p2，p3 决定，分两种情况。

1. 只有 p1，p2，p3 的状态都变为 Fulfilled，p 的状态才会变成 Fulfilled，此时 p1，p2，p3 的返回值组成一个数组，传递给 p 的回调函数。
2. 只要 p1，p2，p3 中有一个被 Rejected，p 的状态就会被成 Rejected，此时第一个被 Rejected 的实例的返回值会传递给 p 的回调函数。如果作为参数的 Promise 实例自身定义了 catch 方法，那么它 Rejected 时并不会触发 Promise.all() 的 catch 方法。

```js
const p1 = new Promise((resolve, reject) => {
  resolve('hello');
}).then(result => result)
	.catch(e => e);
const p2 = new Promise((resolve, reject) => {
  throw new Error('报错了');
}).then(result => result)
	.catch(e => e);
Promise.all([p1, p2])
.then(result => console.log(result))	// ['hello', Error: 报错了]
.catch(e => console.log(e));
```

上面的代码中，p1 是 Fulfilled，而 p2 是 Rejected，但最后 Promise.all 却是 Fulfilled。这是因为 p2 有自己的 catch 方法，该方法返回的是一个新的 Promise 实例，p2 实际上指向的是这个实例。该实例执行完 catch 方法后也变成 Fulfilled，导致 Promise.all 方法参数里面的两个实例都是 Fulfilled 的，所以会调用 then 方法输出 p1 和 p2 的返回结果。如果 p2 没有自己的 catch 方法，那么就会调用 Promise.all 的 catch 方法。

```js
const p1 = new Promise((resolve, reject) => {
  resolve('hello');
})
.then(result => result);
const p2 = new Promise((resolve, reject) => {
  throw new Error('报错了');
})
.then(result => result);
Promise.all([p1, p2])
.then(result => console.log(result))
.catch(e => console.log(e));	// Error: 报错了
```

## Promise.race()

Promise.race 方法同样是将多个 Promise 实例包装成一个新的 Promise 实例。

```js
var p = new Promise([p1, p2, p3]);
```

只要 p1，p2，p3 中有一个实例率先改变状态，p 的状态就会跟着改变。那个率先改变的 Promise 实例的返回值就传递给 p 的回调函数。Promise.race 方法的参数与 Promise.all 方法一样，如果不是 Promise 会调用下面介绍的 Promise.resolve 方法，将参数转为 Promise 实例。下面这个例子，如果 5s 内没有获得结果，就将 Promise 的状态变为 Rejected，否则变为 Fulfilled。

```js
Promise.race([
  fetch('/xxx'),	// 以 Promise 方式发起 ajax 请求
  new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error('request timeout')), 5000)
  })
])
.then(response => console.log(response))
.catch(error => console.log(error));
```

## Promise.resolve()

Promise.resolve 方法可以将现有对象转为 Promise 对象。

```js
Promise.resolve('foo');
// 等价于
new Promise(resolve => resolve('foo'));
```

Promise.resolve 方法的参数分成以下 4 种情况。

### 参数是一个 Promise 实例

如果参数是一个 Promise 实例，那么 Promise.resolve 将不做任何修改，直接返回这个实例。

### 参数是一个 thenable 对象

thenable 对象指的是具有 then 方法的对象。Promise.resolve 方法会将这个对象转为 Promise 对象，然后立即执行 thenable 对象的 then 方法。	[case](code\第14章-Promise对象\14.7.2-参数是一个thenable对象.js)

```js
let thenable = {
  then: (resolve, reject) => {
    console.log(1);
    resolve(2);
  }
}

let p1 = Promise.resolve(thenable);		// 1
p1.then(value => console.log(value));	// 2
```

### 参数不是具有 then 方法的对象或根本不是对象

如果参数是一个原始值，或者是一个不具有 then 方法的对象，那么 Promise.resolve 方法返回一个新的 Promise 对象，状态为 Fulfilled，Promise.resolve 方法的参数会传递给返回的 Promise 对象的回调函数。

```js
var p = Promise.resolve('hello');
p.then(value => console.log(value));	// hello
```

### 不带有任何参数

Promise.resolve 方法允许在调用时不传参数，而直接返回一个 Fulfilled 状态的 Promise 对象。需要注意的是，立即 resolve 的 Promise 对象是在本轮事件循环结束时，而不是在下一轮事件循环开始时。

```js
setTimeout(() => {
  console.log('three');
}, 0);
Promise.resolve().then(() => {
  console.log('two');
})
console.log('one');
// one
// two
// three
```

setTimeout(fn, 0) 是在下一轮事件循环开始时执行，Promise.resolve 在本轮事件循环结束时执行，console.log() 则是立即执行。

## Promise.reject()

Promise.reject 方法也返回一个新的 Promise 实例，状态为 Rejected。

```js
var p = Promise.reject('出错了');
// 等同于
var p = new Promise((resolve, reject) => reject('出错了'));

p.then(null, e => console.log(e));	// 出错了
```

Promise.reject 的参数会原封不动的传给回调函数。

```js
const thenable = {
  then (resolve, reject) {
    reject('出错了');
  }
}
Promise.reject(thenable)
.catch(e => console.log(e === thenable));	// true
```

## 两个有用的附加方法

ES6 的 Promise API 提供的方法不多，可以自己部署一些有用的方法。

### done()

无论 Promise 对象的回调链是以 then 方法还是 catch 方法结尾，只要最后一个方法抛出错误，就有可能无法捕捉到，因为 Promise 内部的错误不会冒泡到全局。为此，可以提供一个 done 方法，处于回调链的尾端，保证捕捉任何可能抛出的错误。

```js
Promise.prototype.done = (onFulfilled, onRejected) => {
  this.then(onFulfilled, onRejected)
  		.catch(reason => setTimeout(() => { throw reason; }, 0))
}
new Promise().then().catch().then().done();
```

### finally()

finally 方法用于指定不管 Promise 对象最后的状态如何都会执行的操作。它与 done 方法最大区别在于，它接受一个普通的回调函数作为参数，该函数不管怎样都必须执行。

```js
Promise.prototype.finally = function (callback) {
  let P = this.constructor;
  return this.then(
  	value => P.resolve(callback()).then(() => value),
  	reason => P.resolve(callback()).then(() => { throw reason })
  );
}
server.listen(0)
.then(() => {
  // run test
})
.finally(server.top);
```

## 应用

### 加载图片

```js
const preloadImage = function (path) {
  return new Promise((resolve, reject) => {
    var image = new Image();
    image.onload = resolve;
    image.onerror = reject;
    image.src = path;
  })
}
```

### Generator 函数与 Promise 的结合

使用 Generator 函数管理流程，遇到异步操作时通常返回一个 Promise 对象。

```js
function getFoo () {
  return new Promise((resolve, reject) => {
    resolve('foo');
  })
}
var g = function* () {
  try {
    var foo = yield getFoo();
    console.log(foo);
  } catch (e) {
    console.log(e);
  }
}
function run (generator) {
  var it = generator();
  function go (result) {
    if (result.done) return result.value;
    return result.value.then(value => {
      return go(it.next(value));
    }, error => {
      return go(it.throw(error));
    })
  }
  go(it.next());
}
run(g);
```

## Promise.try()

在不知道或不想区分函数 f 是同步函数还是异步操作的情况下，可以用 Promise 来处理它。这样做的好处是可以不管 f 是否包含异步操作，都用 then 方法指定下一步流程，用 catch 方法处理 f 抛出的错误。一般写法如下：

```js
Promise.resolve().then(f)
```

上面的写法有一个缺点，就是如果 f 是同步函数，那么会在本轮事件循环的末尾执行。

```js
const f = () => console.log('now');
Promise.resolve().then(f);
console.log('next');
// next
// now
```

函数 f 是同步的，但是使用 Promise 包装后就变成异步执行了。那么有没有办法可以让同步函数同步执行，异步函数异步执行，并且让它们具有统一的 API 呢？回答是有的，且有两种写法。第一种是使用 `async` 函数。

```js
const f = () => console.log('now');
(async () => f())();
console.log('next');
// now
// next
```

上面的代码中，如果 f 是异步的，可以用 then 指定下一步。需要注意，`async () => f()` 会吃掉 f() 抛出的错误，所以需要使用 catch 方法捕获。

```js
(async () => f())()
.then(...)
.catch(...)
```

第二种写法是使用 new Promise()。

```js
const f = () => console.log('now');
(
	() => new Promise(
  	resolve => resolve(f())
  )
)();
console.log('next');
// now
// next
```

因为这种需求很常见，所以有一个提案提供了 `Promise.try` 方法替代上面的写法。

```js
const f = () => console.log('now');
Promise.try(f);
console.log('next');
// now
// next
```

由于 Promise.try 为所有操作提供了统一的处理机制，所以如果想用 then 方法管理流程，最好都用 Promise.try 包装一下。这样可以更好地管理异常。事实上，Promise.try 是模拟了 try 代码块，Promise.prototype.catch 模拟了 catch 代码块。







# 第 15 章		Iterator 和 for ... of 循环

## Iterator（遍历器）的概念

JavaScript 原有的表示集合的数据结构主要是数组和对象，ES6 又新增了 Map 和 Set。这样就有 4 种数据集合，用户还可以组合使用它们，定义自己的数据结构。如此，就需要一种统一的接口机制来处理所有不同的数据结构。遍历器 Iterator 就是这样一种机制，它是一种接口，为各种不同的数据结构提供统一的访问机制。任何数据结构，只要部署 Iterator 接口，就可以完成遍历操作，即依次处理该数据结构的所有成员。

Iterator 的作用有 3 个：一是为各种数据结构提供一个统一的、简便的访问接口；二是使得数据结构的成员能够按某种次序排列；三是供 ES6 新增的遍历命令——for ... of 循环消费。

Iterator 的遍历过程如下：

1. 创建一个指针对象，指向当前数据结构的起始位置。也就是说，遍历器本质上就是一个指针对象。
2. 指针对象的 next 方法用于移动指针。第一次调用指针对象的 next 方法，可以将指针指向数据结构的第一个成员。
3. 第二次调用指针对象的 next 方法，指针就指向数据结构的第二个成员。
4. 不断调用指针对象的 next 方法，直到它指向数据结构的结束位置。

每次调用 next 方法都会返回数据结构的当前成员的信息。具体来说，就是返回一个包含 value 和 done 两个属性的对象。其中 value 属性表示当前成员的值，done 属性是一个布尔值，表示遍历是否结束。下面模拟 next 方法返回值。

```js
var it = makeIterator(['a', 'b']);
it.next()	// { value: 'a', done: false }
it.next()	// { value: 'b', done: false }
it.next()	// { value: undefined, done: true }
function makeIterator (array) {
  var nextIndex = 0;
  return {
    next: function () {
      return nextIndex < array.length ?
        { value: array[nextIndex++], done: false } :
      	{ value: undefined, done: true };
    }
  }
}
```

## 默认 Iterator 接口

Iterator 接口的目的是为所有数据结构提供一种统一的访问机制，即 for ... of 循环。当使用 for ... of 循环遍历某种数据结构时，该循环会自动去寻找 Iterator 接口。数据结构只要部署了 Iterator 接口，就称这种数据结构为可遍历的。ES6 规定，默认的 Iterator 接口部署在数据结构的 `Symbol.iterator` 属性，亦或说一个数据结构只要具有 Symbol.iterator 属性，就可以认为是可遍历的。Symbol.iterator 属性是一个方法，执行该方法，会返回一个遍历器对象，遍历器对象的根本特征就是具有 next 方法。

ES6 有些数据结构原生具备 Iterator 接口，既不用任何处理就可以被 for ... of 循环遍历，因为这些数据结构原生部署了 Symbol.iterator 属性。所有部署了 Symbol.iterator 属性的数据结构都称为部署了遍历器接口，调用这个接口就会返回一个遍历器对象。原生具备 Iterator 接口的数据结构如下：

- Array
- Map
- Set
- String
- TypedArray
- 函数的 arguments 对象
- NodeList 对象

```js
// 数组的 Symbol.iterator 属性例子
let arr = ['a', 'b', 'c'];
let iter = arr[Symbol.iterator]();
iter.next();	// { value: 'a', done: false }
iter.next();	// { value: 'b', done: false }
iter.next();	// { value: 'c', done: false }
iter.next();	// { value: undefined, done: true}
```

对于原生部署 Iterator 接口的数据结构，不用自己编写遍历器生成函数，for ... of 循环会自动遍历它们。除此之外，其他数据结构（主要是对象）的 Iterator 接口都需要自己在 Symbol.iterator 属性上面部署，这样才会被 for ... of 循环遍历。

```js
let obj = {
  data: ['hello', 'world'],
  [Symbol.iterator]: function () {
    const self = this;
    let index = 0;
    return {
      next: function () {
       	if (index < self.data.length) {
          return {
            value: self.data[index++],
            done: false
          }
        } else {
        	return {
          	value: undefined,
          	done: true
        	}
        }
      }
    }
  }
}
```

对于类似数组的对象（存在数值键名和 length 属性），部署 Iterator 接口有一个简便方法，即使用 Symbol.iterator 方法直接引用数组的 Iterator 接口。

```js
let iterable = {
  0: 'a',
  1: 'b',
  2: 'c',
  length: 3,
  [Symbol.iterator]: Array.prototype[Symbol.iterator]
}
for (let item of iterable) {
  console.log(item);	// 'a', 'b', 'c'
}
```

注意，普通对象部署数组的 Symbol.iterator 方法并无效果，因为不是数值键名。

```js
let iterable = {
  a: 'a',
  b: 'b',
  c: 'c',
  length: 3,
  [Symbol.iterator]: Array.prototype[Symbol.iterator]
}
for (let item of iterable) {
  console.log(item);	// undefiend, undefined, undefined
}
```

如果 Symbol.iterator 方法对应的不是遍历器生成函数（即会返回一个遍历器对象），解释引擎会报错。

```js
var obj = {};
obj[Symbol.iterator] = () => 1;
[...obj];	// TypeError
```

## 调用 Iterator 接口的场合

有一些场合会默认调用 Iterator 接口，即 Symbol.iterator 方法。

### 解构赋值

对数组和 Set 结构进行解构赋值时，会默认调用 Symbol.iterator 方法。

```js
let set = new Set().add('a').add('b').add('c');
let [x, y] = set;
// x='a'	y='b'
let [first, ...rest] = set;
// first='a'	rest=['b','c']
```

### 扩展运算符

扩展运算符 `...` 会调用默认的 Iterator接口。只要某个数据结构部署了 Iterator 接口，就可以对它使用扩展运算符，将其转为数组。

### yield*

yield* 后面跟的是一个可遍历的结构，它会调用该结构的遍历器接口。

```js
let generator = function* () {
  yield 1;
  yield* [2, 3, 4];
  yield 5;
}
var iterator = generator();
iterator.next()	// { value: 1, done: false }
iterator.next()	// { value: 2, done: false }
iterator.next()	// { value: 3, done: false }
iterator.next()	// { value: 4, done: false }
iterator.next()	// { value: 5, done: false }
iterator.next()	// { value: undefined, done: true }
```

### 其他场合

由于数组的遍历会调用遍历器接口，所以任何接受数组作为参数的场合其实都调用了遍历器接口。

## 字符串的 Iterator 接口

字符串是一个类似数组的对象，也具有原生 Iterator 接口。可以覆盖原生的 Symbol.iterator 方法达到修改遍历器行为的目的。

## Iterator 接口与 Generator 函数

Symbol.iterator 方法的最简单实现是使用 Generator 函数。这样 Symbol.iterator 方法几乎不用部署任何代码，只要用 yield 命令给出每一步的返回值即可。

```js
var myIterable = {};
myIterable[Symbol.iterator] = function* () {
  yield 1;
  yield 2;
  yield 3;
}
[...myIterable]	// [1, 2, 3]

let obj = {
  * [Symbol.iterator] () {
    yield 'hello';
    yield 'world';
  }
}
for (let x of obj) {
  console.log(x);
}
// hello
// world
```

## 遍历器对象的 return()、throw()

遍历器对象除了具有 next 方法，还可以具有 return 方法和 throw 方法。如果自己写遍历器对象生成函数，那么 next 方法是必须部署的，return 方法和 throw 方法则是可选部署的。

return 方法的使用场合是，如果 for ... of 循环提前退出，通常是因为出错，或使用 break 语句和 continue 语句，就会调用 return 方法。如果一个对象在完成遍历前需要清理或释放资源，就可以部署 return 方法。注意，return 方法必须返回一个对象。

```js
function readLinesSync (file) {
  return {
    next () {
      return { done: true }
    },
    return () {
      file.close();
      return { done: true }
    }
  }
}
for (let line of readLinesSync(fileName)) {
  console.log(line);
  break;
}
```

throw 方法主要配合 Generator 函数使用，一般的遍历器对象用不到。

## for ... of 循环

ES6 引入了 for ... of 循环作为遍历所有数据结构的统一的方法。一个数据结构只要部署了 Symbol.iterator 属性，就视为具有 iterator 接口，就可以使用 for ... of 循环遍历它的成员。也就是说，for ... of 循环内部调用的是数据结构的 Symbol.iterator 方法。

for ... of 循环可以使用的范围包括数组、Set 和 Map 结构、某些类似数组的对象（如 arguments 对象，DOM NodeList 对象）、后文的 Generator 对象，以及字符串。

### 数组

数组原生具备 iterator 接口，即默认部署了 Symbol.iterator 属性。for ... of 循环本质上就是调用这个接口产生的遍历器的 next 方法，返回对象的 value 属性值。

```js
const arr = ['red', 'green', 'blue'];
const obj = {};
obj[Symbol.iterator] = arr[Symbol.iterator].bind(arr);
for (let v of obj) {
  console.log(v);	// red green blue
}
```

for ... of 循环可以替代数组实例的 forEach 方法。JavaScript 原有的 for ... in 循环只能获得对象的键名，不能获取键值，而 ES6 提供的 for ... of 循环可以遍历获得键值。如果要通过 for ... of 循环获取数组的索引，可以借助数组实例的 entries 和 keys 方法。

for ... of 循环调用遍历器接口，数组的遍历器接口只返回具有数字索引的属性，这一点与 for ... in 循环也不一样。

```js
let arr = [1, 3, 5];
arr.foo = 'hello';
for (let i in arr) {
  console.log(i);	// '0';	'1'; '2';	'foo';
}
for (let i of arr) {
  console.log(i);	// '1';	'3'; '5';
}
```

### Set 和 Map 结构

Set 和 Map 结构原生具有 Iterator 接口，可以直接使用 for ... of 循环。遍历的顺序是按照各个成员被添加进数据结构的顺序；Set 结构遍历时返回的是一个值，而 Map 结构遍历的时候返回的是一个数组，数组的两个成员分别是当前 Map 的键名和键值。

### 计算生成的数据结构

ES6 的数组、Set、Map 都部署了以下 3 个方法，调用后都返回遍历器对象。

- entries() 返回一个遍历器对象，用于遍历 [键名, 键值] 组成的数组。对于 Set，键名和键值相同。Map 结构的 iterator 接口默认就是调用 entries 方法。
- keys() 返回一个遍历器对象，用于遍历所有键名。
- values() 返回一个遍历器对象，用于遍历所有的键值。

```js
let arr = ['a', 'b'];
let iterator = arr.entries();
iterator.next();	// { value: [0, 'a'], done: false }
```

### 类似数组的对象

类似数组的对象包括好几类，像字符串、DOM NodeList 对象、arguments 对象。对于字符串来说，for ... of 循环有个特点，就是能够正确识别 32 位 UTF-16 字符。

```js
for (let x of 'a\uD83D\uDC0A') {
  console.log(x);	// 'a'	'\uD83D\uDC0A'
}
```

并不是所有类似数组的对象都具有 Iterator 接口，一个简便的解决方法就是使用 Array.from 方法将其转为数组。

```js
let arrayLike = {
  0: 'a',
  1: 'b',
  length: 2
}
// 报错
for (let x of arrayLike) {
  console.log(x);
}
// 正确
for (let x of Array.from(arrayLike)) {
  console.log(x);	// 'a'	'b'
}
```

### 对象

对于普通的对象，for ... in 循环可以直接遍历键名，for ... of 循环会报错，因为没有 Iterator 接口。

```js
let obj = {
  name: 'zs',
  age: 21
}
for (let o in obj) {
  console.log(o);	// 'name', 'age'
}
for (let o of obj) {
  console.log(o);	// TypeError
}
```

一种解决方法是，使用 Object.keys 方法将对象的键名生成一个数组，然后遍历这个数组。

```js
for (var key of Object.keys(someObject)) {
  console.log(key + '->' + someObject[key]);
}
```

另一个方法是使用 Generator 函数将对象重新包装一下。

```js
function* entries (obj) {
  for (let key of Object.keys(obj)) {
    yield [key, obj[key]];
  }
}
for (let [key, value] of entries(obj)) {
  console.log(key + '->' + value);
}
```

### 与其他遍历语法的比较

以数组为例，JavaScript 提供了多种遍历语法。

- for 循环：最原始的写法。

- forEach 方法：因为 for 循环写法麻烦，所以数组提供了内置的 forEach 方法。这种写法存在一个问题：无法中途跳出 forEach 循环，break 命令或 return 命令都不能。

- for ... in 循环：for ... in 可以遍历数组的键名。缺点如下：

  - 数组的键名是数值，for ... in 循环会把键名转为字符串。
  - for ... in 循环不仅遍历数字键名，还会遍历手动添加的其他键，甚至包括原型链上的键。
  - 某些情况下，for ... in 循环会以任意顺序遍历键名。

  for ... in 循环主要是为遍历对象设计的，不适用于遍历数组。

- for ... of 循环：有着同 for ... in 循环一样的简洁写法，但没有 for ... in 循环的缺点。不同于 forEach 方法，可以与 break、continue 和 return 配合使用。提供了遍历所有数据结构的统一操作接口。







# 第 16 章		Generator 函数的语法

## 简介

### 基本概念

Generator 函数是 ES6 提供的一种异步编程解决方案，语法行为与传统函数完全不同。

对于 Generator 函数有多种理解角度。从语法上，可以理解为状态机，里面封装了多个内部状态。执行 Generator 函数会返回一个遍历器对象，也就是说 Generator 函数除了是状态机，还是一个遍历器对象生成函数。返回的遍历器对象可以依次遍历 Generator 函数内部的每一个状态。从形式上，Generator 函数是一个普通函数，但是有两个特征：一是 function 命令与函数名之间有一个星号；二是函数体内部使用 `yield` 语句定义不同的内部状态。ES6 没有规定 function 关键字与函数名之间的星号写在哪个位置，所以以下写法都可以通过。

```js
function * foo (x, y) { ... }
function* foo (x, y) { ... }
function *foo (x, y) { ... }
function*foo (x, y) { ... }
```

```js
function* helloWorldGenerator () {
  console.log(1);
  yield 'hello';
  console.log(2);
  yield 'world';
  console.log(3);
  return 'ending';
  console.log(4);
}
var hw = helloWorldGenerator();
hw.next();	// 1 { value: 'hello', done: false }
hw.next();	// 2 { value: 'world', done: false }
hw.next();	// 3 { value: 'ending', done: true }
hw.next();	// { value: undefined, done: true }
```

上面的代码定义了一个 Generator 函数 —— helloWorldGenerator，内部有两个 yield 语句 hello 和 world 和一个 return 语句，即该函数有 3 个状态：hello、world 和 return 语句（结束执行）。Generator 函数的调用方法与普通函数一样，都是在函数名后面加上一对圆括号。不同的是，调用 Generator 函数后，该函数并不执行，返回的也不是函数运行结果，而是一个指向内部状态的指针对象 —— 遍历器对象，通过这个指针对象调用 next 方法，使得指针移向下个状态。每次调用 next 方法，内部指针就从函数头部或上一次停下来的地方开始执行，直到遇到下一条 yield 语句或 return 语句为止。next 方法返回一个对象，其 value 属性就是指针移动后 yield 语句或 return 语句的表达式的值，done 属性的值表示遍历是否结束。换言之，Generator 函数是分段执行的，yield 语句是暂停执行的标志，next 方法可以恢复执行。

总结：调用 Generator 函数返回一个遍历器对象，代表 Generator 函数的内部指针。每次调用遍历器对象的 next 方法，就会返回一个有着 value 和 done 两个属性的对象。value 属性表示当前的内部状态的值，是 yield 语句或 return 语句后面表达式的值；done 属性是一个布尔值，表示遍历是否结束。

### yield 表达式

由于 Generator 函数返回的遍历器对象只有调用 next 方法才会遍历下一个内部状态，所以 yield 语句就是暂停标志。遍历器对象的 next 方法的运行逻辑如下：

1. 遇到 yield 语句就暂停执行后面的操作，并将紧跟在 yield 后的表达式的值作为返回的对象的 value 属性值。
2. 下一次调用 next 方法时再继续往下执行，直到遇到下一条 yield 语句。
3. 如果没有再遇到新的 yield 语句，且后面没有 return 语句，就一直运行到函数结束。如果有 return 语句，就将 return 语句后面的表达式的值作为返回对象的 value 属性值。
4. 如果没有 return 语句或已执行完 return 语句，那么再次执行 next 方法时，返回对象的 value 属性值为 undefined。

yield 语句与 return 语句既有相似之处，又有所不同。相似之处在于都能返回紧跟在语句后的表达式的值。区别在于每次遇到 yield 函数暂停执行，下一次会从该位置继续向后执行，而 return 语句不具备位置记忆功能。一个函数里面只能执行一次 return 语句，但可以有多条 yield 语句。

Generator 函数可以不用 yield 语句，此时函数就变成了一个单纯的暂缓执行函数，只有调用 next 方法才会执行。

```js
function* f () {
  console.log('执行');
}
var generator = f();
generator.next();	// 执行
```

注意，yield 表达式只能用在 Generator 函数里面，用在其他地方都会报错。

```js
(function () {
  yield 1;	// SyntaxError
})()
```

```js
var arr = [1, [[2, 3], 4], [5, 6]];
var flat = function* (a) {
  a.forEach(function (item) {
    if (typeof item !== 'number') {
      yield* flat(item);
    } else {
      yield item;
    }
  })
}
for (var f of flat(arr)) {
  console.log(f);
}
```

上面的代码也会产生句法错误，因为 forEach 方法的参数是一个普通函数，但是在里面使用了 yield 表达式和 yield* 表达式。forEach 方法可以使用 for 循环替代。

另外，yield 表达式如果用在另一个表达式之中，必须放在圆括号中，否则会报错。如果是用作函数参数或放在赋值表达式的右边，可以不加括号。

```js
function* demo () {
  console.log('hello' + yield);	// SyntaxError
  console.log('world' + (yield));	// 正确
  let input = yield;	// 正确
}
// 例子
function* demo () {
    console.log(1);
    console.log('world' + (yield ' hello'));
    console.log(2);
}
var d = demo();
d.next();	// 1 { value: ' hello', done: false }
d.next();	// worldundefined 2 { value: undefined, done: true }
d.next();	// { value: undefined, done: true }
```

### 与 Iterator 接口的关系

Symbol.iterator 方法等于对象的遍历器对象生成函数，由于 Generator 函数就是遍历器生成函数，所以可以把 Generator 函数赋值给对象的 Symbol.iterator 属性，从而使得该对象具有 Iterator 接口。

```js
var myIterable = {};
myIterable[Symbol.iterator] = function* () {
  yield 1;
  yield 2;
  yield 3;
}
[...myIterable];	// 1 2 3
```

Generator 函数执行后，返回一个遍历器对象，该对象本身也具有 Symbol.iterator 属性，执行后返回对象自身。

```js
function* gen () {
  // some code
}
var g = gen();
g[Symbol.iterator]() === g;	// true
```

## next 方法的参数

yield 语句本身没有返回值，或说总是返回 undefined。next 方法可以接受一个参数，该参数会被当作上一条 yield 语句的返回值。该功能有很重要的语法意义，可以在 Generator 函数运行的不同阶段从外部向内部注入不同的值，从而调整函数的行为。

```js
function* foo (x) {
  var y = 2 * (yield (x + 1));
  var z = yield (y / 3);
  return x + y + z;
}
var a = foo(5);	// x = 5
a.next();	// { value: 6, done: false }
a.next(12);	// y = 2 * 12 = 24	{ value: 8, done: false}
a.next(13);	// z = 13	{ value: 42, done: true}
```

由于 next 方法的参数表示上一条 yield 语句的返回值，所以第一次使用 next 方法时传递参数是无效的。V8 引擎直接忽略第一次使用 next 方法时的参数，只有从第二次使用 next 方法开始，参数才是有效的。

## for ... of 循环

for ... of 循环可以自动遍历 Generator 函数生成的 Iterator 对象，自动调用其 next 方法且返回方法的返回值对象的 value 属性值。

```js
function* foo () {
  yield 1;
  yield 2;
  yield 3;
  return 4;
}
for (let v of foo()) {
  console.log(v);	// 1 2 3
}
```

一旦 next 方法的返回对象的 done 属性为 true，for ... of 循环就会终止，且不包含该返回对象，所以上面的例子不包含 return 的值。下面是一个使用 Generator 函数和 for ... of 循环实现斐波那契数列的例子。

```js
function* fibonacci () {
  let [prev, curr] = [0, 1];
  yield prev;
  yield curr;
  for ( ; ; ) {
    [prev, curr] = [curr, prev + curr];
    yield curr;
  }
}
for (let n of fibonacci()) {
  if (n > 1000) break;
  console.log(n);
}
```

原生的 JavaScript 对象没有遍历接口，无法使用 for ... of 循环，通过 Generator 函数为它加上这个接口后就可以使用了。

除了 for ... of 循环，扩展运算符、解构赋值和 Array.from 方法内部调用的都是遍历器接口。

## Generator.prototype.throw()

Generator 函数返回的遍历器对象都有一个 throw 方法，可以在函数体外抛出错误，然后在 Generator 函数体内捕获。throw 方法可以接受一个参数，该参数会被 Generator 函数体内的 try ... catch 代码块的 catch 语句接收，建议抛出 Error 对象的实例。

```js
var g = function* () {
  try {
    yield;
  } catch (e) {
    console.log('内部捕获 ', e);
  }
}
var i = g();
i.next();
try {
  i.throw('a');
  i.throw('b');
} catch (e) {
  console.log('外部捕获 ', e);
}
// 内部捕获 a
// 外部捕获 b
```

如果 Generator 函数内部没有部署 try ... catch 代码块，那么 throw 方法抛出的错误将被外部的 try ... catch 代码块捕获。

```js
var g = function* () {
  while (true) {
    yield;
    console.log('内部捕获 ', e);
  }
}
var i = g();
i.next();
try {
  i.throw('a');
  i.throw('b');
} catch (e) {
  console.log('外部捕获 ', e);
}
// 外部捕获 a
```

如果 Generator 函数内部部署了 try ... catch 代码块，那么遍历器的 throw 方法抛出的错误不影响下一次遍历，否则遍历直接终止。

```js
var gen = function* () {
  yield console.log('hello');
  yield console.log('world');
}
var g = gen();
g.next();
g.throw();
// hello
// Uncaught undefined
```

上面的代码中，g.throw 方法抛出错误以后，Generator 内部没有任何 try ... catch 代码块可以捕获这个错误，外部也没有，所以程序报错，中断执行。

throw 方法被捕获以后会附带执行下一条 yield 表达式，即附带执行一次 next 方法。

```js
var gen = function* () {
  try {
    yield console.log('a');
  } catch (e) {
    // ...
  }
  yield console.log('b');
  yield console.log('c');
}
var g = gen();
g.next();	// a
g.throw();	// b
g.next();	// c
```

另外，throw 命令与 g.throw 方法是无关的，两者互不影响。throw 命令是全局的，g.throw 方法是遍历器对象的。

```js
var gen = function* () {
  yield console.log('hello');
  yield console.log('world');
}
var g = gen();
g.next();	// hello
try {
  throw new Error();
} catch (e) {
  g.next();	// world
}
```

Generator 函数体外抛出的错误可以在函数体内捕获，反过来 Generator 函数体内抛出的错误也可以被函数体外的 catch 捕获。

```js
function* foo () {
  var x = yield 3;
  var y = x.toUpperCase();
  yield y;
}
var it = foo();
it.next();	// { value: 3, done: false }
try {
  it.next(42);
} catch (e) {
  console.log(e);	// TypeError: x.toUpperCase is not a function
}
```

一旦 Generator 执行过程中抛出错误，就不会再执行下去。如果后面还调用 next 方法，将返回一个 value 属性等于 undefined、done 属性等于 true 的对象，即 JavaScript 引擎认为这个 Generator 已经运行结束。

```js
function* g () {
  yield 1;
  console.log('throwing an exception');
  throw new Error('generator broke!');
  yield 2;
  yield 3;
}
function log (generator) {
  var v;
  console.log('starting generator');
  try {
    v = generator.next();
    console.log('第一次运行 next 方法', v);
  } catch (err) {
    console.log('捕捉错误', err);
  }
  try {
    v = generator.next();
    console.log('第二次运行 next 方法', v);
  } catch (err) {
    console.log('捕捉错误', err);
  }
  try {
    v = generator.next();
    console.log('第三次运行 next 方法', v);
  } catch (err) {
    console.log('捕捉错误', err);
  }
  console.log('caller done');
}
log(g());
// starting generator
// 第一次运行 next 方法 { value: 1, done: false }
// throwing an exception
// 捕捉错误 Error: generator broke!
// 第三次运行 next 方法 { value: undefined, done: true }
// caller done
```

## Generator.prototype.return()

Generator 函数返回的遍历器对象还有一个 return 方法，可以返回给定的值，并终结 Generator 函数的遍历。如果 return 方法调用时不传参数，那么返回值的 value 属性值为 undefined。如果传入参数，那么返回值的 value 属性值就是参数。同时 Generator 函数的遍历终止，返回值的 done 属性为 true，以后在调用 next 方法，返回值的 value 属性值为 undefined，done 属性值为 true。

```js
function * gen () {
  yield 1;
  yield 2;
  yield 3;
}
var g = gen();
g.next();	// { value: 1, done: false }
g.return();	// { value: undefined, done: true }
g.next();	// { value: undefined, done: true }
```

如果 Generator 函数内部有 try ... finally 代码块，那么 return 方法会推迟到 finally 代码块执行完毕再执行。

```js
function* number () {
  yield 1;
  try {
    yield 2;
    yield 3;
  } finally {
    yield 4;
    yield 5;
  }
  yield 6;
}
var g = number();
g.next();	// { value: 1, done: false }
g.next();	// { value: 2, done: false }
g.return(7);	// { value: 4, done: false }
g.next();	// { value: 5, done: false }
g.next();	// { value: 7, done: true }
```

## yield* 表达式

如果在 Generator 函数内部调用另一个 Generator 函数，默认情况下是没有效果的。

```js
function* foo () {
  yield 'a';
  yield 'b';
}
function* bar () {
  yield 'x';
  foo();
  yield 'y';
}
for (let v of bar()) {
  console.log(v);
}
// x
// y
```

这时就需要用到 `yield*` 语句，用来在一个 Generator 函数里面执行另一个 Generator 函数。从语法角度看，如果 yield 命令后面跟的是一个遍历器对象，那么需要在 yield 命令后面加上星号，表明这是一个遍历器对象。

```js
function* inner () {
  yield 'hello';
}
function* outer1 () {
  yield 'open';
  yield inner();
  yield 'close';
}
var gen = outer1();
gen.next().value;	// open
gen.next().value;	// 返回一个遍历器对象
gen.next().value;	// close

function* outer2 () {
  yield 'open';
  yield* inner();
  yield 'close';
}
// 等同于
function* outer2 () {
  yield 'open';
  yield 'hello';
  yield 'close';
}
// 等同于
function* outer2 () {
  yield 'open';
  for (let v of inner()) {
    yield v;
  }
  yield 'close';
}
var gen = outer2();
gen.next().value;	// 'open'
gen.next().value;	// 'hello'
gen.next().value;	// 'close'
```

yield* 后面的 Generator 函数，在其没有 return 语句时，等同于在使用 yield* 命令的 Generator 函数内部部署一个 for ... of 循环。就像上面例子中的第三个 outer2 函数一样，没有 return 语句时完全可以用 for ... of 循环代替 yield* 命令。如果有 return 语句，则需要使用 `var value = yield* iterator` 的形式获取 return 语句的值。

```js
function* genFuncWithReturn () {
  yield 'a';
  yield 'b';
  return 'The result';
}
function* logReturned (genObj) {
  let result = yield* genObj;
  console.log(result);
}
[...logReturned(genFuncWithReturn())];	// ['a', 'b']
// The result
```

实际上，任何数据结构只要有 Iterator 接口，就可以被 yield* 遍历。

```js
let read = (function* () {
  yield 'hello';
  yield* 'hello';
})();
read.next().value;	// hello
read.next().value;	// h
```

yield* 命令可以很方便的取出嵌套数组的所有成员。

```js
function* iterTree (tree) {
  if (Array.isArray(tree)) {
    for (let i = 0; i < tree.length; i++) {
      yield* iterTree(tree[i]);
    }
  } else {
    yield tree;
  }
}
const tree = ['a', ['b', ['c', 'd']], 'e', ['f', 'g']];
for (let x of iterTree(tree)) {
  console.log(x);	// a b c d e f g
}
```

使用 yield* 语句遍历完全二叉树的例子在 p341。

## 作为对象属性的 Generator 函数

如果一个对象的属性是 Generator 函数，需要加上 *。

```js
let obj = {
  myGeneratorMethod: function* () { ... }
  // 等同于
  * myGeneratorMethod () { ... }
}
```

## Generator 函数 this

Generator 函数总是返回一个遍历器，ES6 规定这个遍历器是 Generator 函数的实例，它也继承了 Generator 函数的 prototype 对象上的属性和方法。

```js
function* g () {}
g.prototype.hello = function () {
  return 'hi';
}
let obj = g();
obj.hello();	// hi
obj instanceof g;	// true
```

Generator 函数 g 返回的遍历器 obj 是 g 的实例，而且继承了 g.prototype。但是，g 返回的遍历器对象并不等于g 的 this 对象。

```js
function* g () {
  this.a = 11;
}
let obj = g();
obj.a;	// undefined
```

Generator 函数也不能跟 new 命令一起用，否则会报错。

```js
function* F () {
  yield this.x = 2;
  yield this.y = 3;
}
new F();	// TypeError
```

那么有没有办法可以让 Generator 函数返回一个正常的对象实例，既可以用 next 方法，又可以获得正常的 this 呢？可以用 call 方法绑定 Generator 函数内部的 this 为 Generator 函数的 prototype 对象。这样返回的遍历器对象可以正常调用 next 方法，又因为遍历器对象会继承 Generator 函数的 prototype 对象，所以可以获得 this 上的属性和方法。

```js
function* F () {
  this.a = 1;
  yield this.b = 2;
  yield this.c = 3;
}
var f = F.call(F.prototype);
f.next();	// { value: 2, done: false }
f.next();	// { value: 3, done: false }
f.next();	// { value: undefined, done: true }
f.a;	// 1
f.b;	// 2
f.c;	// 3
```

然后将 Generator 函数包装一下，这样就可以使用 new 命令了。

```js
function* gen () {
  this.a = 1;
  yield this.b = 2;
  yield this.c = 3;
}
function F () {
  return gen.call(gen.prototype);
}
var f = new F();
f.next();	// { value: 2, done: false }
f.next();	// { value: 3, done: false }
f.next();	// { value: undefined, done: true }
f.a;	// 1
f.b;	// 2
f.c;	// 3
```

## 含义

### Generator 与状态机

Generator 是实现状态机的最佳结构。对比 Generator 与 ES5 写法，可以发现实现更加简洁，且不需要一个保存状态的外部变量，更安全，更符合函数式编程思想。

```js
// ES5
var ticking = true;
var clock = function () {
  if (ticking) {
    console.log('Tick');
  } else {
    console.log('Tock');
  }
  ticking = !ticking;
}

// Generator
var clock = function* () {
  while (true) {
    console.log('Tick');
    yield;
    console.log('Tock');
    yield;
  }
}
```

### Generator 与协程

协程是一种程序运行的方式，可以理解为协作的线程或协作的函数。协程既可以用单线程实现，也可以用多线程实现。前者是一种特殊的子例程，后者是一种特殊的线程。

#### 协程与子例程的差异

#### 协程与普通线程的差异

## 应用

### 异步操作的同步化表达

Generator 函数的暂停执行效果，意味着可以把异步操作写在 yield 语句里面，等到调用 next 方法时再往后执行。这实际上等同于不需要写回调函数了，因为异步操作的后续操作可以放在 yield 语句下面，调用 next 方法时再执行。所以，Generator 函数的一个重要实际意义就是用于处理异步操作，改写回调函数。

```js
// 通过 Generator 函数使用 yield 语句手动逐行读取文件
function* numbers () {
  let file = new FileReader('number.txt');
  try {
    while (!file.eof) {
      yield parseInt(file.readLine(), 10);
    }
  } finally {
    file.close();
  }
}
```

### 控制流管理

```js
step1(function (value1) {
  step2(value1, function (value2) {
    step3(value2, function (value3) {
      step4(value3, function (value4) {
        // Do something with value4
      })
    })
  })
})
// 采用 Promise 改写
Promise.resolve(step1)
.then(step2)
.then(step3)
.then(step4)
.then(function (value4) {
  // Do something with value4
}, function (error) {
  // Handle any error from step1 through step4
})
.done();
// 使用 Generator 进一步改善
function* longRunningTask (initValue) {
  try {
    var value1 = yield step1(initValue);
    var value2 = yield step2(value1);
    var value3 = yield step3(value2);
    var value4 = yield step4(value3);
    // Do something with value4
  } catch (e) {
    // Handle any error from step1 through step4
  }
}
scheduler(longRunningTask(initialValue));
function scheduler (task) {
  var taskObj = task.next(task.value);
  if (!taskObj.done) {
    task.value = taskObj.value;
    scheduler(task);
  }
}
```

### 部署 Iterator 接口

利用 Generator 函数可以在任意对象上部署 Iterator 接口。

```js
function* iterEntries (obj) {
  let keys = Object.keys(obj);
  for (let i = 0; i < keys.length; i++) {
    let key = keys[i];
    yield [key, obj[key]];
  }
}
let myObj = { foo: 3, bar: 7 };
for (let [key, value] of iterEntries(myObj)) {
  console.log(key, value);
}
// foo 3
// bar 7
```

### 作为数据结构

Generator 可以看作数据结构，更确切地说，可以看作一个数组结构，因为 Generator 函数可以返回一系列的值，这意味着它可以对任意表达式提供类似数组的接口。







# 第 17 章		Generator 函数的异步应用

异步编程对 JavaScript 语言来说非常重要。JavaScript 语言的执行环境是单线程的，如果没有异步编程，根本无法使用，不然会造成卡死。

## 传统方法

ES6 诞生以前，异步编程的方法大概有下面 4 种。

- 回调函数
- 事件监听
- 发布 / 订阅
- Promise 对象

Generator 函数将 JavaScript 异步编程带入了一个全新的阶段。

## 基本概念

### 异步

所谓异步，简单来说就是一个任务不是连续完成的，可以理解成该任务被人为分成两段，先执行第一段，然后转而执行其他任务，等做好准备后再回过头执行第二段。

### 回调函数

JavaScript 语言对异步编程的实现就是回调函数。所谓回调函数，就是把任务的第二段单独写在一个函数里面，等到重新执行这个任务的时候直接调用这个函数。回调函数英文名为 `callback`，直译重新调用。读取文件进行处理的代码如下：

```js
fs.readFile('/etc/passwd', 'utf-8', function (err, data) {
  if (err) throw err;
  console.log(data);
})
```

上面代码中，readFile 函数的第三个参数就是回调函数，也就是任务的第二段。等到操作系统返回 /etc/passwd 文件后，回调函数才会执行。

为什么 Node 约定回调函数的第一个参数必须是错误对象 err（如果没有错误，该参数就是 null）呢？这是因为执行分成两段，第一段执行完以后，任务所在的上下文环境就已经结束了。在这以后抛出的错误，其原来的上下文环境已经无法捕捉，因此只能当作参数被传入第二段。

### Promise

回调函数本身没有问题，它的问题出现在多个回调函数的嵌套上。假定读取 A 文件之后再读取 B 文件，代码如下：

```js
fs.readFile(fileA, 'utf-8', function (err, data) {
  fs.readFile(fileB, 'utf-8', function (err, data) {
    // ...
  })
})
```

如果读取 B 文件后再读取别的文件，就会出现多重嵌套。那么多个异步操作形成强耦合，只要有一个操作需要修改，它的上层回调函数和下层回调函数都需要跟着修改。这种情况就是 callback hell 回调函数地狱。

Promise 对象就是为了解决这个问题而提出的，它不是新的语法功能，而是一种新的写法，允许将回调函数的嵌套改写成链式调用。采用 Promise 连续读取多个文件的写法如下：

```js
var readFile = require('fs-readfile-promise');	// fs-readfile-promise 返回一个 Promise 版本的 readFile 函数
readFile(fileA)
.then(data => console.log(data.toString()))
.then(() => {
  return readFile(fileB);
})
.then(data => console.log(data.toString()))
.catch(err => console.log(err));
```

Promise 的写法只是回调函数的改进，使用 then 方法后，异步任务的两段执行更清楚了。但 Promise 有个问题：代码冗余。原来的任务被 Promise 包装之后，无论什么操作一眼看去都是许多 then 的堆积，原来的语义变得很不清楚。

## Generator 函数

### 协程

传统的编程语言中早有异步编程的解决方案（其实是多任务的解决方案），其中一种叫做协程，意思是多个线程相互协作，完成异步任务。协程有点像函数，又有点像线程。运行流程大致如下：

- 第一步：协程 A 开始执行。
- 第二部：协程 A 执行到一半，进入暂停状态，执行权转移到协程 B 中。
- 第三步：一段时间后，协程 B 交还执行权。
- 第四步：协程 A 恢复执行。

上述流程的协程 A 就是异步任务，因为它分成两段或多段执行。用 Generator 实现读取文件协程的写法如下：

```js
function* asyncJob () {
  // some code
  var f = yield readFile(fileA);
  // some code
}
```

### 协程的 Generator 函数实现

Generator 函数是协程在 ES6 中的实现，最大特点是可以交出函数的执行权。整个 Generator 就是异步任务的容器，异步操作需要暂停的地方都用 yield 语句注明。调用 Generator 返回的遍历器对象的 next 方法可以分阶段执行 yield 语句间的任务。

### Generator 函数的数据交换和错误处理

Generator 函数可以暂停执行和恢复执行，这是它能封装异步任务的根本原因。除此之外，还有两个特性使它可以作为异步编程的完整解决方案：函数体内外的数据交换和错误处理机制。

next 返回值的 value 属性是 Generator 函数向外输出数据，next 方法接收一个参数用于向 Generator 函数输入数据。

Generator 函数内还可以部署错误处理代码，捕获函数体外抛出的错误。出错的代码和处理错误的代码实现了时间和空间上的分离，这对于异步编程无疑是很重要的。

### 异步任务的封装

```js
var fetch = require('node-fetch');
function* gen () {
  var url = 'https://test';
  var result = yield fetch(url);
  console.log(result.bio);
}
var g = gen();
var result = g.next();
result.value.then(data => {
  return data.json();
}).then(data => g.next(data));
```

Fetch 模块返回一个 Promise 对象。可以看到，虽然 Generator 函数将异步操作表示的很简洁，但是流程管理却不方便，不知道何时执行第一阶段，何时执行第二阶段。

## Thunk 函数

Thunk 函数是自动执行 Generator 函数的一种方法。

### 参数的求值策略

编程语言刚起步时，有一个争论焦点：求值策略，即函数的参数到底应该在何时求值。一种意见是传值调用，即在进入函数体内之前就计算参数的值，再将这个值传入函数，C 语言就采用了这种策略。另一种意见是传名调用，即直接将参数原原本本传入函数体，只有在用到它的时候求值，Haskell 语言就采用这种策略。两种策略各有利弊。传值调用较简单，但是对参数求值后，可能没有用到这个参数，会造成性能损失。

### Thunk 函数的含义

传名调用的实现往往是将参数放到一个临时函数之中，再将这个临时函数传入函数体。这个临时函数就称为 Thunk 函数。

```js
var x = 8;
function f (m) {
  return m * 2;
}
f(x + 2);
// 等同于
var thunk = function () {
  return x + 2;
}
function f (thunk) {
  return thunk() * 2;
}
f(thunk);
```

### JavaScript 语言的 Thunk 函数

JavaScript 语言是传值调用，它的 Thunk 函数含义有所不同。在 JavaScript 语言中，Thunk 函数替换的不是表达式，而是多参数函数，将其替换成一个只接受回调函数作为参数的单参数函数。

```js
// 正常版本的 readFile，多参数版本
fs.readFile(fileName, callback);
// Thunk 版本的 readFile，单参数版本
var Thunk = function (fileName) {
  return function (callback) {
    return fs.readFile(fileName, callback);
  }
}
var readFileThunk = Thunk(fileName);
readFileThunk(callback);
```

任何函数，只要参数有回调函数，就能写出 Thunk 函数的形式。下面是一个简单的 Thunk 函数转换器例子。

```js
// ES5
var Thunk = function (fn) {
  return function () {
    var args = Array.prototype.slice.all(arguments);
    return function (callback) {
      args.push(callback);
      return fn.call(this, args);
    }
  }
}

// ES6
const Thunk = function (fn) {
  return function (...args) {
    return function (callback) {
      return fn.call(this, ...args, callback);
    }
  }
}

// 应用
var readFileThunk = Thunk(fs.readFile);
readFileThunk(fileA)(callback);

function f (a, cb) {
  cb(a);
}
const ft = Thunk(f);
ft(1)(console.log);	// 1
```

### Thunkify 模块

生成环境中的转换器建议使用 Thunkify 模块。

```shell
# 安装
npm i thunkify
```

```js
// 使用
var thunkify = require('thunkify');
var fs = require('fs');
var read = thunkify(fs.readFile);
read('package.json')(function (err, str) {
  // ...
})
```

Thunkify 的源码与上一节中的简单转换器非常像。区别在于多了一个检查机制，确保回调函数只运行一次。具体翻阅 p364。

### Generator 函数的流程管理

Thunk 函数在以前没什么用，但是 ES6 中有了 Generator 函数，Thunk 函数就可以用于 Generator 函数的自动流程管理，使得 Generator 函数自动执行。

Generator 函数不用 Thunk 也可以自动执行，但这不适合异步操作。如果必须保证前一步执行完才能执行下一步，那么下面这种自动执行就不可行。

```js
// 不用 Thunk 自动执行
function* gen () {
  // ...
}
var g = gen();
var res = g.next();
while (!res.done) {
  console.log(res.value);
  res = g.next();
}
```

可以用 yield 命令将程序的执行权移出 Generator 函数，再用 Thunk 函数将执行权交还给 Generator 函数。以读取文件为例，下面的 Generator 函数封装了两个异步操作，这两个异步操作都是 Thunk 函数。

```js
var fs = require('fs');
var thunkify = require('thunkify');
var readFileThunk = thunkify(fs.readFile);
var gen = function* () {
  var r1 = yield readFileThunk('/etc/fstab');
  console.log(r1.toString());
  var r2 = yield readFileThunk('/etc/shells');
  console.log(r2.toString());
}
// 手动执行
var g = gen();
var r1 = g.next();
r1.value(function (err, data) {
  if (err) throw err;
  var r2 = g.next(data);
  r2.value(function (err, data) {
    if (err) throw err;
    g.next(data);
  })
})
```

通过手动执行，不难发现 Generator 函数的执行过程其实是将同一个回调函数反复传入 next 方法的 value 属性。所以可以使用递归来自动完成这个过程。

### Thunk 函数的自动流程管理

Thunk 函数真正的威力在于可以自动执行 Generator 函数。

```js
function run (fn) {
  var gen = fn();
  function next (err, data) {
    var result = gen.next(data);
    if (result.done) return;
    result.value(next);
  }
  next();
}
function* g () {
  // ...
}
run(g);
```

有了 run 这个执行器，执行 Generator 函数就方便多了。不管内部有多少个异步操作，直接把 Generator 函数传入 run 函数即可。当然前提是每一个异步操作都要是 Thunk 函数，也就是说跟在 yield 命令后面的必须是 Thunk 函数。

Thunk 函数并不是 Generator 函数自动执行的唯一方案。因为自动执行的关键是，必须有一种机制自动控制 Generator 函数的流程，接收和交还程序的执行权。回调函数可以做到这一点，Promise 对象也可以做到这一点。

## co 模块

### 基本用法

co 模块用于 Generator 函数的自动执行。使用 co 模块无须编写 Generator 函数的执行器，Generator 函数只要传入 co 函数就会自动执行。co 函数返回一个 Promise 对象，因此可以用 then 方法添加回调函数。

```js
var co = require('co');
var gen = function* () {
  var f1 = yield readFile('/test');
  var f2 = yield readFile('/test2');
  console.log(f1.toString());
  console.log(f2.toString());
}
co(gen).then(() => console.log('Generator 函数执行完成'));
```

### co 模块的原理

co 模块之所以可以自动执行 Generator 函数，是因为它将两种自动执行器（Thunk 函数和 Promise 对象）包装成一个模块。使用 co 的前提条件是，Generator 函数的 yield 命令后面只能是 Thunk 函数或 Promise 对象。如果数组或对象的成员全都是 Promise 对象，也可以使用 co。co v4.0 以后，yield 命令后只能是 Promise 对象，不再支持 Thunk 函数。

### 基于 Promise 对象的自动执行

```js
var fs = require('fs');
var readFile = function (fileName) {
  return new Promise(function (resolve, reject) {
    fs.readFile(fileName, function (err, data) {
      if (err) return reject(err);
      resolve(data);
    })
  })
}
var gen = function* () {
  var f1 = yield readFile('test1');
  var f2 = yield readFile('test2');	
  console.log(f1.toString());
  console.log(f2.toString());
}

// 手动执行
var g = gen();
g.next().value.then(function (data) {
  g.next(data).value.then(function (data) {
    g.next(data);
  })
})

// 自动执行
function run (gen) {
  var g = gen();
  function next (data) {
    var result = g.next(data);
    if (result.done) return result.value;
    result.value.then(function (data) {
      next(data);
    })
  }
  next();
}
run(gen);
```

### co 模块的源码

详情翻阅 p371。

### 处理并发的异步操作

co 支持并发的异步操作，即允许某些操作同时进行，等到它们全部完成才进行下一步。这时需要把并发的操作都放在数组或对象里面，跟在 yield 语句后面。

```js
co(function* () {
  var res = yield [
    Promise.resolve(1);
    Promise.resolve(2);
  ];
  console.log(res);
}).catch(onerror);
```

## 实例：处理 Stream

Node 提供 Stream 模式读写数据，特点是一次只处理数据的一部分，数据被分成一块一块依次处理，就好像数据流一样。这对于处理大规模数据非常有利。Stream 模式使用 EventEmitter API，会释放 3 个事件：

- data 事件：下一块数据块已经准备好。
- end 事件：整个数据流处理完成。
- error 事件：发生错误。

使用 Promise.race() 函数可以判断 3 个事件之中哪个最先发生，只有当 data 事件最先发生时，才进入下一个数据块的处理流程。

```js
const co = require('co');
const fs = require('fs');
const stream = fs.createReadStream('./test.txt');

co(function* () {
  while (true) {
    const res = yield Promise.race([
      new Promise(resolve => stream.once('data', resolve)),
      new Promise(resolve => stream.once('end', resolve)),
      new Promise((resolve, reject) => stream.once('error', reject))
    ])
    if (!res) {
      break;
    }
    stream.removeAllListeners('data');
    stream.removeAllListeners('end');
    stream.removeAllListeners('error');
    // do something
  }
})
```







# 第 18 章		async 函数

## 含义

ES2017 标准引入了 `async` 函数，使得异步操作变得更加方便。async 函数用一句话来说，就是 Generator 函数的语法糖。

```js
var fs = require('fs');
var readFile = function (fileName) {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    })
  })
}

// Generator
var gen = function* () {
  var f1 = yield readFile('/test1');
  var f2 = yield readFile('/test2');
  console.log(f1.toString());
  console.log(f2.toString());
}
var g = gen();
g.next().value.then(data => {
	g.next(data).value.then(data => {
    g.next(data);
  })
});

// async
var asyncReadFile = async function () {
  var f1 = await readFile('/test1');
  var f2 = await readFile('/test2');
  console.log(f1.toString());
  console.log(f2.toString());
}
asyncReadFile();
```

通过比较可以发现，async 函数就是将 Generator 函数的星号 * 替换成 `async`，将 yield 替换成 `await`。

async 函数对 Generator 函数的改进体现在以下 4 点：

1. 内置执行器

   Generator 函数的执行必须依靠执行器，所以才有了 co 模块，而 async 函数自带执行器，即 async 函数的执行与普通函数一样，只要调用函数这一行代码即可。

   ```js
   asyncReadFile();
   ```

   调用了 async 函数后，就会自动执行，输出最终的结果。这完全不像 Generator 函数需要调用自身返回遍历器对象，使用遍历器对象调用 next 方法或者使用 co 模块才能真正执行并得到最终结果。

2. 更好的语义

   async 和 await 比起星号和 yield，语义更清楚。async 表示函数里面有异步操作，await 表示紧跟在后面的表达式需要等待结果。

3. 更广的适用性

   co 模块约定，yield 命令后面只能是 Thunk 函数或 Promise 对象，而 async 函数的 await 命令后面，可以是 Promise 对象和原始类型的值。如果是原始类型的值，则为同步操作。

4. 返回值是 Promise

   async 函数的返回值是 Promise 对象，这比 Generator 函数的返回值是 Iterator 对象方便多了。可以用 then 方法指定下一步的操作。

进一步说，async 函数完全可以看作由多个异步操作包装成的一个 Promise 对象，而 await 命令就是内部 then 命令的语法糖。

## 用法

async 函数返回一个 Promise 对象，可以使用 then 方法添加回调函数。当函数执行时，遇到 await 就会先返回，等到异步操作完成，再接着执行函数体内后面的语句。

```js
function timeout (ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
async function asyncPrint (value, ms) {
  await timeout(ms);
  console.log(value);
}
asyncPrint('hello world', 1000);	// 1s 后输出 hello world
```

因为 async 函数返回的是 Promise 对象，可以作为 await 命令的参数，所以上面的例子可以改写为下面这种形式。

```js
async function timeout (ms) {
  await new Promise(resolve => setTimeout(resolve, ms));
}
async function asyncPrint (value, ms) {
  await timeout(ms);
  console.log(value);
}
asyncPrint('hello world', 1000);	// 1s 后输出 hello world
```

async 函数有多种使用形式。

```js
// 函数声明
async function foo () {}

// 函数表达式
const foo = async function () {}

// 箭头函数
const foo = async () => {}
```

## 语法

### 返回 Promise 对象

async 函数返回一个 Promise 对象。async 函数内部 return 语句的值，会成为 then 方法回调函数的参数。

```js
async function f () {
  return 'hello world';
}
f().then(v => console.log(v));	// hello world
```

async 函数内部抛出错误会导致返回的 Promise 对象变为 Rejected 状态。抛出的错误对象会被 catch 方法或 reject 回调函数接收。

```js
async function f () {
  throw new Error('出错了');
}
f().then(
	v => console.log(v),
	e => console.log(e)	// Error: 出错了
)
```

### Promise 对象的状态变化

async 函数返回的 Promise 对象必须等到内部所有 await 命令后面的 Promise 对象执行完才会发生状态改变，除非遇到 return 语句或抛出错误。即只有 async 函数内部的异步操作执行完，才会执行 then 方法指定的回调函数。	[case](code\第18章-async函数\18.3.2-Promise对象的状态变化.js)

### await 命令

await 命令的返回值是后面 Promise 对象执行 resolve 函数时的参数。

```js
async function f () {
  let result = await new Promise(resolve => setTimeout(resolve('hello'), 1000));
  console.log(result);	// hello
}
```

正常情况下，await 命令后面是一个 Promise 对象，如果不是会被转成一个立即 resolve 的 Promise 对象。

```js
async function f () {
  return await 123;
}
f().then(v => console.log(v));	// 123
```

await 命令后面的 Promise 对象如果变为 reject 状态，则 reject 的参数会被 async 函数返回的 Promise 对象的 catch 方法或 reject 回调函数接收。只要一个 await 语句后面的 Promise 变为 reject，那么整个 async 函数都会中断执行。

```js
async function f () {
  await Promise.reject('出错了');
  await Promise.resolve('hello world');	// 不会执行
}
f().catch(e => console.log(e));	// 出错了
```

如果希望即使前面的异步操作失败，也不要中断后面的异步操作，可以采用下面两种方法。第一个方法将 await 放在 try ... catch 代码块中，这样不管该异步操作是否成功，后面的 await 都会执行。第二个方法是为 await 后面的 Promise 对象添加 catch 方法，处理该 await 语句的 Promise 对象可能出现的错误。

```js
// 第一种
async function f () {
  try {
    await Promise.reject('出错了');
  } catch (e) {}
  return await Promise.resolve('hello world');
}
f().then(v => console.log(v));	// hello world

// 第二种
async function f () {
  await Promise.reject('出错了').catch(e => console.log(e));
  return await Promise.resolve('hello world');
}
f().then(v => console.log(v));
// 出错了
// hello world
```

### 错误处理

如果 await 后面的异步操作出错，那么等同于 async 函数返回的 Promise 对象被 reject。

```js
async function f () {
  await new Promise((resolve, reject) => {
    throw new Error('出错了');
  })
}
f()
.then(v => console.log(v))
.catch(e => console.log(e));	// Erro: 出错了
```

防止出错的方法是为 await 后面的 Promise 对象加上 catch 方法，或将 await 放在 try ... catch 代码块中，如果有多个 await 命令，可以统一放在 try ... catch 代码块中。

```js
async function f () {
  try {
    var val1 = await firstStep();
    var val2 = await secondStep(val1);
    var val3 = await thirdStep(val1, val2);
    console.log('Final:', val3);
  } catch (err) {
    console.log(err);
  }
}
```

### 使用注意点

1. 前面说过，await 命令后面的 Promise 对象的运行结果可能是 Rejected，所以最好把 await 命令放在 try ... catch 代码块中或为 await 命令的 Promise 对象加上 catch 方法。

2. 多个 await 命令后面的异步操作如果不存在继发关系，最好让它们同时触发，以节省时间。

   ```js
   let foo = await getFoo();
   let bar = await getBar();
   ```

   上面的代码中，getFoo 和 getBar 是两个独立的异步操作，互不依赖，被写成继发关系。这样比较耗时，因为只有 getFoo 执行完才会执行 getBar，完全可以让它们同时触发。

   ```js
   // 写法一
   let [foo, bar] = await Promise.all([getFoo(), getBar()]);
   
   // 写法二
   let fooPromise = getFoo();
   let barPromise = getBar();
   let foo = await fooPromise();
   let bar = await barPromsie();
   ```

   上面两种写法中，getFoo 和 getBar 都是同时触发，以缩短程序执行时间。

3. await 命令只能用在 async 函数中，如果用在普通函数中就会报错。

   ```js
   async function f () {
     let arr = [{}, {}, {}];
     arr.forEach(function (item) {
       await item.someMethod();	// 报错
     })
   }
   ```

   就算把 forEach 方法的参数改成 async 函数，也会出现问题。这是因为 forEach 中的操作都将是并发执行，而不是继发执行。

   ```js
   async function f () {
     let arr = [{}, {}, {}];
     arr.forEach(async function (item) {
       await item.someMethod();	// 可能得到错误结果
     })
   }
   ```

   正确写法应该采用 for 循环。如果想并发执行，可以使用 Promise.all() 方法。

## async 函数的实现原理

async 函数的实现原理就是将 Generator 函数和自动执行器包装在一个函数里。

```js
async function fn (args) {
  // ...
}

// 等同于

function fn (args) {
  return spawn(function* () {
    // ...
  })
}
function spawn (genF) {
  return new Promise(function (resolve, reject) {
    var gen = genF();
    function step (nextF) {
      try {
        var next = nextF();
      } catch (e) {
        return reject(e);
      }
      if (next.done) {
        return resolve(next.value);
      }
      Promise.resolve(next.value).then(function (v) {
        step(function () { return gen.next(v); })
      }, function (e) {
        step(function () { return gen.throw(e); })
      })
    }
    step(function () { return gen.next(undefined); })
  })
}
```

所有的 async 函数都可以写成上面的第二种形式，其中的 spawn 函数就是自动执行器，基本就是前文自动执行器的翻版。

## 其他异步处理方法的比较

Promise 写法相比回调函数的写法大大改进，但是一眼看去，代码完全是 Promise 的 API，如 then、catch 等，操作本身的语义反而不容易看出来。

Generator 写法语义比 Promise 更清晰，用户定义的操作全部都出现在任务运行器内部。该写法的问题在于，必须有一个任务运行器自动执行 Generator 函数，且必须保证 yield 语句后面的表达式返回一个 Promise。

async 写法的实现最简洁，最符合语义，几乎没有与语义不相关的代码。它将 Generator 写法中的自动执行器改在语言层面提供，不暴露给用户，不需要用户自己定义自动执行器。

## 实例：按顺序完成异步操作

依次远程读取一组 URL，然后按照读取的顺序输出结果。

```js
// Promise
function logInOrder (urls) {
  // 远程读取所有 URL
  const textPromises = urls.map(url => {
    return fetch(url).then(response => response.test());
  })
  // 按次序输出
  textPromises.reduce((chain, textPromise) => {
    return chain.then(() => textPromise)
    .then(text => console.log(text));
  }, Promise.resolve());
}

// async
async function logInOrder (urls) {
  // 并发读取远程 URL
  const textPromises = urls.map(async url => {
    const response = await fetch(url);
    return response.text();
  })
  // 按次序输出
  for (const textPromise of textPromises) {
    console.log(await textPromsie);
  }
}
```

## 异步遍历器

前面提到 Iterator 接口是一种数据遍历的协议，调用遍历器对象的 next 方法就会得到一个表示当前遍历指针所在位置的信息对象。这里隐含一个规定，next 方法必须是同步的，只要调用就必须立刻返回值。对于异步操作就不太合适了。目前有一个提案，为异步操作提供原生的遍历器接口，即 value 和 done 这两个属性都是异步产生的，这称为异步遍历器。

### 异步遍历的接口

异步遍历器的最大的语法特点就是，调用遍历器的 next 方法返回的是一个 Promise 对象。

```js
asyncIterator.next().then(({ value, done }) => /* ... */)
```

一个对象的同步遍历器的接口部署在 Symbol.iterator 属性上面。同样的，对象的异步遍历器接口部署在 `Symbol.asyncIterator` 属性上面。不管什么对象，只要它的 Symbol.asyncIterator 属性有值，就表示应该对它进行异步遍历。

```js
const asyncIterable = createAsyncIterable(['a', 'b']);
const asyncIterator = asyncIterable[Symbol.asyncIterator]();
asyncIterator.next()
.then(iterResult1 => {
  console.log(iterResult1);	// { value: 'a', done: false }
  return asyncIterator.next();
})
.then(iterResult2 => {
  console.log(iterResult2);	// { value: 'b', done: false }
  return asyncIterator.next();
})
.then(iterResult3 => {
  console.log(iterResult3);	// { value: undefined, done: true }
})
```

因为异步遍历器的 next 方法返回的是一个 Promise 对象，所以可以把它放在 await 命令后面。

```js
async function f () {
  const asyncIterable = createAsyncIterable(['a', 'b']);
	const asyncIterator = asyncIterable[Symbol.asyncIterator]();
  console.log(await asyncIterator.next());	// { value: 'a', done: false }
  console.log(await asyncIterator.next()):	// { value: 'b', done: false }
  console.log(await asyncIterator.next());	// { value: undefined, done: true }
}
```

异步遍历器的 next 方法是可以连续调用的，不必等到上一步产生的 Promise 对象 resolve 以后再调用。连续调用时，next 方法会累积起来，自动按照每一步的顺序运行下去。

```js
const asyncGenObj = createAsyncIterable(['a', 'b']);
const [{ value: v1 }, { value: v2 }] = await Promise.all([
  asyncGenObj.next(), asyncGenObj.next()
])
console.log(v1, v2);	// a b
```

### for await ... of

引入 `for await ... of` 循环用于遍历异步的 Iterator 接口。

```js
async function f () {
  for await (const x of createAsyncIterable(['a', 'b'])) {
    console.log(x);
    // a
    // b
  }
}
```

部署了 asyncIterator 操作的异步接口，可以直接放入 for await ... of 循环中。如果 next 方法返回的 Promise 对象被 reject，for await ... of 就会报错，要用 try ... catch 捕捉。

```js
async function () {
  try {
    for await (const x of createRejectingIterable()) {
      console.log(x);
    }
  } catch (err) {
    console.error(err);
  }
}
```

注意，for await ... of 循环也可以用于同步遍历器。

```js
(async function () {
  for await (const x of ['a', 'b']) {
    console.log(x);
    // a
    // b
  }
})();
```

### 异步 Generator 函数

就像 Generator 函数返回一个同步遍历器对象一样，异步 Generator 函数的作用是返回一个异步遍历器对象。在语法上，异步 Generator 函数就是 async 函数与 Generator 函数的结合。

```js
async function* gen () {
  yield 'hello';
}
const genObj = gen();
genObj.next().then(x => console.log(x));	// { value: 'hello', done: false }
```

上面的代码中，gen 是一个异步 Generator 函数，执行后返回一个异步 Iterator 对象。调用该对象的 next 方法返回一个 Promise 对象。异步遍历器的设计目的之一，就是使 Generator 函数处理同步操作和异步操作时能够使用同一套接口。

```js
// 同步 Generator
function * map (iterable, func) {
  const iter = iterable[Symbol.iterator]();
  while (true) {
    const { value, done } = iter.next();
    if (done) break;
    yield func(value);
  }
}

// 异步 Generator
async function * map (iterable, func) {
  const iter = iterable[Symbol.asyncIterator]();
  while (true) {
    const { value, done } = await iter.next();
    if (done) break;
    yield func(value);
  }
}
```

可以看到有了异步遍历器以后，同步 Generator 函数和异步 Generator 函数的写法基本一致。

异步操作前面使用 await 关键字标明，即 await 后面的操作应该返回 Promise 对象。凡是使用 yield 关键字的地方，就是 next 方法停下来的地方，后面的表达式的值会作为 next 返回对象的 value 属性，这一点与同步 Generator 函数一致。

```js
async function* readLines (path) {
  let file = await fileOpen(path);
  try {
    while (!file.EOF) {
      yield await file.readFile();
    }
  } finally {
    await file.close();
  }
}
(async function () {
  for await (const line of readLines(filePath)) {
    console.log(line);
  }
})();
```

异步 Generator 函数的返回值是一个异步 Iterator，即每次调用它的 next 方法都会返回一个 Promise 对象，也就是说，yield 命令返回的是一个 Promise 对象。

```js
async function* asyncGenerator () {
  const result = await doSomethingAsync();	// A
  yield 'Result: ' + result;	// B
}
const ag = asyncGenerator();
ag.next().then({ value, done } => {
  // ...
})
```

上述代码中，调用 ag.next() 后 asyncGenerator 函数内部的执行顺序如下。

1. await 命令返回一个 Promise 对象，但程序不会停在这里，而是继续往下执行。
2. 程序在 B 处暂停执行，yield 命令立即返回一个 Promise 对象，该对象就是 ag.next() 的返回值。
3. A 处 await 命令后面的 Promise 对象 Resolved，产生的值放入 result 变量中。
4. B 处的 Promise 对象 Resolved，then 方法指定的回调函数开始执行，该函数的参数是一个对象，value 的值是表达式 `'Result: ' + result` 的值，done 属性的值是 false。

A 和 B 两行的作用类似下面的代码：

```js
return new Promise((resolve, reject) => {
  doSomethingAsync()
  .then(result => {
    resolve({
      value: 'Result: ' + result,
      done: false
    })
  })
})
```

如果异步 Generator 函数抛出错误，则会被 next 方法返回的 Promise 对象 reject，然后抛出的错误会被 catch 方法捕获。

```js
async function * asyncGenerator () {
  throw new Error('Problem');
}
asyncGenerator().next().catch(err => console.log(err));	// Error: Problem
```

普通的 async 函数返回的是一个 Promise 对象，而异步 Generator 函数返回的是一个异步 Iterator 对象。async 函数和异步 Generator 函数是封装异步操作的两种方法，都用来达到同一种目的。区别在于，前者自带执行器，后者通过 for await ... of 执行，或自定义执行器。下面是一个异步 Generator 函数的执行器。

```js
async function takeAsync (iterable) {
  const result = [];
  while (true) {
    const { value, done } = await iterator.next();
    if (done) break;
    result.push(value);
  }
  return result;
}

// 实例
async function f () {
  async function* gen () {
    yield 'a';
    yield 'b';
    yield 'c';
  }
  return await takeAsync(gen());
}
f().then(result => console.log(result));	// ['a', 'b', 'c']
```

异步 Generator 函数出现以后，JavaScript 就有了 4 种函数形式：普通函数、async 函数、Generator 函数和异步 Generator 函数。基本上，如果是一系列按照顺序执行的异步操作（比如读取文件，然后写入新内容再存入硬盘），可以使用 async 函数；如果是一系列产生相同数据结构的异步操作（比如一行一行读取文件），可以使用异步 Generator 函数。

### yield* 语句

yield* 语句也可以与一个异步遍历器一同使用。

```js
async function* gen1 () {
  yield 'a';
  yield 'b';
  return 2;
}
async function* gen2 () {
  const result = yield* gen1();
 	// result 的值最终为 2
}
```

与同步 Generator 函数一样，for await ... of 循环会展开 yield*。

```js
(async function () {
  for await (const x of gen2()) {
    console.log(x);
    // a
    // b
  }
})();
```







# 第 19 章		Class 的基本语法

## 简介

JavaScript 语言的传统方法是通过构造函数定义并生成新对象。

```js
function Point (x, y) {
  this.x = x;
  this.y = y;
}
Point.prototype.toString = function () {
  return `( ${this.x}, ${this.y} )`;
}
var p = new Point(1, 2);
```

ES6 提供了更接近传统语言的写法，引入了 Class 类的概念作为对象的模板。通过 `class` 关键字可以定义类。基本上，ES6 中的 class 可以看作只是一个语法糖，它的绝大部分功能，ES5 都可以做到，新的 class 写法只是让对象原型的写法更加清晰，更像面向对象编程的语法而已。上面的代码用 ES6 的类改写如下：

```js
class Point {
  constructor (x, y) {
    this.x = x;
    this.y = y;
  }
  toString () {
    return `( ${this.x}, ${this.y} )`;
  }
}
var p = new Point();
```

类中的 `constructor` 方法就是构造方法，而 this 关键字代表实例对象。也就是说 ES5 的构造函数 Point 对应 ES6 的 Point 类的构造方法。定义类的方法时，前面不需要加上 function 这个保留字，直接写函数定义就好了。另外，方法之间不需要逗号分隔，加了反而会报错。ES6 的类完全可以看作构造函数的另一种写法，类本身就指向构造函数，即类的 prototype 对象的 constructor 属性直接指向类本身，这与 ES5 的行为一致。

```js
class Point () {}
typeof Point;	// function
Point === Point.prototype.constructor;	// true
```

类的用法跟构造函数的用法一致，使用的时候直接对类使用 new 命令。

构造函数的 prototype 属性在 ES6 的类上也存在着。事实上，类的所有方法都定义在类的 prototype 属性上。

```js
class Point {
  constructor () {}
  toString () {}
  toValue () {}
}
// 等同于
Point.prototype = {
  constructor () {},
  toString () {},
  toValue () {}
}
```

在类的实例上调用方法，其实就是调用类的原型上的方法。

```js
class B {}
let b = new B();
b.constructor === B.prototype.constructor;	// true
```

由于类的方法都定义在 prototype 对象上，所以类的新方法可以添加在 prototype 对象上。Object.assign 方法可以很方便的一次向类添加多个方法。

```js
class Point () {
  constructor () {}
}
Object.assign(Point.prototype, {
  toString () {},
  toValue () {}
})
```

类的内部定义的所有方法都是不可枚举的。

```js
class Point () {
  constructor () {}
  toString () {}
}
Object.keys(Point.prototype);	// []
Object.getOwnPropertyNames(Point.prototype);	// ['constructor', 'toString']
```

ES5 写法中，内部定义的方法就是可枚举的。

```js
var Point = function () {}
Point.prototype.toString = function () {}
Object.keys(Point.prototype);	// ['toString']
Object.getOwnPropertyNames(Point.prototype);	// ['constructor', 'toString']
```

类的属性名可以采用表达式。

```js
let methodName = 'getArea';
class Square {
  constructor (length) {
    this.length = length;
  }
  [methodName] () {
    return length ** 2;
  }
}
```

## 严格模式

类和模块的内部默认使用严格模式，所以不需要使用 `use strict` 指定运行模式。因为只要将代码写在类或模块之中，就只有严格模式可用。考虑到未来所有代码其实都是运行在模块之中，所以 ES6 实际上已经把整个语言都升级到了严格模式下。

## constructor 方法

constructor 方法是类的默认方法，通过 new 命令生成对象实例时自动调用该方法。一个类必须有 constructor 方法，如果没有显示定义，一个空的 constructor 方法会被默认添加。

```js
class Point {}
// 等同于
class Point {
  constructor () {}
}
```

constructor 方法默认返回实例对象，即 this，不过完全可以指定返回另外一个对象。

```js
class Foo {
  constructor () {
    return Object.create(null);
  }
}
new Foo() instanceof Foo;	// false
```

类必须使用 new 来调用，否则会报错。这是它跟普通构造函数的一个主要区别，后者不用 new 也可以执行。

```js
class Foo {
  constructor () {}
}
Foo();	// TypeError: Class constructor Foo cannot be invoked without 'new'
```

## 类的实例对象

生成实例对象的写法与 ES5 一样，也是使用 new 命令。如果忘记加上 new，像函数一样调用 Class 会报错。

与 ES5 一样，实例的属性除非显式定义在其本身（即 this 对象）上，否则都是定义在类本身（即 Class）上，可以在类的原型上找到。

```js
class Point {
  constructor (x, y) {
    this.x = x;
    this.y = y;
  }
  toString () {
    return `( ${this.x}, ${this.y} )`;
  }
}
var p = new Point(1, 2);
p.toString();	// ( 1, 2 )
p.hasOwnProperty('x');	// true
p.hasOwnProperty('y');	// true
p.hasOwnProperty('toString');	// false
p.__proto__.hasOwnProperty('toString');	// true
```

与 ES5 一样，类的所有实例共享一个原型对象。

```js
var p1 = new Point(2, 3);
var p2 = new Point(3, 4);
p1.__proto__ === p2.__proto__;	// true
```

上述代码中，p1 和 p2 都是 Point 类的实例，它们的原型都是 Point.prototype，所以 \_\_proto\_\_ 属性是相等的。这也意味着，可以通过实例的 \_\_proto\_\_ 属性为类添加方法。但使用实例的 \_\_proto\_\_ 属性改写原型必须相当谨慎，不推荐使用，因为这会改变 Class 的原始定义，影响到所有实例。

```js
var p1 = new Point(1, 2);
var p2 = new Point(2, 3);
p1.__proto__.printName = function () { console.log('x') };
p1.printName();	// x
p2.printName();	// x
var p3 = new Point(3, 4);
p3.printName();	// x
```

\_\_proto\_\_ 并不是语言本身的特性，而是各大厂商具体实现时添加的私有属性，虽然目前很多现代浏览器的 JS 引擎中都提供了这个私有属性，但依旧不建议在生产中使用该属性，避免对环境产生依赖。生产环境中，可以使用 Object.getPrototypeOf 方法来获取实例对象的原型，然后再来为原型添加方法和属性。当然，还是不推荐改写原型。

## Class 表达式

与函数一样，Class 也可以使用表达式的形式定义。

```js
const MyClass = class Me {
  getClassName () {
    return Me.name;
  }
}
let inst = new MyClass();
inst.getClassName();	// Me
Me.name;	// ReferenceError: Me is not defined
```

上面的代码中，类的名字是 MyClass 而不是 Me，Me 只在 Class 的内部可用，指代当前类。如果 Class 内部没有用到，可以省略 Me，也就是写成下面的形式。

```js
const MyClass = class { ... }
```

采用 Class 表达式，可以写出立即执行的 Class。

```js
let person = new class {
  constructor (name) {
    this.name = name;
  }
  sayName () {
    console.log(this.name);
  }
}('zs');
person.sayName();	// zs
```

## 不存在变量提升

类不存在变量提升，这与 ES5 完全不同。这种规定的原因与下文要提到的继承有关，必须保证子类在父类之后定义。

```js
new Foo();	// ReferenceError
class Foo {}

{
  let Foo = class {};
  class Bar extends Foo {}
}
```

上述第二段代码不会报错，因为 Bar 在继承 Foo 的时候，Foo 已经定义好了。但是，如果存在 class 的提升，上面的代码就会报错，因为 class 会被提升到代码头部，而 let 命令不提升，所以导致 Bar 继承 Foo 的时候，Foo 还未定义。

## 私有方法

私有方法是常见需求，但是 ES6 不提供，只能通过以下变通方法来模拟实现。一种是在命名上加以区别，加下划线的方法为私有方法，但这种命名不保险，在类的外部依旧可以调用。

```js
class Widget {
  // 公有方法
  foo (baz) {}
  // 私有方法
  _bar (baz) {}
}
```

另一种方法是将私有方法移出模块，因为模块内部的所有方法都是对外可见的。

```js
class Widget {
  foo (baz) {
    bar.call(this, baz);
  }
}
function bar (baz) {
  return this.snaf = baz;
}
```

还有一种方法是利用 Symbol 值的唯一性将私有方法的名字命名为一个 Symbol 值。

```js
const bar = Symbol('bar');
const snaf = Symbol('snaf');
export default class MyClass {
  // 公有方法
  foo (baz) {}
  // 私有方法
  [bar] (baz) {
    return this[snaf] = baz;
  }
}
```

## 私有属性

与私有方法一样，ES6 不支持私有属性。但目前有一个提案为 class 加了私有属性。方法是在属性名之前使用 `#` 来表示。

```js
class Test {
  #x;
  constructor (x = 0) {
    #x = +x;	// 写成 this.#x 也可以
  }
	get x () {
    return #x;
  }
	set x (value) {
    #x = +value;
  }
}
```

上述代码中，#x 就表示私有属性 x，在 Test 类外部是读取不到这个属性的。另外，私有属性 #x 与实例的属性 get x() 是可以同名的。私有属性可以指定初始值在构造函数执行时进行初始化。

```js
class Test {
  #x = 0;
  constructor () {
    #x;	// 0
  }
}
```

之所以使用 # 表示私有属性，而不是使用 private 关键字，是因为 JavaScript 是一门动态语言，使用独立的符号能够准确的区分一种属性是否为私有属性。

该提案只规定了私有属性的写法，但也可以用来编写私有方法。

```js
class Foo {
  #a;
  #b;
  #sum () {
  	return #a + #b;
	}
}
```

## this 的指向

类的方法内部如果含有 this，它将默认指向类的实例。但是单独使用该方法，可能会报错。

```js
class Logger {
  printName (name = 'there') {
    this.print('Hello ' + name);
  }
  print (text) {
    console.log(text);
  }
}
const logger = new Logger();
const { printName } = logger;
printName();	// TypeError: Cannot read property 'print' of undefined
```

上述代码中，printName 方法中的 this 默认指向 Logger 类的实例，但如果将这个方法取出来单独使用，this 会指向该方法运行时所在的环境，因为找不到 print 方法而导致错误。解决方法如下。

一种简单的解决方法是，在构造方法中绑定 this（使用 bind 绑定 this 对象后，方法的 this 对象就不得再改变）。

```js
class Logger {
  constructor () {
    this.printName = this.printName.bind(this);
  }
  // ...
}
```

另一种解决方法是使用箭头函数。

```js
class Logger {
  constructor () {
    this.printName = (name = 'there') => {
      this.print('Hello ' + name);
    }
  }
}
```

还有一种解决方法是使用 Proxy，在获取方法的时候自动绑定 this。

```js
function selfish (target) {
  const cache = new WeakMap();
  const handler = {
    get (target, key) {
      const value = Reflect.get(target, key);
      if (typeof value !== 'function') {
        return value;
      }
      if (!cache.has(value)) {
        cache.set(value, value.bind(target));
      }
      return cache.get(value);
    }
  }
  const proxy = new Proxy(target, handler);
  return proxy;
}
const logger = selfish(new Logger());
```

## name 属性

本质上，ES6 的类只是 ES5 的构造函数的一层包装，所以函数的许多特征都被 Class 继承，包括 name 属性。name 属性总是返回紧跟在 class 关键字后面的类名。

```js
class Point {}
Point.name;	// 'Point'
```

## Class 的取值函数（getter）和存值函数（setter）

与 ES5 一样，在类的内部可以使用 get 和 set 关键字对某个属性设置存值函数和取值函数，拦截该属性的存取行为。

```js
class MyClass {
  constructor () {}
  get prop () {
    return 'getter';
  }
  set prop (value) {
    console.log('setter: ' + value)
  }
}
let inst = new MyClass();
inst.prop = 123;	// setter: 123
inst.prop;	// getter
```

存值函数和取值函数是设置在属性的描述对象 Descriptor 对象上的，这与 ES5 完全一致。

```js
class CustomHTMLElement {
  constructor (element) {
    this.element = element;
  }
  get html () {
    return this.element.innerHTML;
  }
  set html (value) {
    this.element.innerHTML = value;
  }
}
var descriptor = Object.getOwnPropertyDescriptor(CustomHTMLElement, 'html');
'get' in descriptor;	// true
'set' in descriptor;	// true
```

## Class 的 Generator 方法

如果某个方法之前加上星号 *，就表示该方法是一个 Generator 函数。

```js
class Foo {
  constructor (...args) {
    this.args = args;
  }
  * [Symbol.iterator] () {
    for (let arg of this.args) {
      yield arg;
    }
  }
}
for (let x of new Foo('hello', 'world')) {
  console.log(x);
  // hello
  // world
}
```

## Class 的静态方法

类相当于实例的原型，所有在类中定义的方法都会被实例继承。如果在一个方法前加上 `static` 关键字，就表示该方法不会被实例继承，而是直接通过类调用，称为静态方法。

```js
class Foo {
  static classMethod () {
    return 'hello';
  }
}
Foo.classMethod();	// hello
new Foo().classMethod();	// TypeError: (intermediate value).classMethod is not a function
```

子类会继承父类的静态方法。

```js
class Foo {
  static classMethod () {
    return 'hello';
  }
}
class Bar extends Foo {}
Bar.classMethod();	// hello
```

子类可以通过 `super` 对象调用父类的静态方法。

```js
class Foo {
  static classMethod () {
    return 'hello';
  }
}
class Bar extends Foo {
  static classMethod () {
    return super.classMethod() + ', too';
  }
}
Bar.classMethod();	// hello, too
```

## Class 的静态属性和实例属性

静态属性指的是 Class 本身的属性，而不是定义在实例对象 this 上的属性。

```js
class Foo {}
Foo.prop = 1;
Foo.prop;	// 1
```

目前，只有上面这种写法可以读/写静态属性，因为 ES6 明确规定，Class 内部只有静态方法，没有静态属性。

```js
// 以下两种写法都无效：
class Foo {
  // 写法一
  prop: 2
	// 写法二
	static prop: 2
}
Foo.prop;	// undefined
```

目前有一个关于静态属性的提案，其中对实例属性和静态属性都规定了新写法，具体见下文。

### Class 的实例属性

以前，定义实例属性时只能写在类的 constructor 方法里面。下面定义一个 test 实例属性。

```js
class Foo {
  constructor (test) {
    this.test = test;
  }
}
```

现在可以把 Class 的实例属性用等式写入类的定义中。下面定义一个 num 实例属性。

```js
class Foo {
  num = 3;
	constructor () {
    console.log(this.num);	// 3
  }
}
```

为了获得更强的可读性，对于在 constructor 里面已经定义的实例属性，新写法允许直接列出。

```js
class Foo {
  name;
  age;
  constructor (name, age) {
    this.name = name;
    this.age = age;
  }
}
```

### Class 的静态属性

Class 的静态属性只要在上面的实例属性写法前面加上 static 关键字就可以了。这个新写法大大方便了静态属性的表达。

```js
// 旧写法
class Foo {}
Foo.prop = 1;

// 新写法
class Foo {
  static prop = 1;
	constructor () {
    console.log(Foo.prop);
  }
}
new Foo();	// 1
```

## new.target 属性

new 是从构造函数生成实例的命令，ES6 为 new 命令引入了 `new.target` 属性。在构造函数中使用返回 new 命令所作用的构造函数；在构造函数外部使用会报错。如果构造函数不是通过 new 命令调用的，那么 new.target 会返回 undefined，因此这个属性可用于确定构造函数是怎么调用的。

```js
function Person (name) {
  if (new.target !== 'undefined') {	// 这里条件也可以写成 new.target === Person
    this.name = name;
  } else {
    throw new Error('必须使用 new 生成实例');
  }
}
var person = new Person('zs');	// 正确
var notAPerson = Person.call(person, 'ls');	// 报错
```

Class 内部调用 new.target，返回当前 Class。

```js
class Test {
  constructor () {
    console.log(new.target === Test);
  }
}
var t = new Test();	// true
```

注意，子类继承父类时 new.target 返回子类。

```js
class Father {
  constructor () {
    console.log(new.target === Father);
  }
}
class Son extends Father {}
var f = new Father();	// true
var s = new Son();	// false
```

利用 new.target 返回子类的特点，可以写出不能独立使用而必须继承后才能使用的类。

```js
class Father {
  constructor () {
    if (new.target === Father) {
      throw new Error('本类不能直接实例化');
    } else {
      console.log('子类正常实例化');
    }
  }
}
class Son extends Father {}
var s = new Son();	// 子类正常实例化
var f = new Father();	// Error: 本类不能直接实例化
```







# 第 20 章		Class 的继承

## 简介

Class 可以通过 extends 关键字实现继承，这比 ES5 通过修改原型链实现继承的方式更加清晰和方便。

```js
class Father {}
class Son extends Father {}
```

子类中可以使用 super 关键字，它在子类的构造函数中表示父类的构造函数，用来新建父类的 this 对象。子类必须在 constructor 方法中调用 super 方法，否则新建实例会报错。这是因为子类其实没有自己的 this 对象，而是继承父类的 this 对象，然后对其进行加工。如果不调用 super 方法，子类就得不到 this 对象。

```js
class Father {}
class Son extends Father {
  constructor () {}
}
let cp = new Son();	// ReferenceError
```

ES5 的继承实质是先创造子类的实例对象 this，然后再将父类的方法添加到 this 上面。ES6 的继承机制完全不同，实质是先创造父类的实例对象 this，所以必须先调用 super 方法，然后再用子类的构造函数修改 this。如果子类没有定义 constructor 方法，那么这个方法会被默认添加，且默认调用 super 方法。也就是说，无论有没有显式定义，任何子类都有 constructor 方法。

```js
class Son extends Father {}
// 等同于
class Son extends Father {
  constructor () {
    super()
  }
}
```

在子类的构造函数中，只有调用 super 之后才可以使用 this 对象，否则会报错。这是因为子类实例的构建是基于对父类实例加工，只有 super 方法才能返回父类实例。

```js
class Father {
  x;
  y;
  constructor (x, y) {
    this.x = x;
    this.y = y;
  }
}
class Son extends Father {
 	z;
  constructor (x, y, z) {
    this.z = z;	// ReferenceError
    super(x, y);
    this.z = z;	// 正确
  }
}
```

子类的实例对象同时是父类和子类的实例，这与 ES5 的行为一致。

```js
var son = new Son();
son instanceof Son;	// true
son instanceof Father;	// true
```

## Object.getPrototypeOf()

Object.getPrototypeOf 方法可以用来从子类上获取父类。因此，可以使用这个方法来判断一个类是否继承了另一个类。

```js
Object.getPrototypeOf(Son) === Father;	// true
```

## super 关键字

super 这个关键字既可以当作函数使用，也可以当作对象使用。使用 super 时，必须显式指定是作为函数还是作为对象使用，否则会报错。

```js
class Father {}
class Son extends Father {
  constructor () {
    super();
    console.log(super);	// 报错
  }
}
```

super 作为函数调用时代表父类的构造函数。ES6 要求，子类的构造函数必须执行一次 super 函数，否则 JavaScript 引擎会报错。super 虽然代表了父类的构造函数，但是返回的是子类的实例，即 super 内部的 this 指向的是子类，因此 super() 相当于 `Father.prototype.constructor.call(this)`。

```js
class Father {
  constructor () {
    console.log(new.target.name);
  }
}
class Son extends Father {}
new Father();	// Father
new Son();	// Son
```

作为函数时，super() 只能在子类的构造函数之中使用，用在其他地方会报错。

```js
class Father {}
class Son {
  test () {
    super();	// 报错
  }
}
```

super 作为对象时在普通方法中指向父类的原型对象；在静态方法中指向父类。无论在普通方法中还是在静态方法中，都无法通过 super 调用定义在父类实例上的方法或属性。

```js
class Father {
  constructor () {
    this.num = 1;
  }
  p () {
    return 2;
  }
}
class Son extends Father {
  constructor () {
    super();
    console.log(super.p());
  }
  m () {
    return super.num;
  }
}
let s = new Son();	// 2
s.m();	// undefined
```

如果属性定义在父类的原型对象上，super 就可以取到。

```js
class Father {}
Father.prototype.num = 2;
class Son extends Father {
  constructor () {
    super();
    console.log(super.num);
  }
}
new Son();	// 2
```

ES6 规定，通过 super 调用父类的方法时，super 会绑定子类的 this。

```js
class Father {
  constructor () {
    this.str = 'father';
  }
  print () {
    console.log(this.str);
  }
}
class Son extends Father {
  constructor () {
    super();
    this.str = 'son';
  }
  m () {
    super.print();	// super.print.call(this)
  }
}
new Son().m();	// son
```

因为 super 绑定子类的 this，所以通过 super 对某个属性赋值，赋值的属性会变成子类实例的属性。

```js
class Father {
  constructor () {
    this.x = 1;
  }
}
class Son extends Father {
  constructor () {
    super();
    this.x = 2;
    super.x = 3;
    console.log(this.x);	// 3
    console.log(super.x);	// undefined
  }
}
new Son();
```

如果 super 作为对象用在静态方法中，这时 super 指向父类，而不是父类的原型对象。

```js
class Father {
  static myMethod (msg) {
    console.log('static', msg);
  }
  myMethod (msg) {
    console.log('instance', msg);
  }
}
class Son extends Father {
  static myMethod (msg) {
    super.myMethod(msg);
  }
  myMethod (msg) {
    super.myMethod(msg);
  }
}
Son.myMethod(1);	// static 1
new Son().myMethod(1);	// instance 1
```

因为对象总是继承其他对象的，对象的顶层对象为 Object，所以可以在任意一个对象中使用 super 关键字。

```js
var obj = {
  toString () {
    return 'MyObejct: ' + super.toString();
  }
}
obj.toString();	// MyObject: [object Object]
```

## 类的 prototype 属性和 \_\_proto\_\_ 属性

在大多数浏览器的 ES5 实现中，每一个对象都具有 \_\_proto\_\_ 属性，指向对应的构造函数的 prototype 属性。Class 作为构造函数的语法糖，同时有 prototype 属性和 \_\_proto\_\_ 属性，因此同时存在两条继承链。

- 子类的 \_\_proto\_\_ 属性表示构造函数的继承，总是指向父类。
- 子类的 prototype 属性的 \_\_proto\_\_ 属性表示方法的继承，总是指向父类的 prototype 属性。

```js
class A {}
class B extends A {}
B.__protp__ === A	// true
B.prototype.__proto__ === A.prototype	// true
```

之所以出现这种情况是因为类的继承是按照下面这种模式实现的。

```js
class A {}
class B {}
// B 的实例继承 A 的实例
Object.setPrototypeOf(B.prototype, A.prototype);
// B 的实例继承 A 的静态属性
Object.setPrototypeOf(B, A);
```

而 Object.setPrototypeOf 方法的实现为下面这样。

```js
Object.setPrototypeOf = function (obj, proto) {
  obj.__proto__ = proto;
  return obj;
}
```

### extends 的继承目标

`extends` 关键字后面可以跟多种类型的值。

```js
class B extends A {}
```

上述代码中，A 只要是一个有 prototype 属性的函数，就能被 B 继承。由于函数都有 prototype 属性（除了 Function.prototype，Function.prototype 可以认为就是普通函数 function () {}），因此 A 可以是任意函数。下面讨论三种特殊情况。

第一种情况，子类继承 Object 类。这种情况下，A 其实是构造函数 Object 的复制，A 的实例就是 Object 的实例。

```js
class A extends Obejct {}
A.__proto__ === Object;	// true
A.prototype.__proto__ === Object.prototype;	// true
```

第二种情况，不存在任何继承。这种情况下，A 作为一个基类，不存在任何继承，就是一个普通函数，所以直接继承 Function.prototype。但是 A 调用后返回一个空对象，即 Object 实例，所以 A.prototype.\_\_proto\_\_ 指向构造函数 Object 的 prototype 属性。

```js
class A {}
A.__proto__ === Function.prototype;	// true
A.prototype.__proto__ === Object.prototype;	// true
```

第三种情况，子类继承 null。这种情况与第二种情况类似，A 也是一个普通函数，所以直接继承 Function.prototype。但是 A 调用后返回的对象不继承任何方法，所以它的 \_\_proto\_\_ 指向 Function.prototype。

```js
class A extends null {}
A.__proto__ === Function.prototype;	// true
A.prototype.__proto__ === undefined;	// true

// 实际上 A 类的 constructor 是这样的
class A extends null {
  constructor () {
    return Object.create(null);
  }
}
```

### 实例的 \_\_proto\_\_ 属性

子类实例的 \_\_proto\_\_ 属性的 \_\_proto\_\_ 属性指向父类实例的 \_\_proto\_\_ 属性。也就是说，子类实例的原型的原型是父类实例的原型。因此，可以通过子类实例的 \_\_proto\_\_.\_\_proto\_\_ 属性修改父类实例的行为。

```js
class Father {}
class Son extends Father {}
var f = new Father()
var s = new Son()
s.__proto__ === f.__proto__	// false
s.__proto__.__proto__ === f.__proto__	// true
s.__proto__.__proto__.printName = function () {
  console.log('zs');
}
f.printName();	// zs
```

## 原生构造函数的继承

原生构造函数是指语言内置的构造函数，通常用来生成数据结构，ECMAScript 的原生构造函数大致有下面这些。

- Boolean()
- Number()
- String()
- Array()
- Date()
- Function()
- RegExp()
- Error()
- Object()

以前，这些原生构造函数是无法继承的，如不能定义 Array 的子类。如果定义了，那么这个子类的行为与 Array 完全不一致。

```js
function MyArray () {
  Array.apply(this, arguments);
}
MyArray.prototype = Object.create(Array.prototype, {
  constructor: {
    value: MyArray,
    writable: true,
    configurable: true,
    enumerable: true
  }
})
var colors = new MyArray();
colors[0] = 'red';
colors.length;	// 0
colors.length = 0;
colors[0];	// red
```

之所以会像上面这样，是因为子类无法获得原生构造函数的内部属性。ES5 先新建子类的实例对象 this，再将父类的属性添加到子类上，由于父类的内部属性无法获取，导致无法继承原生构造函数。比如，Array 构造函数有一个内部属性 `[[DefineOwnProperty]]`，用来定义新属性时更新 length 属性，这个内部属性便无法在子类中获取，所以导致子类的 length 属性行为不正常。

ES6 允许继承原生构造函数定义子类，因为 ES6 先新建父类的实例对象 this，然后再用子类的构造函数修饰 this，使得父类的所有行为都可以继承。extends 不仅可以继承类，还可以继承原生的构造函数。

```js
class MyArray extends Array {
  constructor (...args) {
    super(...args);
  }
}
var arr = new MyArray();
arr[0] = 12;
arr.length;	// 1
arr.length = 0;
arr[0];	// undefined
```

需要注意，继承 Object 的子类有一个行为差异。这是因为 ES6 改变了 Object 构造函数的行为，一旦发现 Object 方法不是通过 `new Object()` 这种形式调用，ES6 规定 Object 构造函数会忽略参数。

```js
class NewObj extends Object {
  constructor () {
    super(...arguments);
  }
}
var o = new NewObj({attr: true});
o.attr === true;	// false
```

## Mixin 模式的实现

Mixin 模式指的是将多个类的接口混入（mix in）另一个类，ES6 实现如下。

```js
function mix (...mixins) {
  class Mix {}
  for (let mixin of mixins) {
    copyProperties(Mix, mixin);
    copyProperties(Mix.prototype, mixin.prototype);
  }
  return Mix;
}
function copyProperties (target, source) {
  for (let key of Reflect.ownKeys(source)) {
    if (key !== 'constructor' && key !== 'prototype' && key !== 'name') {
      let desc = Object.getOwnPropertyDescriptor(source, key);
      Object.defineProperty(target, key, desc);
    }
  }
}
```

上述代码中的 mix 函数可以将多个对象合成为一个类，使用时，只要继承这个方法返回的类即可。

```js
class DistributedEdit extends mix(Loggable, Serializable) {}
```







# 第 21 章		修饰器

## 类的修饰

修饰器是一个函数，用来修改类的行为。ES2017 引入了这项功能，目前 Babel 转码器已经支持。修饰器对类的行为的改变是在代码编译时发生的，而不是在运行时。这意味着修饰器能够在编译阶段运行代码，也就是说修饰器的本质就是编译时执行的函数。

```js
@testable
class MyTestableClass {}
function testable (target) {
  target.isTestable = true;
}
MyTestableClass.isTestable;	// true
```

上面代码中，@testable 就是一个修饰器，它修改了 MyTestableClass 这个类的行为，为它加上了静态属性 isTestable。修饰器的行为基本如下：

```js
@decorator
class A {}
// 等同于
class A {}
A = decorator(A) || A;
```

修饰器函数的第一个参数就是要修饰的目标类。如果觉得一个参数不够用，可以在修饰器外面再封装一层函数。

```js
function testable (isTestable) {
  return function (target) {
    target.isTestable = isTestable;
  }
}
@testable(true)
class MyClass {}
MyClass.isTestable;	// true
```

如果想添加实例属性，可以通过目标类的 prototype 对象进行操作。

```js
function testable (target) {
  target.prototype.isTestable = true;
}
@testable
class MyClass {}
new MyClass().isTestable;	// true
```

实际开发中，React 与 Redux 库结合使用时常常需要写成下面这样。

```js
class MyReactComponent extends React.Component {}
export default connect(mapStateToProps, mapDispatchToProps)(MyReactComponent);
```

有了装饰器，可以改写上面的代码。

```js
@connect(mapStateToProps, mapDispatchToProps)
export default class MyReactComponent extends React.Component {}
```

## 方法的修饰

修饰器不仅可以修饰类，还可以修饰类的属性。修饰类的属性时，修饰器函数一共可以接受 3 个函数，第一个参数是所要修饰的目标对象，第二个参数是所要修饰的属性名，第三个参数是该属性的描述对象。

```js
function decorator (target, name, descriptor) {
  // descriptor 对象原来的值如下
  {
    value: specifiedFunction,
    enumerable: false,
    configurable: true,
    writable: true
  }
}
```

修饰器有注释的作用，如下一眼可以看出 Person 类是可测试的，name 方法是只读且不可枚举的。

```js
@testable
class Person {
  @readonly
  @nonenumerable
  name () {}
}
```

如果同一个方法有多个修饰器，那么该方法会先从外到内进入修饰器，然后由内向外执行。

```js
function dec (id) {
  console.log('evaluated', id);
  return (target, name, descriptor) => console.log('executed', id);
}
class Example {
  @dec(1)
  @dec(2)
 	method () {}
}
// evaluated 1
// evaluated 2
// executed 2
// executed 1
```

除了注释，修饰器还能用来进行类型检查。所以，对于类来说，这项功能相当有用，它将是 JavaScript 代码静态分析的重要工具。

## 为什么修饰器不能用于函数

修饰器只能用于类和类的方法，不能用于函数，因为存在函数提升。

```js
var readOnly = require('some-decorator');
@readOnly
function foo () {}

// 实际上执行的代码如下
var readOnly;
@readOnly
function foo () {}
readOnly = require('some-decorator');
```

总之，由于存在函数提升，修饰器不能用于函数，而类不会提升，所以可以使用修饰器。如果一定要修饰函数，可以采用高阶函数的形式直接执行，高阶函数例子在 p446。

## core-decorators.js

[core-decorators.js](https://github.com/jayphelps/core-decorators.js) 是一个第三方模块，提供了几个常见的修饰器。

### @autobind

autobind 修饰器使得方法中的 this 对象绑定原始对象，也就是绑定类的实例对象。

```js
import { autobind } from 'core-decorators';
class Person {
  @autobind
  getPerson () {
    return this;
  }
}
let person = new Person();
let getPerson = person.getPerson;
getPerson() === person;	// true
```

### @readonly

readonly 修饰器使得属性或方法不可写。

```js
import { readonly } from 'core-decorators';

class Meal {
  @readonly
  entree = 'steak';
}

var dinner = new Meal();
dinner.entree = 'salmon';
// Cannot assign to read only property 'entree' of [object Object]
```

### @override

override 修饰器检查子类的方法是否正确覆盖了父类的同名方法，如果不正确会报错。

```js
import { override } from 'core-decorators';

class Parent {
  speak(first, second) {}
}

class Child extends Parent {
  @override
  speak() {}
  // SyntaxError: Child#speak() does not properly override Parent#speak(first, second)
}

// or

class Child extends Parent {
  @override
  speaks() {}
  // SyntaxError: No descriptor matching Child#speaks() was found on the prototype chain.
  //
  //   Did you mean "speak"?
}
```

### @deprecate（别名 @deprecated）

deprecate 或 deprecated 修饰器在控制台显示一条警告，表示该方法将废除。

```js
import { deprecate } from 'core-decorators';

class Person {
  @deprecate
  facepalm() {}

  @deprecate('We stopped facepalming')
  facepalmHard() {}

  @deprecate('We stopped facepalming', { url: 'http://knowyourmeme.com/memes/facepalm' })
  facepalmHarder() {}
}

let person = new Person();

person.facepalm();
// DEPRECATION Person#facepalm: This function will be removed in future versions.

person.facepalmHard();
// DEPRECATION Person#facepalmHard: We stopped facepalming

person.facepalmHarder();
// DEPRECATION Person#facepalmHarder: We stopped facepalming
//
//     See http://knowyourmeme.com/memes/facepalm for more details.
//
```

### @suppressWarnings

suppressWarnings 修饰器抑制 decorated 修饰器导致的 console.warn() 调用，但异步代码发出的调用除外。

```js
import { suppressWarnings } from 'core-decorators';

class Person {
  @deprecated
  facepalm() {}

  @suppressWarnings
  facepalmWithoutWarning() {
    this.facepalm();
  }
}

let person = new Person();

person.facepalmWithoutWarning();
// no warning is logged
```

## 使用修饰器实现自动发布事件

可以使用修饰器使得对象的方法被调用时自动发出一个事件。详情翻阅 p449。

## Mixin

在修饰器的基础上可以实现 Mixin 模式。所谓 Mixin 模式，就是对象继承的一种替代方案，中文译为混入（mix in），意为在一个对象中混入另一个对象的方法。下面是混入模式的一个简单实现例子。

```js
const Foo = {
  foo () {
    console.log('foo');
  }
}
class MyClass {}
Object.assign(MyClass.prototype, Foo);
new MyClass().foo();	// 'foo'
```

下面，将 Mixin 写成一个修饰器，使用这个修饰器为类混入各种方法。

```js
function mixins (...list) {
  return function (target) {
    Object.assign(target.prototype, ...list);
  }
}
const Foo = {
  foo () { console.log('foo') }
}
@mixins(Foo)
class MyClass {}
new MyClass().foo();	// foo
```

但是上面这种方法会改写 MyClass 类的 prototype 对象，如果不喜欢这一点，可以通过类的继承实现 Mixin。如果 B 继承了 A，但想在 B 里面混入其他方法，其中一个办法是在 B 和 A 之间插入一个混入类，这个类具有想混入的方法，并且继承了 A 的所有方法，然后 B 再继承这个类。且这种写法可以使用 super，避免混入过程中覆盖父类的同名方法。

```js
let Mixin1 = (superclass) => class extends superclass {
  foo () {
    console.log('foo from Mixin1');
    if (super.foo) super.foo();
  }
}
let Mixin2 = (superclass) => class extends superclass {
  foo () {
    console.log('foo from Mixin2');
    if (super.foo) super.foo();
  }
}
class S {
  foo () {
    console.log('foo from S');
  }
}
class C extends Mixin1(Mixin2(S)) {
  foo () {
    console.log('foo from C');
    super.foo();
  }
}
new C().foo();
// foo from C
// foo from Mixin1
// foo from Mixin2
// foo from S
```

## Trait

Trait 也是一种修饰器，效果与 Mixin 类似，但是提供了更多功能，比如防止同名方法的冲突，排除混入某些方法，为混入的方法起别名等。下面以 [traits-decorator](https://github.com/CocktailJS/traits-decorator) 这个第三方模块为例进行说明。这个模块提供的 traits 修饰器不仅可以接受对象，还可以接受 ES6 类作为参数。

```js
import { traits } from 'traits-decorator';
class TFoo {
  foo () { console.log('foo') }
}
const TBar = {
  bar () { console.log('bar') }
}
@traits(TFoo, TBar)
class MyClass {}
let obj = new MyClass();
obj.foo();	// foo
obj.bar();	// bar
```

Trait 不允许混入同名方法。

```js
import { traits } from 'traits-decorator';
class TFoo {
  foo () { console.log('foo') }
}
const TBar = {
  bar () { console.log('bar') }
  foo () { console.log('foo') }
}
@traits(TFoo, TBar)
class MyClass {}	// 报错
```

一种解决方法是排除 TBar 的 foo 方法。

```js
import { traits } from 'traits-decorator';
class TFoo {
  foo () { console.log('foo') }
}
const TBar = {
  bar () { console.log('bar') }
  foo () { console.log('foo') }
}
@traits(TFoo, TBar::excludes('foo'))
class MyClass {}
let obj = new MyClass();
obj.foo();	// foo
obj.bar();	// bar
```

使用绑定运算符 :: 在 TBar 上排除了 foo 方法，混入就不会报错了。另一种方法是为 TBar 的 foo 方法取别名。

```js
import { traits } from 'traits-decorator';
class TFoo {
  foo () { console.log('foo') }
}
const TBar = {
  bar () { console.log('bar') }
  foo () { console.log('TBar foo') }
}
@traits(TFoo, TBar::alias({foo: 'aliasFoo'}))
class MyClass {}
let obj = new MyClass();
obj.foo();	// foo
obj.aliasFoo();	// TBar foo
```

alias 和 excludes 方法可以结合起来使用。也可以用 as 方法统一起来。

```js
@traits(TExample::as({ excludes: ['foo', 'bar'], alias: { baz: 'exampleBaz' } }))
class MyClass {}
```

## Babel 转码器的支持

目前，Babel 转码器已经支持 Decorator。首先安装 babel-core 和 babel-plugin-transform-decorators。由于后者包括在 babel-preset-stage-0 之中，所以改为安装 babel-preset-stage-0 亦可。

```shell
npm install babel-core babel-plugin-transform-decorators
```

然后，设置配置文件 .babelrc。

```json
{
  "plugins": ["transform-decorators"]
}
```

这时 Babel 就可以对 Decorator 进行转码了。脚本中打开的命令如下。

```js
babel.transform('code', { plugins: ['transform-decorators']})
```

Babel 的官网提供一个[在线转码器](https://babeljs.io/repl)，只要勾选 `Experimental`，就能支持 Decorator 的在线转码。







# 第 22 章		Module 的语法

## 概述

JavaScript 一直没有模块 module 体系，无法将一个大程序拆分成互相依赖的小文件，再用简单的方法将它们拼装起来。

在 ES6 之前，社区制定了一些模块加载方案，最主要的有 CommonJS 和 AMD 两种。前者用于服务器，后者用于浏览器。ES6 在语言规格的层面上实现了模块功能，而且实现的相当简单，完全可以取代现有的 CommonJS 和 AMD 规范，成为浏览器和服务器通用的模块解决方案。

ES6 模块的设计思想是尽量静态化，使得编译时就能确定模块的依赖关系，以及输入和输出的变量。CommonJS 和 AMD 模块都只能在运行时确定这些东西。比如 CommonJS 模块就是对象，输入时必须查找对象属性。

```js
// CommonJS 模块
let { stat, exists, readFile } = require('fs');

// 等同于
let _fs = require('fs');
let stat = _fs.stat;
let exists = _fs.exists;
let readFile = _fs.readFile;
```

上面代码的实质是整体加载 fs 模块，即加载 fs 的所有方法，生成一个对象 _fs，然后再从这个对象上面读取 3 个方法。这种加载称为`运行时加载`，因为只有运行时才能得到这个对象，导致完全没有办法在编译时进行`静态优化`。

ES6 模块不是对象，而是通过 `export` 命令显式指定输出的代码，再通过 `import` 命令输入。

```js
// ES6 模块
import { stat, exists, readFile } from 'fs';
```

上面的代码的实质是从 fs 模块加载 3 个方法，而不加载其他方法。这种加载称为`编译时加载`或者静态加载，即 ES6 可以在编译时就完成模块加载，效率比 CommonJS 模块的加载方式高。当然，这也导致了 ES6 模块本身无法被引用，因为它不是对象。

由于 ES6 模块是编译时加载，使得静态分析成为可能。有了它就能进一步拓展 JavaScript 的语法，比如引入宏 macro 和类型检验 type system 这些只能靠静态分析实现的功能。

除了静态加载带来的各种好处，ES6 模块还有以下好处。

- 不再需要 UMD 模块格式，将来服务器和浏览器都会支持 ES6 模块格式。
- 将来浏览器的新 API 可以用模块格式提供，不再需要做成全局变量或者 navigator 对象的属性。
- 不再需要对象作为命名空间，未来这些功能可以通过模块来提供。

## 严格模式

ES6 的模块自动采用严格模式，不管有没有在模块头部加上 `use strict`。严格模式主要有以下限制。

- 变量必须声明后再使用。
- 函数的参数不能有同名参数，否则报错。
- 不能使用 with 语句。
- 不能对只读属性赋值，否则报错。
- 不能使用前缀 0 表示八进制数，否则报错。
- 不能删除不可删除的属性，否则报错。
- 不能删除变量 delete prop，会报错。只能删除属性 delete global[prop]。
- eval 不会在它的外层作用域引入变量。
- eval 和 arguments 不能被重新赋值。
- arguments 不会自动反映函数参数的变化。
- 不能使用 arguments.callee。
- 不能使用 arguments.caller。
- 禁止 this 指向全局对象。
- 不能使用 fn.caller 和 fn.arguments 获取函数调用的堆栈。
- 增加了保留字，如 protected、static 和 interface。

尤其需要注意 this 的限制。在 ES6 模块中，顶层的 this 指向 undefined，即不应该在顶层代码中使用 this。

## export 命令

模块功能主要由两个命令构成：export 和 import。export 命令用于规定模块的对外接口，import 命令用于输入其他模块提供的接口。

一个模块就是一个独立的文件。该文件内部的所有变量，外部无法获取。如果希望外部能够读取模块内部的某个变量，就必须使用 export 关键字输出该变量。

```js
// profile.js
export var firstName = 'zs';
export var lastName = 'ls';
```

上面的代码是一个单独的文件 profile.js，ES6 将其视为一个模块，里面使用 export 命令对外输出 2 个变量。export 的写法除了像上面这样，还有另一种写法：使用大括号指定所要输出的一组变量。这种写法与前一种写法直接放置在 var 语句前是等价的，但应该优先考虑这种写法。因为这样就可以在脚本尾部一眼看清输出了哪些变量。

```js
// profile.js
var firstName = 'zs';
var lastName = 'ls';
export { firstName, lastName };
```

export 命令除了输出变量，还可以输出函数或类。

```js
export function fn () {}
```

通常情况下，export 命令输出的变量就是本来的名字，但是可以使用 `as` 关键字重命名。

```js
function v1 () {}
function v2 () {}
export {
	v1 as newV1,
  v2 as newV2
}
```

需要特别注意的是，export 命令规定的是对外的接口，必须与模块内部的变量建立一一对应的关系。

```js
// 报错
export 1;

// 报错
var m = 1;
export m;
```

如果没有提供对外的接口，而是直接输出 1 或通过变量 m 直接输出 1，都会报错。上面的代码中，1 只是一个值，而不是接口。正确的写法应该是下面这样。

```js
// 写法一
export var m = 1;

// 写法二
var m = 1;
export { m };

// 写法三
var n = 1;
export { n as m };
```

上面三种写法都是正确的，它们规定了对外的接口 m。其他脚本可以通过这个接口取到值 1。它们的实质是，在接口名与模块内部变量之间建立了一一对应的关系。同样的，function 和 class 的输出也必须遵守这样的写法。

```js
// 报错
function f () {}
export f;

// 正确
function f () {}
export { f };

// 正确
export function f () {};
```

export 语句输出的接口与其对应的值是动态绑定关系，即通过接口可以取到模块内部实时的值。这一点与 CommonJS 规范完全不同，CommonJS 模块输出的是值的缓存，不存在动态更新，详见下一章。

```js
export var foo = 'bar';
setTimeout(() => foo = 'baz', 500);
```

上面的代码 export 输出变量 foo，值为 bar，500ms 后变为 baz。

export 命令可以出现在模块的任何位置，只要处于模块顶层就可以。如果处于块级作用域内，就会报错，下一节的 import 命令也是。这是因为处于条件代码块中，就没法做静态优化了，违背了 ES6 模块的设计初衷。

```js
function foo () {
  export default 'bar';	// SyntaxError
}
foo()
```

## import 命令

使用 export 命令定义了模块的对外接口以后，其他 JS 文件就可以通过 import 命令加载这个模块了。import 命令接受一个对象，用大括号表示，里面指定要从其他模块导入的变量名。大括号中的变量名必须与被导入模块对外接口的名称相同。

```js
// main.js
import { firstName, lastName } from './profile';
console.log(firstName, lastName);
```

如果想为输入的变量重命名，可以在 import 命令中使用 as 关键字，将输入的变量重命名。

```js
import { lastName as secondName } from './profile';
```

import 后面的 from 用于指定模块文件的位置，可以是相对路径，也可以是绝对路径，.js 后缀可以省略。如果只是模块名，不带有路径，那么必须有配置文件告诉 JavaScript 引擎该模块的位置。

```js
import { myMethod } from 'util';
```

注意，import 命令具有提升效果，会提升到整个模块的头部并首先执行。这种行为的本质是，import 命令是编译阶段执行的，在代码运行之前。

```js
foo();	// 正常执行
import { foo } from 'my_module';
```

因为 import 是静态执行，所以不能使用表达式和变量等只有在运行时才能得到结果的语法结构。

```js
// 报错
import { 'f' + 'oo' } from 'my_module';

// 报错
let module = 'my_module';
import { foo } from module;

// 报错
if (x === 1) {
  import { foo } from 'module'
}
```

import 语句会执行所加载的模块，所以可以有下面这种用法。

```js
import 'someJS';	// 加载并执行 someJS.js 文件，不会输入任何值
```

如果多次重复执行同一句 import 语句，那么只会执行一次，而不会执行多次。

```js
import 'someJS';
import 'someJS';
```

上面的代码加载了两次 someJS，但只会执行一次。

```js
import { foo } from 'my_module';
import { bar } from 'my_module';

// 等同于
import { foo, bar } from 'my_module';
```

虽然 foo 和 bar 在两个语句中加载，但是它们对应的是同一个 my_module 实例。也就是说，import 语句是 [Singleton](# 实例：模块的 Singleton 模式) 模式。

CommonJS 模块的 require 和 ES6 模块的 import 命令可以写在同一个模块里面，但是最好不要这样做。因为 import 在静态解析阶段执行，所以它是一个模块之中最早被执行的。

## 模块的整体加载

除了指定加载某个输出值，还可以使用星号 * 来整体加载进一个对象，所有的输出值都加载在这个对象上。

```js
// circle.js
export function area (radius) {}
export function circumference (radius) {}

// main.js
import { area, circumference } from './circle'
```

上面的写法将逐一指定要加载的方法，整体加载写法如下：

```js
// main.js
import * as circle from './circle';
console.log('圆面积', circle.area(4));
```

注意，模块整体加载的对象（上例是 circle）应该是可以静态分析的，所以不允许运行时改变。

```js
import * as circle from './circle';
// 不允许修改 circle
circle.foo = 'hello';
```

## export default 命令

从前面的例子可以看出，使用 import 命令时需要用户知道所要加载的变量名或函数名，否则无法加载。为了方便用户，使其不用阅读文档就能加载模块，可以使用 `export default` 命令为模块指定默认输出。指定默认输出后，其他模块加载时可以用 import 命令为该默认输出指定任意名字。import 输入 export default 定义的默认输出时，后面的变量名不用加大括号。

```js
// export-default.js
export default function foo () {	// 这里的函数名 foo 在模块外部无效，加载时视为匿名函数
  console.log('foo');
}
// import-default.js
import customName from './export-default';
customName();	// foo
```

下面比较默认输出和正常输出，可以发现默认输出不需要 import 后加大括号。

```js
// 默认
export default function crc32 () {}
import crc32 from 'crc32';

// 正常
export function crc32 () {}
import { crc32 } from 'crc32'
```

export default 命令用于指定模块的默认输出。显然，一个模块只能有一个默认输出，因此 export default 命令只能使用一次。这也是 import 命令后面不用加大括号的原因，因为只可能对应加载模块中的一个方法。

本质上，export default 就是输出一个叫做 default 的变量或方法，然后系统允许我们为它取任意名字。所以下面的写法是有效的。

```js
// module.js
function add () {}
export { add as default }
// 等同于
export default add

// app.js
import { default as add } from 'module';
// 等同于
import add from 'module'
```

也正是因为 export default 命令其实只是输出一个叫做 default 的变量，所以它后面不能跟变量声明语句。

```js
// 正确
export var a = 1;

// 错误
export default var a = 1;

// 正确
var a = 1;
export default a;

// 错误
var a = 1;
export a;
```

同样，因为 export default 本质是将该命令后面的值赋给 default 变量以后再默认，所以可以直接将一个值写在 export default 后。

```js
// 正确
export default 2

// 报错
export 2
```

如果想在一条 import 语句中同时输入默认方法和其他接口，可以这样写。

```js
import defaultProp, { prop1, prop2 } from 'export'
```

export default 也可以用来输出类。

```js
// MyClass.js
export default class {}

// main.js
import MyClass from 'MyClass';
let o = new MyClass();
```

## export 与 import 的复合写法

如果在一个模块之中先输入后输出同一个模块，import 语句和 export 语句可以写在一起。

```js
import { foo, bar } from 'my_module';
export { foo, bar }
// 等同于
export { foo, bar } from 'my_module'
```

默认接口的写法如下。

```js
export { default } from 'foo'
```

## 模块的继承

模块之间也可以继承。

```js
// circleplus.js
export * from 'circle';
export var e = 2.7;
export default function () {}
```

上面的 export * 表示输出 circle 模块的所有属性和方法。注意，export * 命令会忽略 circle 模块的 export default 接口。

## 跨模块常量

前面介绍 const 命令的时候说过，const 声明的常量只在当前代码块内有效。如果想设置跨模块的常量，可以采用下面的写法。

```js
// constants.js 模块
export const A = 1;
export const B = 2;

// test1.js
import * as constants from './constants';
console.log(constatns.A);	// 1

// test2.js
import { B } from './constants';
console.log(B);	// 2
```

## import()

### 简介

import 命令会被 JavaScript 引擎静态分析，这样设计有利于编译器提高效率，但也导致无法在运行时加载模块。如果 import 命令想要取代 Node 的 require 方法，这就形成了一个障碍。因为 require 是运行时加载模块，import 命令无法取代 require 的动态加载功能。

```js
const path = '/'
const myModule = require(path);
```

上面的语句是动态加载，require 到底加载哪个模块只有运行时才能知道。import 语句做不到这一点。因此有一个提案建议引入 `import()` 函数，完成动态加载。import 命令能够接受什么参数，import() 函数就能接受什么参数，两者的区别主要是后者为动态加载。且 import() 返回一个 Promise 对象。

```js
const path = './test'
import(path).then()
```

import() 函数可以用在任何地方，不像 import 与 export 只能在模块的顶层。import() 函数与所加载的模块没有静态连接关系，这点也与 import 语句不相同。import() 类似 Node 的 require 方法，区别主要是前者为异步加载，后者为同步加载。

### 适用场合

#### 按需加载

import() 可以在需要的时候再加载某个模块。

#### 条件加载

import() 可以放在 if 代码块中，根据不同的情况加载不同的模块。

#### 动态的模块路径

import() 允许模块路径动态生成。

### 注意点

import() 加载模块成功后，这个模块的输出会封装为一个对象当作 then 方法的参数，因此可以使用对象解构赋值的语法来获取输出接口。

```js
import('./myModule.js')
.then(({export1, export2}) => {})
```

如果模块有 default 输出接口，可以用参数直接获得，也可以用具名输入的形式。

```js
import('./myModule.js')
.then(myModule => {
  console.log(myModule.default);
})

// 具名输入
import('./myModule.js')
.then(({ default: theDefault }) => {
  console.log(theDefault);
})
```







# 第 23 章		Module 的加载实现

## 浏览器加载

### 传统方法

默认情况下，浏览器同步加载 JavaScript 脚本，即渲染引擎遇到 `<script>` 标签就会停下来，等到脚本执行完毕再继续向下渲染。如果是外部脚本，还必须加入脚本下载的时间。如果脚本体积大，下载和执行的时间就会很长，因此造成浏览器堵塞，页面卡死。这是很不好的体验，所以浏览器允许脚本异步加载，下面是两种异步加载的语法。

```html
<script src="" defer></script>
<script src="" async></script>
```

script 标签打开 `defer` 或 `async` 属性，脚本就会异步加载。渲染引擎遇到这一行命令就会开始下载外部脚本，不会等它下载和执行，而是直接执行后面的命令。defer 与 async 的区别是，前者要等到整个页面正常渲染结束才会执行，即渲染完再执行；后者一旦下载完成，渲染引擎就会中断渲染，执行这个脚本以后再继续渲染，即下载完就执行。如果有多个 defer 脚本，会按照它们在页面出现的顺序加载，而多个 async 脚本是不能保证加载顺序的。

### 加载规则

浏览器加载 ES6 模块时也使用 `<script>` 标签，但是要加入 `type="module"` 属性。

```html
<script type="module" src="foo.js"></script>
```

对于带有 type="module" 的 script 标签，浏览器知道这是一个 ES6 模块，所以会异步加载，不会造成浏览器堵塞。即等到整个页面渲染完再执行模块脚本，等同于打开了 script 标签的 defer 属性。如果显式声明了打开 script 标签的 async 属性，那么这时只要加载完成，渲染引擎就会中断渲染立即执行，执行完成后再恢复渲染。

对于外部的模块脚本，像上面的 foo.js，需要注意此时 foo.js 模块中如果使用 import 命令加载其他模块，那么 `.js` 不可省略。

ES6 模块也允许内嵌在网页中，语法行为与加载外部脚本完全一致。

```html
<script type="module">
	import utils from './utils.js'
</script>
```

## ES6 模块与 CommonJS 模块的差异

具体的两大差异如下：

- CommonJS 模块是运行时加载，ES6 模块是编译时输出接口。

  因为 CommonJS 加载的是一个对象，即 `module.exports` 属性，该对象只有在脚本运行结束时才会生成。而 ES6 模块不是对象，它的对外接口只是一种静态定义，在代码静态解析阶段就会生成。

- CommonJS 模块输出的是一个值的复制，ES6 模块输出的是值的引用。

  CommonJS 模块输出的是值的复制，也就是说一旦输出一个值，模块内部的变化就影响不到这个值。

  ```js
  // lib.js
  var counter = 3;
  function incCounter () {
    counter++;
  }
  module.exports = {
    counter: counter,
    incCounter: incCounter
  }
  
  // main.js
  var mod = require('./lib');
  console.log(mod.counter);	// 3
  mod.incCounter();
  console.log(mod.counter);	// 3
  ```

  上面的代码说明，lib.js 模块加载以后，它的内部变化就影响不到输出的 mod.counter 了。这是因为 mod.counter 是一个原始类型的值，会被缓存。如果写成一个函数，如取值器函数，就可以正确读取内部变量 counter 变动后的值了。

  ```js
  // lib.js
  var counter = 3;
  function incCounter () {
    counter++;
  }
  module.exports = {
    get counter () {
      return counter;
    },
    incCounter: incCounter
  }
  
  // main.js
  var mod = require('./lib');
  console.log(mod.counter);	// 3
  mod.incCounter();
  console.log(mod.counter);	// 4
  ```

  ES6 模块的运行机制与 CommonJS 不一样。JS 引擎对脚本静态分析的时候，遇到模块加载命令 import 会生成一个只读引用。等到脚本真正执行时，再根据这个只读引用到被加载的模块中取值。也就是说被加载模块的原始值改变，import 加载的值也会跟着变。因此，ES6 模块是动态引用，并且不会缓存值，模块里面的变量绑定其所在的模块。

  ```js
  // lib.js
  export let counter = 3;
  export function incCounter () {
    counter++;
  }
  
  // main.js
  import { counter, incCounter } from './lib';
  console.log(counter);	// 3
  incCounter();
  console.log(counter);	// 4
  ```

  上面的代码表明，ES6 模块不会缓存运行结果，而是动态地去被加载的模块取值，并且变量总是绑定其所在的模块。由于 ES6 输入的模块变量只是一个只读引用，所以对它重新赋值会报错。

  ```js
  // lib.js
  export let obj = {}
  
  // main.js
  import { obj } from './lib';
  obj.prop = 123;	// OK
  obj = {};	// TypeError
  ```

  注意 export 通过接口输出的是同一个值，不同的脚本加载这个接口得到的是同一个实例。

  ```js
  // mod.js
  function C () {
    this.sum = 0;
    this.add = function () {
      this.sum += 1;
    }
    this.show = function () {
      console.log(this.sum);
    }
  }
  export let c = new C();
  
  // x.js
  import { c } from './mod';
  c.add();
  
  // y.js
  import { c } from './mod';
  c.show();
  
  // main.js
  import './x';
  import './y';	// 1
  ```

## Node 加载

### 概述

Node 对 ES6 模块的处理比较麻烦，因为它有自己的 CommonJS 模块格式，与 ES6 模块格式是不兼容的。

在静态分析阶段，一个模块脚本只要有一行 import 或 export 语句，Node 就会认为该脚本为 ES6 模块，否则为 CommonJS 模块。如果不输出任何接口但希望 Node 认为这是 ES6 模块，可以在脚本加上如下语句。这条命令并不是输出一个空对象，而是不输出任何接口的 ES6 标准写法。

```js
export {};
```

如果不指定绝对路径以及后缀名，Node 加载 ES6 模块会依次寻找以下脚本，与 require() 的规则一致。

```js
import './foo';
// 依次寻找
// ./foo.js
// ./foo/package.json
// ./foo/index.js

import 'baz';
// 依次寻找
// ./node_modules/baz.js
// ./node_modules/baz/package.json
// ./node_modules/baz/index.js
// 如果同级目录没有找到，则继续向上级目录依次寻找
// ../node_modules/baz.js
// ../node_modules/baz/package.json
// ../node_modules/baz/index.js
// 如果没有再继续往上，直到找到为止，如果没有则报错
```

### import 命令加载 CommonJS 模块

Node 采用 CommonJS 模块格式，模块的输出都定义在 `module.exports` 属性上面。在 Node 环境中，使用 import 命令这种 ES6 方式加载 CommonJS 模块，Node 会自动将 module.exports 属性当作模块的默认输出，即等同于 export default。

```js
// a.js
module.exports = {
  foo: 'hello',
  bar: 'world'
}
// 等同于
export default {
  foo: 'hello',
  bar: 'world'
}

// b.js
// 写法一
import baz from './a'
// baz = { foo: 'hello', bar: 'world' }

// 写法二
import { default as baz } from './a'
// baz = { foo: 'hello', bar: 'world' }

// 写法三
import * as baz from './a'
// baz = {
//   get default () { return module.exports },
//   get foo () { return this.default.foo }.bind(baz),
//   get bar () { return this.default.bar }.bind(baz)
// }
```

如果采用整体输入的写法 `import * as xxx from someModule`，default 会取代 module.exports 作为输入的接口。像上面的第三种写法，baz.default 取代了 module.exports。Node 会自动为 baz 添加 default 属性，通过 baz.default 获取 module.exports。

```js
// c.js
module.exports = function two () {
  return 2;
}

// es.js
import foo from './c';
foo();	// 2

import * as foo from './c';
foo.default();	// 2
foo();	// throw, foo is not a function
```

CommonJS 模块的输出缓存机制在 ES6 加载方式下依然有效。对于加载以下模块的脚本，module.exports 将一直是 123，不会变成 null。

```js
module.exports = 123;
setTimeout(() => module.exports = null);
```

由于 ES6 模块是编译时确定输出接口，CommonJS 模块是运行时确定输出接口，所以采用 import 命令加载 CommonJS 模块时，不允许采用下面这种写法。

```js
import { readFile } from 'fs';
```

上面的写法不正确，因为 fs 是 CommonJS 格式，只有在运行时才能确定 readFile 接口，而 import 命令要求编译时就确定这个接口。解决方法是改为整体输入。

### require 命令加载 ES6 模块

采用 require 命令加载 ES6 模块时，ES6 模块的所有输出接口都会成为输入对象的属性。另外，require 命令是 CommonJS 模块的命令，所以缓存机制依旧存在。

```js
// es.js
let foo = { bar: 'hello' };
export default foo;
foo = null;

// cjs.js
const es_namespace = require('./es');
console.log(es_namespace.default);	// { bar: 'hello' }
```

```js
// es.js
export let foo = { bar: 'hello' };
export { foo as bar };
export function f () {};
export class c {};

// cjs.js
const es_namespace = require('./es');
// es_namespace = {
//   get foo () { return foo }
//   get bar () { return foo }
// 	 get f () { return f }
// 	 get c () { return c }
// }
```

## 循环加载

循环加载指的是，a 脚本的执行依赖 b 脚本，而 b 脚本的执行有依赖 a 脚本。通常，循环加载表示存在强耦合，如果处理不好还可能导致递归加载，使得程序无法正常执行，因此应该避免出现这种现象。但是在依赖关系复杂的大项目中很容易出现 a 依赖 b，b 依赖 c，c 又依赖 a 这样的情况。这意味着模块加载机制必须考虑循环加载的情况。对于 JavaScript 语言来说，CommonJS 和 ES6 在处理循环加载时的方法是不一样的，返回的结果也不一样。

```js
// a.js
var b = require('b');

// b.js
var a = require('a');
```

### CommonJS 模块的加载原理

CommonJS 的一个模块就是一个脚本文件。require 命令第一次加载该脚本时就会执行整个脚本，然后在内存中生成一个对象。对象的 id 属性是模块名，exports 属性是模块输出的各个接口，loaded 属性是一个布尔值，表示该模块的脚本是否执行完毕，其他还有很多属性。

CommonJS 模块无论加载多少次，都只会在第一次加载时运行一次，以后再加载时就返回第一次运行的结果（到 exports 属性上面取值），除非手动清除系统缓存。

### CommonJS 模块的循环加载

CommonJS 模块的重要特性是加载时执行，即脚本代码在 require 的时候就会全部执行。一旦出现某个模块被循环加载，就只输出已经执行的部分，还未执行的部分不会输出。

```js
// a.js
exports.done = false;
var b = require('./b.js');
console.log('在 a.js 中，b.done = ', b.done);
exports.done = true;
console.log('a.js 执行完毕');

// b.js
exports.done = false;
var a = require('./a.js');
console.log('在 b.js 中，a.done = ', a.done);
exports.done = true;
console.log('b.js 执行完毕');

// main.js
var a = require('./a.js');
var b = require('./b.js');
console.log('在 main.js 之中，a.done=%j, b.done=%j', a.done, b.done);
// 在 b.js 中，a.done = false
// b.js 执行完毕
// 在 a.js 中，b.done = true
// a.js 执行完毕
// 在 main.js 之中，a.done=true, b.done=true
```

### ES6 模块的循环加载

ES6 处理循环加载与 CommonJS 有本质的不同。ES6 模块是动态引用，如果使用 import 从一个模块中加载变量，那么变量不会被缓存，而是成为一个指向被加载模块的引用，需要开发者保证在真正取值的时候能够取到值。

```js
// a.js
import { bar } from './b.js';
console.log('a.js');
console.log(bar);
export let foo = 'foo';

// b.js
import { foo } from './a.js';
console.log('b.js');
console.log(foo);
export let bar = 'bar';

// 执行 a.js
// b.js
// undefined
// a.js
// bar
```

上面的代码中，因为 a.js 的第一行是加载 b.js，所以执行 a.js 时，就会先执行 b.js。而 b.js 的第一行又是加载 a.js，由于 a.js 已经开始执行，所以不会重复执行，而是继续执行 b.js，所以第一行输出 b.js，后面输出 foo 变量时由于 a.js 还没有执行到输出变量的代码，所以 foo 变量为 undefined。

```js
// a.js
import { bar } from './b.js';
export function foo () {
  console.log('foo');
  bar();
  console.log('over');
}
foo();

// b.js
import { foo } from './a.js';
export function bar () {
  console.log('bar');
  if (Math.random() > 0.5) {
    foo();
  }
}
```

上面的代码按照 CommonJS 规范，是无法执行的。这是因为 a 先加载 b，然后 b 又加载 a，这时 a 还没有任何执行结果，所以输出结果为 null，即对于 b.js 来说，变量 foo 的值等于 null，后面随机数大于 0.5 时执行 foo() 就会报错。究其原因，也是因为 CommonJS 模块是加载时执行，其中的变量会缓存。而 ES6 加载的变量都是动态引用其所在的模块，只要引用存在，代码就能执行。所以 ES6 可以执行上面的代码。

```shell
node a.js
# foo
# bar
# over 假设随机数小于 0.5
```

## ES6 模块的转码

浏览器目前还不支持 ES6 模块（[case](code\第23章-Module的加载实现\23.5-ES6模块的转码\test.html)），为了实现立刻使用，我们可以将其转为 ES5 的写法。除了 Babel 可以用来转码，还有以下两个方法也可以用来转码。

### ES6 module transpiler

[ES6 module transpiler](https://github.com/esnext/es6-module-transpiler) 是 square 公司开源的一个转码器，可以将 ES6 模块转为 CommonJS 模块或 AMD 模块，从而在浏览器中使用。首先，安装这个转码器。

```shell
npm install -g es6-module-transpiler
```

然后，使用 `compile-modules convert` 命令将 ES6 模块文件转码。

```shell
compile-modules convert file1.js file2.js
```

`-o` 参数可以指定转码后的文件名。

```shell
compile-modules convert -o out.js file1.js
```

### SystemJS

[SystemJS](https://github.com/systemjs/systemjs) 是一个垫片库 polyfill，可以在浏览器内加载 ES6 模块、AMD 模块和 CommonJS 模块，将其转为 ES5 格式。它在后台调用的是 Google 的 Traceur 转码器。使用时，先在网页上载入 `system.js` 文件；然后使用 `System.import` 方法加载模块文件。

```html
<script src="system.js"></script>
<script>
	System.import('./app.js')
</script>
```

System.import 使用异步加载，返回一个 Promise 对象，可以针对这个对象编程。

```js
// app/es6-file.js
export class q {
  constructor () {
   	this.es6 = 'hello';
  }
}
```

```html
<script src="system.js"></script>
<script>
	System.import('app/es6-file').then(m => {
    console.log(new m.q().es6);	// hello
  })
</script>
```







# 第 24 章		编程风格

有多家公司和组织已经公开其风格规范，具体可参阅 [风格规范](http://jscs.info/)，下面的内容主要参考 [Airbnb](https://github.com/airbnb/javascript) 的 JavaScript 风格规范。

## 块级作用域

### let 取代 var

ES6 提出了两个新的声明变量的命令：let 和 const。其中 let 完全可以取代 var，因为两者语义相同，且 let 没有副作用。变量应该只在其声明的代码块内有效，var 命令做不到这一点。var 命令存在变量提升效用，let 命令没有这个问题。

### 全局常量和线程安全

在 let 和 const 之间，建议优先使用 const，尤其在全局环境中，不应该设置变量，只应设置常量。const 优于 let 有以下几个原因。

- const 可以提醒阅读程序的人，这个变量不应该改变。
- const 比较符合函数式编程思想，运算不改变值，只是新建值，而且这样也有利于将来的分布式计算。
- JavaScript 编译器会对 const 进行优化，所以多使用 const 有利于提高程序的运行效率，也就是说 let 和 const 的本质区别其实是编译器内部的处理不同。

```js
// bad
var a = 1, b = 2, c = 3;

// good
const a = 1;
const b = 2;
const c = 3;

// best
const [a, b, c] = [1, 2, 3];
```

所有的函数都应该设置为常量。

长远来看，JavaScript 可能会有多线程的实现，这时 let 表示的变量只应出现在单线程运行的代码中，不能是多线程共享的，这样有利于保证线程安全。

## 字符串

静态字符串一律使用单引号或反引号，不使用双引号。动态字符串使用反引号。

```js
// bad
const a = "foobar";
const b = 'foo' + a + 'bar';

// acceptable 可接受的
const c = `foobar`;

// good
const a = 'foobar';
const b = `foo${a}bar`;
```

## 解构赋值

使用数组成员对变量赋值时，优先使用解构赋值。

```js
const arr = [1, 2, 3, 4];

// bad
const first = arr[0];
const second = arr[1];

// good
const [first, , third] = arr;
```

函数的参数如果是对象的成员，优先使用解构赋值。

```js
// bad
function getFullName (user) {
  const firstName = user.firstName;
  const lastName = user.lastName;
}

// good
function getFullName (obj) {
  const { firstName, lastName } = obj;
}

// best
function getFullName ({ firstName, lastName }) {}
```

如果函数返回多个值，优先使用对象的解构赋值，而不是数组的解构赋值。这样便于以后添加返回值，且不用更改返回值的顺序。

```js
// bad
function processInput (input) {
  return [left, right, top, bottom];
}

// good
function processInput (input) {
  return { left, right, top, bottom };
}

const [left, , , bottom] = processInput(input);
const { left, bottom } = processInput(input);
```

## 对象

单行定义的对象，最后一个成员不以逗号结尾。多行定义的对象，最后一个成员以逗号结尾。

```js
// bad
const a = { k1: v1, k2: v2, };
const b = {
  k1: v1,
  k2: v2
}

// good
const a = { k1: v1, k2: v2 };
const b = {
  k1: v1,
  k2: v2,
}
```

对象尽量静态化，一旦定义，就不得随意添加新的属性。如果不可避免的需要添加属性，要使用 Object.assign 方法。

```js
// bad
const a = {}
a.x = 3;

// if reshape unavoidable	如果重塑不可避免
const a = {};
Object.assign(a, { x: 3 });

// good
const a = { x: null };
a.x = 3;
```

如果对象的属性名是动态的，可以在创造对象的时候使用属性表达式定义。尽量把所有属性定义在一个地方。

```js
// bad
const obj = {
  id: 5,
}
obj[getKey('name')] = 'zs';

// good
const obj = {
  id: 5,
  [getKey('name')]: 'zs'
}
```

对象的属性和方法尽量采用简介表达法，这样易于描述和书写。

```js
var ref = 'some value';

// bad
const atom = {
  ref: ref,
  value: 1,
  addValue: function (value) {
    return atom.value + value;
  },
}

// good
const atom = {
  ref,
  value: 1,
  addValue (value) {
    return atom.value + value
  },
}
```

## 数组

使用扩展运算符 `...` 复制数组。

```js
// bad
const len = items.length;
const itemsCopy = [];
let i;

for (i = 0; i < len; i++) {
  itemsCopy[i] = items[i];
}

// good
const itemsCopy = [...items];
```

使用 `Array.from` 方法将类似数组的对象转为数组。

```js
const foo = document.querySelectorAll('.foo');
const nodes = Array.from(foo);
```

## 函数

立即执行函数可以写成箭头函数的形式。

```js
(() => {
  console.log('hi');
})();
```

需要使用函数表达式的场合，尽量使用箭头函数代替。因为这样更简洁，且绑定了 this。

```js
// bad
[1, 2, 3].map(function (x) {
  return x * x;
})

// good
[1, 2, 3].map((x) => {
  return x * x;
})

// best
[1, 2, 3].map(x => x * x);
```

箭头函数取代 `Function.prototype.bind`，不需要再绑定 this。

```js
// bad
const self = this;
const boundMethod = function (...params) {
  return method.apply(self, params);
}

// acceptable
const boundMethod = method.bind(this);

// best
const boundMethod = (...params) => method.apply(this, params);
```

简单的，单行的，不会复用的函数，建议采用箭头函数。如果函数体较为复杂，行数较多，还是应该采用传统的函数写法。

所有配置项都应该集中在一个对象，放在最后一个参数，布尔值不可以直接作为参数。

```js
// bad
function devide (a, b, option = false) {}

// good
function devide (a, b, { option = false } = {}) {}
```

不要在函数体内使用 `arguments` 变量，使用 `rest` 运算符 `...` 代替。因为 rest 运算符可以显示表明我们想要获取参数，且 arguments 是一个类似数组的对象，而 rest 运算符可以提供一个真正的数组。

```js
// bad
function concatenateAll () {
  const args = Array.prototype.slice.call(arguments);
  return args.join('');
}

// good
function concatenateAll (...args) {
  return args.join('');
}
```

使用默认值语法设置函数参数的默认值。

```js
// bad
function handleThings (opts) {
  opts = opts || {};
}

// good
function handleThings (opts = {}) {}
```

## Map 结构

注意区分 Object 和 Map，只有模拟实体对象时才使用 Object。如果只是需要 key:value 的数据结构，则只用 Map。因为 Map 具有内建的遍历机制。

```js
let map = new Map(arr);

for (let key of map.keys()) {
  console.log(key);
}

for (let value of map.values()) {
  console.log(value);
}

for (let item of map.entries()) {
  console.log(item[0], item[1]);
}
```

## Class

总是用 Class 取代需要 prototype 的操作。因为 Class 的写法更简洁，更易于理解。

```js
// bad
function Queue (contents = []) {
  this._queue = [...contents];
}
Queue.prototype.pop = function () {
  const value = this._queue[0];
  this._queue.splice(0, 1);
  return value;
}

// good
class Queue {
  constructor (contents = []) {
    this._queue = [...contents];
  }
  pop () {
    const value = this._queue[0];
    this._queue.splice(0, 1);
    return value;
  }
}
```

使用 extends 实现继承，因为这样更简单，不存在破坏 instanceof 运算的危险。

```js
// bad
const inherits = require('inherits');
function PeekableQueue (contents) {
  Queue.apply(this, contents);
}
inherits(PeekableQueue, Queue);
PeekableQueue.prototype.peek = function () {
  return this._queue[0];
}

// good
class PeekableQueue extends Queue {
  peek () {
    return this._queue[0];
  }
}
```

## 模块

Module 语法是 JavaScript 模块的标准写法，要坚持使用这种写法。使用 import 取代 require 的代码如下。

```js
// bad
const moduleA = require('moduleA');
const func1 = moduleA.func1;
const func2 = moduleA.func2;

// good
import { func1, func2 } from 'moduleA';
```

使用 export 取代 module.exports。

```js
// CommonJS
var React = require('react');

var Breadcrumbs = React.createClass({
  render () {
    return <nav />;
  }
})

module.exports = Breadcrumbs;

// ES6
import React from 'react';

class Breadcrumbs extends React.component {
  render () {
    return <nav />;
  }
}

export default Breadcrumbs;
```

如果模块只有一个输出值，就使用 export default；如果模块有多个输出值，就不使用 export default，使用 export。不要同时使用 export default 和普通的 export。

不要在模块输入中使用通配符。因为这样可以确保模块中有一个默认输出。

```js
// bad
import * as myObject from './importModule';

// good
import myObject from './importModule';
```

如果模块默认输出一个函数，函数名的首字母应该小写。

```js
function makeStyleGuide () {}
export default makeStyleGuide;
```

如果模块默认输出一个对象，对象名的首字母应该大写。

```js
const StyleGuide = {}
export default StyleGuide;
```

## ESLint 的使用

ESLint 是一个语法规则和代码风格的检查工具，可用于保证写出语法正确、风格统一的代码。首先，安装 ESLint。

```shell
npm i -g eslint
```

然后，安装语法规则，这里安装 Airbnb 语法规则。

```shell
npm i -g eslint-config-airbnb
```

最后在项目的根目录下新建 `.eslintrc` 文件，配置 ESLint。

```json
{
  "extends": "eslint-config-airbnb"
}
```

现在就可以检查当前项目的代码是否符合预设的规则。假设检查 index.js 文件的代码。

```shell
eslint index.js
```







# 第 25 章		读懂 ECMAScript 规格

## 概述

规格文件是计算机语言的官方标准，详细描述了语言规则和实现方法。一般情况下没有必要阅读规格，除非要写编译器。但如果遇到疑难的语法问题，实在找不到答案，可以查看规格文件，了解语言标准是怎么说的。规格是解决问题的最后一招。

ES6 的规格可以在 [ECMA 国际标准组织的官方网站](http://www.ecma-international.org/ecma-262/6.0/) 上免费下载和在线阅读。

ES6 规格有 26 章。第 1 章 ~ 第 3 章是对文件本身的介绍。第 4 章是对这门语言总体设计的描述。第 5 章 ~ 第 8 章是对语言宏观层面的描述。第 5 章是规格的名词解释和写法的介绍，第 6 章介绍数据类型，第 7 章介绍语言内部用到的抽象操作，第 8 章介绍代码如何运行。第 9 章 ~ 第 26 章介绍具体的语法。

## 相等运算符

本节介绍了规格是怎么规定相等运算符的行为的。

## 数组的空位

数组成员可以省略。只要逗号前面没有任何表达式，数组的 length 属性就会加 1，并且相应增加其后成员的位置索引。被省略的成员不会被定义。如果被省略的成员是数组的最后一个成员，则不会导致数组 length 属性增加。

```js
const a1 = [undefined, undefined, undefined];
const a2 = [, , ,];

a1.length;	// 3
a2.length;	// 3

a1[0];	// undefined
a2[0];	// undefined

a1[0] === a2[0];	// true

0 in a1;	// true
0 in a2;	// false

a1.hasOwnProperty(0);	// true
a2.hasOwnProperty(0);	// false

Object.keys(a1);	// ['0', '1', '2']
Object.keys(a2);	// []

a1.map(n => 1);	// [1, 1, 1]
a2.map(n => 1);	// []
```

## 数组的 map 方法

数组的 map 方法处理一个全是空位的数组时会抛出错误停止执行，进而返回一个全是空位的数组。具体算法请翻阅 p513。







# 第 26 章		ArrayBuffer

ArrayBuffer 对象、TypedArray 视图和 DataView 视图是 JavaScript 操作二进制数据的一个接口。这些对象早就存在，ES6 将它们纳入了 ECMAScript 规格，并添加了新的方法。它们都以数组的语法处理二进制数据，所以统称为二进制数组。二进制数组允许开发者以数组下标的形式直接操作内存，大大增强了 JavaScript 处理二进制数据的能力，使开发者有可能通过 JavaScript 与操作系统的原生接口进行二进制通信。

- ArrayBuffer 对象：代表内存中的一段二进制数据，可以通过视图进行操作。视图部署了数组接口，这意味着可以用数组的方法操作内存。
- TypedArray 视图：共包括 9 种类型的视图。具体翻阅 p518 的 TypedArray 视图支持的数据类型表。
- DataView 视图：可以自定义复合格式的视图，比如第一个字节是 Unit8 无符号 8 位整数，第二和第三个字节是 Int16 16 位整数，第四个字节开始是 Float32 32 位浮点数等，此外还可以自定义字节序。

简而言之，ArrayBuffer 对象代表原始的二进制数据，TypedArray 视图用于读/写简单类型的二进制数据，DataView 视图用于读/写复杂类型的二进制数据。

二进制数组并不是真正的数组，而是类似数组的对象。很多浏览器操作的 API 用到了二进制数组操作二进制数据。

## ArrayBuffer 对象

### 概述

ArrayBuffer 对象代表储存二进制数据的一段内存，它不能直接读/写，只能通过 TypedArray 视图和 DataView 视图读/写，视图的作用是以指定格式解读二进制数据。ArrayBuffer 也是一个构造函数，可分配一段可以存放数据的连续内存区域。构造函数的参数是所需要的内存大小，单位为字节，生成的每个字节的值默认为 0。

```js
var buf = new ArrayBuffer(32);
```

为了读/写这段内存，需要为它指定视图。创建 DataView 视图，需要提供 ArrayBuffer 对象实例作为参数。

```js
var buf = new ArrayBuffer(32);
var dataView = new DataView(buf);
dataView.getUint8(0);	// 0
```

TypedArray 视图与 DataView 视图的一个区别是，它不是一个构造函数，而是一组构造函数，代表不同的数据格式。

```js
var buffer = new ArrayBuffer(32);

var x1 = new Int32Array(buffer);
x1[0] = 1;
var x2 = new Uint8Array(buffer);
x2[0] = 2;

x1[0];	// 2
```

上面的代码分别对同一段内存建立两种视图：32 位带符号整数和 8 位不带符号整数。由于两个视图对应的是同一段内存，因此一个视图修改底层内存会影响到另一个视图。

TypedArray 视图的构造函数除了接受 ArrayBuffer 实例作为参数，还可以接受普通数组作为参数，直接分配内存生成底层的 ArrayBuffer 实例，同时完成对这段内存的赋值。

```js
var typedArray = new Uint8Array([0, 1, 2]);
typedArray.length;	// 3

typedArray[0] = 5;
typedArray;	// [5, 1, 2]
```

### ArrayBuffer.prototype.byteLength

ArrayBuffer 实例的 `byteLength` 属性返回所分配的内存区域的字节长度。如果要分配的内存区域很大，有可能分配失败，因为没有那么多的连续空余内存，所以有必要检查是否分配成功。

```js
var buffer = new ArrayBuffer(32);
if (buffer.byteLength === 32) {
  // success
} else {
 	// fail
}
```

### ArrayBuffer.prototype.slice()

ArrayBuffer 实例有一个 `slice` 方法，允许将内存区域的一部分复制生成一个新的 ArrayBuffer 对象。slice 方法有两个步骤，第一步先分配一段新内存，第二步将原来那个 ArrayBuffer 对象要复制的区域复制过去。slice 方法接受两个参数，第一个参数表示复制开始的字节序号，包含该字节；第二个参数表示复制截止的字节序号，不含该字节。如果省略第二个参数，则默认到复制到原 ArrayBuffer 对象的结尾。除了 slice 方法，ArrayBuffer 对象不提供任何直接读/写内存的方法，只允许在其上建立视图，然后通过视图读/写。

```js
var buffer = new ArrayBuffer(32);
var newBuffer = buffer.slice(0, 12);
```

### ArrayBuffer.isView()

ArrayBuffer 有一个静态方法 `isView`，返回一个布尔值，表示参数是否为 ArrayBuffer 的视图实例。这个方法大致可以判断用于生成 TypedArray 实例和 DataView 实例的参数是否为 ArrayBuffer 实例。

```js
var buffer = new ArrayBuffer(32);
ArrayBuffer.isView(buffer);	// false

var v = new Int32Array(buffer);
ArrayBuffer.isView(v);	// true
```

## TypedArray 视图

### 概述

ArrayBuffer 对象作为内存区域，可以存放多种类型的数据。同一段内存，不同数据有不同的解读方式，这就叫做视图。目前，TypedArray 视图一共包括 9 种类型，每一种视图都是一种构造函数。

- Int8Array：8 位有符号整数，长度为 1 个字节。
- Uint8Array：8 位无符号整数，长度为 1 个字节。
- Uint8ClampedArray：8 位无符号整数，长度为 1 个字节，溢出处理不同。
- Int16Array：16 位有符号整数，长度为 2 个字节。
- Uint16Array：16 位无符号整数，长度为 2 个字节。
- Int32Array：32 位有符号整数，长度为 4 个字节。
- Unit32Array：32 位无符号整数，长度为 4 个字节。
- Float32Array：32 位浮点数，长度为 4 个字节。
- Float64Array：64 位浮点数，长度为 8 个字节。

这 9 个构造函数生成的数组，统称为 TypedArray 视图。它们很像普通数组，都有 length 属性，都能用方括号运算符 `[]` 获取单个元素，所有数组方法都能在其上使用。普通数组与 TypedArray 数组的差异主要在以下方面。

- TypedArray 数组的所有成员都是同一种类型。
- TypedArray 数组的成员是连续的，不会有空位。
- TypedArray 数组成员的默认值是 0。`new Array(10)` 返回的空数组中是 10 个空位。
- TypedArray 数组只是一层视图，本身不存储数据，它的数据都存储在底层的 ArrayBuffer 对象中，要获取底层对象必须使用 buffer 属性。

### 构造函数

#### TypedArray(buffer, byteOffset=0, length?)

同一个 ArrayBuffer 对象之上，可以根据不同的数据类型建立多个视图。视图的构造函数可以接受 3 个参数，其中 byteOffset 必须能被要建立的数据类型所需的字节数整除。如下面新生成一个 8 字节的 ArrayBuffer 对象，然后在这个对象的第一个字节建立带符号的 16 位整数视图，但因为带符号的 16 位整数需要 2 个字节，byteOffset 参数为 1 不能被整除，所以报错。

```js
var buffer = new ArrayBuffer(8);
var i16 = new Int16Array(buffer, 1);	// Uncaught RangeError

// 创建一个指向 buffer 的 Int32 视图，开始于字节 0，直到缓冲区的末尾
var v1 = new Int32Array(buffer);

// 创建一个指向 buffer 的 Uint8 视图，开始于字节 2，直到缓冲区的末尾
var v2 = new Uint8Array(buffer, 2);

// 创建一个指向 buffer 的 Int16 视图，开始于字节 2，长度为 2
var v3 = new Int16Array(buffer, 2, 2);
```

上面的 v1，v2，v3 是重叠的：v1[0] 是一个 32 位整数，指向字节 0 ~ 字节 3；v2[0] 是一个 8 位无符号整数，指向字节 2；v3[0] 是一个 16 位整数，指向字节 2 ~ 字节 3。只要任何一个视图对内存有所修改，就会在另外两个视图上反映出来。

如果想从任意字节开始解读 ArrayBuffer 对象，必须使用 DataView 视图，因为 TypedArray 视图只提供 9 种固定的解读格式。

#### TypedArray(length)

视图还可以不通过 ArrayBuffer 对象，而是直接分配内存生成。此时视图构造函数的参数就是成员的个数。

```js
var f64a = new Float64Array(8);	// 一共 8 * 8 = 64 个字节
f64a[0] = 10;
```

#### TypedArray(typedArray)

TypedArray 数组的构造函数还可以接受另一个 TypedArray 实例作为参数。此时生成的新数组只是复制了参数数组的值，对应的底层内存不一样。新数组会开辟一段新的内存存储数据，不会在原数组的内存之上建立视图。

```js
var x = new Int8Array([1, 1]);
var y = new Int8Array(x);
x[0]	// 1
y[0]	// 1

x[0] = 2
y[0]	// 1
```

如果想基于同一段内存构造不同的视图，可以采用下面的写法。

```js
var x = new Int8Array([1, 1]);
var y = new Int8Array(x.buffer);
x[0]	// 1
y[0]	// 1

x[0] = 2;
y[0]	// 2
```

#### TypedArray(arrayLikeObject)

构造函数的参数也可以是一个普通数组，然后直接生成 TypedArray 实例。这时 TypedArray 视图会重新开辟内存，不会在原数组的内存上建立视图。TypedArray 数组也可以转换回普通数组。

```js
var typedArray = new Uint8Array([1, 2, 3]);
var normalArray = Array.prototype.slice.call(typedArray);
```

### 数组方法

普通数组的操作方法和属性对 TypedArray 数组完全适用。另外 TypedArray 数组与普通数组一样部署了 Iterator 接口，所以可以遍历。注意：TypedArray 数组并没有 concat 方法，如果想合并多个 TypedArray 数组，可以用 p525 页的函数。

### 字节序

字节序指的是数值在内存中的表示方式。本节介绍了`小端字节序`和`大端字节序` 的概念，具体翻阅 p526。TypedArray 数组内部采用小端字节序读/写数据，更准确的说，按照本机操作系统设定的字节序读/写数据。如果一段数据不符合 TypedArray 数组采用的字节序，那么 TypedArray 数组将无法正确解析这段数据，为了解决这个问题，JavaScript 引入了 DataView 对象，可以设定字节序。

p527 提供了一个函数用于判断当前视图是小端字节序还是大端字节序。

总之，于普通数组相比，TypedArray 数组的最大优点就是可以直接操作内存，不需要数据类型转换，所以速度快得多。

### BYTES_PER_ELEMENT 属性

每一种视图的构造函数都有一个 `BYTES_PER_ELEMENT` 属性，表示这种数据类型占据的字节数。这个属性在 TypedArray 实例上也能获取到，即有 `TypedArray.prototype.BYTES_PER_ELEMENT`。

```js
Int8Array.BYTES_PER_ELEMENT	// 1
```

### ArrayBuffer 与字符串的互相转换

ArrayBuffer 转为字符串或字符串转为 ArrayBuffer 都有一个前提，即字符串的编码方法是确定的。假定字符串采用 JavaScript 的内部编码方式 UTF-16 编码，那么可以自己编写转换函数。转换函数在 p529。

### 溢出

不同的视图类型所能容纳的数值范围是确定的。超出这个范围就会出现溢出。如 8 位视图只能容纳一个 8 位二进制数值，如果放入一个 9 位的值，就会溢出。TypedArray 数组的溢出处理规则简单来说就是抛弃溢出的位，然后按照视图类型进行解释。具体溢出转换规则可以查阅 p529。

### TypedArray.prototype.buffer

TypedArray 实例的 `buffer` 属性返回整段内存区域对应的 ArrayBuffer 对象，该属性为只读属性。

### TypedArray.prototype.byteLength、TypedArray.prototype.byteOffset

`byteLength` 属性返回 TypedArray 数组占据的内存长度，单位为字节。`byteOffset` 属性返回 TypedArray 数组从底层 ArrayBuffer 对象的哪个字节开始。这两个属性都是只读属性。

### TypedArray.prototype.length

`length` 属性表示 TypedArray 数组含有多少个成员。

### TypedArray.prototype.set()

TypedArray 数组的 `set` 方法用于复制数组（普通数组或 TypedArray 数组），也就是将一段内存完全复制到另一段内存。这种复制是整段内存的复制，比一个个复制成员的那种方式快得多。set 方法接受两个参数，第一个参数为要复制的源数组，第二个参数可选，表示从第几位开始复制。

```js
var a = new Uint16Array(8);
var b = new Uint16Array(8);

b.set(a, 2);
```

### TypedArray.prototype.subarray()

`subarray` 方法是对于 TypedArray 数组的一部分再建立一个新的视图。方法的第一个参数是起始的成员序号，第二个参数是结束的成员序号（不含该成员），如果省略则包含剩余的全部成员。

### TypedArray.prototype.slice()

`slice` 方法可以返回一个指定位置的新的 TypedArray 实例。参数表示原数组的具体位置，可以是负数。-1 为倒数第一个位置，-2 为倒数第二个位置，以此类推。

### TypedArray.of()

TypedArray 数组的所有构造函数都有一个静态方法 `of`，用于将参数转为一个 TypedArray 实例。

```js
let tarr = new Uint8Array([1, 2, 3]);
// 等同于
let tarr = Uint8Array.of(1, 2, 3);
```

### TypedArray.from()

静态方法 `from` 接受一个可遍历的数据结构如数组作为参数，返回一个基于此结构的 TypedArray 实例。这个方法还可以将一种 TypedArray 实例转为另一种。方法还接受一个函数作为第二个参数，用来对每个元素进行遍历，功能类似 map 方法。

```js
Int8Array.of(127, 126, 125).map(x => 2 * x);	// Int8Array [-2, -4, -6]
Int16Array.from(Int8Array.of(127, 126, 125), x => 2 * x);	// Int16Array [254, 252, 250]
```

上面的第二段代码没有像第一段代码一样发生溢出，说明 from 方法的遍历不是针对原来的 8 位数组。也就是说 from 会将第一个参数指定的 TypedArray 数组复制到另一段内存之中，处理之后再将结果转为指定的数组格式。

## 复合视图

由于视图的构造函数可以指定起始位置和长度，所以在同一段内存中可以依次存放不同类型的数据，这叫作复合视图。

## DataView 视图

如果一段数据包括多种类型，这时除了建立 ArrayBuffer 对象的复合视图之外，还可以通过 DataView 视图进行操作。DataView 视图提供更多操作选项，且支持设定字节序。DataView 视图本身也是构造函数，接受一个 ArrayBuffer 对象作为参数生成视图。

```js
DataView(ArrayBuffer buffer, [, 字节起始位置 [, 长度]])
```

DataView 实例有以下属性，含义与 TypedArray 实例的同名属性相同。

- DataView.prototype.buffer：返回对应的 ArrayBuffer 对象。
- DataView.prototype.byteLength：返回占据的内存字节长度。
- DataView.prototype.byteOffset：返回当前视图从对应的 ArrayBuffer 对象的哪个字节开始。

DataView 实例提供 8 个方法读取内存。

- getInt8：读取 1 个字节，返回一个 8 位整数。
- getUint8：读取 1 个字节，返回一个无符号的 8 位整数。
- getInt16：读取 2 个字节，返回一个 16 位整数。
- getUint16：读取 2 个字节，返回一个无符号的 16 位整数。
- getInt32：读取 4 个字节，返回一个 32 位整数。
- getUint32：读取 4 个字节，返回一个无符号的 32 位整数。
- getFloat32：读取 4 个字节，返回一个 32 位浮点数。
- getFloat64：读取 8 个字节，返回一个 64 位浮点数。

这一系列的 get 方法的参数都是一个字节序号，表示从哪个字节开始读取。不能是负数，否则会报错。如果一次读取两个或两个以上字节，必须明确数据的存储方式，到底是小端字节序还是大端字节序。默认情况下，DataView 的 get 方法使用大端字节序解读数据，如果需要使用小端字节序解读，必须给 get 方法传入第二个参数 true。

DataView 视图提供 8 个方法写入内存。

- setInt8：写入 1 个字节的 8 位整数。
- setUint8：写入 1 个字节的 8 位无符号整数。
- setInt16：写入 2 个字节的 16 位整数。
- setUint16：写入 2 个字节的 16 位无符号整数。
- setInt32：写入 4 个字节的 32 位整数。
- setUint32：写入 4 个字节的 32 位无符号整数。
- setFloat32：写入 4 个字节的 32 位浮点数。
- setFloat64：写入 8 个字节的 64 位浮点数。

这一系列的 set 方法接受三个参数：第一个参数是字节序号，表示从哪个字节开始写入；第二个参数为写入的数据；对于那些写入两个或两个以上字节的方法，需要指定第三个参数，false 或 undefined 表示使用大端字节序写入，true 表示使用小端字节序写入，如果不指定默认使用大端字节序写入。

如果不确定正在使用的计算机的字节序，可以采用下面的判断方式。如果返回 true 就是小端字节序，返回 false 就是大端字节序。

```js
var littleEndian = (function () {
  var buffer = new ArrayBuffer(2);
  new DataView(buffer).setInt16(0, 256, true);
  return new Int16Array(buffer)[0] === 256;
})();
```

## 二进制数组的应用

大量的 Web API 用到了 ArrayBuffer 对象和它的视图对象。

### AJAX

传统上，服务器通过 AJAX 操作只能返回文本数据，即 `responseType` 属性默认为 text。XMLHttpRequest 第 2 版 —— XHR2 允许服务器返回二进制数据，这时分两种情况：如果明确知道返回的是二进制数据，可以把返回类型 responseType 设置为 arraybuffer ；如果不知道，就设置为 blob。

### Canvas

网页 Canvas 元素输出的二进制像素数据就是 TypedArray 数组。TypedArray 数组中的 `Uint8ClampedArray` 视图类型就是一种针对 Canvas 元素的专有类型。这个视图类型的特点就是专门针对颜色把每个字节解读为 8 位无符号的整数，即只能取值 0 ~ 255，且发生运算的时候自动过滤高位溢出，确保将小于 0 的值设为 0，大于 255 的值设为 255，这为图像处理带来了巨大的方便。注意，IE10 不支持该类型。

### WebSocket

WebSocket 可以通过 ArrayBuffer 发送或接收二进制数据。

### Fetch API

Fetch API 取回的数据就是 ArrayBuffer 对象。

```js
fetch(url)
.then(request => {
  return request.arrayBuffer();
})
.then(arrayBuffer => {})
```

### File API

如果知道一个文件的二进制数据类型，也可以将这个文件读取为 ArrayBuffer 对象。读取方法可查阅 p540。

## SharedArrayBuffer

JavaScript 是单线程的，Web worker 引入了多线程：主线程用来与用户互动，Worker 线程用来承担计算任务。每个线程的数据都是隔离的，通过 `postMessage()` 通信。线程之间的数据交换可以是各种格式的，不仅仅是字符串，也可以是二进制数据。这种交换采用的是复制机制，即一个进程将需要分享的数据复制一份，通过 postMessage 方法交给另一个进程。如果数据量大，这种通信方式效率比较低，可以留出一块内存区域由主线程和 Worker 线程共享，两方都可以读写，如此就会大大提高效率。

ES2017 引入了 `SharedArrayBuffer`，允许 Worker 线程与主线程共享同一块内存。SharedArrayBuffer 的 API 与 ArrayBuffer 一样，唯一区别是后者无法共享。

```js
// 主线程
var w = new Worker('myworker.js');

// 新建 1KB 共享内存
var sharedBuffer = new SharedArrayBuffer(1024);

// 主线程将共享内存的地址发送出去
w.postMessage(sharedBuffer);

// 在共享内存上建立视图，供写入数据
const sharedArray = new Int32Array(sharedBuffer);
```

## Atomics 对象

多线程共享内存，最大的问题就是如何防止两个线程同时修改某个地址，或者说当一个线程修改共享内存以后，必须有一个机制让其他线程同步。SharedArrayBuffer API 提供 `Atomics` 对象，保证所有共享内存的操作都是原子性的，且可以在所有线程内同步。

什么叫原子性操作？现代编程语言中，一条普通的命令被编译器处理以后会变成多条机器指令。如果是单线程运行，没有什么问题。如果是多线程环境且共享内存，那么就会出现问题。因为这一组机器指令的运行期间可能会插入其他线程的指令，从而导致运行结果出错。

Atomics 对象就是为了解决这个问题而提出的，它可以保证一个操作所对应的多条机器指令一定是作为一个整体来运行的，中间不会被打断。也就是说，它所涉及的操作都可以看作是原子性的单操作，这可以避免线程竞争，提高多线程共享内存时的操作安全。

### Atomics.store()、Atomics.load()

store() 方法用来向共享内存写入数据，load() 方法用来从共享内存中读出数据。比起直接的读写操作，它们的好处是保证了读写操作的原子性。使用 store 和 load 方法，编译器不会为了优化而打乱机器指令的执行顺序。store 方法接受 3 个参数：SharedBuffer 的视图、位置索引和值，返回 sharedArray[index] 的值。load 方法只接受两个参数：SharedBuffer 的视图和位置索引，也是返回 sharedArray[index] 的值。

### Atomics.wait()、Atomics.wake()

使用 while 循环等待主线程的通知不是很高效，如果用在主线程，就会造成卡顿。Atomics 对象提供了 wait() 和 wake() 方法用于等待通知。这两个方法相当于锁内存，即在一个线程进行操作时，让其他线程休眠（建立锁），等到操作结束，再唤醒那些休眠的线程（解除锁）。

```js
Atomics.wait(sharedArray, index, value, time);
Atomics.wake(sharedArray, index, count);
```

当 sharedArray[index] 不等于 value，Atomics.wait 返回 not-equal，否则就进入休眠。只有使用 Atomics.wake() 或者 time ms 后才能唤醒。被 Atomics.wake() 唤醒时，wait 返回 ok，超时唤醒返回 timed-out。Atomics.wake() 用于唤醒 count 数目在 sharedArray[index] 位置休眠的线程，让它继续往下运行。

```js
// 线程一
console.log(ia[37]);	// 163
Atomics.store(ia, 37, 123456);
Atomics.wake(ia, 37, 1);

// 线程二
Atomics.wait(ia, 37, 163);
console.log(ia[37]);	// 123456
```

注意，浏览器的主线程有权拒绝休眠，这是为了防止用户失去响应。

### 运算方法

共享内存上面的某些运算是不能被打断的，即不能在运算过程中让其他线程改写内存上面的值。Atomics 对象提供了一些运算方法，防止数据被改写。

- `Atomics.add(sharedArray, index, value)`：方法将 value 加到 sharedArray[index] 中，返回 sharedArray[index] 的旧值。
- `Atomics.sub(sharedArray, index, value)`：方法将 value 从 sharedArray[index] 减去，返回 shareArray[index] 的旧值。
- `Atomics.and(sharedArray, index, value)`：方法将 value 与 sharedArray[index] 进行位运算 and，放入 sharedArray[index] 中，返回旧值。
- `Atomics.or(sharedArray, index, value)`：方法将 value 与 sharedArray[index] 进行位运算 or，放入 sharedArray[index] 中，返回旧值。
- `Atomics.xor(sharedArray, index, value)`：方法将 value 与 sharedArray[index] 进行位运算 xor，放入 sharedArray[index] 中，返回旧值。

### 其他方法

- `Atomics.compareExchange(sharedArray, index, oldval, newval)`：如果 sharedArray[index] 等于 oldval，就写入 newval，返回 oldval。
- `Atomics.exchange(sharedArray, index, value)`：设置 sharedArray[index] 的值，返回旧的值。
- `Atomics.isLockFree(size)`：返回一个布尔值，表示 Atomics 对象是否可以处理某个 size 的内存锁定。如果返回 false，应用程序便需要自己来实现锁定。

