var p1 = new Promise((resolve, reject) => {
  console.log('p1 pending')
  setTimeout(() => reject(new Error('fail')), 3000);
  // resolve('p1 success')
  console.log('p1 over')
})
var p2 = new Promise((resolve, reject) => {
  console.log('p2 pending')
  setTimeout(() => resolve(p1), 1000);
  // resolve(p1)
  console.log('p2 over')
})
p2
  .then(result => console.log('now ' + result), error => console.log('omg ' + error))
  // .catch(error => console.log(error));	// Error: fail