new Promise((resolve, reject) => { 
  resolve(1);
})
	.then(valueA => { return valueA })
  .then(valueB => console.log(valueB))


new Promise((resolve, reject) => {
  resolve(2);
})
	.then(value => {
  	console.log(value);
  	return new Promise((resolve, reject) => {
      return resolve('new Promise is resolve')
      // return reject('new Promise is reject')
    });
	})
	.then(function funcA (success) {
    // resolve
    console.log(success)
	}, function funcB (err) {
    // reject
    console.log(err)
	})