async function f () {
  let result1 = await new Promise(resolve => {
    setTimeout(resolve('one'), 10000)
  });
  console.log('this is ' + result1);
  let result2 = await new Promise(resolve => {
    setTimeout(resolve('two'), 5000)
  });
  console.log('this is ' + result2);
}

f().then(() => console.log('this is three'));
