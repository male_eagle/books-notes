console.log(eagle)  // undefined
var eagle = 'cat'
console.log(eagle)  // cat

function eagleFn () {
  var cat = 'eagle'
}

// console.log(cat) // 报错
eagleFn()
// console.log(cat) // 报错

console.log(str)  // undefined
{
  var str = 'eagle_cat'
}
console.log(str)  // eagle_cat