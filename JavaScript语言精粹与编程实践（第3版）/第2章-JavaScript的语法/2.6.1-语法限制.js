// "use strict"

// 非严格模式和严格模式下都可以通过，都取最后一个值
var obj = {
  name: 'eagle',
  name: 'eagle-cat'
}
console.log(obj)


// 非严格模式下可以运行，取最后一个值
// 严格模式下报错
// function foo (x, x, y) {
//   console.log(x + y)
// }
// foo(1, 2, 3)


// 非严格模式下显示 10
// 严格模式下报错
const num = 012
console.log(num)