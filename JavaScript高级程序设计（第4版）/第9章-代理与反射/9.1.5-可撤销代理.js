const target = {
  foo: 'bar'
}

const handler = {}

const { proxy, revoke } = Proxy.revocable(target, handler);

console.log(proxy.foo);
console.log(target.foo);

revoke();

proxy.foo;