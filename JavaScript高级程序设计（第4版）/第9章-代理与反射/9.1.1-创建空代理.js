// 目标对象
const target = {
  id: 'target'
}

// 处理程序对象
const handler = {}

// 代理对象
const proxy = new Proxy(target, handler);

// id 属性会访问同一个值
console.log(target.id);
console.log(proxy.id);

// 给目标属性赋值会反映在两个对象上
target.id = 'foo';
console.log(target.id);
console.log(proxy.id);

// hasOwnProperty() 方法在两个地方都会应用到目标对象
console.log(target.hasOwnProperty('id'));
console.log(proxy.hasOwnProperty('id'));

// 严格相等可以区分代理和目标
console.log(target == proxy);
console.log(target === proxy);

// Proxy.prototype 是 undefined，因此不能使用 instanceof 操作符
console.log(Proxy.prototype);
console.log(target instanceof Proxy);
console.log(proxy instanceof Proxy);
