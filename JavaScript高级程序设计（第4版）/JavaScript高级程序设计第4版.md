# 第 1 章  什么是 JavaScript

## 1.2 JavaScript 实现
完整的*JavaScript*实现包含以下三个部分：
- 核心-*ECMAScript*。
- 文档对象模型-*DOM*。
- 浏览器对象模型-*BOM*。








# 第 2 章  HTML 中的 JavaScript

## 2.1 `<script>`元素
将*JavaScript*插入*HTML*的主要方法是使用`<script>`元素，该元素有 8 个属性：
- `async`：可选。表示应该立即下载脚本，但不妨碍页面中的其他操作，比如下载其他资源或等待加载其他脚本。只对外部脚本文件有效。
- `charset`：可选。表示通过*src*属性指定的代码的字符集。但是大多数浏览器会忽略它的值，因此比较少用。
- `crossorigin`：可选。配置相关请求的*CORS*（跨域资源共享）设置。默认不使用*CORS*。`crossorigin="anonymous"`配置文件请求不必设置凭据标志。`crossorigin="use-credentials"`设置凭据标志，意味着出站请求会包含凭据。
- `defer`：可选。表示脚本可以延迟到文档完全被解析和显示之后再执行。只对外部脚本文件有效。
- `integrity`：可选。允许比对接收到的资源和指定的加密签名以验证子资源完整性。如果接收到的资源的签名与这个属性指定的签名不匹配，则页面会报错，脚本不会执行。这个属性可以用于确保内容分发网络*CDN*不会提供恶意内容。
- `language`：已废弃。原来用于表示编写代码使用的脚本语言。
- `src`：可选。表示包含要执行代码的外部文件的*url*。引用的文件不一定是`.js`文件，如果不是`.js`文件需要确保服务器能返回正确的*MIME*类型。
- `type`：可选。可以说是*language*属性的替代属性。表示编写代码使用的脚本语言内容类型（这种类型称为*MIME*类型）。按照惯例，该属性的值始终都是`text/javascript`，尽管这个值和`text/ecmascript`都已经废弃了*JavaScript*文件的*MIME*类型通常是`application/x-javascript`，不过设置这个值有可能导致脚本被忽略。在非*IE*浏览器中有效的其他值还有`application/javascript`和`application/ecmascript`。如果属性值为`module`，则代码会被当成*ES6*模块，只有这种时候代码中才能出现`import`和`export`关键字。

在解释器对`<script>`元素内部的所有代码求值完毕以前，页面中的其余内容都不会被浏览器加载或显示。








# 第 3 章  语言基础
*ECMA-262*以一个名为*ECMAScript*的伪语言的形式，定义了*JavaScript*中所有涉及语法、操作符、数据类型以及内置功能的基础。到 2017 年底，大多数主流浏览器几乎或全部实现了*ECMAScript*第 6 版的规范，所以本章接下来的内容主要基于*ES6*。

## 3.4 数据类型
*ECMAScript*有 6 种简单数据类型：`Undefined`、`Null`、`Boolean`、`Number`、`String`和`Symbol`。还有一种复杂数据类型：`Object`。

### 3.4.1 typeof 操作符
`typeof`操作符用于确定任意变量的数据类型。

对一个函数使用`typeof`操作符会返回`function`，表示值为函数。严格来讲，函数在 ECMAScript 中被认为是对象，并不代表一种数据类型。但是函数也有自己的特殊属性，所以就有必要通过`typeof`操作符来区分函数和其他对象。

### 3.4.2 Undefined 类型
`Undefined`类型只有一个值，就是特殊值`undefined`。增加这个特殊值的目的就是为了正式明确空对象指针`null`和未初始化变量的区别。当使用`var`或`let`声明了变量但是没有初始化时，就相当于给变量赋予了`undefined`值。

在对未初始化的变量和未声明的变量调用`typeof`操作符时，返回的结果都是`undefined`。对未声明的变量，只能执行一个有用的操作：对它调用`typeof`。

### 3.4.3 Null 类型
`Null`类型同样只有一个值，即特殊值`null`。`null`值表示一个空对象指针，这也是给`typeof`传一个`null`会返回`object`的原因。

`undefined`值是由`null`值派生而来的，因此*ECMA-262*将它们定义为表面上相等。

```js
null == undefined		// true
```

### 3.4.4 Boolean 类型
`Boolean`布尔值类型有两个字面值：`true`和`false`。这两个布尔值不同于数值，因此`true`不等于 1，`false`不等于 0。

```js
true == 1			// true
true === 1		// false
false == 0		// true
false === 0		// false
```

虽然布尔值只有两个，但所有其他类型的值都有相应布尔值的等价形式。要将一个其他类型的值转换为布尔值，可以调用特定的`Boolean()`转型函数：
```js
let message = 'Hello World';
let messageAsBoolean = Boolean(message);
```

因为`if`等流控制语句会自动执行其他类型值到布尔值的转换，所以理解转换规则非常重要，下表总结了不同类型与布尔值之间的转换规则。
| 数据类型  | 转换为 true 的值       | 转换为 false 的值 |
| --------- | ---------------------- | ----------------- |
| Boolean   | true                   | false             |
| String    | 非空字符串             | ""（空字符串）    |
| Number    | 非零数值（包括无穷值） | 0、NaN            |
| Object    | 任意对象               | null              |
| Undefined | N/A（不存在）          | undefined         |

### 3.4.5 Number 类型
`Number`类型使用*IEEE754*格式表示整数和浮点值（某些语言中也叫双精度值）。

不同的数值类型相应地也有不同的数值字面量格式。最基本的数值字面量格式是十进制整数，直接写出来即可：
```js
let intNum = 88;	// 整数
```

整数也可以用八进制或十六进制字面量表示。对于八进制字面量，需要以前缀`0`或`0o`开头（严格模式下前缀 0 会被视为语法错误，最好是使用 0o 前缀），然后是相应的八进制数字（数值 0 ~ 7）。如果字面量中包含的数字超出了应有的范围，就会忽略前缀，后面的数字序列会被当成十进制数。
```js
let octalNum1 = 0o70;		// 八进制的 56
let octalNum2 = 0o79;		// 无效的八进制值，当成 79 处理
let octalNum3 = 0o8;		// 无效的八进制值，当成 8 处理
```

要创建十六进制字面量，需要以前缀`0x`开头，然后是十六进制数字（0 ~ 9 以及 A ~ F）。十六进制数字中的字母大小写均可，如果数字超出范围会报错。
```js
let hexNum1 = 0xA;		// 十六进制 10
let hexNum2 = 0x1G;		// 报错
```

使用八进制和十六进制格式创建的数值在所有数学操作中都被视为十进制数值。
```js
let octalNum = 0o70;
let hexNum = 0x2C;
console.log(octalNum + hexNum);		// 100
```

由于*JavaScript*保存数值的方式，实际中可能存在正零 +0 和负零 -0。正零和负零在所有情况下都被认为是等同的。

#### 3.4.5.1 浮点值
要定义浮点值，数值中必须包含小数点，且小数点后面必须至少有一个数字，小数点前不是必须有整数，但推荐加上。因为存储浮点值使用的内存空间是存储整数值的两倍，所以*ECMAScript*总是想方设法把值转换为整数。在小数点后面没有数字的情况下，数值就会变为整数。如果数值本身就是整数，只是小数点后面跟着 0，那也会被转换为整数。
```js
let floatNum1 = 1.1;		// 标准的浮点值
let floatNum2 = .1;			// 有效，但不推荐
let floatNum3 = 1.;			// 小数点后没有数字，当成整数 1 处理
let floatNum4 = 10.0;		// 小数点后面是零，当成整数 10 处理
```

对于非常大或非常小的数值，浮点值可以用科学记数法来表示。*ECMAScript*中科学记数法的格式要求是一个数值（整数或浮点数）后跟一个大写或小写的字母`e`，再加上一个要乘的 10 的多少次幂。*ECMAScript*也会将小数点后至少包含 6 个零的浮点值转换为科学记数法。
```js
let floatNum1 = 3.125e7;			// 等于 31250000
let floatNum2 = 3e-17;				// 等于 0.00000000000000003
let floatNum3 = 0.0000003;		// 转换为 3e-7
```

浮点值的精确度最高可达 17 位小数，但算术计算中远不如整数精确，例如 0.1 加上 0.2 等于 0.30000000000000004，所以永远不要使用浮点数进行比较。之所以存在这种舍入错误，是因为使用了*IEEE754*格式，并非 *ECMAScript* 所独有，其它使用相同格式的语言也有这个问题。
```js
0.05 + 0.25 === 0.3;	// true
0.1 + 0.2 === 0.3;		// false
```

#### 3.4.5.2 值的范围
由于内存的限制，*ECMAScript*并不支持世界上的所有数值。*ECMAScript*可以表示的最小数值保存在`Number.MIN_VALUE`中，值一般为`5e-324`；最大数值保存在`Number.MAX_VALUE`中，值一般为`1.7976931348623157e+308`。

如果计算得到的数值结果超出了上述两个范围，那么数值会自动转换为特殊的无穷值，负数以`-Infinity`负无穷大表示，正数以`Infinity`正无穷大表示。使用`isFinite()`函数可以确定一个值是否处在*JavaScript*能表示的最小值和最大值之间。

使用`Number.NEGATIVE_INFINITY`和`Number.POSITIVE_INFINITY`也可以获取正、负`Infinity`。

#### 3.4.5.3 NaN
有一个特殊的数值`NaN`（Not a Number，不是数值）用于表示本来要返回数值的操作失败了，而不是操作失败后抛出错误。例如，用 0 除以任意数值在其他语言中通常都会导致错误，从而中止代码执行。但在*ECMAScript*中，0 除以任意不为 0 的数值，返回被除数；0、+0 或 -0 相除会返回`NaN`。
```js
0 / 23;		// 23
0 / 0;		// NaN
-0 / +0;	// NaN
```

如果分子是非 0 值，分母是有符号 0 或无符号 0，则会返回`Infinity`或`-Infinity`。
```js
5 / 0;		// Infinity
5 / -0;		// -Infinity
```

任何涉及`NaN`的操作始终返回`NaN`，其次`NaN`不等于任何值，包括自身。
```js
NaN / 10;			// NaN
NaN == NaN;		// false
```

*ECMAScript*提供了`isNaN()`函数，函数会尝试把参数转换为数值，判断参数是否为`NaN`。如果参数是对象，首先会调用对象的`valueOf()`方法，然后再确定返回的值是否可以转换为数值。如果不能，再调用`toString()`方法，并测试其返回值。

```js
isNaN(NaN);			// true
isNaN("10");		// false, 可以转换为数值 10
isNaN("blue");	// true, 不可以转换为数值
```

#### 3.4.5.4 数值转换
`Number()`、`parseInt()`和`parseFloat()`3 个函数可以将非数值转换为数值。`Number()`是转型函数，可用于任何数据类型，后两个函数主要用于将字符串转换为数值。

`Number()`函数基于如下规则执行转换：

- 布尔值：`true`转换为 1，`false`转换为 0。
- 数值：直接返回。
- `null`：返回 0。
- `undefined`：返回`NaN`。
- 字符串：应用以下规则：
  - 如果字符串包含数值字符，包括数值字符前面带加减号的情况，则转换为一个十进制数值。
    ```js
    Number('011');	// 11, 忽略前面的 0
    ```
  - 如果字符串包含有效的浮点值格式，则会转换为相应的浮点值，同样忽略前面的 0。
    ```js
    Number('12.12');		// 12.12
    Number('12.12.12');	// NaN
    ```
  - 如果字符串包含有效的十六进制格式，则会转换为与该十六进制值对应的十进制整数值。
  - 如果是空字符串，则返回 0。
  - 如果字符串包含除上述情况之外的其他字符，则返回*NaN*。
    ```js
    Number('test1234');		// NaN
    Number('1234test');		// NaN
    ```
- 对象：调用`valueOf()`方法，并按照上述规则转换返回的值。如果转换结果是*NaN*，则调用`toString()`方法，再按照转换字符串的规则转换。

`parseInt()`函数的转换规则为：字符串最前面的空格会被忽略，从第一个非空格字符开始转换。如果第一个字符不是数值字符、加号或减号，立即返回`NaN`，这意味着空字符串也会返回`NaN`。如果第一个字符是数值字符、加号或减号，则继续检测每个字符，直到字符串末尾或者遇到非数值字符。

```js
parseInt('  0001234fjskal');	// 1234
```

`parseInt()`函数也能识别不同的整数格式，如果字符串以`0x`开头，就会被解释为十六进制整数。`parseInt()`函数也接受第二个参数，用于指定进制数。如果直到要解析的值是十六进制，可以传入 16 作为第二个参数，以便正确解析。且传入第二个参数后，数值的进制前缀还可以省略。

```js
parseInt('0xAF');		// 175
parseInt('0xAF', 16);	// 175
parseInt('AF', 16);		// 175
parseInt('AF');			// NaN
```

**因为不传进制参数相当于让`parseInt()`自己决定如何解析，所以为了避免解析错误，建议始终传入进制参数。**

`parseFloat()`函数的工作方式跟`parseInt()`函数类似，都是从位置 0 开始检测每个字符。同样，也是解析到字符串末尾或者解析到一个无效的浮点数值字符为止，这意味着第一个小数点是有效的，第二个小数点就无效了。另外`parseFloat()`只解析十进制数值，其他带前缀的进制格式数值都会返回 0。

```js
parseFloat('12.12');		// 12.12
parseFloat('12.12.12');	    // 12.12
parseFloat('0o123');		// 0
parseFloat('0x123');		// 0
parseFloat('888.000');	    // 888
```

### 3.4.6 String 类型
#### 3.4.6.2 字符串的特点
*ECMAScript*中的字符串是不可变的，一旦创建，值就不能变了。如果修改某个变量中的字符串值，会先销毁原始的字符串，然后将包含新值的另一个字符串保存到该变量中。

#### 3.4.6.3 转换为字符串
有三种方式把一个值转换为字符串。首先是使用几乎所有值都有的`toString()`方法，`null`和`undefined`值没有这个方法。这个方法的唯一用途就是返回当前值的字符串等价物。多数情况下，`toString()`不接收任何参数，但对数值调用这个方法时，`toString()`可以接收一个进制参数，然后返回以该进制表示的数值字符串。
```js
(19).toString();		// '19'
true.toString();		// 'true'
'22'.toString();		// '22'
(10).toString(16);		// 'a'
```

如果不确定一个值是不是`null`或`undefined`，可以使用`String()`转型函数，该函数遵循如下规则：
- 如果值有`toString()`方法，则调用该方法且不传参数，返回结果。
- 如果值是`null`，返回`"null"`。
- 如果值是`undefined`，返回`"undefined"`。

用加号操作符给一个值加上一个空字符串`""`也可以将其转换为字符串。
```js
true + "";	// 'true'
```

#### 3.4.6.6 模板字面量标签函数
模板字面量（即使用反引号**\`\`**表示的字符串）也支持定义标签函数，通过标签函数可以自定义插值行为。[case](第3章-语言基础\3.4.6.6-模板字面量标签函数.js)

#### 3.4.6.7 原始字符串
无论是使用单引号、双引号或者反引号表示的字符串，都会自动转义原始字符串中的转义字符或*Unicode*字符，如果想直接获取原始的字符串内容，可以使用默认的模板字面量标签函数`String.raw`。
```js
// Unicode 示例
// \u00A9 是版权符号 ©
console.log(`\u00A9`);						// ©
console.log('\u00A9');						// ©
console.log("\u00A9");						// ©
console.log(String.raw`\u00A9`);	// \u00A9

// 换行符示例
console.log(`first line\nsecond line`);
// first line
// second line
console.log(String.raw`first line\nsecond line`);	// first line\nsecond line
```

也可以通过标签函数的第一个参数，即字符串数组的`.raw`属性取得每个字符串的原始内容。[case](第3章-语言基础\3.4.6.7-原始字符串.js)


### 3.4.7 Symbol 类型
`Symbol`（符号）是*ECMAScript6*新增的数据类型。符号实例是唯一的，不可变的。符号就是用来创建唯一记号，进而用作非字符串形式的对象属性。

#### 3.4.7.1 符号的基本用法
符号需要使用`Symbol()`函数初始化，不能与`new`关键字一起使用，因为不是构造函数。因为符号本身是原始类型，所以`typeof`操作符对符号返回`symbol`。
```js
let sym = Symbol();
console.log(typeof sym);    // symbol

let newSymbol = new Symbol();    // TypeError: Symbol is not a constructor
```

调用`Symbol()`函数时，可以传入一个字符串参数作为对符号的描述，将来可以通过这个字符串来调试代码。但因为唯一性，所以即使传入的字符串参数相同，创建的符号也不同。
```js
let testSymbol = Symbol('test');
let otherTestSymbol = Symbol('test');
console.log(testSymbol == otherTestSymbol);    // false
```

#### 3.4.7.2 使用全局符号注册表
可以使用`Symbol.for()`方法在全局符号注册表中创建并重用符号。使用`Symbol.for()`方法创建符号时，会首先在全局注册表中查找是否有使用相同字符串参数创建的符号，如果没有则创建返回符号并添加到全局注册表中，如果有则直接返回已创建的符号。即使使用相同的字符串参数，`Symbol.for()`创建的全局符号和`Symbol()`创建的符号也并不等同。
```js
let fooGlobalSymbol = Symbol.for('foo');    							// 创建新符号
let otherFooGlobalSymbol = Symbol.for('foo');    					// 重用已有符号
console.log(fooGlobalSymbol == otherFooGlobalSymbol);    	// true
console.log(typeof fooGlobalSymbol);    									// symbol

let fooSymbol = Symbol('foo');
console.log(fooSymbol == fooGlobalSymbol);    						// false
```

使用`Symbol.keyFor()`可以查询全局注册表。该方法接收符号参数，返回该全局符号对应的字符串键，如果查询的不是全局符号，返回`undefined`。
```js
let s1 = Symbol.for('foo');
let s2 = Symbol('bar');

console.log(Symbol.keyFor(s1));    	// foo
console.log(Symbol.keyFor(s2));    	// undefined
Symbol.keyFor(123);    							// TypeError: 123 is not a symbol
```

#### 3.4.7.3 使用符号作为属性

可以使用字符串或数值作为属性的地方，都可以使用符号。包括对象字面量属性和`Object.defineProperty()`或`Object.defineProperties()`定义的属性。

类似于`Object.getOwnPropertyNames()`返回对象实例的常规属性数组，`Object.getOwnPropertySymbols()`返回对象实例的符号属性数组。这两个方法的返回值彼此互斥。`Object.getOwnPropertyDescriptors()`会同时返回包含常规属性和符号属性在内的属性描述符对象。`Reflect.ownKeys()`会返回常规属性和符号属性的键。

[case](第3章-语言基础\3.4.7.3-使用符号作为属性.js)

#### 3.4.7.4 常用内置符号

*ECMAScript6* 引入了一批常用内置符号，用于暴露语言内部的行为，开发者可以直接访问、重写或模拟这些行为。这些内置符号都以`Symbol`工厂函数字符串属性的形式存在，也就是全局函数`Symbol`的普通字符串属性，指向一个符号的实例。所有内置符号属性都是不可写、不可枚举、不可配置的。

内置符号最重要的用途之一是重新定义它们，从而改变原生结构的行为。例如，`for-or`循环会在相关对象上使用`Symbol.iterator`属性，那么就可以通过在自定义对象上重新定义`Symbol.iterator`的值来改变`for-or`在迭代该对象时的行为。

#### 3.4.7.5 Symbol.asyncIterator

这个符号作为一个属性，表示一个方法，该方法返回对象默认的`AsyncIterator`，由`for-await-of`语句使用。也就是说这个符号表示实现异步迭代器*API*的函数。

`for-await-of`循环会利用这个函数执行异步迭代操作。循环时会调用以`Symbol.asyncIterator`为键的函数，并期望这个函数会返回一个实现迭代器*API*的对象。很多时候返回的对象是实现该*API*的`AsyncGenerator`。

```js
class Foo {
	async *[Symbol.asyncIterator] () {}
}

let f = new Foo();

console.log(f[Symbol.asyncIterator]());
// AsyncGenerator {<suspended>}
```

由`Symbol.asyncIterator`函数生成的对象应该通过其`next()`方法陆续返回`Promise`期约实例。可以通过显式调用`next()`方法返回，也可以隐式地通过异步生成器函数返回：[case](第3章-语言基础\3.4.7.5-Symbol.asyncIterator.js)。

`Symbol.asyncIterator`是*ES2018*规范定义的，使用时注意支持性。关于异步迭代和`for-await-of`循环的细节，参见[附录A](本书资源\54538 附录A_D.pdf)。

#### 3.4.7.6 Symbol.hasInstance

该符号作为属性表示一个方法。该方法决定一个构造器对象是否认可一个对象是它的实例，由`instanceof`操作符使用。*ES6* 中，`instanceof`操作符会使用`Symbol.hasInstance`函数来确定关系，以`Symbol.hasInstance`为键的函数会执行同样的操作。

这个属性定义在`Function`的原型上，因此默认在所有函数和类上都可以调用，可以在继承的类上通过静态方法重新定义这个函数：[case](第3章-语言基础\3.4.7.6-Symbol.hasInstance.js)。

#### 3.4.7.7 Symbol.isConcatSpreadable

该符号作为属性表示一个布尔值。如果是`true`，意味着对象应该用`Array.prototype.concat()`打平其数组元素。数组对象默认情况下会被打平到已有的数组，`false`或假值会导致整个对象被追加到数组末尾。类数组对象默认情况下会被追加到数组末尾，`true`或真值会导致这个类数组对象被打平到数组实例。其他不是类数组对象的对象在符号被设置为`true`的情况下将被忽略。

```js
let initial = ['foo'];

let array = ['bar'];
console.log(array[Symbol.isConcatSpreadable]);	// undefined
console.log(initial.concat(array));							// ['foo', 'bar']
array[Symbol.isConcatSpreadable] = false;
console.log(initial.concat(array));							// ['foo', Array(1)]

let arrayLikeObject = { length: 1, 0: 'baz' };
console.log(arrayLikeObject[Symbol.isConcatSpreadable]);	// undefined
console.log(initial.concat(arrayLikeObject));							// ['foo', {...}]
arrayLikeObject[Symbol.isConcatSpreadable] = true;
console.log(initial.concat(arrayLikeObject));							// ['foo', 'baz']

let otherObject = new Set().add('qux');
console.log(otherObject[Symbol.isConcatSpreadable]);		// undefined
console.log(initial.concat(otherObject));								// ['foo', Set(1)]
otherObject[Symbol.isConcatSpreadable] = true;
console.log(initial.concat(otherObject));								// ['foo']
```

#### 3.4.7.8 Symbol.iterator

该符号作为属性表示一个方法，返回对象默认的迭代器。由`for-of`语句使用，也就是说这个符号表示实现迭代器*API*的函数。`for-of`循环会利用这个函数执行迭代操作，循环时会调用以`Symbol.iterator`为键的函数，并默认这个函数会返回一个实现迭代器*API*的对象。很多时候，返回的对象是实现该*API*的*Generator*。该符号和`Symbol.asyncIterator`类似，不过该符号为同步版本。

```js
class Foo {
  *[Symbol.iterator] () {}
}

let f = new Foo();

console.log(f[Symbol.iterator]());	// Generator {<suspended>}
```

#### 3.4.7.9 Symbol.match

该符号作为属性表示一个正则表达式方法，该方法用正则表达式去匹配字符串。由`String.prototype.match()`方法使用，用以`Symbol.match`为键的函数来对正则表达式求值。正则表达式的原型上默认有这个函数的定义，因此所有正则表达式实例默认是这个`String`方法的有效参数。

```js
console.log(RegExp.prototype[Symbol.match]);	// f [Symbol.match] () { [native code ]}
console.log('foobar'.match(/bar/));	// ["bar", index: 3, input: "foobar", groups: undefined]
```

给这个方法传入非正则表达式值会导致该值被转换为*RegExp*对象，可以重新定义`Symbol.match`函数以取代默认对正则表达式求值的行为，从而让`match()`方法使用非正则表达式实例。`Symbol.match`函数接收一个参数，就是调用`match()`方法的字符串实例：[case](第3章-语言基础\3.4.7.9-Symbol.match.js)。

#### 3.4.7.10 Symbol.replace

该符号作为属性表示一个正则表达式方法，替换一个字符串中匹配的子串。由`String.prototype.replace()`方法使用。与`Symbol.match`符号类似，不同的是，`Symbol.replace`函数接收两个参数，一个是调用*replace()*方法的字符串实例和替换字符串：[case](第3章-语言基础\3.4.7.10-Symbol.replace.js)。

#### 3.4.7.11 Symbol.search

该符号作为属性表示一个正则表达式方法，返回字符串中匹配正则表达式的索引。由`String.prototype.search()`方法使用。与`Symbol.match`和`Symbol.replace`两个符号类似。`Symbol.search`函数接收的参数就是调用*search()*方法的字符串实例：[case](第3章-语言基础\3.4.7.11-Symbol.search.js)。

#### 3.4.7.12 Symbol.species

该符号作为属性表示一个函数值，该函数作为创建派生对象的构造函数。这个属性在内置类型中最常用，用于对内置类型实例方法的返回值暴露实例化派生对象的方法。用`Symbol.species`定于静态的获取器*getter*方法，可以覆盖新创建实例的原型定义：[case](第3章-语言基础\3.4.7.12-Symbol.species.js)。

#### 3.4.7.13 Symbol.split

该符号作为属性表示一个正则表达式方法，在匹配正则表达式的索引位置拆分字符串。由`String.prototype.split()`方法使用。与`Symbol.match`、`Symbol.replace`和`Symbol.search`三个符号类似。`Symbol.split`函数接收一个参数，即调用*split()*方法的字符串实例：[case](第3章-语言基础\3.4.7.13-Symbol.split.js)。

#### 3.4.7.14 Symbol.toPrimitive

该符号作为属性表示一个方法，该方法将对象转换为相应的原始值。由*ToPrimitive*抽象操作使用。对于一个自定义对象实例，通过在这个实例的`Symbol.toPrimitive`属性上定义一个函数可以改变默认行为：[case](第3章-语言基础\3.4.7.14-Symbol.toPrimitive.js)。

#### 3.4.7.15 Symbol.toStringTag

该符号作为属性表示一个字符串，该字符串用于创建对象的默认字符串描述。由内置方法`Object.prototype.toString()`使用。通过*toString()*方法获取对象标识时，会检索由`Symbol.toStringTag`指定的实例标识符，默认为*'Object'*。内置类型已经指定了这个值，但自定义类实例还需要明确定义：[case](第3章-语言基础\3.4.7.15-Symbol.toStringTag.js)。

#### 3.4.7.16 Symbol.unscopables

该符号作为属性表示一个对象，该对象所有的以及继承的属性都会从关联对象的`with`环境绑定中排除。设置这个符号并让其映射对应属性的键值为`true`，就可以阻止该属性出现在`with`环境绑定中：[case](第3章-语言基础\3.4.7.16-Symbol.unscopables.js)。

**注意：不推荐使用*with*,因此也不推荐使用*Symbol.unscopables*。**

### 3.4.8 Object 类型

*ECMAScript*中的对象其实就是一组数据和功能的集合。对象通过`new`操作符后跟对象类型的名称来创建。*ECMAScript*中*Object*是所有对象的基类，所以任何对象都有*Object*类型的所有属性和方法。每个*Object*实例都有如下属性和方法：

- `constructor`：用于创建当前对象的函数。
- `hasOwnProperty(propertyName)`：用于判断当前对象实例（不是原型）上是否存在给定的属性。参数必须是字符串或符号。
- `isPrototypeOf(object)`：用于判断当前对象是否为另一个对象的原型。
- `propertyIsEnumerable(propertyName)`：用于判断给定的属性是否可以使用`for-in`语句枚举。参数必须是字符串。
- `toLocaleString()`：返回对象的字符串表示，该字符串反映对象所在的本地化执行环境。
- `toString()`：返回对象的字符串表示。
- `valueOf()`：返回对象对应的字符串、数值或布尔值表示。通常与*toString()*的返回值相同。

**注意：严格来讲，*ECMA-262*中对象的行为不一定适合*JavaScript*中的其他对象。比如浏览器环境中的*BOM*和*DOM*对象，都是由宿主环境定义和提供的宿主对象。而宿主对象不受*ECMA-262*约束，所以它们可能会也可能不会继承*Object*。**

## 3.5 操作符

### 3.5.2 位操作符

*ECMAScript*中的数值应用位操作符时，后台会发生如下转换：64 位数值会转换为 32 位数值，然后执行位操作，最后再把结果从 32 位转换为 64 位存储起来。这个转换导致了特殊值`NaN`和`Infinity`在位操作中会被当成 0 处理。

如果将位操作应用到非数值，首先会使用`Number()`函数将该值转换为数值，然后再应用位操作，最终结果是数值。

#### 3.5.2.1 按位非

按位非操作符用波浪符`~`表示，作用是返回数值的一补数，最终效果就是对数值取反并减 1。

```js
let num1 = 25;
let num2 = ~num1;		// num2 = -num1 - 1
console.log(num2);	// -26
```

**尽管对数值取反再减 1 和按位非两个操作结果一样，但是位操作的速度快得多。这是因为位操作是在数值的底层表示上完成的。**

### 3.5.5 指数操作符

*ECMAScript7*新增了指数操作符：`**`，作用和`Math.pow()`一样。

```js
console.log(Math.pow(3, 2));	// 9
console.log(3 ** 2);	// 9

console.log(Math.pow(16, 0.5));	// 4
console.log(16 ** 0.5);	// 4
```

指数操作符也有自己的指数赋值操作符 `**=`。

```js
let squared = 3;
squared **= 2;	// 3 ** 2
console.log(squared);	// 9
```

### 3.5.7 关系操作符

一般如果一个值不小于另一个值，那就一定大于或等于它。但在涉及`NaN`的比较中，都会返回*false*。

```js
NaN < 3;	// false
NaN >= 3;	// false
```

如果要得到确实按字母顺序比较的结果，就必须把两者都转换为相同的大小写形式。

```js
"Brick" < "alphabet";	// true
"Brick".toLowerCase() < "alphabet".toLowerCase();	// false
```

## 3.6 语句

### 3.6.5 for-in 语句

`for-in`语句是一种严格的迭代语句，用于枚举对象中的**非符号键属性**，语法如下：

```js
const obj = {
  name: 'eagle',
  age: 21
}
for (const propertyName in obj) {
  console.log(propertyName);	// name, age
}
```

*ECMAScript*中对象的属性是无序的，因此不能保证返回对象属性的顺序。

如果*for-in*循环要迭代的变量是`null`或`undefined`，则不执行循环体。

### 3.6.6 for-of 语句

`for-of`语句是一种严格的迭代语句，用于遍历可迭代对象的元素。如果尝试迭代的变量不支持迭代，则*for-of*语句会抛出错误。语法如下：

```js
const obj = {
  name: 'eagle',
  age: 21
}
const arr = [1, 3, 5, 7, 9];
for (const propertyValue of obj) {}	// TypeError: obj is not iterable
for (const value of arr) {
  console.log(value);	// 1, 3, 5, 7, 9
}
```

上述*obj*使用*for-of*会报错，是因为这个自定义对象不支持迭代。只有提供了`Iterator`接口才能使用*for-of*循环，可以为其指定`Symbol.iterator`，使其变为可迭代。*for-of*循环会按照可迭代对象的`next()`方法产生值的顺序迭代元素。

[为 obj 添加 Symbol.iterator 使其支持 for-of 循环](第3章-语言基础\3.6.6-for-of语句.js)

*ES2018*对*for-of*语句进行了扩展，增加了`for-await-of`循环，以支持生成期约*promise*的异步可迭代对象。详情请见 [附录A](本书资源\54538 附录A_D.pdf)。

### 3.6.7 标签语句

标签语句用于给语句加标签，可以在后面通过`break`或`continue`语句引用，典型应用场景是嵌套循环。例子看下面的*3.6.8*节。语法如下：

```js
label: statement
```

### 3.6.8 break 和 continue 语句

`break`语句用于退出当前循环，`continue`语句用于退出本次循环。它们都可以与标签语句结合使用，特别是在嵌套循环中。

```js
let num = 0;

outermost:
for (let i = 0; i < 10; i++) {
  for (let j = 0; j < 10; j++) {
		if (i == 5 && j == 5) {
			break outermost;
    }
    num++;
  }
}

alert(num);	// 55
```

### 3.6.9 with 语句

`with`语句的用途是将代码作用域设置为特定的对象。主要使用场景是针对一个对象反复操作时，将代码作用域设置为该对象能提供便利。语法如下：

```js
with (expression) statement;
```

**由于*with*语句影响性能且难于调试其中的代码，通常不推荐使用。**

### 3.6.10 switch 语句

***switch*语句在比较每个条件的值时会使用全等操作符，因此不会强制转换数据类型。**







# 第 4 章  变量、作用域与内存

## 4.1 原始值与引用值

保存原始值的变量是**按值**访问的。保存引用值的变量是**按引用**访问的。

### 4.1.1 动态属性

对于引用值而言，可以随时添加、修改和删除其属性和方法。而原始值不能有属性，尽管尝试给原始值添加属性不会报错。原始类型的初始化可以只使用原始字面量形式，如果使用的是*new*关键字，则*JavaScript*会创建一个*Object*类型的实例，其行为类似原始值。

```js
let name1 = 'eagle';
let name2 = new String('eagle');
name1.age = 21;
name2.age = 21;
console.log(name1.age);	// undefined
console.log(name2.age);	// 21
console.log(typeof name1);	// string
console.log(typeof name2);	// object
```

### 4.1.4 确定类型

前面提到的`typeof`操作符最适合用来判断一个变量是否为原始类型。而如果想知道一个值是什么类型的对象，就该使用`instanceof`操作符。如果变量是给定引用类型的实例，则*instanceof*操作符返回`true`。如果用*instanceof*检测原始值，则始终会返回`false`，因为原始值不是对象。

## 4.3 垃圾回收

*JavaScript*是使用垃圾回收的语言，也就是说执行环境负责在代码执行时管理内存。







# 第 5 章  基本引用类型

**引用类型虽然有点像类，但跟类并不是一个概念，有时会被称为对象定义。函数也是一种引用类型。**

## 5.1 Date

`Date`类型将日期保存为自*UTC*时间 1970 年 1 月 1 日午夜零时至今所经过的毫秒数。要创建日期对象，使用`new`操作符来调用`Date`构造函数。不传参数的情况下，创建的对象将保存当前日期和时间。

```js
let now = new Date();
```

要基于其它日期和时间创建日期对象，必须传入其毫秒表示。*ECMAScript*为此提供了两个辅助方法：`Date.parse()`和`Date.UTC()`。

`Date.parse()`方法接收一个表示日期的字符串参数，尝试将这个字符串转换为表示该日期的毫秒数。如果传递的字符串参数并不表示日期，则该方法会返回`NaN`。支持的日期字符串格式如下：

- "月/日/年"："12/3/2020"
- "月名 日, 年"："December 3, 2020"
- "周几 月名 日 年 时:分:秒 时区"："Thu December 3 2020 09:00:00 GMT-0700"
- *ISO 8601*扩展格式*"YYYY-MM-DDTHH:mm:ss.sssZ"*(只适用于兼容*ES5*的实现)："2020-12-03T09:00:00"

如果直接把表示日期的字符串传给`Date`构造函数，那么*Date*会在后台调用`Date.parse()`。所以下面两行代码是等价的：

```js
let someDate = new Date(Date.parse("December 3, 2020"));
// 等价于
let someDate = new Date("December 3, 2020");
```

**上面两行代码在不同环境下返回值不同。**在浏览器中，返回值为*Thu Dec 03 2020 00:00:00 GMT+0800 (中国标准时间)*，而在*node*中，返回值为*2020-12-02T16:00:00.000Z*。浏览器环境的返回值是符合预期的。

`Date.UTC()`方法也返回日期的毫秒表示，传给该方法的参数是年、零起点月数(0~11)、日(1~31)、时(0~23)、分、秒和毫秒。这些参数中，年和月是必需的，其余参数默认为 0。同*Date.parse()*一样，如果直接把这些参数传给`Date`构造函数，也会在后台隐式调用*Date.UTC()*。

```js
let someDate = new Date(Date.UTC(2020, 11, 3, 9, 0, 0));	// 2020-12-03T09:00:00.000Z
let another = new Date(2020, 11, 3, 9, 0, 0);	// 2020-12-03T01:00:00.000Z
```

**上面两行代码在不同环境下返回值也不同。**`new Date(Date.UTC(2020, 11, 3, 9, 0, 0))`在浏览器环境中返回*Thu Dec 03 2020 17:00:00 GMT+0800 (中国标准时间)*，而在*node*中返回*2020-12-03T09:00:00.000Z*。`new Date(2020, 11, 3, 9, 0, 0)`在浏览器环境中返回*Thu Dec 03 2020 09:00:00 GMT+0800 (中国标准时间)*，而在*node*中返回*2020-12-03T01:00:00.000Z*。所以，在浏览器环境中，直接把日期数字参数传给*Date*构造函数能够获得符合预期的日期对象；在*node*环境中，先用*Date.UTC()*方法转换日期数字，再传给*Date*构造函数能够获得符合预期的日期对象。

> 综上所述，在浏览器环境下想要获得本地时间的日期对象，可以直接把日期字符串参数或日期数字参数传给`Date`构造函数，或先用`Date.parse()`方法转换日期字符串参数再传给*Date*构造函数。
>
> 在*node*环境下想要获得本地时间的日期对象，需要先用`Date.UTC()`方法转换日期数字，再传给*Date*构造函数。
>
> [浏览器环境下测试](第5章-基本引用类型\5.1-Date.html)
>
> [node 环境下测试](第5章-基本引用类型\5.1-Date.js)

*ECMAScript*还提供了`Date.now()`方法，返回表示方法执行时日期和时间的毫秒数。

### 5.1.1 继承的方法

`Date`类型的`toLocaleString()`方法返回与浏览器运行的本地环境一致的日期和时间，包含针对时间的*AM*或*PM*，但不包含时区信息，具体格式可能因浏览器而不同。`toString()`方法通常返回带时区信息的日期或时间。现代浏览器在这两个方法的输出上已经趋于一致，但较老的浏览器上可能不同，**这意味着*toLocaleString()*和*toString()*可能只对调试有用，不能用于显式**。`valueOf()`方法返回的是日期的毫秒表示。

```js
new Date().toLocaleString();	// "2020/12/3 下午1:26:39"
new Date().toString();		// "Thu Dec 03 2020 13:26:51 GMT+0800 (中国标准时间)"
new Date().valueOf();		// 1606973219918
```

### 5.1.2 日期格式化方法

*Date*类型有以下几个专门用于格式化日期的方法，它们都会返回字符串：

- `toDateString()`：显式日期中的周几、月、日、年，格式特定于实现。
- `toTimeString()`：显式日期中的时、分、秒和时区，格式特定于实现。
- `toLocaleDateString()`：显式日期中的周几、月、日、年，格式特定于实现和地区。
- `toLocaleTimeString()`：显式日期中的时、分、秒，，格式特定于实现和地区。
- `toUTCString()`：显式完整的*UTC*日期，格式特定于实现。
- `toGMTString()`：跟*toUTCString()*是一样的，目的是为了向后兼容，规范建议使用*toUTCString()*。

**这些方法的输出与*toLocaleString()*和*toString()*一样，会因浏览器而异。因此不能用于在界面上显式日期。**

### 5.1.3 日期/时间组件方法

下表展示了*Date*类型剩下的方法，用于取得或设置日期值的特定部分。表中*UTC*日期指的是没有时区偏移时的日期，也就是没有转换为*GMT*时的日期。中国标准时间 = GMT+0800 = UTC+8，GMT = UTC+0。

| 方法                     | 说明                                                         |
| ------------------------ | ------------------------------------------------------------ |
| getTime()                | 返回表示日期的毫秒数。与 valueOf() 方法返回的值相同。        |
| setTime( ms )            | 以毫秒数设置日期，会改变整个日期。                           |
| getFullYear()            | 取得 4 位数的年份。                                          |
| getUTCFullYear()         | 返回 UTC 日期的 4 位数年份。                                 |
| setFullYear( y )         | 设置日期的年份。传入的年份值必须是 4 位数字。                |
| setUTCFullYear( y )      | 设置 UTC 日期的年份。传入的年份值必须是 4 位数字。           |
| getMonth()               | 返回日期中的月份。0 表示一月，11 表示十二月。                |
| getUTCMonth()            | 返回 UTC 日期中的月份。0 表示一月，11 表示十二月。           |
| setMonth( M )            | 设置日期的月份。传入的月份值必须大于 0，超过 11 则增加年份。 |
| setUTCMonth( M )         | 设置 UTC 日期的月份。传入的月份值必须大于 0，超过 11 则增加年份。 |
| getDate()                | 返回日期月份中的天数 1 ~ 31。                                |
| getUTCDate()             | 返回 UTC 日期月份中的天数 1 ~ 31。                           |
| setDate( d )             | 设置日期月份中的天数。如果传入的值超过了该月中应有的天数，则增加月份。 |
| setUTCDate( d )          | 设置 UTC 日期月份中的天数。如果传入的值超过了该月中应有的天数，则增加月份。 |
| getDay()                 | 返回日期中星期的星期几，0 表示星期日，6 表示星期六。         |
| getUTCDay()              | 返回 UTC 日期中星期的星期几，0 表示星期日，6 表示星期六。    |
| getHours()               | 返回日期中的小时数 0 ~ 23。                                  |
| getUTCHours()            | 返回 UTC 日期中的小时数 0 ~ 23。                             |
| setHours( h )            | 设置日期中的小时数。传入的值超过了 23 则增加月份中的天数。   |
| setUTCHours( h )         | 设置 UTC 日期中的小时数。传入的值超过了 23 则增加月份中的天数。 |
| getMinutes()             | 返回日期中的分钟数 0 ~ 59。                                  |
| getUTCMinutes()          | 返回 UTC 日期中的分钟数 0 ~ 59。                             |
| setMinutes( m )          | 设置日期中的分钟数。传入的值超过 59 则增加小时数。           |
| setUTCMinutes( m )       | 设置 UTC 日期中的分钟数。传入的值超过 59 则增加小时数。      |
| getSeconds()             | 返回日期中的秒数 0 ~ 59。                                    |
| getUTCSeconds()          | 返回 UTC 日期中的秒数 0 ~ 59。                               |
| setSeconds( s )          | 设置日期中的秒数。传入的值超过 59 则增加分钟数。             |
| setUTCSeconds( s )       | 设置 UTC 日期中的秒数。传入的值超过 59 则增加分钟数。        |
| getMilliseconds()        | 返回日期中的毫秒数。                                         |
| getUTCMilliseconds()     | 返回 UTC 日期中的毫秒数。                                    |
| setMilliseconds( ms )    | 设置日期中的毫秒数。                                         |
| setUTCMilliseconds( ms ) | 设置 UTC 日期中的毫秒数。                                    |
| getTimezoneOffset()      | 返回本地时间与 UTC 时间相差的分钟数。                        |

## 5.2 RegExp

*ECMAScript*通过`RegExp`类型支持正则表达式。语法如下：

```js
let expression = /pattern/flags;
```

*pattern*模式可以是任何简单或复杂的正则表达式，包括字符类、限定符、分组、向前查找和反向引用。每个正则表达式可以带零个或多个*flags*标记，用于控制正则表达式的行为。标记有下列几个：

- `g`：全局模式。表示查找字符串的全部内容，而不是找到第一个匹配的内容就结束。
- `i`：不区分大小写。表示在查找匹配时忽略*pattern*和字符串的大小写。
- `m`：多行模式。表示查找到一行文本末尾时会继续查找。
- `y`：粘附模式。表示只查找从*lastIndex*开始及之后的字符串。
- `u`：*Unicode*模式。启用*Unicode*匹配。
- `s`：*dotAll*模式。表示元字符`.`匹配任何字符，包括*\n*或*\r*。

因为元字符在正则表达式中都有一种或多种特殊功能，所以要匹配元字符必须使用反斜杠来转义。元字符包括：`(`、`[`、`{`、`\`、`^`、`$`、`|`、`)`、`]`、`}`、`?`、`*`、`+`、`.`。

```js
// 匹配第一个 bat 或 cat，忽略大小写
let pattern1 = /[bc]at/i;

// 匹配第一个 [bc]at，忽略大小写
let pattern2 = /\[bc\]at/i;

// 匹配所有以 at 结尾的三字符组合，忽略大小写
let pattern3 = /.at/gi;

// 匹配所有 .at，忽略大小写
let pattern4 = /\.at/gi;
```

正则表达式除了像上面这样使用字面量形式定义，还可以使用`RegExp`构造函数创建。构造函数接收两个参数：模式字符串和可选的标记字符串。**因为用构造函数创建正则表达式的模式是字符串，所以元字符的转义需要使用两个反斜杠*\\\\***。此外，使用*RegExp*也可以基于已有的正则表达式实例创建，并可选择性地修改它们的标记。

```js
// 匹配第一个 [bc]at，忽略大小写
let pattern1 = /\[bc\]at/i;

// 跟 pattern1 一样
let pattern2 = new RegExp("\\[bc\\]at", "i");

// 根据 pattern1 创建，修改为匹配所有的 [bc]at
let pattern3 = new RegExp(pattern1, "g");
```

### 5.2.1 RegExp 实例属性

每个*RegExp*实例都有下列属性，提供有关模式的各方面信息。

- `global`：布尔值，表示是否设置了`g`标记。
- `ignoreCase`：布尔值，表示是否设置了`i`标记。
- `unicode`：布尔值，表示是否设置了`u`标记。
- `sticky`：布尔值，表示是否设置了`y`标记。
- `multiline`：布尔值，表示是否设置了`m`标记。
- `dotAll`：布尔值，表示是否设置了`s`标记。
- `lastIndex`：整数，表示在源字符串中下一次搜索的开始位置，始终从 0 开始。
- `source`：正则表达式的字面量字符串，没有开头和结尾的斜杆。
- `flags`：正则表达式的标记字符串。

### 5.2.2 RegExp 实例方法

方法`exec()`主要用于配合捕获组（捕获组就是使用`()`捕获字符串中的零到多个匹配项）使用。该方法只接收一个参数，即要应用模式的字符串。如果找到了匹配项，则返回包含第一个匹配信息的数组；如果没有找到匹配项，则返回`null`。返回的数组包含两个额外的属性：`index`和`input`。*index*是字符串中匹配模式的起始位置，*input*是要查找的字符串。返回数组的第一个元素是匹配整个模式的字符串，其他元素是与表达式中的捕获组匹配的字符串。如果模式中没有捕获组，则数组只包含一个元素。

```js
let text = "mom and dad and baby";
let pattern = /mom( and dad( and baby)?)?/gi;
let matches = pattern.exec(text);
console.log(matches.index);	// 0
console.log(matches.input);	// "mom and dad and baby"
console.log(matches[0]);		// "mom and dad and baby"
console.log(matches[1]);		// " and dad and baby"
console.log(matches[2]);		// " and baby"
```

调用*exec()*后找到了一个匹配项，因为整个字符串匹配模式，所以数组的*index*属性就是 0。数组的第一个元素是匹配的整个字符串，第二个元素是匹配第一个捕获组的字符串，第三个元素是匹配第二个捕获组的字符串。

如果模式设置了全局标记，则每次调用*exec()*方法会返回一个匹配的信息。如果没有设置全局标记，则无论对同一个字符串调用多少次*exec()*，也只会返回第一个匹配的信息：[有无全局标记的区别](第5章-基本引用类型\5.2.2-RegExp实例方法.js)。

另一个方法`test()`，接收一个字符串参数。如果输入的文本与模式匹配，则方法返回`true`，否则返回`false`。

### 5.2.3 RegExp 构造函数属性

*RegExp*构造函数本身也有几个属性，适用于作用域中的所有正则表达式，且会根据最后执行的正则表达式操作而变化。这些属性都有一个全名和简写，下表列出了这些属性：

| 全名         | 简写 | 说明                                |
| ------------ | ---- | ----------------------------------- |
| input        | $_   | 最后搜索的字符串                    |
| lastMatch    | $&   | 最后匹配的文本                      |
| lastParen    | $+   | 最后匹配的捕获组                    |
| leftContext  | $`   | input 字符串中 lastMatch 之前的文本 |
| rightContext | $'   | input 字符串中 lastMatch 之后的文本 |

```js
let text = "this has been a short summer";
let pattern = /(.)hort/g;

if (pattern.test(text)) {
  console.log(RegExp.input);				// 简写：RegExp.$_，"this has been a short summer"
  console.log(RegExp.leftContext);	// 简写：RegExp["$`"]，"this has been a "
  console.log(RegExp.rightContext);	// 简写：RegExp["$'"]，" summer"
  console.log(RegExp.lastMatch);		// 简写：RegExp["$&"]，"short"
  console.log(RegExp.lastParen);		// 简写：RegExp["$+"]，"s"
}
```

可以通过`RegExp.$1 ~ RegExp.$9`来访问正则的最多 9 个捕获组的匹配项，在调用*exec()*或*test()*时，这些属性就会被填充。

```js
let text = "this has been a short summer";
let pattern = /(..)or(.)/g;

if (pattern.test(text)) {
  console.log(RegExp.$1);	// sh
  console.log(RegExp.$2);	// t
}
```

***RegExp*构造函数的所有属性都没有任何*Web*标准出处，因此不要在生产环境中使用它们。**

## 5.3 原始值包装类型

为了方便操作原始值，*ECMAScript*提供了 3 种特殊的引用类型：`Boolean`、`Number`和`String`。每当用到某个原始值的方法或属性时，后台都会创建一个相应原始包装类型的对象，从而暴露出操作原始值的各种方法。看下面的例子：

```js
let s1 = "some text";
let s2 = s1.substring(2);
```

上面的例子中，*s1*是一个包含字符串的变量，是一个原始值。当第二行访问*s1*时，是以读模式访问的，以读模式访问字符串值得时候，后台会执行以下 3 步：

1. 创建一个*String*类型得实例；
2. 调用实例上的特定方法；
3. 销毁实例。

```js
let s1 = new String("some text");
let s2 = s1.substring(2);
s1 = null;
```

这种行为可以让原始值拥有对象的行为。对布尔值和数值而言，以上 3 步也会在后台发生，只不过使用的是*Boolean*和*Number*包装类型而已。

引用类型和原始值包装类型的主要区别在于对象的生命周期。通过*new*实例化引用类型后，得到的实例会在离开作用域时被销毁，而自动创建的原始值包装对象则只存在于访问它的那行代码执行期间。这意味着无法在运行时给原始值添加属性和方法。

```js
let s1 = "some text";
s1.color = "red";
console.log(s1.color);	// undefined
```

在原始值包装类型的实例上调用`typeof`会返回*"object"*。所有原始包装类型对象都会转换为布尔值*true*，尽管值在字面量形式时会返回*false*。

```js
let str = new String('');
console.log(typeof str);	// object
console.log(!!str);		// true
```

*Object*构造函数作为一个工厂方法，能够根据传入值的类型返回相应原始值包装类型的实例。

```js
let obj = new Object("some text");
console.log(obj instanceof String);	// true
```

使用*new*调用原始值包装类型的构造函数，与调用同名的转型函数并不一样。

```js
let value = "21";
let number = Number(value);
console.log(typeof number);	// "number"
let obj = new Number(value);
console.log(typeof obj);		// "object"
```

### 5.3.2 Number

*Number*类型提供了几个用于将数值格式化为字符串的方法。`toFixed()`方法返回包含指定小数点位数的数值字符串。如果数值本身的小数位超过了参数指定的位数，则四舍五入到最接近的小数位。

```js
let num1 = 10, num2 = 10.005;
console.log(num1.toFixed(2));	// 10.00
console.log(num2.toFixed(2));	// 10.01
```

`toExponential()`返回以科学记数法表示的数值字符串。方法也接收一个参数，表示结果中小数的位数。

```js
let num = 10;
console.log(num.toExponential(1));	// 1.0e+1
```

`toPrecision()`方法会根据数值和精度来决定调用*toFixed()*还是*toExponential()*。这三个方法都会为了以正确的小数位来精确表示数值而向上或向下舍入。

```js
let num = 99;
console.log(num.toPrecision(1));	// 1e+2
console.log(num.toPrecision(3));	// 99.0
```

*ES6*新增了`Number.isInteger()`方法，用于辨别一个数值是否保存为整数。

```js
console.log(Number.isInteger(1.00));	// true
console.log(Number.isInteger(1.01));	// false
```

*IEEE 754*数值格式的整数范围从`Number.MIN_SAFE_INTEGER`到`Number.MAX_SAFE_INTEGER`，对超出这个范围（[-(2\*\*53)+1 = -9007199254740991] ~ [9007199254740991 = 2\*\*53-1]）的数值，即使尝试保存为整数，也可能会表示为一个完全不同的数值。为了鉴别整数是否在这个范围内，可以使用`Number.isSafeInteger()`方法。

```js
console.log(Number.isSafeInteger(-1 * (2 ** 53)));	// false
console.log(Number.isSafeInteger(-1 * (2 ** 53) + 1));	// true
```

### 5.3.3 String

#### 5.3.3.3 字符串操作方法

`slice()`、`substr()`和`substring()`三个方法都会返回被操作字符串的一个子字符串。都接受两个参数，第一个参数指定字符串的开始位置，第二个参数不是必须的。*slice()*和*substring()*方法的第二个参数提取结束的位置，也就是该位置前的字符会被提取出来；*substr()*的第二个参数指定的是子字符串的字符个数。省略第二个参数都意味着提取到字符串末尾。

```js
let str = "hello world";
alert(str.slice(3));	// lo world
alert(str.substring(3));	// lo world
alert(str.substr(3));	// lo world
alert(str.slice(3, 7));	// lo w
alert(str.substring(3, 7));	// lo w
alert(str.substr(3, 7));	// lo worl
```

如果传入的参数是负数，*slice()*方法会将传入的负值与字符串的长度相加，结果作为新参数；*substr()*方法的第一个参数如果是负的，则加上源字符串的长度值，第二个参数如果是负的则变为 0；*substring()*方法会把所有负值参数转换为 0，*substring()*方法较小的参数作为子字符串的开始位置，较大的参数作为子字符串的结束位置。

```js
let str = "hello world";
alert(str.slice(-3));	// rld
alert(str.substring(-3));	// hello world
alert(str.substr(-3));	// rld
alert(str.slice(3, -4));	// lo w
alert(str.substring(3, -4));	// hel
alert(str.substr(3, -4));	// '' 空字符串，因为字符个数为 0
```

#### 5.3.3.5 字符串包含方法

*ECMAScript6*增加了 3 个用于判断字符串中是否包含另一个字符串的方法：`startsWith()`、`endsWith()`和`includes()`。*startsWith()*方法从头开始向后检查，可选的第二个参数表示开始检查的位置。*endsWith()*方法从末尾开始向前检查，可选的第二个参数表示字符串的结束位置。*includes()*检查整个字符串，可选的第二个参数表示开始检查的位置。

```js
let message = "foobarbaz";

console.log(message.startsWith("foo"));	// true
console.log(message.startsWith("foo", 1));	// false

console.log(message.endsWith("baz"));	// true
console.log(message.endsWith("bar", 6));	// true

console.log(message.includes("bar"));	// true
console.log(message.includes("bar", 4));	// false
```

#### 5.3.3.6 trim() 方法

`trim()`方法会创建字符串的副本，删除前后所有的空格并返回副本。`trimLeft()`删除字符串开始的空格，`trimRight()`删除字符串结尾的空格。

#### 5.3.3.7 repeat() 方法

`repeat()`方法接收一个整数参数，表示将字符串复制多少次，然后返回拼接所有副本后的结果。

#### 5.3.3.8 padStart() 和 padEnd() 方法

`padStart()`和`padEnd()`方法会复制字符串。第一个参数是长度，第二个参数是可选的填充字符串，默认为空格。如果第一个参数小于或等于字符串长度，会返回原始字符串。第二个参数可能会被截断。

```js
let str = "foo";
console.log(str.padStart(6));	// "   foo"
console.log(str.padStart(2));	// "foo"
console.log(str.padEnd(8, "bar"));	// "foobarba"
```

#### 5.3.3.9 字符串迭代与解构

字符串的原型上暴露了`@@iterator`方法，也就是`Symbol.iterator`。表示可以迭代字符串的每个字符。

```js
let message = "abc";
let stringIterator = message[Symbol.iterator]();

console.log(stringIterator.next());	// { value: "a", done: false}
console.log(stringIterator.next());	// { value: "b", done: false}
console.log(stringIterator.next());	// { value: "c", done: false}
console.log(stringIterator.next());	// { value: undefined, done: true}
```

*for-of*循环中就是通过迭代器按序访问每个字符的。有了这个迭代器后，就可以通过解构操作符`...`来解构字符串了。

```js
let message = "eagle";
console.log([...message]);	// ["e", "a", "g", "l", "e"]
```

#### 5.3.3.10 字符串大小写转换

`toLowerCase()`、`toLocaleLowerCase()`、`toUpperCase()`和`toLocaleUpperCase()`四个方法用于大小写转换。其中*toLocaleLowerCase()*和*toLocaleUpperCase()*两个方法旨在基于特定地区实现，在少数语言中，*Unicode*大小写转换需应用特殊规则，要使用地区特定的方法才能实现正确转换。如果不知道代码涉及什么语言，最好使用地区特定的转换方法。

#### 5.3.3.11 字符串模式匹配方法

*String*类专门为在字符串中实现模式匹配设计了几个方法。第一个`match()`方法本质上和*RegExp*对象的*exec()*方法相同。该方法接收一个参数，可以是正则表达式字符串，也可以是*RegExp*对象。

```js
let text = "cat, bat, sat, fat";
let pattern = /.at/;
// 等价于 pattern.exec(text)
let matches = text.match(pattern);
console.log(matches.index);	// 0
console.log(matches[0]);		// "cat"
console.log(matches.lastIndex);	// 0
```

另一个查找模式的方法是`search()`。该方法也接收一个正则表达式字符串或*RegExp*对象。返回模式第一个匹配的位置索引，如果没找到则返回*-1*。

```js
let text = "cat, bat, sat, fat";
let pos = text.search(/at/);
console.log(pos);	// 1
```

`replace()`方法用于字符串替换操作。这个方法接收两个参数，第一个参数是一个*RegExp*对象或一个字符串，第二个参数可以是一个字符串或函数。如果第一个参数是字符串，那么只会替换第一个子字符串，想要替换所有的子字符串，需要使用正则表达式。

```js
let text = "cat, bat, sat, fat";
let result = text.replace("at", "ond");	// cond, bat, sat, fat
result = text.replace(/at/g, "ond");	// cond, bond, sond, fond
```

第二个参数是字符串的情况下，有几个特殊的字符序列，可以用来插入正则表达式操作的值。

| 字符序列 | 替换文本                                                     |
| -------- | ------------------------------------------------------------ |
| $$       | $                                                            |
| $&       | 匹配整个模式的子字符串。与*RegExp.lastMatch*相同             |
| $'       | 匹配的子字符串之前的字符串。与*RegExp.rightContext*相同      |
| $`       | 匹配的子字符串之后的字符串。与*RegExp.leftContext*相同       |
| $n       | 匹配第*n*个捕获组的字符串，其中*n*是 0~9。比如，*$1*是匹配第一个捕获组的字符串，*$2*是匹配第二个捕获组的字符串，以此类推。如果没有捕获组，则值为空字符串 |
| $nn      | 匹配第*nn*个捕获组的字符串，其中*nn*是 00~99。比如，*$01*是匹配第一个捕获组的字符串，*$02*是匹配第二个捕获组的字符串，以此类推。如果没有捕获组，则值为空字符串 |

```js
let text = "cat, bat, sat, fat";
let result = text.replace(/(.at)/g, "word ($1)");
console.log(result);	// word (cat), word (bat), word (sat), word (fat)
```

第二个参数是函数的情况下，只有一个匹配项时，这个函数会收到 3 个参数：与整个模式匹配的字符串、匹配项在字符串中的开始位置，以及整个字符串。在有多个捕获组的情况下，每个匹配捕获组的字符串也会作为参数传给这个函数，但最后两个参数还是与整个模式匹配的开始位置和原始字符串。该函数也应该返回一个字符串，表示应该把匹配项替换成什么。使用函数作为第二个参数可以更细致地控制替换过程。

```js
function htmlEscape (text) {
  return text.replace(/[<>"&]/g, function (match, pos, originalText) {
    switch (match) {
      case "<":
        return "&lt;";
      case ">":
        return "&gt;";
      case "&":
        return "&amp;";
      case "\"":
        return "&quot;";
    }
  })
}

console.log(htmlEscape("<p class=\"greeting\">Hello world!</p>"));
// &lt;p class=&quot;greeting&quot;&gt;Hello world!&lt;/p&gt;
```

`split()`方法会根据传入的分隔符将字符串拆分为数组。分隔符参数可以是字符串也可以是*RegExp*对象。还可以传入第二个参数，用于确保返回的数组不会超过指定大小。

```js
let colorText = "red,blue,green,yellow";
let color1 = colorText.split(",");	// ["red", "blue", "green", "yellow"]
let color2 = colorText.split(",", 2);	// ["red", "blue"]
```

#### 5.3.3.12 localeCompare() 方法

`localeCompare()`方法比较两个字符串，根据以下情况返回具体的值：

- 如果按照字母表顺序，字符串应该排在字符串参数前面，则返回负值，通常为 -1，具体可能因具体实现而异。
- 如果字符串和字符串参数相等，则返回 0。
- 如果按照字母表顺序，字符串应该排在字符串参数后面，则返回正值，通常为 1，具体可能因具体实现而异。

**因为返回的值不一定为 -1 或 1，所以不要以是否等于 -1 或 1 为条件来判断，而应该判断是否小于零或大于零。**

```js
let str = "yellow";
console.log(str.localeCompare("brick"));	// 1
console.log(str.localeCompare("yellow"));	// 0
console.log(str.localeCompare("zoo"));		// -1
```

## 5.4 单例内置对象

*ECMA-262*对内置对象的定义是：由*ECMAScript*实现提供、与宿主环境无关，并在程序开始执行时就存在的对象。*Object*、*Array*和*String*就是内置对象。本节介绍另外两个：`Global`和`Math`。

### 5.4.1 Global

`Global`对象是*ECMAScript*中最特别的对象，因为代码不会显式地访问它。它所针对的是不属于任何对象的属性和方法。事实上，不存在全局变量或全局函数这种东西，在全局作用域中定于的变量和函数都会变成*Global*对象的属性。

#### 5.4.1.1 URL 编码方法

`encodeURI()`和`encodeURIComponent()`方法用于编码统一资源标识符*URI*，以便传给浏览器。`decodeURI()`只对使用*encodeURI()*编码过的字符解码。`decodeURIComponent()`解码所有被*encodeURIComponent()*编码的字符。

#### 5.4.1.2 eval() 方法

`eval()`方法是一个完整的*ECMAScript*解释器。它接收一个参数，即一个要执行的*ECMAScript/JavaScript*字符串。当解释器发现*eval()*调用时，会将参数解释为实际的*ECMAScript*语句，然后将其插入到该位置。

```js
eval("console.log('hi')");
// 等价于
console.log("hi");
```

#### 5.4.1.4 window 对象

浏览器将`window`对象实现为*Global*对象的代理。所有全局作用域中声明的变量和函数都变成了*window*的属性。

### 5.4.2 Math

*ECMAScript*提供了`Math`对象作为保存数学公式、信息和计算的地方。***Math*对象上提供的计算要比直接在*JavaScript*实现的快得多，因为*Math*对象上的计算使用了*JavaScript*引擎中更高效的实现和处理器指令。但使用*Math*计算的问题是精度会因浏览器、操作系统、指令集和硬件而异。**

#### 5.4.2.1 Math 对象属性

| 属性         | 说明                          |
| ------------ | ----------------------------- |
| Math.E       | 自然对数的底数，即常量 e 的值 |
| Math.LN10    | 10 的自然对数                 |
| Math.LN2     | 2 的自然对数                  |
| Math.LOG2E   | 以 2 为底 e 的对数            |
| Math.LOG10E  | 以 10 为底 e 的对数           |
| Math.PI      | Π 的值                        |
| Math.SQRT1_2 | 1/2 的平方根                  |
| Math.SQRT2   | 2 的平方根                    |

#### 5.4.2.2 min() 和 max() 方法

这两个方法用于确定一组数值中的最小值和最大值。要找到数组中的最大值或最小值可以这样做。

```js
let values = [1, 2, 3, 4, 5, 6, 7, 8, 9];
let max = Math.max(...values);	// 9
```

#### 5.4.2.3 舍入方法

- `Math.ceil()`：将数值向上舍入为最接近的整数。
- `Math.floor()`：将数值向下舍入为最接近的整数。
- `Math.round()`：将数值四舍五入。
- `Math.fround()`：返回数组最接近的单精度 32 位浮点值表示。

#### 5.4.2.4 random() 方法

`random()`方法随机返回 0~1 之间的数，包括 0 不包括 1。如果需要更高的不确定性数字，建议使用`window.crypto.getRandomValues()`方法。

#### 5.4.2.5 其他方法

这些方法的精度可能因实现而异。

| 方法                | 说明                             |
| ------------------- | -------------------------------- |
| Math.abs(x)         | 返回 x 的绝对值                  |
| Math.exp(x)         | 返回 Math.E 的 x 次幂            |
| Math.expm1(x)       | 等于 Math.exp(x) - 1             |
| Math.log(x)         | 返回 x 的自然对数                |
| Math.log1p(x)       | 等于 1 + Math.log(x)             |
| Math.pow(x, power)  | 返回 x 的 power 次幂             |
| Math.hypot(...nums) | 返回 nums 中每个数平方和的平方根 |
| Math.clz32(x)       | 返回 32 位整数 x 的前置零的数量  |
| Math.sign(x)        | 返回表示 x 符号的 1、0、-0 或 -1 |
| Math.trunc(x)       | 返回 x 的整数部分，删除所有小数  |
| Math.sqrt(x)        | 返回 x 的平方根                  |
| Math.cbrt(x)        | 返回 x 的立方根                  |
| Math.acos(x)        | 返回 x 的反余弦                  |
| Math.acosh(x)       | 返回 x 的反双曲余弦              |
| Math.asin(x)        | 返回 x 的反正弦                  |
| Math.asinh(x)       | 返回 x 的反双曲正弦              |
| Math.atan(x)        | 返回 x 的反正切                  |
| Math.atanh(x)       | 返回 x 的反双曲正切              |
| Math.atan2(y, x)    | 返回 y/x 的反正切                |
| Math.cos(x)         | 返回 x 的余弦                    |
| Math.sin(x)         | 返回 x 的正弦                    |
| Math.tan(x)         | 返回 x 的正切                    |







# 第 6 章  集合引用类型

## 6.1 Object

显式地创建`Object`的实例有两种方式。第一种是使用*new*操作符和*Object*构造函数。第二种是使用对象字面量表示法。**在使用对象字面量表示法定义对象时，并不会实际调用*Object*构造函数。**

## 6.2 Array

*ECMAScript*的数组是一组有序的数据。数组中每个槽位可以存储任意类型的数据。数组是动态大小的。

### 6.2.1 创建数组

创建数组可以使用`Array`构造函数，也可以使用数组字面量表示法。**与对象一样，使用数组字面量表示法创建数组不会调用*Array*构造函数。**构造函数*Array*可以传入参数，如果不传参数，表示创建一个空数组。如果传入一个参数，且参数为数值类型，则表示创建一个*length*为数值参数的数组；如果传入一个参数，参数不是数值类型，则表示创建一个数组，该数组的第一个槽位为参数。如果传入多个参数，则这些参数为数组要保存的元素。

```js
let arr1 = new Array();		// []
let arr2 = [];		// []
let arr3 = new Array(10);	// [<10 empty items>], arr3.length = 10
let arr4 = new Array("10");		// ["10"]
let arr5 = new Array("red", "green", "blue");		// ["red", "green", "blue"]
```

*ES6*为*Array*构造函数新增了两个用于创建数组的静态方法：`from()`和`of()`。*from()*用于将类数组结构转换为数组实例，而*of()*用于将一组参数转换为数组实例。

`Array.from()`的第一个参数是一个类数组对象，即任何可迭代的结构，或者有一个*length*属性和可索引元素的结构。可用于如下场合：

```js
// 拆分字符串为单字符数组
console.log(Array.from("eagle"));	// [ 'e', 'a', 'g', 'l', 'e' ]

// 将集合或映射转换为一个新数组
const m = new Map().set(1, 2).set(3, 4);
const s = new Set().add(1).add(2).add(3).add(4);
console.log(Array.from(m));	// [ [ 1, 2 ], [ 3, 4 ] ]
console.log(Array.from(s));	// [ 1, 2, 3, 4 ]

// 对现有数组进行浅复制
// 浅复制是只复制基本类型值，如果有子引用类型，则复制指针
const a1 = [1, 2, 3, [4, 5]];
const a2 = Array.from(a1);
a1[0] = -1;
console.log(a2);	// [1, 2, 3, [4, 5]]
a1[3][0] = -4;
console.log(a2);	// [1, 2, 3, [-4, 5]]

// 可以迭代任何可迭代对象
const iter = {
  *[Symbol.iterator] () {
    yield 1;
    yield 2;
    yield 3;
  }
};
console.log(Array.from(iter));	// [1, 2, 3]

// 轻松将 arguments 对象转换为数组
function getArgsArray () {
  return Array.from(arguments);
}
console.log(getArgsArray(1, 2, 3));	// [1, 2, 3]

// from() 也能转换带有类数组结构必要属性的自定义对象
const arrayLikeObject = {
  0: 1,
  1: 2,
  2: 3,
  length: 3
};
console.log(Array.from(arrayLikeObject));	// [1, 2, 3]
```

*Array.from()*还接收第二个可选的映射函数参数。这个函数可以直接增强新数组的值，而无需像调用*Array.from().map()*那样先创建一个中间数组。还可以接收第三个可选参数，用于指定映射函数中*this*的值，但该*this*值在箭头函数中不适用。

```js
const a1 = [1, 2, 3, 4]
const a2 = Array.from(a1, x => x ** 2);	// [1, 4, 9, 16]
const a3 = Array.from(a1, function (x) { return x ** this.exponent }, { exponent: 2 });
// [1, 4, 9, 16]
```

`Array.of()`可以把一组参数转换为数组。

```js
console.log(Array.of(1, 2, 3, 4));	// [1, 2, 3, 4]
console.log(Array.of(undefined));		// [undefined]
```

### 6.2.2 数组空位

使用数组字面量初始化数组时，可以使用逗号间隔`[1, , 3]`来创建空位。*ES6*新增方法普遍将这些空位当成存在的元素，只不过值为*undefined*。*ES6*之前的方法则会忽略这个空位，但具体的行为也会因方法而异。

**由于行为不一致和存在性能隐患，因此实践中要避免使用数组空位。如果确实需要空位，可以显式地用*undefined*值代替。**

### 6.2.3 数组索引

数组最多可以包含**4,294,967,295**个元素，如果尝试添加更多项，则会导致抛出错误。

### 6.2.4 检测数组

使用`instanceof`操作符可以判断一个对象是不是数组，但需要在只有一个全局执行上下文的情况下。为了打破这个前提，*ECMAScript*提供了`Array.isArray()`方法，该方法用于确定一个值是否为数组，而不用管它是在哪个全局执行上下文中创建的。

### 6.2.5 迭代器方法

`keys()`返回数组索引的迭代器，`values()`返回数组元素的迭代器，而`entries()`返回索引/值对的迭代器。

```js
const a = ['foo', 'bar', 'baz'];
console.log(Array.from(a.keys()));	// [0, 1, 2]
console.log(Array.from(a.values()));	// ['foo', 'bar', 'baz']
console.log(Array.from(a.entries()));	// [[0, 'foo'], [1, 'bar'], [2, 'baz']]
```

使用*ES6*的解构可以非常容易地在循环中拆分键/值对。

```js
const a = ['foo', 'bar', 'baz'];
for (const [index, element] of a.entries()) {
  console.log(index);		// 0			1				2
  console.log(element);	// 'foo'	'bar'		'baz'
}
```

### 6.2.6 复制和填充方法

`copyWithin()`方法批量复制，`fill()`方法填充数组。这两个方法都是*ES6*新增的方法，两个方法的函数签名类似：都需要指定既有数组实例上的一个范围，包括开始索引，不包含结束索引。

*fill()*方法可以向一个已有的数组中插入全部或部分相同的值。第一个参数为要插入的值；第二个可选参数为开始填充的索引位置，如果不提供则默认为 0；第三个可选参数为结束索引，如果不提供则默认为数组长度，即填充到数组末尾。如果索引为负值，则从数组末尾开始计算，也可以将负索引加上数组长度得到正索引。该方法会静默忽略超出数组边界、零长度及方向相反的索引范围。

```js
const zeroes = [0, 0, 0, 0, 0]

zeroes.fill(5);	// 用 5 填充整个数组 [5, 5, 5, 5, 5]
zeroes.fill(0);	// 重置

zeroes.fill(6, 3);	// 用 6 填充索引大于等于 3 的元素 [0, 0, 0, 6, 6]
zeroes.fill(0);	// 重置

zeroes.fill(7, 1, 3);	// 用 7 填充索引大于等于 1 且小于 3 的元素 [0, 7, 7, 0, 0]
zeroes.fill(0);	// 重置

zeroes.fill(2, 4, 2);	// 索引反向，忽略	[0, 0, 0, 0, 0]
```

*copyWithin()*会按照指定范围**浅复制**数组中的部分内容，然后将它们插入到指定索引开始的位置。第一个参数为复制内容要插入的索引位置，第二和第三个参数与*fill()*方法一样。该方法会静默忽略超出数组边界、零长度及方向相反的索引范围。

```js
let ints,
    reset = () => ints = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
reset();

// 从 ints 中复制索引 0 开始的内容，插入到索引 5 开始的位置
ints.copyWithin(5);	// [0, 1, 2, 3, 4, 0, 1, 2, 3, 4]
reset();

// 从 ints 中复制索引 5 开始的内容，插入到索引 0 开始的位置
ints.copyWithin(0, 5);	// [5, 6, 7, 8, 9, 5, 6, 7, 8, 9]
reset();

// 从 ints 中复制索引 0 开始到所有 3 结束的内容，插入到索引 4 开始的位置
ints.copyWithin(4, 0, 3);	// [0, 1, 2, 3, 0, 1, 2, 7, 8, 9]
reset();

// 索引过低，忽略
ints.copyWithin(1, -15, -12);	// [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

### 6.2.8 栈方法

`push()`方法接收任意数量的参数，并将它们添加到数组末尾，返回数组的最新长度。`pop()`方法则用于删除数组的最后一项，同时减少数组的*length*值，返回被删除的项。

### 6.2.9 队列方法

`shift()`方法会删除数组的第一项并返回它，然后数组长度减 1。`unshift()`方法在数组开头添加任意多个值，然后返回数组的新长度。***unshift(1, 2)*添加后的数组是*[1, 2]*而不是*[2, 1]*。**

### 6.2.10 排序方法

`reverse()`方法将数组元素反向排列。

`sort()`方法默认按照升序重新排列数组元素，为此*sort()*方法会在每一项上调用*String()*转型函数，然后比较字符串来决定顺序。

```js
let values = [0, 1, 5, 10, 15];
values.sort();	// [0, 1, 10, 15, 5]
```

上例中，字符串 "5" 排在 "10" 和 "15" 后面。这明显不符合我们的预期，所以*sort()*方法还可以接收一个比较函数，用于判断哪个值应该排在前面。比较函数接收两个参数，如果第一个参数应该排在第二个参数前面，就返回负值；如果两个参数相等，即位置不变，就返回 0；如果第一个参数应该排在第二个参数后面，就返回正值。

```js
let values = [0, 1, 5, 10, 15]
values.sort((a, b) => a < b ? 1 : a > b ? -1 : 0)	// [15, 10, 5, 1, 0]
```

如果数组的元素是数值，或者是其*valueOf()*方法返回数值的对象，这个比较函数可以写为两数相减。

```js
let values = [3, 6, 3, 9, 32];
values.sort((a, b) => b - a);	// [32, 9, 6, 3, 3]
```

***reverse()*和*sort()*都返回调用它们的数组的引用。**

### 6.2.11 操作方法

`concat()`方法会在现有数组的基础上创建一个新数组，然后把参数添加到新数组的末尾，最后返回这个新数组。

```js
let colors1 = ['red', 'green', 'blue'];
let colors2 = colors1.concat('yellow', ['black', 'brown']);
console.log(colors1);	// [ 'red', 'green', 'blue' ]
console.log(colors2);	// [ 'red', 'green', 'blue', 'yellow', 'black', 'brown' ]
```

*concat()*方法打平数组参数的行为可以通过`Symbol.isConcatSpreadable`符号重写。把这个符号设为*true*可以强制打平类数组对象，设为*false*强制不打平数组。

```js
let colors1 = ['red', 'green', 'blue'];
let newColors = ['black', 'brown'];
// 不打平数组
newColors[Symbol.isConcatSpreadable] = false;
let colors2 = colors1.concat('yellow', newColors);
console.log(colors2);	// ["red", "green", "blue", "yellow", ["black", "brown", Symbol(Symbol.isConcatSpreadable): false]]

let moreNewColors = {
  // 打平类数组对象
  [Symbol.isConcatSpreadable]: true,
  length: 2,
  0: 'pink',
  1: 'cyan'
};
let colors3 = colors1.concat(moreNewColors);
console.log(colors3);	// ['red', 'green', 'blue', 'pink', 'cyan']
```

`slice()`用于创建一个包含原有数组中一个或多个元素的新数组并返回。该方法接收一个或两个参数：返回元素的开始索引和结束索引。如果只有一个参数，会返回该索引到数组末尾的所有元素。如果有两个参数，则返回从开始索引到结束索引对应的所有元素，不包括结束索引对应的元素。**如果参数有负值，则将其加上数组长度并把结果作为新参数。如果结束位置小于开始位置，则返回空数组。**

```js
let colors1 = ['red', 'green', 'blue']
let colors2 = colors1.slice(1);	// ['green', 'blue']
let colors3 = colors1.slice(-2, -1);	// ['green']
let colors4 = colors1.slice(2, 1);	// []
```

`splice()`方法返回从数组中被删除的元素。主要目的是在数组中间插入元素，但有 3 种不同的方式使用这个方法。

- 删除。需要给*splice()*传 2 个参数：要删除的第一个元素的位置和要删除的元素数量。可以从数组中删除任意多个元素，比如`splice(0, 2)`会删除前两个元素。
- 插入。至少需要给*splice()*传 3 个参数：开始位置、0（要删除的元素数量）和要插入的元素，可以在数组中指定的位置插入元素。第三个参数之后还可以传第四、第五个参数，乃至任意多个要插入的元素。比如`splice(2, 0, 'red', 'green')`会从数组位置 2 开始插入字符串*'red'*和*'green'*。
- 替换。*splice()*在删除元素的同时在指定位置插入新元素。比如`splice(2, 1, 'red', 'green')`会删除位置 2 的元素，然后从该位置开始向数组中插入*'red'*和*'green'*。

### 6.2.12 搜索和位置方法

#### 6.2.12.1 严格相等

`indexOf()`、`lastIndexOf()`和`includes()`方法都接收两个参数：要查找的元素和一个可选的起始搜索位置。*indexOf()*和*includes()*方法从数组第一项开始向后搜索，*lastIndexOf()*方法从数组最后一项开始向前搜索。*indexOf()*和*lastIndexOf()*都返回要查找的元素在数组中的位置，如果没找到则返回 -1。*includes()*返回布尔值，表示是否至少找到一个与指定元素匹配的项。这些方法在比较时都会使用全等操作符`===`比较。

```js
let person = { name: 'eagle' };
let people = [ { name: 'eagle' } ];
let morePeople = [ person ];
console.log(people.indexOf(person));	// -1
console.log(morePeople.indexOf(person));	// 0
console.log(people.includes(person));	// false
console.log(morePeople.includes(person));	// true
```

#### 6.2.12.2 断言函数

*ECMAScript*也允许按照定义的断言函数搜索数组，数组中的每个元素都会调用这个函数，直到找到匹配项后，数组剩下的元素不会再调用断言函数。断言函数的返回值决定了相应元素是否被认为匹配。断言函数接收 3 个参数：元素、索引和数组本身。其中元素是数组中当前调用断言函数的元素，索引是该元素对应的索引，数组就是正在搜索的数组。断言函数应该返回真值表示是否匹配。

`find()`和`findIndex()`方法都接受断言函数作为参数。*find()*返回第一个匹配的元素，*findIndex()*返回第一个匹配元素的索引。这两个方法都接收第二个可选的参数，用于指定断言函数内部*this*的值。

```js
const people = [
  { name: 'eagle', age: 21 },
  { name: 'male_eagle', age: 27 }
];
console.log(people.find((element, index, array) => element.age < 24));
// { name: 'eagle', age: 21 }
console.log(people.findIndex((element, index, array) => element.age > 24));	// 1
```

### 6.2.13 迭代方法

 *ECMAScript*为数组定义了 5 个迭代方法。每个方法接收两个参数：以每一项为参数运行的函数，可选的用于指定函数*this*值的作用域对象。第一个函数参数接收 3 个参数：数组元素，元素索引和数组本身。这些方法都不会改变调用它们的数组。

- `every()`：对数组每一项都运行传入的函数，如果每一项的函数都返回*true*，则该方法返回*true*，否则返回*false*。
- `some()`：对数组每一项都运行传入的函数，如果有一项函数返回*true*，则该方法返回*true*，否则返回*false*。
- `map()`：对数组每一项都运行传入的函数，返回由每次函数调用的结果构成的数组。
- `filter()`：对数组每一项都运行传入的函数，函数返回*true*的项会组成数组，最后返回该数组。
- `forEach()`：对数组每一项都运行传入的函数，没有返回值，相当于*for*循环遍历了数组。

### 6.2.14 归并方法

*ECMAScript*为数组提供了两个归并方法：`reduce()`和`reduceRight()`。这两个方法都会迭代数组的所有项，并在此基础上构建一个最终返回值。*reduce()*方法从数组第一项开始遍历到最后，*reduceRight()*从最后一项开始遍历至第一项。

这两个方法都接收两个参数：对每一项都会运行的归并函数，以及可选的作为归并起点的初始值。归并函数接收 4 个参数：上一个归并值、当前项、当前项的索引和数组本身。归并函数的返回值会作为下一个归并函数的第一个参数值。如果没有给这两个方法传入可选的第二个参数（归并起始值），则第一次迭代将从数组的第二项开始，因此一开始传给归并函数的第一个参数是数组的第一项，第二个参数是数组的第二项。

```js
let values = [1, 2, 3, 4, 5];
let sum = values.reduce((prev, cur, index, array) => prev + cur);	// 15
```

上面这个例子累加了数组中所有元素的值。第一次执行归并函数时，*prev*是 1，*cur*是 2。第二次执行时，*prev*是 3，即 1 + 2，*cur*是 3，即数组第三项。如此循环，直到把所有项都遍历一次，最后返回归并结果。

## 6.3 定型数组

定型数组是一种特殊的包含数值类型的数组，目的是提升向原生库传输数据的效率。

### 6.3.1 历史

#### 6.3.1.1 WebGL

*WebGL*是*OpenGL*专注于*2D*和*3D*计算机图形的子集。在*WebGL*的早期版本中，因为*JavaScript*数组与原生数组之间不匹配，所以出现了性能问题。

#### 6.3.1.2 定型数组

*Mozilla*为了解决上述问题而实现了*CanvasFloatArray*。这是一个提供*JavaScript*接口的、*C*语言风格的浮点值数组。*JavaScript*运行时使用这个类型可以分配、读取和写入数组。这个数组可以直接传给底层图形驱动程序*API*，也可以直接从底层获取到。最终，*CanvasFloatArray*变成了*Float32Array*，也是定型数组中可用的第一个 “ 类型 ”。

### 6.3.2 ArrayBuffer

上面说到的*Float32Array*实际上是一种 “ 视图 ”，可以允许*JavaScript*运行时访问一块名为`ArrayBuffer`的预分配内存。*ArrayBuffer*是所有定型数组及视图引用的基本单位。

`ArrayBuffer()`是一个普通的*JavaScript*构造函数，可用于在内存中分配特定数量的字节空间，一经创建就不能再调整大小，但可以使用`slice()`复制其全部或部分到一个新实例中。

```js
const buf1 = new ArrayBuffer(16);
console.log(buf1.byteLength);	// 16
const buf2 = buf1.slice(4, 12);
console.log(buf2.byteLength);	// 8
```

*ArrayBuffer*某种程度上类似于*C++*的*malloc()*，但有以下区别：

- *malloc()*再分配失败时会返回一个*null*指针。*ArrayBuffer*在分配失败时会抛出错误。
- *malloc()*可以利用虚拟内存，因此最大可分配尺寸只受可寻址系统内存限制。*ArrayBuffer*分配的内存不能超过*Number.MAX_SAFE_INTEGER (2**53-1)*字节。
- *malloc()*调用成功不会初始化实际的地址。声明*ArrayBuffer*则会将所有二进制位初始化为 0。
- 通过*malloc()*分配的堆内存除非调用*free()*或程序退出，否则系统不能再使用。而通过声明*ArrayBuffer*分配的堆内存可以被当成垃圾回收，不用手动释放。

要想读取或写入*ArrayBuffer*的内容，就必须通过视图。视图有多种类型，但引用的都是*ArrayBuffer*中存储的二进制数据。

### 6.3.3 DataView

*DataView*视图专为文件*I/O*和网络*I/O*设计，其*API*支持对缓冲数据的高度控制，但相比其他类型的视图性能也差一些。*DataView*对缓冲内容没有任何预设，也不能迭代。

必须在对已有的*ArrayBuffer*读取或写入时才能创建*DataView*实例。这个实例可以使用全部或部分*ArrayBuffer*，且维护着对该缓冲实例的引用，以及视图在缓冲中开始的位置。

```js
const buf = new ArrayBuffer(16);

// DataView 默认使用整个 ArrayBuffer
const fullDataView = new DataView(buf);
console.log(fullDataView.byteOffset);	// 0
console.log(fullDataView.byteLength);	// 16
console.log(fullDataView.buffer === buf);	// true

// 构造函数接收一个可选的字节偏移量和字节长度
// byteOffset=0 表示视图从缓冲起点开始
// byteLength=8 限制视图为前 8 个字节
const firstHalfDataView = new DataView(buf, 0, 8);
console.log(firstHalfDataView.byteOffset);	// 0
console.log(firstHalfDataView.byteLength);	// 8
console.log(firstHalfDataView.buffer === buf);	// true

// 如果不指定字节长度，那么 DataView 会使用剩余的缓冲
// byteOffset=8 表示视图从缓冲的第 9 个字节开始
// byteLength 未指定，默认为剩余缓冲
const secondHalfDataView = new DataView(buf, 8);
console.log(secondHalfDataView.byteOffset);	// 8
console.log(secondHalfDataView.byteLength);	// 8
console.log(secondHalfDataView.buffer === buf);	// true
```

要通过*DataView*读取缓冲，还需要几个条件。

- 要读或写的字节偏移量。可以看成*DataView*中的某种 “ 地址 ”。
- *DataView*应该使用*ElementType*来实现*JavaScript*的*Number*类型到缓冲内二进制格式的转换。
- 内存中值的字节序。默认为大端字节序。

#### 6.3.3.1 ElementType

*DataView*对存储在缓冲内的数据类型没有预设。它暴露的*API*强制开发者在读、写时指定一个*ElementType*，然后*DataView*会完成相应的转换。*ECMAScript6*支持 8 种不同的*ElementType*。

| ElementType | 字节 | 说明               | 等价的 C 类型  | 值的范围               |
| ----------- | ---- | ------------------ | -------------- | ---------------------- |
| Int8        | 1    | 8位有符号整数      | signed char    | -128~127               |
| Uint8       | 1    | 8位无符号整数      | unsigned char  | 0~255                  |
| Int16       | 2    | 16位有符号整数     | short          | -32768~32767           |
| Uint16      | 2    | 16位无符号整数     | unsigned short | 0~65535                |
| Int32       | 4    | 32位有符号整数     | int            | -2147483648~2147483647 |
| Uint32      | 4    | 32位无符号整数     | unsigned int   | 0~4294967295           |
| Float32     | 4    | 32位IEEE-754浮点数 | float          | -3.4e+38~3.4e+38       |
| Float64     | 8    | 64位IEEE-754浮点数 | double         | -1.7e+308~1.7e+308     |

*DataView*为上表中的每种类型都暴露了*get*和*set*方法，这些方法使用*byteOffset*字节偏移量定位要读取或写入值的位置。类型之间可以互换使用，如下例所示：

```js
// 在内存中分配两个字节并声明一个 DataView
const buf = new ArrayBuffer(2);
const view = new DataView(buf);

// 说明整个缓冲确实所有二进制位都是 0
// 检查第一个和第二个字符
console.log(view.getInt8(0));	// 0
console.log(view.getInt8(1));	// 0
// 检查整个缓冲
console.log(view.getInt16(0));	// 0

// 将整个缓冲都设置为 1
// 255 的二进制表示是 1111 1111
view.setUint8(0, 255);
// 255 的十六进制表示是 0xFF，DataView 会自动将数据转换为特定的 ElementType
view.setUint8(1, 0xFF);

// 现在，缓冲里都是 1，如果把它当成二补数的有符号整数，则是 -1
console.log(view.getInt16(0));	// -1
```

#### 6.3.3.2 字节序

字节序是计算机系统维护的一种字节顺序的约定。*DataView*只支持两种约定：大端字节序和小端字节序。大端字节序最高有效位保存在第一个字节，最低有效位保存在最后一个字节。小端字节序正好相反。

*JavaScript*运行时所在系统的原生字节序决定了如何读取或写入字节，但*DataView*是一个中立接口，它只会遵循你指定的字节序。*DataView*所有*API*方法都以大端字节序作为默认值，但接收一个可选的布尔参数，设置为*true*即可启用小端字节序。

```js
// 在内存中分配两个字节并声明一个 DataView
const buf = new ArrayBuffer(2);
const view = new DataView(buf);

// 填充缓冲，让第一位和最后一位都是 1
view.setUint8(0, 0x80);	// 最左边的位等于 1
view.setUint8(1, 0x01);	// 最右边的位等于 1

// 缓冲内容
// 0x8  0x0  0x0  0x1
// 1000 0000 0000 0001

// 按大端字节序读取 0x80 是高字节，0x01 是低字节 0x8001 = 2 ^ 15 + 2 ^ 0 = 32769
console.log(view.getUint16(0));	// 32769

// 按小端字节序读取 0x01 是高字节，0x80 是低字节 0x0180 = 2 ^ 8 + 2 ^ 7 = 384
console.log(view.getUint16(0, true));	// 384

// 按大端字节序写入 4
view.setUint16(0, 0x0004);
// 缓冲内容
// 0x0  0x0  0x0  0x4
// 0000 0000 0000 0100
console.log(view.getUint8(0));	// 0
console.log(view.getUint8(1));	// 4

// 按小端字节序写入 2
view.setUint16(0, 0x0002, true);
// 缓冲内容
// 0x0  0x2  0x0  0x0
// 0000 0010 0000 0000
console.log(view.getUint8(0));	// 2
console.log(view.getUint8(1));	// 0
```

#### 6.3.3.3 边界情形

*DataView*完成读、写操作的前提是必须有充足的缓冲区，否则就会抛出*RangeError*。

*DataView*在写入缓冲里会尽量把一个值转换为适当的类型，后备为 0。如果无法转换则抛出错误。

```js
const buf = new ArrayBuffer(1);
const view = new DataView(buf);

view.setInt8(0, 1.5);
console.log(view.getInt8(0));	// 1

view.setInt8(0, [4]);
console.log(view.getInt8(0));	// 4

view.setInt8(0, 'f');
console.log(view.getInt8(0));	// 0

view.setInt8(0, Symbol());	// TypeError
```

### 6.3.4 定型数组

定型数组是另一种形式的*ArrayBuffer*视图。概念上与*DataView*接近，但区别在于定型数组特定于一种*ElementType*且遵循系统原生的字节序。相应地，定型数组提供了适用面更广的*API*和更高的性能，设计定型数组的目的就是提高与*WebGL*等原生库交换二进制数据的效率。

创建定型数组的方式包括读取已有的缓冲、使用自有缓冲、填充可迭代结构，以及填充基于任意类型的定型数组。另外，通过`<ElementType>.from()`和`<ElementType>.of()`也可以创建定型数组。如果定型数组没有用任何值初始化，其关联的缓冲会以 0 填充。

```js
const buf = new ArrayBuffer(12);
// 创建一个引用该缓冲的 Int32Array
const ints1 = new Int32Array(buf);
// 这个定型数组知道自己的每个元素需要 4 字节，因此长度为 12 / 4 = 3
console.log(ints1.length);	// 3

// 创建一个长度为 6 的 Int32Array
const ints2 = new Int32Array(6);
// 没有用值初始化，则关联的缓冲以 0 填充
console.log(ints2[0]);	// 0
console.log(ints2.length);	// 6
// 类似 DataView，定型数组也有一个指向关联缓冲的引用
console.log(ints2.buffer.byteLength);	// 24

// 创建一个包含 [2, 4, 6, 8] 的 Int32Array
const ints3 = new Int32Array([2, 4, 6, 8]);
console.log(ints3.length);	// 4
console.log(ints3[2]);	// 6

// 通过复制 ints3 的值创建一个 Int16Array
const ints4 = new Int16Array(ints3);
// 这个新类型数组会分配自己的缓冲，对应索引的每个值会相应的转换为新格式
console.log(ints4.length);	// 4
console.log(ints4.buffer.byteLength);	// 8
console.log(ints4[2]);	// 6

// 使用 from() 方法创建 Int16Array
const ints5 = Int16Array.from([3, 5, 7, 9]);
console.log(ints5.length);	// 5

// 使用 of() 方法创建 Float32Array
const floats = Float32Array.of(3.14, 1.23, 3.21);
console.log(floats.length);	// 3
```

定型数组的构造函数和实例都有一个`BYTES_PER_ELEMENT`属性，返回该类型数组中每个元素的字节大小。

```js
console.log(Int16Array.BYTES_PER_ELEMENT);	// 2
console.log(Float64Array.BYTES_PER_ELEMENT);	// 8
```

#### 6.3.4.1 定型数组行为

定型数组与普通数组很相似，支持很多操作符、方法和属性：

- *[]*
- *copyWithin()*
- *entries()*
- *every()*
- *fill()*
- *filter()*
- *find()*
- *findIndex()*
- *forEach()*
- *indexOf()*
- *join()*
- *keys()*
- *lastIndexOf()*
- *length*
- *map()*
- *reduce()*
- *reduceRight()*
- *reverse()*
- *slice()*
- *some()*
- *sort()*
- *toLocaleString()*
- *toString()*
- *values()*

会返回新数组的方法也会返回包含同样元素类型的新定型数组。

定型数组也有*Symbol.iterator*符号属性，因此可以通过*for...of*循环和扩展操作符来操作。

#### 6.3.4.2 合并、复制和修改定型数组

定型数组同样使用数组缓冲*ArrayBuffer*来存储数据，而数组缓冲一经创建无法调整大小，所以*concat()*、*push()*、*pop()*、*shift()*、*unshift()*和*splice()*方法不适用于定型数组。但定型数组也提供了`set()`和`subarray()`方法用于快速向外或向内复制数据。

*set()*从提供的数组或定型数组中把值复制到当前定型数组中指定的索引位置。*subarray()*会基于从原始定型数组中复制的值返回一个新定型数组，复制时的开始索引和结束索引是可选的。

```js
const container = new Int16Array(8);
// set 偏移量默认为 0
container.set(Int8Array.of(1, 2, 3, 4));
console.log(container);	// [1, 2, 3, 4, 0, 0, 0, 0]
container.set([5, 6, 7, 8], 4);
console.log(container);	// [1, 2, 3, 4, 5, 6, 7, 8]
// 溢出会抛出错误
container.set([5, 6, 7, 8], 7);	// RangeError

const source = Int16Array.of(2, 4, 6, 8);
const fullCopy = source.subarray();	// [2, 4, 6, 8]
const halfCopy = source.subarray(2);	// [6, 8]
const partialCopy = source.subarray(1, 3);	// [4, 6]
```

定型数组没有原生的拼接能力，但可以手动构建：[手动构建定型数组的拼接*API*](第6章-集合引用类型\6.3.4.2-合并、复制和修改定型数组.js)。

#### 6.3.4.3 下溢和上溢

定型数组中值的下溢和上溢虽然不会影响到其他索引，但会对自身索引的内容有影响。定型数组对于可以存储的每个索引只接收一个相关位，而不考虑它们对实际数值的影响。

```js
const ints = new Int8Array(2);	// 范围为 -128~127
const unsignedInts = new Uint8Array(2);	// 范围为 0~255

// 上溢的位不会影响相邻索引
unsignedInts[1] = 256;	// [0, 0]
unsignedInts[1] = 511;	// [0, 255]
// 下溢的位会被转换为其无符号的等价位
// -1 的十六进制为 0xFF
unsignedInts[1] = -1;	// [0, 255]

// 上溢自动变成二补数形式
// 128 无符号整数十六进制为 0x80，是二补数形式的 -128
ints[1] = 128;	// [0, -128]
// 下溢自动变成二补数形式
// 255 无符号整数十六进制为 0xFF，是二补数形式的 -1
ints[1] = 255;	// [0, -1]
```

除了上述 8 种元素类型，还有一种夹板数组类型`Uint8ClampedArray`，不允许任何方向溢出。超出 255 的值会向下舍入为 255，小于 0 的值会向上舍入为 0。*JavaScript*之父说：*Uint8ClampedArray*完全是*HTML5 canvas*元素的历史留存。**除非真的做跟*canvas*相关的开发，否则不要使用它。**

```js
const clampedInts = new Uint8ClampedArray([-1, 0, 255, 256]);	// [0, 0, 255, 255]
```

## 6.4 Map

`Map`是*ECMAScript6*新增的一种集合类型，带来了真正的**键/值**存储机制。*Map*的大多数特性都可以通过*Object*类型实现，但二者之间还是存在一些细微的差异。

### 6.4.1 基本 API

使用*new*关键字和*Map*构造函数可以创建一个空映射。如果想在创建的同时初始化实例，可以给*Map*构造函数传入一个可迭代对象，需要包含键/值对数组。可迭代对象中的每个键/值对都会按照迭代顺序插入到映射实例中。

```js
const m1 = new Map();	// 空映射

// 使用嵌套数组初始化映射
const m2 = new Map([
  ["key1", "value1"],
  ["key2", "value2"]
]);
console.log(m2.size);	// 2

// 使用自定义迭代器初始化映射
const m3 = new Map({
  [Symbol.iterator]: function * () {
    yield ["key1", "value1"];
    yield ["key2", "value2"]
  }
});
console.log(m3.size);	// 2
```

初始化之后，还可以使用`set()`方法添加键/值对，*set()*方法返回映射实例，所以可以把多个操作连缀起来；可以使用`get()`和`has()`进行查询，可以通过`size`属性获取映射中键/值对的数量，还可以使用`delete()`和`clear()`删除值。

```js
const m = new Map().set("middleName", "eagle");

console.log(m.has("firstName"));	// false
console.log(m.get("lastName"));	// undefined
console.log(m.size);	// 1

m.set("firstName", "eagle").set("lastName", "eagle");
console.log(m.has("firstName"));	// true
console.log(m.get("firstName"));	// eagle
console.log(m.size);	// 3

m.delete("firstName");
console.log(m.has("firstName"));	// false
console.log(m.size);	// 2

m.clear();
console.log(m.has("lastName"));	// false
console.log(m.has("middleName"));	// false
console.log(m.size);	// 0
```

与*Object*只能使用数值、字符串或符号作为键不同，*Map*可以使用任何*JavaScript*数据类型作为键。*Map*内部使用*SameValueZero*比较操作，基本上相当于使用严格对象相等的标准来检查键的匹配性。映射的值与*Object*一样没有限制。

```js
const m = new Map();

const functionKey = function () {},
      objKey = {},
      objVal = {},
      arrKey = [],
      arrVal = [];

m.set(functionKey, "functionValue").set(objKey, objVal).set(arrKey, arrVal);

objKey.foo = "foo";
objVal.bar = "bar";
arrKey.push("foo");
arrVal.push("bar");

console.log(m.get(functionKey));	// functionValue
console.log(m.get(function () {}));	// undefined
console.log(m.get(objKey));	// { bar: "bar" }
console.log(m.get(arrKey));	// [ "bar" ]
```

*SameValueZero*比较也可能导致意想不到的冲突：

```js
const m = new Map();

const a = 0 / "", // NaN
      b = 0 / "",	// NaN
      pz = +0,
      nz = -0;

console.log(a === b);	// false
console.log(pz === nz);	// true

m.set(a, "foo").set(pz, "bar");
console.log(m.get(b));	// "foo"
console.log(m.get(nz));	// "bar"
```

### 6.4.2 顺序与迭代

与*Object*类型的一个主要差异是，*Map*实例会维护键值对的插入顺序，因此可以根据插入顺序执行迭代操作。映射实例提供一个迭代器，能以插入顺序生成*[key, value]*形式的数组。可以通过`entries()`方法或`Symbol.iterator`符号属性取得这个迭代器。

```js
const m = new Map([
  ["key1", "value1"],
  ["key2", "value2"]
]);

console.log(m.entries === m[Symbol.iterator]);	// true

for (let pair of m.entries()) {
  console.log(pair);	// ["key1", "value1"] ["key2", "value2"]
}
```

因为映射实例提供了迭代器，所以可以直接对映射实例使用扩展操作，把映射转换为数组：

```js
const m = new Map([
  ["key1", "value1"],
  ["key2", "value2"]
]);
console.log([...m]);	// [["key1", "value1"], ["key2", "value2"]]
```

如果不使用迭代器，则可以调用映射的`forEach(callback, opt_thisArg)`方法并传入回调，依次迭代每个键/值对。传入的回调接收可选的第二个参数，用于重写回调内部*this*的值。

`keys()`和`values()`分别返回以插入顺序生成的键和值的迭代器。

```js
const m = new Map([
  ["name", "eagle"],
  ["age", 21]
]);

m.forEach((val, key) => console.log(`${key} -> ${val}`));
// name -> eagle
// age -> 21

for (let key of m.keys()) console.log(key);	// name age

for (let value of m.values()) console.log(value);	// eagle 21
```

### 6.4.3 选择 Object 还是 Map

对象和映射在内存和性能方面存在显著的差别。

1. 内存占用

   两种类型存储单个键/值对所占用的内存数量会随键的数量线性增加。不同浏览器情况不同，**如果给定固定大小的内存，*Map*大约可以比*Object*多存储 50% 的键/值对。**

2. 插入性能

   向两种类型插入新键/值对的消耗大致相当，但*Map*在所有浏览器中会稍微快一点。对两种类型来说，插入速度并不会随着键/值对数量而线性增加。**如果代码涉及大量插入操作，*Map*性能更佳。**

3. 查找速度

   大型*Object*和*Map*中性能差异极小，但如果只包含少量键/值对，则*Object*速度更快。对两种类型来说，查找速度并不会随着键/值对数量而线性增加。**如果代码涉及大量查找操作，选择*Object*更好一些。**

4. 删除性能

   *Map*的*delete()*操作比插入和查找还要快，而*Object*的*delete*操作性能一直以来都饱受诟病。**所以如果代码涉及大量删除操作，那么毫无疑问选择*Map*。**

## 6.5 WeakMap

*ECMAScript6*新增的弱映射`WeakMap`是一种新的集合类型，是*Map*的兄弟类型，其*API*也是*Map*的子集。*WeakMap*的弱是描述*JavaScript*垃圾回收程序对待弱映射中键的方式。

### 6.5.1 基本 API

弱映射中的键只能是*Object*或者继承自*Object*的类型，尝试使用非对象设置键会抛出*TypeError*。值的类型没有限制。

构造函数可以接收一个可迭代对象用于初始化，其中需要包含键/值对数组。可迭代对象中的每个键/值对都会按照迭代顺序插入新实例中。

```js
const key1 = { id: 1 },
      key2 = { id: 2 },
      key3 = { id: 3 };
// 使用嵌套数组初始化弱映射
const wm1 = new WeakMap([
  [key1, "val1"],
  [key2, "val2"],
  [key3, "val3"]
]);
console.log(wm1.get(key1));	// val1

// 初始化是整体操作，如果有一个键无效就会抛出错误，导致整个初始化失败
const wm2 = new WeakMap([
  [key1, "val1"],
  ["BADKEY", "val2"],
  [key3, "val3"]
]);	// TypeError: Invalid value used as weak map key

// 原始值可以先包装成对象再用作键
const stringKey = new String("key1");
const wm3 = new WeakMap([
  [stringKey, "val1"]
]);
console.log(wm3.get(stringKey));	// val1
```

初始化之后可以使用`set()`添加键/值对，该方法返回弱映射实例。可以使用`get()`和`has()`查询。可以使用`delete()`删除。

### 6.5.2 弱键

*WeakMap*中的键不属于正式的引用，不会阻止垃圾回收。只要键存在，键/值对就会存在于映射中，并被当作对值的引用，因此就不会被当作垃圾回收。

```js
const wm = new WeakMap();
wm.set({}, "val");
```

上面的例子中，*set()*方法初始化了一个新对象并将它用作一个字符串的键。因为没有指向这个对象的其他引用，所以当这行代码执行完成后，这个对象键就会被当成垃圾回收。然后这个键/值对就从弱映射中消失了，使其成为一个空映射。且因为值也没有被引用，所以这对键/值对被破坏以后，值本身也会成为垃圾回收的目标。

```js
const wm = new WeakMap();
const container = {
  key: {}
};
wm.set(container.key, "val");
function removeReference () {
  container.key = null;
}
// removeReference();
```

上面这个例子中，*container*对象维护着一个对弱映射键的引用，所以这个对象键不会成为垃圾回收的目标。但是如果调用了*removeReference()*，就会摧毁键对象的最后一个引用，垃圾回收程序就可以把这个键/值对清理掉。

### 6.5.3 不可迭代键

*WeakMap*没有迭代其键/值对的能力。

### 6.5.4 使用弱映射

#### 6.5.4.1 私有变量

弱映射造就了在*JavaScript*中实现真正私有变量的一种新方式。前提是：私有变量会存储在弱映射中，以对象实例为键，以私有成员的字典为值。

[用闭包将弱映射与外界完全隔离开](第6章-集合引用类型\6.5.4.1-私有变量.js)：这是整个代码陷入了*ES6*之前的闭包私有变量模式。

#### 6.5.4.2 DOM 节点元数据

```js
const m = new Map();
const loginButton = document.querySelector('#login');
m.set(loginButton, { disabled: true });
```

上面的例子中使用了*Map*来保存关联元数据，一旦登录按钮从*DOM*树中移除，*Map*映射中还保存着按钮的引用，所以对应的*DOM*节点仍然会逗留在内存中。如果使用弱映射，那么当节点从*DOM*树中移除后，垃圾回收程序就可以立即释放其内存，因为*WeakMap*实例不会妨碍垃圾回收，所以非常适合保存关联元数据。

## 6.6 Set

`Set`是*ECMAScript6*新增的一种集合类型，是一种集合数据结构。在很多方面都像是加强的*Map*。但是，*Set*集合中的值是唯一的，如果插入的值已经在实例中，那么这个值将不会添加到集合实例中。

### 6.6.1 基本 API

使用*new*关键字和*Set*构造函数可以创建一个空集合。如果想在创建的同时初始化实例，则可以给*Set*构造函数传入一个可迭代对象，其中需要包含插入到新集合实例中的元素。

```js
const s = new Set();

const s1 = new Set(["val1", "val2", "val2", "val3"]);
console.log(s1);	// Set(3) { 'val1', 'val2', 'val3' }
```

初始化之后，可以使用`add()`增加值，*add()*返回集合的实例，且如果新增的值已经存在于实例中，那么将不会插入实例中；使用`has()`查询集合中是否包含该值，返回布尔值；通过`size`取得元素数量；使用`delete()`和`clear()`删除元素。

与*Map*类似，*Set*可以包含任何*JavaScript*数据类型作为值。集合内部也使用*SameValueZero*来判断是否有相等的值，基本上相当于使用严格对象相等的标准来检查值的匹配性。

### 6.6.2 顺序与迭代

*Set*会维护值插入时的顺序，且有方法提供迭代器，所以能够按顺序迭代。集合实例可以通过`values()`方法及其别名方法`keys()`（或者`Symbol.iterator`数学，它引用*values()*）取得这个迭代器。

```js
const s = new Set([1, 2, 3]);
console.log(s.values === s[Symbol.iterator]);	// true
console.log(s.keys === s[Symbol.iterator]);	// true
for (let value of s.values()) {
  console.log(value);
}
// 1
// 2
// 3
```

因为*values()*时默认迭代器，所以可以直接对集合实例使用扩展操作，把集合转换为数组。

```js
const s = new Set([1, 2, 3]);
console.log([...s]);	// [1, 2, 3]
```

集合的`entries()`方法返回一个迭代器，可以按照插入顺序产生包含两个元素的数组，这两个元素是集合中每个值的重复出现。

```js
const s = new Set(['val1', 'val2', 'val3']);
for (let pair of s.entries()) {
  console.log(pair);
}
// ['val1', 'val1']
// ['val2', 'val2']
// ['val3', 'val3']
```

集合的`forEach()`方法依次迭代每个键/值对，键和值相等。方法接收一个回调函数和一个可选的用于重写回调内部*this*的值。

```js
const s = new Set(['val1', 'val2', 'val3']);
s.forEach((val, dupVal) => console.log(`${val} -> ${dupVal}`));
// val1 -> val1
// val2 -> val2
// val3 -> val3
```

**在迭代中修改原始值或着引用值的属性不会改变其作为集合值的身份。**

```js
const s1 = new Set(['val']);
for (let value of s1.keys()) {
  value = 'newVal';
  console.log(s1.has('val'));	// true
}

const valObj = { id: 1 };
const s2 = new Set([valObj]);
for (let value of s2.values()) {
  value.id = 2;
  console.log(value);	// { id: 2 }
  console.log(s2.has(valObj));	// true
}
console.log(valObj);	// { id: 2 }
```

## 6.7 WeakSet

`WeakSet`是*Set*的兄弟类型，其*API*也是*Set*的子集。*WeakSet*中的*weak*弱，描述的是*JavaScript*垃圾回收程序对待弱集合中值的方式。

### 6.7.1 基本 API

弱集合中的值只能是*Object*或者继承自*Object*的类型，尝试使用非对象设置值会抛出*TypeError*。初始化时，只要有一个值无效就会抛出错误导致整个初始化失败。

`add()`添加值，`has()`查询值，`delete()`删除值。

### 6.7.2 弱值

*WeakSet*弱集合中的值不属于正式的引用，不会阻止垃圾回收。

```js
const ws = new WeakSet();
ws.add({});
```

上述示例中，*add()*方法初始化了一个新对象并把它用作一个值。但因为没有指向这个对象的其他引用，所以当执行添加操作的代码执行完成后，这个对象值就会被当作垃圾回收。然后这个值就从弱集合中消失了，使集合成为了一个空集合。

### 6.7.3 不可迭代值

因为*WeakSet*中的值任何时候都可能被销毁，所以没有提供迭代其值的能力。







# 第 7 章  迭代器与生成器

## 7.1 理解迭代

迭代会在一个有序集合上进行。循环是迭代机制的基础，但循环有时并不理想：

- 迭代之前需要事先知道如何使用数据结构，如数组可以通过下标来获取数据。
- 遍历顺序并不是数据结构固有的，如数组可以通过递增索引来访问数据。

在*ECMAScript*较早版本中，执行迭代必须使用循环或其他辅助结构，随着代码量增加，代码会变得混乱。很多语言通过原生语言结构解决了这个问题，解决方案就是**迭代器模式**。*JavaScript*在*ECMAScript6*以后也支持了迭代器模式。

## 7.2 迭代器模式

迭代器模式描述了一个方案：可以把有些结构称为可迭代对象*iterable*。因为它们实现了正式的*Iterable*接口，而且可以通过迭代器*Iterator*消费。任何实现*Iterable*接口的数据结构都可以被实现*Iterator*接口的结构消费。迭代器*iterator*是**按需创建的一次性对象**。每个迭代器都会关联一个可迭代对象，迭代器会暴露迭代其关联可迭代对象的*API*。迭代器无须了解与其关联的可迭代对象的结构，只需要知道如何取得连续的值。这种概念上的分离正是*Iterable*和*Iterator*的强大之处。

### 7.2.1 可迭代协议

可迭代对象实现了*Iterable*接口，而实现*Iterable*接口（可迭代协议）要求同时具备两种能力：支持迭代的自我识别能力和创建实现*Iterator*接口的对象的能力。要想实现*Iterator*接口，就必须暴露一个属性作为默认迭代器。在*ECMAScript*中这个属性必须使用特殊的`Symbol.iterator`作为键。为默认迭代器属性赋值一个迭代器工厂函数，工厂函数必须返回一个新迭代器。

下面这些内置类型都实现了*Iterable*接口，所以它们都可迭代：

- 字符串
- 数组
- 映射
- 集合
- *arguments*对象
- *NodeList*等*DOM*集合类型

```js
let num = 1;
console.log(num[Symbol.iterator]);	// undefined

// 实现了迭代器工厂函数
let str = 'abc';
console.log(str[Symbol.iterator]);	// [Function: [Symbol.iterator]]
```

实际写代码的过程中，不需要显示调用工厂函数来生成迭代器。以下原生语言特性接收可迭代对象，在后台调用提供的可迭代对象的工厂函数，从而创建一个迭代器。

- *for-of*循环
- 数组解构
- 扩展操作符
- *Array.from()*
- 创建集合
- 创建映射
- *Promise.all()*接收由期约组成的可迭代对象
- *Promise.race()*接收由期约组成的可迭代对象
- *yield\**操作符，在生成器中使用

```js
let arr = ['foo', 'bar', 'baz'];
for (let el of arr) {
  console.log(el);	// foo bar baz
}
```

如果对象原型链上的父类实现了*Iterable*接口，那这个对象也就实现了这个接口。

```js
class SonArray extends Array {}
let sonArr = new SonArray('foo', 'bar', 'baz');
for (let el of sonArr) {
  console.log(el);	// foo bar baz
}
```

### 7.2.2 迭代器协议

迭代器用于迭代与其关联的可迭代对象。迭代器*API*使用`next()`方法在可迭代对象中遍历数据。每次成功调用*next()*，都会返回一个*IteratorResult*对象，其中包含迭代器返回的值。若不调用*next()*，则无法知道迭代器的当前位置。

*next()*方法返回的迭代器对象*IteratorResult*包含两个属性：`done`和`value`。*done*是一个布尔值，表示当前返回值是否是可迭代对象的值；*value*包含本次遍历到的可迭代对象值。当*done*为*false*时，*value*包含的值为本次遍历到的可迭代对象值；当*done*为*true*时，此时迭代状态为耗尽，*value*值为*undefined*。

```js
// 可迭代对象
let arr = ['foo', undefined];

// 迭代器工厂函数
console.log(arr[Symbol.iterator]);	// [Function: [Symbol.iterator]]

// 迭代器
let iter = arr[Symbol.iterator]();
console.log(iter);	// Object [Array Iterator] {}

// 执行迭代
console.log(iter.next());	// { done: false, value: 'foo' }
console.log(iter.next());	// { done: false, value: undefined }
console.log(iter.next());	// { done: true, value: undefined }
console.log(iter.next());	// { done: true, value: undefined }
```

迭代器并不知道怎么从可迭代对象中取得下一个值，也不知道可迭代对象有多大，但只要迭代器达到*done: true*的耗尽状态，就表示可迭代对象已经遍历完成，再调用*next()*方法只会返回`{ done: true, value: undefined }`。

每个迭代器都表示对可迭代对象的一次性有序遍历，不同迭代器的实例相互之间没有联系，只会独立地遍历可迭代对象。

```js
let arr = ['foo', 'bar']
let iter1 = arr[Symbol.iterator]();
let iter2 = arr[Symbol.iterator]();

console.log(iter1.next());	// { done: false, value: 'foo' }
console.log(iter2.next());	// { done: false, value: 'foo' }
console.log(iter2.next());	// { done: false, value: 'bar' }
console.log(iter1.next());	// { done: false, value: 'bar' }
```

迭代器并不与可迭代对象某个时刻的快照绑定，如果可迭代对象在迭代期间被修改了，迭代器也会反映相应的变化。

```js
let arr = ['foo'];
let iter = arr[Symbol.iterator]();
console.log(iter.next());	// { done: false, value: 'foo' }
arr.push('bar');
console.log(iter.next());	// { done: false, value: 'bar' }
console.log(iter.next());	// { done: true, value: undefined }
```

**迭代器维护着一个指向可迭代对象的引用，因此迭代器会阻止垃圾回收程序回收可迭代对象。**

### 7.2.3 自定义迭代器

自定义迭代器：[通过闭包返回迭代器，使得每个实例都能够迭代](第7章-迭代器与生成器\7.2.3-自定义迭代器.js)。

迭代器的`[Symbol.iterator]()`会返回它自身。

```js
let arr = [1, 2, 3];
let iter = arr[Symbol.iterator]();
console.log(iter === iter[Symbol.iterator]());	// true
```

因为每个迭代器也实现了*Iterable*接口，所以可以在任何期待可迭代对象的地方使用它们。

```js
let arr = [1, 2, 3];
let iter = arr[Symbol.iterator]();
for (let item of iter) {
  console.log(item);	// 1 2 3
}
```

### 7.2.4 提前终止迭代器

可选的`return()`方法用于指定在迭代器提前关闭时执行的逻辑。提前关闭指以下情况：

- *for-of*循环通过*break*、*continue*、*return*或*throw*提前退出
- 解构操作并未消费解构完所有的值

*return()*方法必须返回一个有效的*IteratorResult*对象。简单情况下可以只返回*{done: true}*。

[测试*return()*方法以及数组的迭代器不可关闭](第7章-迭代器与生成器\7.2.4-提前终止迭代器.js)

如果迭代器不能关闭，则还可以继续从上次离开的地方继续迭代。因为*return()*方法是可选的，所以并非所有迭代器都是可关闭的，如数组的迭代器就是不能关闭的。要知道某个迭代器是否可关闭，可以测试这个迭代器实例的*return*属性是不是函数对象。**如果给一个不可关闭的迭代器增加*return()*方法，并不能让它变成可关闭的，因为调用*return()*不会强制迭代器进入关闭状态。但是*return()*还是会被调用。**

## 7.3 生成器

生成器是*ECMAScript6*新增的解构，拥有在一个函数块内暂停和恢复代码执行的能力。

### 7.3.1 生成器基础

生成器的形式是一个函数，函数名称前面加上一个`*`星号表示它是一个生成器。只要是可以定义函数的地方，就可以定义生成器。**箭头函数不能用来定义生成器函数。**

```js
// 生成器函数声明
function * generatorFn () {}
// 生成器函数表达式
let generatorFn = function * () {}
```

调用生成器函数会产生一个生成器对象。与迭代器相似，生成器对象也实现了*Iterator*接口，因此具有`next()`方法。生成器对象一开始处于暂停执行的状态，调用*next()*方法会让生成器开始或恢复执行。

```js
function * generatorFn () {}
const g = generatorFn();
console.log(g);		// Object [Generator] {}
console.log(g.next);	// [Function: next]
```

*next()*方法的返回值类似于迭代器，有`done`属性和`value`属性。函数体为空的生成器函数中间不会停留，调用一次*next()*方法就会让生成器到达*done: true*状态。

```js
function * generatorFn () {}
const g = generatorFn();
console.log(g.next());	// { done: true, value: undefined }
```

*value*属性是生成器函数的返回值，默认值为*undefined*，可以通过生成器函数的返回值指定。

```js
function * generatorFn () {
  return 'foo';
}
const g = generatorFn();
console.log(g.next());	// { done: true, value: 'foo' }
```

生成器函数只会在初次调用*next()*方法后开始执行。

```js
function * generatorFn () {
  console.log('foo');
}
// 调用生成器函数只会返回生成器对象，不会执行生成器函数
const g = generatorFn();
// 初次调用 next() 方法才会执行生成器函数中的内容
g.next();	// foo
```

生成器对象实现了*Iterable*接口，其默认迭代器是自引用的。

```js
function * generatorFn () {}
const g = generatorFn();
console.log(g === g[Symbol.iterator]());	// true
```

### 7.3.2 通过 yield 中断执行

`yield`关键字可以让生成器停止和开始执行，也是生成器最有用的地方。生成器函数在遇到*yield*关键字之前会正常执行。遇到这个关键字后，执行会停止，函数作用域的状态会被保留。停止执行的生成器函数只能通过在生成器对象上调用*next()*方法来恢复执行。

```js
function * generatorFn () {
  yield;
}
const g = generatorFn();
console.log(g.next());	// { done: false, value: undefined }
console.log(g.next());	// { done: true, value: undefined }
```

*yield*关键字有点像函数的中间返回语句，它生成的值（后面携带的值）会出现在*next()*方法返回的对象里（*value*属性）。通过*yield*关键字退出的生成器函数会处在*done: false*状态；通过*return*关键字退出的生成器函数会处于*done: true*状态。

```js
function * generatorFn () {
  yield 'foo';
  yield 'bar';
  return 'baz';
  yield 'test';
}
const g = generatorFn();
console.log(g.next());	// { done: false, value: 'foo' }
console.log(g.next());	// { done: false, value: 'bar' }
console.log(g.next());	// { done: true, value: 'baz' }
console.log(g.next());	// { done: true, value: undefined }
```

生成器函数内部的执行流程会针对每个生成器对象区分作用域。在一个生成器对象上调用*next()*不会影响其他生成器对象。

```js
function * generatorFn () {
  yield 'foo';
}
const g1 = generatorFn();
const g2 = generatorFn();
console.log(g1.next());	// { done: false, value: 'foo' }
console.log(g2.next());	// { done: false, value: 'foo' }
```

*yield*关键字只能在生成器函数内部使用，用在其他地方会抛出错误，出现在嵌套的非生成器函数中会抛出语法错误。

```js
// 有效
function * validGeneratorFn () {
  yield;
}

// 无效
function * invalidGeneratorFnA () {
  function a () {
    yield;
  }
}

// 无效
function * invalidGeneratorFnB () {
  const b = () => {
    yield;
  }
}

// 无效
function * invalidGeneratorFnC () {
  (() => {
    yield;
  })();
}
```

#### 7.3.2.1 生成器对象作为可迭代对象

在生成器对象上显示调用*next()*方法用处不大，如果把生成器对象当成可迭代对象，使用起来会更方便。

```js
function * generatorFn () {
  yield 1;
  yield 2;
  yield 3;
}
for (const x of generatorFn()) {
  console.log(x);	// 1 2 3
}
```

#### 7.3.2.2 使用 yield 实现输入和输出

*yield*关键字除了可以作为函数的中间返回语句使用，还可以作为函数的中间参数使用。上一次让生成器函数暂停的*yield*关键字会接收到传给*next()*方法的第一个值。第一次调用*next()*传入的值不会被*yield*接收，因为第一次调用是为了开始执行生成器函数。

```js
function * generatorFn (initial) {
  console.log(initial);
  console.log(yield);
  console.log(yield);
}
let g = generatorFn('foo');
g.next('bar');	// foo
g.next('baz');	// baz
g.next('qux');	// qux
```

#### 7.3.2.3 产生可迭代对象

可以使用星号`*`增强*yield*的行为，让它能够迭代一个可迭代对象，从而一次产出一个值。

```js
function * generatorFn () {
  yield * [1, 2];
  yield * [3, 4, 5];
}
for (const x of generatorFn()) {
  console.log(x);
}
// 1
// 2
// 3
// 4
// 5
```

`yield*`实际上只是将一个可迭代对象序列化为一连串可以单独产出的值，这跟把*yield*放到一个循环中没什么不同。下面两个生成器函数的行为是等价的。

```js
function * generatorFnA () {
  for (const x of [1, 2, 3]) {
    yield x;
  }
}

function * generatorFnB () {
  yield * [1, 2, 3];
}
```

`yield*`的值是关联它后面的迭代器返回*done: true*时的*value*属性。对于普通迭代器而言，这个值是*undefined*；对于生成器函数产生的迭代器而言，这个值就是生成器函数返回的值：[yield*的返回值](第7章-迭代器与生成器\7.3.2.3-产生可迭代对象.js)。

#### 7.3.2.4 使用 yield* 实现递归算法

`yield*`最有用的地方是实现递归操作，因为生成器可以产生自身。

```js
function * nTimes (n) {
  if (n > 0) {
    yield* nTimes(n - 1);
    yield n - 1;
  }
}
for (const x of nTimes(3)) {
  console.log(x);
}
// 0
// 1
// 2
```

### 7.3.3 生成器作为默认迭代器

因为生成器对象实现了*Iterable*接口，且生成器函数和默认迭代器被调用之后都产生迭代器，所以生成器很适合作为默认迭代器。

```js
class Foo {
  constructor () {
    this.values = [1, 2];
	}
  * [Symbol.iterator] () {
    yield* this.values;
  }
}
const f = new Foo();
for (const x of f) {
  console.log(x);
}
// 1
// 2
```

### 7.3.4 提前终止生成器

与迭代器类似，生成器也支持可关闭的概念。一个实现*Iterator*接口的对象一定有*next()*方法，还有一个可选的*return()*方法用于提前终止迭代器。生成器对象除了这两个方法外，还有第三个方法*throw()*。`return()`和`throw`方法都可以用于强制生成器进入关闭状态。

```js
function * generatorFn () {}
const g = generatorFn();
console.log(g.next);		// [Function: next]
console.log(g.return);	// [Function: return]
console.log(g.throw);		// [Function: throw]
```

#### 7.3.4.1 return()

`return()`方法会强制生成器进入关闭状态，进入关闭状态后，就无法恢复了，后续调用*next()*会显示*done: true*状态。提供给*return()*方法的值，就是终止迭代器对象的值。

```js
function * generatorFn () {
  for (const x of [1, 2, 3]) {
    yield x;
  }
}
const g = generatorFn();
console.log(g);	// generatorFn {<suspended>}
console.log(g.next());	// { done: false, value: 1 }
console.log(g.return(4));	// { done: true, value: 4 }
console.log(g);	// generatorFn {<closed>}
console.log(g.next());	// { done: true, value: undefined }
```

*for-of*循环等内置语言结构会忽略状态为*done: true*的*IteratorObject*内部返回的值。

#### 7.3.4.2 throw()

`throw()`方法会在暂停的时候将一个提供的错误（通过参数传递方式）注入到生成器对象中。如果错误未被处理，生成器就会关闭。

```js
function * generatorFn () {
  for (const x of [1, 2, 3]) {
    yield x;
  }
}
const g = generatorFn();
console.log(g);	// generatorFn {<suspended>}
try {
  g.throw('foo');
} catch (e) {
  console.log(e);	// foo
}
console.log(g);	// generatorFn {<closed>}
```

如果生成器函数**内部**处理了这个错误，那么生成器就不会关闭，且还可以恢复执行。错误处理会跳过当次对应的*yield*。

```js
function * generatorFn () {
  for (const x of [1, 2, 3]) {
    try {
      yield x;
    } catch (e) {
      console.log(e);
    }
  }
}
const g = generatorFn();
console.log(g.next());	// { done: false, value: 1 }
g.throw('foo');	// foo
console.log(g.next());	// { done: false, value: 3 }
```

**如果生成器对象还没有开始执行，那么调用*throw()*抛出的错误不会在函数内部被捕获，因为这相当于在外部抛出了错误。**







# 第 8 章  对象、类与面向对象编程

## 8.1 理解对象

### 8.1.1 属性的类型

数据属性：

- `[[Configurable]]`
- `[[Enumerable]]`
- `[[Writable]]`
- `[[Value]]`

访问器属性：

- `[[Configurable]]`
- `[[Enumerable]]`
- `[[Get]]`
- `[[Set]]`

API：

`Object.defineProperty(obj, per, {})`：用于修改属性的默认特性。

### 8.1.2 定义多个属性

`Object.defineProperties()`：通过多个描述符一次性定义多个属性。

### 8.1.3 读取属性的特性

`Object.getOwnPropertyDescriptor()`：取得指定属性的属性描述符对象，该方法只对实例属性有效，要想取得原型属性的描述符，就必须传入对应构造函数的原型对象。

`Object.getOwnPropertyDescriptors()`：取得指定对象所有属性的属性描述符。

### 8.1.4 合并对象

`Object.assign()`：将源对象（可有多个）中可枚举和自有属性复制到目标对象中，执行的是浅复制，相同属性则保留最后一个值。

### 8.1.5 对象标识及相等判定

`Object.is()`：与`===`相似，不同的是可以正确的判定 NaN 和 0。

## 8.2 创建对象

### 8.2.3 构造函数模式

**在实例化时，如果没有传递参数，那么构造函数后面的括号可加可不加。**

构造函数不一定要写成函数声明的形式，赋值给变量的函数表达式也可以表示构造函数。

缺点：每个实例都会重新创建一遍新方法。

### 8.2.4 原型模式

`prototype`：每个函数都会创建的一个属性，该属性是一个对象，在该对象上面定义的属性和方法可以被构造函数实例所共享，也就是原型对象。

#### 8.2.4.1 理解原型

`constructor`：默认情况下所有原型对象默认有一个*constructor*属性，指向构造函数。实例对象通过原型链也可以访问到。

`[[Prototype]]`：构造函数实例的内部指针，指向构造函数原型对象。可以通过`Object.getPrototypeOf`方法获取。

`isPrototypeOf()`：确定实例内部的*[[Prototype]]*指针是否指向调用该函数的构造函数原型对象，如`Person.prototype.isPrototypeOf(p1)`。

`Object.getPrototypeOf()`：返回实例参数的内部特性*[[Prototype]]*的值。

`Object.setPrototypeOf()`：向实例的内部特性*[[Prototype]]*写入一个新值，**慎用**。

`Object.create()`：创建一个新对象，并指定该对象的原型为参数对象。接收两个参数，第一个参数作为新对象原型的对象，第二个可选的给新对象定义额外属性的对象，新增属性通过各自的描述符来描述。

#### 8.2.4.2 原型层级

`hasOwnProperty()`：该方法继承自*Object*，用于确定某个属性是在实例上还是在原型对象上，当属性存在于调用它的对象实例上时返回*true*。

#### 8.2.4.3 原型和*in*操作符

`for-in`：*for-in*循环会将可枚举的属性都返回，**且会沿原型链层层往上遍历**。

`Object.keys()`：该方法接收一个对象作为参数，返回包含该对象所有可枚举属性名称的字符串数组。**不会沿原型链遍历**。

`Object.getOwnPropertyNames()`：返回参数对象的所有属性名称，无论是否可枚举。**不会沿原型链遍历**。

`Object.getOwnPropertySymbols()`： 与*Object.getOwnPropertyNames()*类似，只针对符号。

### 8.2.5 对象迭代

`Object.values()`：接受一个对象，返回该对象的值的数组，符号属性会被忽略。

`Object.entries()`：接收一个对象，返回该对象的键值对数组，符号属性会被忽略。

#### 8.2.5.4 原型的问题

原型弱化了向构造函数传递初始化参数的能力，会导致所有实例默认都取得相同的属性值。而最大的问题在于**共享特性**。

## 8.3 继承

### 8.3.1 原型链

讲述原型链，原型链有两个问题（p242），基本不会被单独使用。

#### 8.3.1.2 原型与继承关系

两种方式确定原型与实例的关系。

`instanceof`：如果一个实例的原型链中出现过相应的构造函数，则返回*true*。

> 前提：instance 是 SubType 构造函数的实例，SubType 继承自 SuperType 。
>
> ```js
> instance instanceof SubType	// true
> instance instanceof SuperType // true
> instance instanceof Object	// true，因为默认情况下，所有引用类型都继承自 Object
> ```

`isPrototypeOf()`：原型链中的每个原型都可以调用这个方法，只要参数对象的原型链中包含这个原型，就返回*true*。方法介绍请戳 [isPrototypeOf 方法介绍](#8.2.4.1 理解原型) 。

> 前提：同上个方法情况一样。
>
> ```js
> SubType.prototype.isPrototypeOf(instance)	// true
> SuperType.prototype.isPrototypeOf(instance)	// true
> Object.prototype.isPrototypeOf(instance)	// true
> ```

### 8.3.2 盗用构造函数

函数就是在特定上下文中执行代码的简单对象，所以可以使用*apply()*和*call()*方法以新创建的对象为上下文执行构造函数。

```js
function SuperType () {
  this.colors = ['red', 'blue', 'green'];
}
function SubType () {
  // 继承 SuperType
  SuperType.call(this);
}

let instance1 = new SubType();
instance1.colors.push('black');
console.log(instance1.colors);	// red, blue, green, black

let instance2 = new SubType();
console.log(instance2.colors);	// red, blue, green
```

盗用构造函数也有缺点，具体翻看[p243]()。

### 8.3.3 组合继承

组合继承综合了原型链和盗用构造函数的优点，弥补了不足，是*JavaScript*中使用最多的继承模式，且保留了*instanceof*操作符和*isPrototypeOf()*方法识别合成对象的能力。[组合继承示例](第8章-对象、类与面向对象编程\8.3.3-组合继承.js).

### 8.3.4 原型式继承

> 一种不涉及严格意义上构造函数的继承方法。本质上，*object()*是对传入的对象执行了一次浅复制。
>
> ```js
> function object (o) {
>   function F () {}
>   F.prototype = o;
>   return new F();
> }
> ```

*ES5*新增的*Object.create()*方法将原型式继承的概念规范化了。

### 8.3.6 寄生式组合继承

组合继承存在父类构造函数会被调用两次的效率问题，而寄生式组合继承只调用一次父类构造函数，避免了子类原型上不必要的属性。基本模式如下所示：

```js
function inheritPrototype (subType, superType) {
  let prototype = Object.create(superType.prototype);	// 复制父类原型
  prototype.constructor = subType;	// 修正原型对象 constructor 的指向
  subType.prototype = prototype;	// 为子类原型赋值
}

function SuperType (name) {
  this.name = name;
}
SuperType.prototype.sayName = function () {}

function SubType (name, age) {
  SuperType.call(this, name);
  this.age = age;
}

inheritPrototype(SubType, SuperType);
```

## 8.4 类

*ES6*新引入的`class`关键字具有正式定义类的能力，是一种基础性语法糖结构。表面上看起来可以支持正式的面向对象编程，但实际上它背后使用的仍然是原型和构造函数的概念。

### 8.4.1 类定义

定义类使用`class`关键字加大括号，有两种方式：类声明和类表达式。

```js
// 类声明
class Person {}

// 类表达式
const Animal = class {};
```

与函数声明不同的地方是，函数受函数作用域限制，而类受块作用域限制：

```js
{
  function FunctionDeclaration () {}
  class ClassDeclaration {}
}
console.log(FunctionDeclaration);	// FunctionDeclaration () {}
console.log(ClassDeclaration);	// ReferenceError: ClassDeclaration is not defined
```

**类的构成：类可以包含构造函数方法、实例方法、获取函数、设置函数和静态类方法，但这些都不是必需的。空的类定义一样有效。默认情况下，类定义中的代码都在严格模式下执行。**

### 8.4.2 类构造函数

`constructor`关键字用于创建类的构造函数。使用`new`操作符创建类的新实例时，会调用方法名为*constructor*关键字的类构造函数。如果不定义，则构造函数默认为空函数。

#### 8.4.2.2 把类当成特殊函数

*ES*中并没有正式的类这个类型。从各方面来看，*ES*类就是一种特殊函数。

```js
class Person {}
console.log(Person);	// class Person {}
console.log(typeof Person);	// function
```

### 8.4.3 实例、原型和类成员

#### 8.4.3.1 实例成员

在类构造函数内部，可以为新创建的实例*this*添加自有属性。

#### 8.4.3.2 原型方法与访问器

类定义语法把在类块中定义的方法作为原型方法。

```js
class Person {
  constructor () {
    this.locate = () => console.log('instance');
  }
  locate () {
    console.log('prototype');
  }
}
let p = new Person();
p.locate();	// instance
Person.prototype.locate();	// prototype
```

可以把方法定义在类构造函数或类块中，但不能在类块中给原型添加原始值或对象作为成员数据。

```js
class Person {
  name: 'eagle'	// Uncaught SyntaxError: Unexpected identifier
}
```

类定义支持获取和设置访问器，其语法与行为跟普通对象一样。通过访问器可以实现在原型上定义属性。

```js
class Person {
  set name (newName) {
    this.name_ = newName;
  }
  get name () {
    return this.name_;
  }
}
let p = new Person();
p.name = 'eagle';
console.log(p.name);	// eagle
```

#### 8.4.3.3 静态类方法

静态类成员在类定义中使用`static`关键字作为前缀。静态成员中，*this*引用类自身。[case](第8章-对象、类与面向对象编程\8.4.3.3-静态类方法.js)。

```js
class Person {
  constructor () {
    this.locate = () => console.log('instance', this);
  }
  
  locate () {
    console.log('prototype', this);
  }
  
  static locate () {
    console.log('class', this);
  }
}
let p = new Person();
p.locate();	// instance, Person {}
Person.prototype.locate();	// prototype, { constructor: ... }
Person.locate();	// class, class Person {}
```

#### 8.4.3.4 非函数原型和类成员

**类定义中没有显示支持添加数据成员，在类上定义的成员都会成为共享目标。**

### 8.4.4 继承

#### 8.4.4.1 继承基础

*ES6*类支持单继承，继承使用`extends`关键字。不仅可以继承类，也可以继承普通的构造函数。

#### 8.4.4.2 构造函数、*HomeObject*和*super()*

`super`关键字只能在子类中使用，且仅限于类构造函数和静态方法内部。类构造函数中使用*super()*可以调用父类构造函数；静态方法中可以通过*super*调用父类上定义的静态方法。关于*super*关键字的注意事项看**p260**。

#### 8.4.4.3 抽象基类

`new.target`保存通过*new*关键字调用的类或函数。

#### 8.4.4.4 继承内置类型

有些内置类型的方法会返回新实例。默认情况下，返回实例的类型与继承的子类类型是一致的，如果想覆盖这个默认行为，可以覆盖`Symbol.species`访问器。







# 第 9 章  代理与反射

## 9.1 代理基础

代理是目标对象的抽象，可以用作目标对象的替身，但又完全独立于目标对象。目标对象既可以直接被操作，也可以通过代理来操作。

### 9.1.1 创建空代理

代理是使用`Proxy`构造函数创建的。该构造函数接收两个参数：目标对象和处理程序对象，两个参数缺一不可。构造函数返回的实例就是代理对象。[空代理演示](第9章-代理与反射\9.1.1-创建空代理.js)

### 9.1.2 定义捕获器

使用代理的主要目的是可以定义捕获器。捕获器是在处理程序对象中定义的操作拦截器，捕获器在处理程序对象中以方法名为键。每个捕获器对应一种基本操作，当在代理对象上调用这些基本操作时，代理就会在这些操作传播到目标对象之前先调用捕获器函数，从而拦截并修改相应的行为。[get捕获器](第9章-代理与反射\9.1.2-定义捕获器.js)

### 9.1.3 捕获器参数和反射*API*

所有捕获器都会接收到一些参数，基于这些参数可以重建被捕获方法的原始行为。[get捕获器的参数及重建get原始行为](第9章-代理与反射\9.1.3-捕获器参数和反射API-get捕获器参数.js)

所有捕获器都可以基于自己的参数重建原始操作，但并不是所有捕获器行为都像*get()*那么简单。实际上并不需要手动重建原始行为，而是可以通过调用全局`Reflect`对象上的同名方法来轻松重建。*Reflect*对象上封装了原始行为。

处理程序对象中所有可以捕获的方法都有对应的反射（*Reflect*）*API*方法。这些方法与捕获器拦截的方法具有相同的名称和函数签名，且具有与被拦截方法相同的行为。反射*API*为开发者准备好了样板代码，在此基础上开发者可以用最少的代码修改捕获的方法。比如只在某个属性被访问时修饰该属性，其余属性保持不变：[Reflect反射演示](第9章-代理与反射\9.1.3-捕获器参数和反射API-Reflect反射演示.js)。

### 9.1.5 可撤销代理

*Proxy*有一个`revocable()`方法，该方法返回的对象包含了代理对象和撤销函数`revoke()`，调用撤销函数可以中断代理对象和目标对象之间的联系。撤销代理之后再次调用代理会抛出*TypeError*。[撤销代理演示](第9章-代理与反射\9.1.5-可撤销代理.js)

### 9.1.6 实用反射*API*

很多反射方法会返回称作状态标记的布尔值，这比原先返回对象或抛出错误的方法更有用。

### 9.1.7 代理另一个代理

[在一个目标对象之上构建多层拦截网](第9章-代理与反射\9.1.7-代理另一个代理.js)

## 9.2 代理捕获器与反射方法

以下介绍 13 种不同的基本操作。这些操作都有各自不同的反射*API*方法、参数、关联*ES*操作和不变式。

### 9.2.1 *get()*

`get()`捕获器会在获取属性值的操作中被调用。对应的反射*API*方法为`Reflect.get()`。

### 9.2.2 *set()*

`set()`捕获器会在设置属性值的操作中被调用。对应的反射*API*方法为`Reflect.set()`。

### 9.2.3 *has()*

`has()`捕获器会在*in*操作符中被调用。对应的反射*API*方法为`Reflect.has()`。*has()*必须返回布尔值。

### 9.2.4 *defineProperty()*

`defineProperty()`捕获器会在*Object.defineProperty()*中被调用。对应的反射*API*方法为`Reflect.defineProperty()`。该捕获器必须返回布尔值。

### 9.2.5 *getOwnPropertyDescriptor()*

`getOwnPropertyDescriptor()`捕获器会在*Object.getOwnPropertyDescriptor()*中被调用。对应的反射*API*方法为`Reflect.getOwnPropertyDescriptor()`。该捕获器必须返回对象。

### 9.2.6 *deleteProperty()*

`deleteProperty()`捕获器会在*delete*操作符中被调用。对应的反射*API*方法为`Reflect.deleteProperty()`。该捕获器必须返回布尔值。

### 9.2.7 *ownKeys()*

`ownKeys()`捕获器会在*Object.keys()*及类似方法中被调用。对应的反射*API*方法为`Reflect.ownKeys()`。该捕获器必须返回包含字符串或符号的可枚举对象。

### 9.2.8 *getPrototypeOf()*

`getPrototypeOf()`捕获器会在*Object.getPrototypeOf()*中被调用。对应的反射*API*方法为`Reflect.getPrototypeOf()`。该捕获器必须返回对象或*null*。

### 9.2.9 *setPrototypeOf()*

`setPrototypeOf()`捕获器会在*Object.setPrototypeOf()*中被调用。对应的反射*API*方法为`Reflect.setPrototypeOf()`。该捕获器必须返回布尔值。

### 9.2.10 *isExtensible()*

`isExtensible()`捕获器会在*Object.isExtensible()*中被调用。对应的反射*API*方法为`Reflect.isExtensible()`。该捕获器必须返回布尔值。

### 9.2.11 *preventExtensions()*

`preventExtensions()`捕获器会在*Object.preventExtensions()*中被调用。对应的反射*API*方法为`Reflect.preventExtensions()`。该捕获器必须返回布尔值。

### 9.2.12 *apply()*

`apply()`捕获器会在调用函数时被调用。对应的反射*API*方法为`Reflect.apply()`。

### 9.2.13 *construct()*

`construct()`捕获器会在*new*操作符中被调用。对应的反射*API*方法为`Reflect.construct()`。该捕获器必须返回一个对象。







# 第 10 章  函数

函数实际上是对象，每个函数都是`Function`类型的实例。因为函数是对象，所以函数名就是指向函数对象的指针。

## 10.1 箭头函数

**箭头函数不能使用*arguments*、*super*和*new.target*，也不能用作构造函数。此外，箭头函数也没有*prototype*属性。**

## 10.9 函数内部

### 10.9.1 *arguments*

`arguments.callee`：指向*arguments*对象所在函数的指针。严格模式下访问会报错。

### 10.9.3 *caller*

`caller`：*ES5*为函数对象添加的一个属性，该属性引用的是调用当前函数的函数。

### 10.9.4 *new.target*

`new.target`：*ES6*新增的用于检测函数是否使用*new*关键字调用。函数正常调用则值为*undefined*，如果使用*new*关键字调用则值为被调用的构造函数。

## 10.10 函数属性与方法

每个函数都有两个属性：`length`和`prototype`。*length*属性保存函数定义的命名参数的个数，*prototype*属性保存引用类型的所有实例方法。

## 10.12 递归

**非严格模式下编写递归函数时，*arguments.callee*是引用当前函数的首选。严格模式下可以采用命名函数表达式。** [命名函数表达式](第10章-函数\10.12-递归.js)

## 10.14 闭包

闭包指那些引用了另一个函数作用域中变量的函数，通常是在嵌套函数中实现的。

## 10.16 私有变量

严格来讲，*JS*中没有私有成员的概念，但是有私有变量的概念。任何定义在函数或块中的变量，都可以认为是私有的，包括函数参数、局部变量以及函数内部定义的其他函数。







# 第 11 章  期约

## 11.2 期约

### 11.2.2 期约基础

*ES6*新增了`Promise`引用类型，可以通过*new*操作符来实例化。创建新期约时需要传入执行器函数作为参数，否则会抛出*SyntaxError*。

#### 11.2.2.1 期约状态机

期约是一个有状态的对象，可能处于以下 3 种状态之一：

- `pending`：待定，表示尚未开始或正在执行中。
- `resolved`：解决，有时候也称为兑现`fulfilled`，表示异步操作已经完成。
- `rejected`：拒绝，表示异步操作没有成功完成。

 *pending*是期约的最初始状态，可以改变为*resolved*或*rejected*状态。状态不可逆，一旦改变了状态就不能再改变。期约的状态是私有的，无法被外部检测到和修改，因为期约故意将异步行为封装起来，从而隔离外部的同步代码。

#### 11.2.2.4 *Promise.resolve()*

通过调用`Promise.resolve()`静态方法，可以实例化一个解决的期约，该期约一开始就处于*resolved*状态。该方法只接收一个参数，多的参数无效，该参数的值就是期约成功后返回的值。

#### 11.2.2.5 *Promise.reject()*

`Promise.reject()`会实例化一个拒绝的期约并抛出一个异步错误，因为对外不暴露，所以该错误不能通过*try/catch*捕获，只能通过拒绝处理程序捕获。该函数接收的第一个参数就是期约拒绝失败的理由。

### 11.2.3 期约的实例方法

#### 11.2.3.3 *Promise.prototype.catch()*

`Promise.prototype.catch()`方法用于给期约添加拒绝处理程序，只接收*onRejected*处理程序，相当于*Promise.prototype.then(null, onRejected)*的语法糖。

#### 11.2.3.4 *Promise.prototype.finally()*

`Promise.prototype.finally()`方法用于给期约添加*onFinally*处理程序，该处理程序在期约转换为解决或拒绝状态时都会执行，但该处理程序无法知道期约的状态是解决还是拒绝。

#### 11.2.3.5 非重入期约方法

当期约变为一个状态时，该状态对应的处理程序并不会立即执行，而是会被排期。跟在这个处理程序代码之后的同步代码一定会在处理程序之前先执行。[case](第11章-期约与异步函数\11.2.3.5-非重入期约方法.js)

### 11.2.4 期约连锁与期约合成

#### 11.2.4.3 *Promise.all()*和*Promise.race()*

`Promise.all()`：一个静态方法，该方法接收一个可迭代对象。该方法创建的期约会在一组期约全部解决之后再解决。如果有一个期约待定，那么合成的期约也会待定。如果有一个期约拒绝，那么合成的期约也会拒绝。

`Promise.race()`：一个静态方法，该方法接收一个可迭代对象。该方法创建的期约状态取决于参数中第一个改变状态的期约。假如参数中第一个改变状态的期约为解决态，那么方法返回的期约状态就为解决态。

## 11.3 异步函数

### 11.3.1 异步函数

#### 11.3.1.1 *async*

`async`关键字用于声明异步函数。该关键字可以用在函数声明、函数表达式、箭头函数和方法上。

异步函数始终返回期约对象。如果使用*return*关键字返回值，那么这个值会被*Promise.resolve()*包装成一个期约对象；如果没有*return*则返回*undefined*。

在异步函数中抛出错误会返回拒绝的期约。

#### 11.3.1.2 *await*

`await`关键字可以暂停异步函数代码的执行，等待期约解决。

#### 11.3.1.3 *await*的限制

***await*关键字必须在异步函数中使用。**

### 11.3.2 停止和恢复执行

*await*关键字并非只是等待一个值可用那么简单。*JS*运行时在碰到*await*关键字时，会记录在哪里暂停执行。等到*await*右边的值可用了，*JS*运行时会向消息队列推送一个任务来恢复异步函数的执行。

```js
const p = new Promise(resolve => {
  console.log(1);
  setTimeout(resolve, 1000, 2);
  console.log(3);
})
p.then(console.log);	// 1 3 2

const asyncFun = async () => {
  console.log(1);
  await 2;
  console.log(3);
}
asyncFun();	// 1 2 3
```

### 11.3.3 异步函数策略

#### 11.3.3.4 栈追踪与内存管理

期约与异步函数的功能有相当程度的重叠，但在内存中的表示差别很大。在重视性能的应用中是可以优先考虑的。







# 第 12 章  BOM

## 12.1 *window*对象

### 12.1.3 窗口位置与像素比

`screenLeft`和`screenTop`：表示窗口相对于屏幕左侧和顶部的位置，返回值的单位是*CSS*像素。

`moveTo()`：移动窗口，接收参数为新位置的绝对坐标*x*和*y*，可能被禁用。

`moveBy()`：移动窗口，接收参数为新位置的相对坐标，可能被禁用。

`window.devicePixelRatio`：屏幕物理像素与*CSS*逻辑像素之间的转换比率。

### 12.1.4 窗口大小

`outerWidth`和`outerHeight`：返回浏览器窗口自身的大小。

`innerWidth`和`innerHeight`：返回浏览器窗口中页面视口的大小。

`document.documentElement.clientWidth`和`document.documentElement.clientHeight`：返回页面视口的宽度和高度。[确定浏览器页面视口的大小](第12章-BOM\12.1.4-窗口大小.html)

`resizeTo()`：调整窗口大小，接收参数为新的宽度和高度值，可能被禁用。

`resizeBy()`：调整窗口大小，接收参数为要缩放的宽度和高度值，可能被禁用。

### 12.1.5 视口位置

`window.pageXoffset`和`window.scrollX`：返回视口横向滚动的距离。

`window.pageYoffset`和`window.scrollY`：返回视口纵向滚动的距离。

`scroll()`、`scrollTo()`和`scrollBy()`：这三个方法用于滚动页面。都接收表示相对视口距离的*x*和*y*坐标，这两个参数在前两个方法中表示要滚动到的坐标，在最后一个方法中表示滚动的距离。这几个方法还可以接收一个对象参数，参数中除了偏移值，还可以通过`behavior`属性告诉浏览器是否平滑滚动。

### 12.1.6 导航与打开新窗口

`window.open()`：可以用于导航到指定*URL*。方法接收 4 个参数：要加载的*URL*、目标窗口、特性字符串和表示新窗口在浏览器历史记录中是否替代当前加载页面的布尔值。

#### 12.1.6.3 弹窗屏蔽程序

浏览器扩展或其他程序屏蔽弹窗时，*window.open()*通常会抛出错误，所以最好用*try/catch*包装起来。如果弹窗被屏蔽了，那么*window.open()*方法会返回*null*。

### 12.1.7 定时器

`setTimeout()`和`setInterval()`：*JS*在浏览器中是单线程执行的，但允许使用这两个方法指定在某个时间之后或每隔一段时间就执行相应的代码。这两个方法会返回一个数值*ID*，用于解除定时。

**时间参数是要等待的毫秒数，而不是要执行代码的确切时间。*JS*维护了一个任务队列，时间参数只是告诉*JS*引擎在指定的毫秒数后将任务添加到这个队列。如果队列不为空，则代码必须等待前面的任务执行完才能执行。**

**所有超时执行的代码都会在全局作用域中的一个匿名函数中运行，所以函数中的*this*在非严格模式下始终指向*window*，严格模式下则是*undefined*。使用箭头函数可以将*this*保留为定义它时所在的词汇作用域。**

`clearTimeout()`和`clearInterval()`：这两个方法接收一个数值*ID*，用于解除定时。

### 12.1.8 系统对话框

`alert()`：弹出提示警告框。

`confirm()`：弹出确认框。点击确认返回*true*，点击取消返回*false*。

`prompt()`：弹出用户输入框。返回值同上。

`find()`和`print()`：异步对话框，控制权会立即返回给脚本。

## 12.2 *location*对象

`location`对象中提供了当前窗口中加载文档的信息及通常的导航功能。该对象独特的地方在于既是*window*的属性，也是*document*的属性。该对象中也保存着*URL*解析后的信息，通过属性访问，属性表在*p372*。

### 12.2.1 查询字符串

`URLSearchParams`：该引用类型中提供了一组标准*API*，用于检查和修改查询字符串（也就是*URL*中的参数字符串）。构造函数接收一个查询字符串。

### 12.2.2 操作地址

`location.assign()`：传入一个*URL*会立即启动导航到新*URL*，同时在浏览器历史记录中增加一条记录。

`location.href`和`window.location`：为这两个属性赋值一个*URL*，会调用*location.assign()*方法，并把*URL*当作参数传入方法中。

`location.replace()`：接收一个*URL*参数，重新加载该*URL*且不会增加历史记录，即不能回退。

`location.reload()`：重新加载当前显示的页面。不传参数则可能从缓存中加载，传入*true*参数会从服务器重新加载。该方法之后的代码可能执行也可能不执行，所以最好把该方法放在最后。

## 12.3 *navigator*对象

`navigator`对象的属性通常用于确定浏览器的类型，属性表在*p376*。

`navigator.userAgent`：返回用户代理字符串。

### 12.3.1 检测插件

`navigator.plugins`：返回一个浏览器插件数组，数组中的每一项都包含如下属性。

- *name*：插件名称。
- *description*：插件介绍。
- *filename*：插件的文件名。
- *length*：由当前插件处理的*MIME*类型数量。

### 12.3.2 注册处理程序

`navigator.registerProtocolHandler()`：借助这个方法可以将*Web*应用程序注册为像桌面软件一样的默认应用程序。

## 12.4 *screen*对象

`window.screen`：该对象中保存着纯粹的客户端能力信息，常用于评估浏览网站的设备信息。

## 12.5 *history*对象

`window.history`对象表示当前窗口首次使用以来用户的导航历史记录。该对象不会暴露用户访问过的*URL*，但可以通过它在不知道实际*URL*的情况下前进和后退。

### 12.5.1 导航

`history.go()`：该方法接收一个整数参数，正数表示在历史记录中前进，负数表示在历史记录中后退。

`history.back()`：模拟浏览器后退按钮。

`history.forward()`：模拟浏览器前进按钮。

`history.length`：该属性反映历史记录的数量。

### 12.5.2 历史状态管理

`history.pushState()`：可以将状态信息推送到历史记录中。







# 第 13 章  客户端检测

## 13.2 用户代理检测

以下是*github*上维护较频繁的第三方用户代理解析程序：

- *Bowser*
- *UAParser.js*
- *Platform.js*
- *CURRENT-DEVICE*
- *Google Closure*
- *Mootools*

## 13.3 软件与硬件检测

### 13.3.1 识别浏览器与操作系统

`navigator.oscpu`：对应用户代理字符串中操作系统/系统架构相关信息。

`navigator.vendor`：一个字符串，包含浏览器开发商信息。

`navigator.platform`：一个字符串，通常表示浏览器所在的操作系统。

`screen.colorDepth`和`screen.pixelDepth`：返回显示器每像素颜色的位深。

`screen.orientation`：返回一个对象，对象中有`angle`和`type`属性。前者返回相对于默认状态下屏幕的角度，后者返回 4 种枚举值。主要用于确定设备旋转后浏览器的朝向变化。

### 13.3.2 浏览器元数据

#### 13.3.2.1 *Geolocation API*

`navigator.geolocation`：该属性中的*API*可以让浏览器脚本感知当前设备的地理位置。这些*API*只在通过*HTTPS*获取的脚本中可用。

`geolocation.getCurrentPosition()`：该方法可以获取浏览器当前的位置。

#### 13.3.2.2 *Connection State*和*NetworkInformation API*

在设备连接到网络时，会在*window*对象上触发`online`事件，相应的断开网络连接后，会在*window*对象上触发`offline`事件。可以通过`navigator.onLine`属性来确定浏览器的联网状态，该属性返回一个布尔值。

怎样才算联网取决于浏览器与系统实现，可能接入局域网而没有真正接入互联网就算联网了。

`navigator.connection`：该属性暴露了*NetworkInformation API*，提供了一些只读属性。

#### 13.3.2.3 *Battery Status API*

`navigator.getBattery()`方法会返回一个期约实例，解决为一个*BatteryManager*对象，通过这个对象可以访问设备电池及充电状态的信息。

解决对象包含 4 个只读属性，提供了设备电池的相关信息。

- `charging`：布尔值，表示设备是否接入电源充电。如果没有电池返回*true*。
- `chargingTime`：整数，表示离充满还有多少秒。如果已充满或设备没有电池，则返回 0 或*Infinity*。
- `dischargingTime`：整数，表示离电量耗尽还有多少秒。如果设备没有电池或正在充电，则返回*Infinity*。
- `level`：浮点数，表示电量百分比。如果设备没有电池返回 1.0。

还提供了 4 个事件属性，可以监听相应的电池事件。

- `onchargingchange`
- `onchargingtimechange`
- `ondischargingtimechange`
- `onlevelchange`

### 13.3.3 硬件

`navigator.hardwareConcurrency`：该属性返回浏览器支持的逻辑处理器核心数量，该数量为浏览器可以并行执行的最大工作线程数量，不一定是实际的*CPU*核心数。

`navigator.deviceMemory`：该属性返回设备大致的系统内存大小，单位为*GB*。

`navigator.maxTouchPoints`：该属性返回触摸屏支持的最大关联触点数量，为一个整数值。

## 13.4 小结

在选择客户端检测方法时，首选是使用能力检测，用户代理检测是最后一个选择，因为它过于依赖用户代理字符串。







# 第 14 章  DOM

## 14.1 节点层级

`document`节点是每个文档的根节点。根节点唯一的子节点是`<html>`元素，称之为文档元素。

### 14.1.1 *Node*类型

每个节点都有`nodeType`属性，表示该节点的类型。节点类型由定义在*Node*类型上的 12 个数值常量表示。节点类型可以通过与这些常量比较来确定。

#### 14.1.1.1 *nodeName*与*nodeValue*

`nodeName`与`nodeValue`保存着有关节点的信息。这两个属性的值完全取决于节点类型，使用前应该先检测节点类型。对元素而言，*nodeName*始终等于元素的标签名，而*nodeValue*则始终为*null*。

#### 14.1.1.2 节点关系

每个节点都有一个`childNodes`属性，该属性是一个`NodeList`的实例。*NodeList*是一个类数组对象，用于存储可按位置存取的有序节点。*NodeList*是一个对*DOM*结构的查询，因此*DOM*结构的变化会自动地在*NodeList*中反映出来。*NodeList*可以通过*item()*方法或中括号访问类数组中的元素。

每个节点都有一个`parentNode`属性，指向其*DOM*树中的父元素。

具有同一个父元素的节点，可以通过`previousSibling`和`nextSibling`在兄弟节点中导航。兄弟节点列表中第一个节点的*previousSibling*为*null*，最后一个节点的*nextSibling*为*null*。

父节点的`firstChild`和`lastChild`分别指向*childNodes*中的第一个和最后一个子节点。

`hasChildNodes()`：返回*true*说明节点有一个或多个子节点。

`ownerDocument`：该属性指向代表整个文档的文档节点，即*document*。

#### 14.1.1.3 操纵节点

`appendChild( newNode )`：用于在*childNodes*列表末尾添加节点。返回新添加的节点。如果把文档中已经存在的节点传入该方法，那么这个节点会从之前的位置转移到新位置，因为一个节点不会在文档中同时出现。

`insertBefore( newNode, referenceNode )`：该方法接收两个参数，分别是要插入的节点和参照节点。调用该方法后，要插入的节点会变成参照节点的前一个同胞节点，并被返回。如果参照节点为*null*，则效果与*appendChild()*方法一样。

`replaceChild( newNode, oldNode )`：接受两个参数，分别是要插入的节点和要替换的节点。要替换的节点会被返回并从文档树中完全移除。

`removeChild( removeNode )`：移除传入的节点并返回。

并非所有节点类型都有子节点，如果在不支持子节点的节点上调用以上方法，会抛出错误。

#### 14.1.1.4 其他方法

`cloneNode( bool )`：返回与调用它的节点一模一样的节点。方法接受一个布尔值参数，如果传入*true*，会进行深复制，复制节点及其整个子*DOM*树。传入*false*只会复制调用该方法的节点。复制后返回的节点属于文档所有，但未指定父节点，称为孤儿节点。**该方法只会复制*HTML*属性，不会复制添加到*DOM*节点的*JS*属性，如事件处理程序。但*IE*在很长时间内会复制，所以复制前最好删除事件处理程序。**

`normalize()`：在节点上调用该方法，会检测该节点的所有后代。如果发现空文本节点，将其删除；如果两个文本节点是相邻的同胞节点，将其合并为一个文本节点。

### 14.1.2 *Document*类型

`Document`类型是*JS*中表示文档节点的类型。在浏览器中，文档对象*document*表示整个*HTML*页面，是*HTMLDocument*的实例，而*HTMLDocument*继承*Document*。*Document*类型的节点具有以下特征：

- *nodeType*等于 9。
- *nodeName*值为*#document*。
- *nodeValue*值为*null*。
- *parentNode*值为*null*。
- 子节点可以是*DocumentType*(最多一个，即*<!doctype>*)、*Element*(最多一个，即*html*元素)、*ProcessingInstruction*或*Comment*类型(注释)。

#### 14.1.2.1 文档子节点

`document.documentElement`：该属性始终指向*HTML*页面中的*html*元素。

`document.body`：该属性始终指向*HTML*页面中的*body*元素。

`document.doctype`：该属性可用于访问*<!doctype>*标签信息。*<!doctype>*标签是*DocumentType*类型的节点。

出现在*html*元素外面的注释也是文档的子节点，它们的类型是*Comment*。但由于浏览器实现不同，所以这些注释不一定能被识别，或是表现不一。

#### 14.1.2.2 文档信息

`document.title`：通过这个属性可以读写页面的标题。

`document.URL`：包含当前页面的完整*URL*。

`document.domain`：包含页面的域名。

`document.referrer`：包含链接到当前页面的那个页面的*URL*。

#### 14.1.2.3 定位元素

`document.getElementById()`：接收一个参数，即要获取元素的*ID*，如果找到了则返回这个元素，如果没找到则返回*null*。如果有多个*ID*相同的元素，则返回在文档中第一个出现的元素。

`document.getElementsByTagName()`：接收一个参数，即要获取元素的标签名，返回包含零个或多个元素的*NodeList*。在*HTML*文档中，该方法返回的是一个*HTMLCollection*对象，该对象与*NodeList*很相似。除了*item()*方法和中括号获取元素外，该对象还有`namedItem()`方法，可通过标签的*name*属性取得某一项元素的引用。

`document.getElementsByName()`：该方法会返回具有给定*name*属性的所有元素。

#### 14.1.2.4 特殊集合

`document.anchors`：包含文档中所有带*name*属性的*a*元素。

`document.links`：包含文档中所有带*href*属性的*a*元素。

`document.forms`：包含文档中所有*form*元素。

`document.images`：包含文档中所有*img*元素。

以上属性都是*HTMLCollection*的实例。

### 14.1.3 *Element*类型

`Element`表示*XML*或*HTML*元素，具有以下特征。

- `nodeType`等于 1。
- `nodeName`值为元素的标签名。
- `nodeValue`值为*null*。
- `parentNode`值为*Document*或*Element*对象。

可以通过`nodeName`或`tagName`属性来获取元素的标签名，这两个属性返回相同的值。**在*HTML*中，元素标签名始终以全大写表示，在*XML*和*XHTML*中，标签名始终与源代码中的大小写一致。**

#### 14.1.3.2 取得属性

`getAttribute()`：获取元素上的属性。

获取*style*属性时，使用*getAttribute()*方法返回的是*CSS*字符串，使用*DOM*对象的属性访问会返回一个*CSSStyleDeclaration*对象。

使用*getAttribute()*访问事件属性，返回的是字符串形式的源代码。通过*DOM*对象的属性访问事件属性时返回的是一个*JS*函数，未指定该属性则返回*null*。

所以一般*getAttribute()*方法主要用于取得自定义属性的值。*DOM*对象无法访问自定义属性。

#### 14.1.3.3 设置属性

`setAttribute()`：设置元素的属性。如果属性已经存在，覆盖原来的值。无法直接在*DOM*对象上添加自定义属性，但可以通过该方法添加自定义属性。

`removeAttribute()`：删除元素的属性。

#### 14.1.3.4 *attributes*属性

`attributes`属性包含一个*NamedNodeMap*实例，是一个类似*NodeList*的实时集合。元素的每一个属性都包含在*attributes*属性中，该属性包含下列方法。

- `getNamedItem(name)`：返回*nodeName*属性等于*name*的节点。
- `removeNamedItem(name)`：删除*nodeName*属性等于*name*的节点。
- `setNamedItem(node)`：向列表中添加*node*节点，以其*nodeName*为索引。
- `item(pos)`：返回索引位置*pos*处的节点。

#### 14.1.3.5 创建元素

`document.createElement()`：该方法用于创建新元素，返回新创建的元素。接收一个参数，即要创建元素的标签名。新创建的元素并没有添加到文档树中。

#### 14.1.3.6 元素后代

要想知道一个元素有多少个后代节点，可以在该元素上调用*getElementsByTagName()*。在元素上调用这个方法与在*document*上调用是一样的，只不过把范围限制在了当前元素之内。

```js
element.getElementsByTagName('*').length;	// 返回 element 元素后代节点的数量
```

### 14.1.4 *Text*类型

*Text*节点由*Text*类型表示。具有以下特征。

- `nodeType`等于 3。
- `nodeName`值为*#text*。
- `nodeValue`值为节点中包含的文本。
- `parentNode`值为*Element*对象。
- 不支持子节点。

*Text*节点中包含的文本可以通过*nodeValue*属性访问，也可以通过*data*属性访问。文本节点有以下操作文本的方法。

- `appendData( text )`：向节点末尾添加文本*text*。
- `deleteData( offset, count )`：从位置*offset*开始删除*count*个字符。
- `insertData( offset, text )`：在位置*offset*插入*text*。
- `replaceData( offset, count, text )`：用*text*替换从位置*offset*到*offset + count*的文本。
- `splitText( offset )`：在位置*offset*将当前文本节点拆分为两个文本节点。
- `substringData( offset, count )`：提取从位置*offset*到*offset + count*的文本。

#### 14.1.4.1 创建文本节点

`document.createTextNode()`：用来创建新文本节点。

#### 14.1.4.2 规范化文本节点

`normalize()`：*Node*类型中定义的方法，用于合并相邻的文本节点。

### 14.1.5 *Comment*类型

*DOM*中的注释通过*Comment*类型表示。*Comment*类型的节点具有以下特征。

- *nodeType*等于 8。
- *nodeName*值为*#comment*。
- *nodeValue*值为注释的内容。
- *parentNode*值为*Document*或*Element*对象。
- 不支持子节点。

*Comment*类型拥有除*spiltText()*之外*Text*节点所有的字符串操作方法。

`document.createComment()`：创建注释节点。

### 14.1.6 *CDATASection*类型

*CDATASection*类型表示*XML*中特有的*CDATA*区块。*CDATASection*类型继承*Text*类型，因此拥有包括*splitText()*在内的所有字符串操作方法。*CDATASection*类型的节点具有以下特征。

- *nodeType*等于 4。
- *nodeName*值为*#cdata-section*。
- *nodeValue*值为*CDATA*区块的内容。
- *parentNode*值为*Document*或*Element*对象。
- 不支持子节点。

在真正的*XML*文档中，可以使用`document.createCDataSection()`并传入节点内容来创建*CDATA*区块。

### 14.1.7 *DocumentType*类型

*DocumentType*类型的节点包含文档的文档类型*doctype*信息，具有以下特征。

- *nodeType*等于 10。
- *nodeName*值为文档类型的名称。
- *nodeValue*值为*null*。
- *parentNode*值为*Document*对象。
- 不支持子节点。

*DocumentType*对象保存在*document.doctype*属性中。*DOM Level 1*规定了*DocumentType*对象的 3 个属性：*name*、*entities*和*notations*。

### 14.1.8 *DocumentFragment*类型

*DocumentFragment*类型是唯一一个在标记中没有对应表示的类型。*DOM*将文档片段定义为**轻量级**文档，能够包含和操作节点，却没有完整文档那样额外的消耗。*DocumentFragment*节点具有以下特征。

- *nodeType*等于 11。
- *nodeName*值为*#document-fragment*。
- *nodeValue*值为*null*。
- *parentNode*值为*null*。
- 子节点可以是*Element*、*ProcessingInstruction*、*Comment*、*Text*、*CDATASection*或*EntityReference*。

 不能直接把文档片段添加到文档。相反，文档片段的作用是充当其他要被添加到文档的节点的仓库。可以使用*document.createDocumentFragment()*方法创建文档片段。[case](第14章-DOM\14.1.8-DocumentFragment类型.html)

### 14.1.9 *Attr*类型

元素数据在*DOM*中通过*Attr*类型表示。*Attr*类型构造函数和原型在所有浏览器中都可以直接访问。*Attr*节点具有以下特征。

- *nodeType*等于 2。
- *nodeName*值为属性名。
- *nodeValue*值为属性值。
- *parentNode*值为*null*。
- 在*HTML*中不支持子节点。
- 在*XML*中子节点可以是*Text*或*EntityReference*。

**将属性作为节点来访问多数情况下并无必要。推荐使用*getAttribute()*、*removeAttribute()*和*setAttribute()*方法操作属性，而不是直接操作属性节点。**

## 14.3 *MutationObserver*接口

`MutationObserver`接口可以在*DOM*被修改时异步执行回调。

### 14.3.1 基本用法

*MutationObserver*的实例要通过调用*MutationObserver*构造函数并传入一个回调函数来创建。

#### 14.3.1.1 *observe()*方法

使用*observe()*方法将新创建的*MutationObserver*实例与要观察的*DOM*节点关联起来。该方法接收两个参数：要观察其变化的*DOM*节点以及一个*MutationObserverInit*对象。*MutationObserverInit*对象用于控制观察哪些方面的变化，是一 个键/值对形式配置选项的字典。[观察*body*元素上的属性变化](第14章-DOM\14.3.1.1-observe()方法.html)

#### 14.3.1.2 回调与*MutationRecord*

每个回调都会收到一个*MutationRecord*实例的数组。*MutationRecord*实例包含的信息包括发生了什么变化，以及*DOM*的哪一部分受到了影响。因为回调执行之前可能同时发生多个满足观察条件的事件，所以每次执行回调都会传入一个包含按顺序入队的*MutationRecord*实例的数组。*MutationRecord*实例的属性表在*p434*。

传给回调函数的第二个参数是观察变化的*MutationObserver*的实例。

```js
let observer = new MutationObserver((mutationRecords, mutationObserver) => {})
```

#### 14.3.1.3 *disconnect()*方法

默认情况下，只要被观察的元素不被垃圾回收，*MutationObserver*的回调就会响应*DOM*变化事件，从而被执行。要提前终止执行回调，可以调用`disconnect()`方法。

#### 14.3.1.4 复用*MutationObserver*

多次调用*observe()*方法，可以复用一个*MutationObserver*对象 观察多个不同的目标节点。***disconnect()*方法是一个一刀切的方案，调用它会停止观察所有目标。**

#### 14.3.1.5 重用*MutationObserver*

调用*disconnect()*并不会结束*MutationObserver*的生命。还可以重新使用这个观察者，再将它关联到新的目标节点。

### 14.3.2 *MutationObserverInit*与观察范围

*MutationObserverInit*对象用于控制对目标节点的观察范围。粗略地讲，观察者可以观察的事件包括属性变化、文本变化和子节点变化。

**在调用*observe()*时，*MutationObserverInit*对象中的*attributes*、*characterData*和*childList*属性必须至少有一项为*true*（无论是直接设置这几个属性，还是通过设置*attributeOldValue*等属性间接导致它们的值转换为*true*）。否则会抛出错误，因为没有任何变化事件可能触发回调。**

#### 14.3.2.1 观察属性

*MutationObserver*可以观察节点属性的添加、移除和修改。要为属性变化注册回调，需要在*MutationObserverInit*对象中将*attributes*属性设置为*true*。

把*attributes*设置为*true*的默认行为是观察所有属性，但不会在*MutationRecord*对象中记录原来的属性值。如果想观察某个或某几个属性，可以使用*attributeFilter*属性来设置白名单，即一个属性名字符串数组。

如果想在变化记录中保存属性原来的值，可以将*attributeOldValue*属性设置为*true*。

#### 14.3.2.2 观察字符数据

*MutationObserver*可以观察文本节点（如*Text*、*Comment*或*ProcessingInstruction*节点）中字符的添加、删除和修改。要为字符数据注册回调，需要在*MutationObserverInit*对象中将*characterData*属性设置为*true*。

将*characterData*属性设置为*true*的默认行为不会在*MutationRecord*对象中记录原来的字符数据。如果想在变化记录中保存原来的字符数据，可以将*characterDataOldValue*属性设置为*true*。

#### 14.3.2.3 观察子节点

*MutationObserver*可以观察目标节点子节点的添加和移除。要观察子节点，需要在*MutationObserverInit*对象中将*childList*属性设置为*true*。

#### 14.3.2.4 观察子树

默认情况下，*MutationObserver*将观察的范围限定为一个元素及其子节点的变化。可以把观察的范围扩展到这个元素的子树（所有后代节点），这需要在*MutationObserverInit*对象中将*subtree*属性设置为*true*。

**被观察子树中的节点被移出子树之后仍然能够触发变化事件。这意味着在子树中的节点离开该子树后，即使严格来讲该节点已经脱离了原来的子树，但它仍然会触发变化事件。**

### 14.3.3 异步回调与记录队列

#### 14.3.3.2 *takeRecords()*方法

调用*MutationObserver*实例的*takeRecords()*方法可以清空记录队列，取出并返回其中的所有*MutationRecord*实例。







# 第 15 章  DOM 扩展

## 15.1 *Selectors API*

*Selectors API Level 1*的核心：*Document*和*Element*类型的实例上都暴露了`querySelector()`和`querySelectorAll()`方法。

*Selectors API Level 2*规范在*Element*类型上新增了更多方法，比如*matches()*、*find()*和*findAll()*。不过，目前还没有浏览器实现或宣称实现*find()*和*findAll()*。

### 15.1.1 *querySelector()*

该方法接收*CSS*选择符参数，返回匹配该模式的第一个后代元素，如果没有匹配项则返回*null*。

用于查询模式的*CSS*选择符可繁可简，依需求而定。如果选择符有语法错误或碰到不支持的选择符，则*querySelector()*方法会抛出错误。

在*Document*上使用*querySelector()*方法时，会从文档元素开始搜索；在*Element*上使用*querySelector()*方法时，则只会从当前元素的后代中查询。

### 15.1.2 *querySelectorAll()*

*querySelectorAll()*方法跟*querySelector()*一样，也接收一个用于查询的参数，但它会返回所有匹配的节点，而不止一个。这个方法返回的是一个*NodeList*的**静态实例**。**这里返回的是静态实例，而不是像前面一样会实时查询的*NodeList*实例。**

以有效*CSS*选择符调用*querySelectorAll()*都会返回*NodeList*，无论匹配多少个元素都可以。如果没有匹配项，则返回空的*NodeList*实例。

### 15.1.3 *matches()*

*matches()*方法接收一个*CSS*选择符参数，如果元素匹配该选择符则返回*true*，否则返回*false*。

## 15.2 元素遍历

*IE9*之前的版本不会把元素间的空格当成空白节点，而其他浏览器会。这就导致了*childNodes*和*firstChild*等属性的差异。为了弥补这个差异，同时不影响*DOM*规范，*W3C*通过了新的*Element Traversal*规范并定义了一组新属性。*Element Traversal API*为*DOM*元素添加了 5 个属性：

- `childElementCount`：返回子元素数量（不包含文本节点和注释）。
- `firstElementChild`：指向第一个*Element*类型的子元素（*Element*版*firstChild*）。
- `lastElementChild`：指向最后一个*Element*类型的子元素（*Element*版*lastChild*）
- `previousElementSibling`：指向前一个*Element*类型的同胞元素（*Element*版*previousSibling*）
- `nextElementSibling`：指向后一个*Element*类型的同胞元素（*Element*版*nextSibling*）。

在支持的浏览器中，所有*DOM*元素都会有这些属性，为遍历*DOM*元素提供便利。这样开发者就不用担心空白文本节点的问题了。

## 15.3 *HTML5*

### 15.3.1 *CSS*类扩展

#### 15.3.1.1 *getElementsByClassName()*

`getElementsByClassName()`是*HTML5*新增的最受欢迎的一个方法，暴露在*document*对象和所有*HTML*元素上。

*getElementsByClassName()*方法接收一个参数，即包含一个或多个类名的字符串，返回类名中包含相应类的元素的*NodeList*。如果提供了多个类名，则顺序无关紧要。

#### 15.3.1.2 *classList*属性

要操作类名，可以通过*className*属性实现添加、删除和替换。但*className*是一个字符串，所以每次操作之后都需要重新设置这个值才能生效，即使只改动了部分字符串也一样。这使得类名操作有点麻烦，*HTML5*为所有元素增加了*classList*属性，从而为类名操作提供了更简单也更安全的实现方式。

*classList*是一个新的集合类型*DOMTokenList*的实例。与其他*DOM*集合类型一 样，*DOMTokenList*也有*length*属性表示自己包含多少项，也可以通过*item()*或中括号取得个别的元素。此外，*DOMTokenList*还增加了以下方法。

- `add(value)`：向类名列表中添加指定的字符串值*value*。如果这个值已经存在，则什么也不做。
- `contains(value)`：返回布尔值，表示给定的*value*是否存在。
- `remove(value)`：从类名列表中删除指定的字符串值*value*。
- `toggle(value)`：如果类名列表中已经存在指定的*value*则删除；如果不存在，则添加。

**添加了*classList*属性之后，除非是完全删除或完全重写元素的*class*属性，否则*className*属性就用不到了。*IE10*及以上版本（部分）和其他主流浏览器（完全）实现了*classList*属性。**

### 15.3.2 焦点管理

`document.activeElement`：始终包含当前拥有焦点的*DOM*元素。默认情况下，*document.activeElement*在页面刚加载完之后会设置为*document.body*。而在页面完全加载之前，*document.activeElement*的值为*null*。

`document.hasFocus()`：该方法返回布尔值，表示文档是否拥有焦点。

确定文档是否获得了焦点，就可以帮助确定用户是否在操作页面。第一个方法可以用来查询文档，确定哪个元素拥有焦点；第二个方法可以查询文档是否获得了焦点，而**这对于保证*Web*应用程序的无障碍使用是非常重要的**。

### 15.3.3 *HTMLDocument*扩展

#### 15.3.3.1 *readyState*属性

`document.readyState`属性有两个可能的值：

- `loading`：表示文档正在加载。
- `complete`：表示文档加载完成。

#### 15.3.3.2 *compatMode*属性

`document.compatMode`属性表示浏览器当前处于什么渲染模式。标准模式下*document.compatMode*的值是*CSS1Compat*，而在混杂模式下，*document.compatMode*的值是*BackCompat*。

#### 15.3.3.3 *head*属性

作为对*document.body*（指向文档的元素）的补充，*HTML5*增加了*document.head*属性，指向文档的*head*元素。

### 15.3.4 字符集属性

*document.characterSet*属性表示文档实际使用的字符集，也可以用来指定新字符集。这个属性的默认值是*UTF-16*，但可以通过*meta*元素或响应头来修改。

### 15.3.5 自定义数据属性

可以使用前缀`data-`为元素指定自定义的非标准的属性，然后通过元素的`dataset`属性来访问。

*dataset*属性是一个*DOMStringMap*的实例，包含一组键/值对映射。元素的每个*data-name*属性在*dataset*中都可以通过*data-*后面的字符串作为键来访问（例如，属性*data-myname*、*data-myName*可以通过*myname*访问，但要注意*data-my-name*、*data-My-Name*要通过*myName*来访问）。

### 15.3.6 插入标记

#### 15.3.6.1 *innerHTML*属性

在读取*innerHTML*属性时，会返回元素所有后代的*HTML*字符串，包括元素、注释和文本节点。而在写入*innerHTML*时，则会根据提供的字符串值以新的*DOM*子树替代元素中原来包含的所有节点。

#### 15.3.6.3 *outerHTML*属性

读取*outerHTML*属性时，会返回调用它的元素（及所有后代元素）的*HTML*字符串。在写入*outerHTML*属性时，调用它的元素会被传入的*HTML*字符串经解释之后生成的*DOM*树取代。

#### 15.3.6.4 *insertAdjacentHTML()*与*insertAdjacentText()*

*insertAdjacentHTML()*和*insertAdjacentText()*这两个方法都接收两个参数：要插入标记的位置和要插入的*HTML*或文本。第一个参数必须是下列值中的一个：

- *beforebegin*：插入当前元素前面，作为前一个同胞节点。
- *afterbegin*：插入当前元素内部，作为新的子节点或放在第一个子节点前面。
- *beforeend*：插入当前元素内部，作为新的子节点或放在最后一个子节点后面。
- *afterend*：插入当前元素后面，作为下一个同胞节点。

#### 15.3.6.5 内存与性能问题

如果被移除的子树元素中之前有关联的事件处理程序或其他*JavaScript*对象（作为元素的属性），那它们之间的绑定关系会滞留在内存中。如果这种替换操作频繁发生，页面的内存占用就会持续攀升。

### 15.3.7 *scrollIntoView()*

*scrollIntoView()*方法存在于所有*HTML*元素上，可以滚动浏览器窗口或容器元素以便包含元素进入视口。这个方法的参数如下：

- *alignToTop*是一个布尔值，默认为*true*。
  - *true*：窗口滚动后元素的顶部与视口顶部对齐。
  - *false*：窗口滚动后元素的底部与视口底部对齐。
- *scrollIntoViewOptions是一个选项对象*。
  - *behavior*：定义过渡动画，可取的值为*smooth*和*auto*，默认为*auto*。
  - *block*：定义垂直方向的对齐，可取的值为*start*、*center*、*end*和*nearest*，默认为*start*。
  - *inline*：定义水平方向的对齐，可取的值为*start*、*center*、*end*和*nearest*，默认为*nearest*。

## 15.4 专有扩展

### 15.4.1 *children*属性

*children*属性是一个*HTMLCollection*，只包含元素的*Element*类型的子节点。如果元素的子节点类型全部是元素类 型，那*children*和*childNodes*中包含的节点应该是一样的。

### 15.4.2 *contains()*方法

*DOM*编程中经常需要确定一个元素是不是另一个元素的后代。*IE*首先引入了*contains()*方法，让开发者可以在不遍历*DOM*的情况下获取这个信息。

*contains()*方法应该在要搜索的祖先元素上调用，参数是待确定的目标节点。如果目标节点是被搜索节点的后代，*contains()*返回*true*，否则返回*false*。

使用*DOM Level 3*的*compareDocumentPosition()*方法也可以确定节点间的关系。这个方法会返回表示两个节点关系的位掩码。*compareDocumentPosition()*方法的结果可以通过按位与来确定参考节点是否包含传入的节点。

### 15.4.3 插入标记

#### 15.4.3.1 *innerText*属性

*innerText*属性对应元素中包含的所有文本内容，无论文本在子树中哪个层级。在用于读取值时，*innerText*会按照深度优先的顺序将子树中所有文本节点的值拼接起来。在用于写入值时，*innerText*会移除元素的所有后代并插入一个包含该值的文本节点。

此外，设置*innerText*也会编码出现在字符串中的*HTML*语法字符（小于号、大于号、引号及和号）。

#### 15.4.3.2 *outerText*属性

*outerText*与*innerText*是类似的，只不过作用范围包含调用它的节点。要读取文本值时，*outerText*与*innerText*实际上会返回同样的内容。但在写入文本值时，*outerText*就大不相同了。写入文本值时，*outerText*不止会移除所有后代节点，而是会替换整个元素。

