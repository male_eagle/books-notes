'use strict';

// function factorial (num) {
//   if (num <= 1) {
//     return 1;
//   }
//   // 严格模式下报错
//   return num * arguments.callee(num - 1);
// }

// 命名函数表达式
const factorial = (function f (num) {
  if (num <= 1) {
    return 1;
  }
  return num * f(num - 1);
});

console.log(factorial(5));