let synchronousResolve;

let p = new Promise(resolve => {
  synchronousResolve = function () {
    console.log('1: invoking resolve')
    resolve()
    console.log('3: resolve() returns')
  }
})

p.then(() => console.log('2: then() handler executes'))

synchronousResolve()

console.log('4: synchronousResolve() returns')
console.log('5')
console.log('6')
console.log('7')
console.log('8')
console.log('9')