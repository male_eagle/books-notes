let book = {
  title: "Professional JavaScript",
  authors: [
    "Nicholas C. Zakas",
    "Matt Frisbie"
  ],
  edition: 4,
  year: 2017,
  releaseDate: new Date(2017, 11, 1)
};

// JSON.stringify 方法的第三个参数可用于排版，默认输出的字符串是没有空格的
let jsonText = JSON.stringify(book, null, 2);
// Date 类型有 toJSON 方法，所以转换为 JSON 字符串时会自动将 Date 对象转换为 JSON 字符串
console.log(jsonText);

let bookCopy1 = JSON.parse(jsonText, (key, value) => key == "releaseDate" ? new Date(value) : value);
// releaseDate 被重新转换为 Date 对象
console.log(bookCopy1);

let bookCopy2 = JSON.parse(jsonText);
// releaseDate 还是 Date 类型 toJSON 方法转换的字符串
console.log(bookCopy2);