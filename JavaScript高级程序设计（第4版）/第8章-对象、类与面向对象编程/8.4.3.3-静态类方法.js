class Person {
  constructor () {
    this.locate = () => console.log('instance', this);
  }
  
  locate () {
    console.log('prototype', this);
  }
  
  static locate () {
    console.log('class', this);
  }
}

let p = new Person();

p.locate();	// instance, Person {}

Person.prototype.name = 'eagle';
Person.prototype.locate();	// prototype, { constructor: ... }

Person.locate();	// class, class Person {}