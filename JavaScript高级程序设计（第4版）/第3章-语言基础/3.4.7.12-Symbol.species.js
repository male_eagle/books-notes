class Bar extends Array {}
class Foo extends Array {
  static get [Symbol.species] () {
    return Array;
  }
}

let bar = new Bar();
console.log(bar instanceof Array);
console.log(bar instanceof Bar);
bar = bar.concat('bar');
console.log(bar instanceof Array);
console.log(bar instanceof Bar);

let foo = new Foo();
console.log(foo instanceof Array);
console.log(foo instanceof Foo);
foo = foo.concat('foo');
console.log(foo instanceof Array);
console.log(foo instanceof Foo);