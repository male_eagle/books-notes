let obj = {
  name: 'eagle',
  age: 21
}

// for (const value of obj) {}  // TypeError: obj is not iterable

obj[Symbol.iterator] = function () {
  let index = 0,
      self = this,
      keys = Object.keys(self);

  console.log(self);  // { name: 'eagle', age: 21, [Symbol(Symbol.iterator)]: [Function (anonymous)] }
  
  return {
    next () {
      if (index < keys.length) {
        return {
          value: self[keys[index++]],
          done: false
        }
      } else {
        return {
          value: undefined,
          done: true
        }
      }
    }
  }
}

for (const value of obj) {
  console.log(value);
}