let s1 = Symbol('foo'),
    s2 = Symbol('bar'),
    s3 = Symbol('baz'),
    s4 = Symbol('qux');

let o = {
  name: 'eagle',
  age: '20',
  [s1]: 'foo value'   // 也可以 o[s1] = 'foo value';
};

Object.defineProperty(o, s2, { value: 'bar value' });

Object.defineProperties(o, {
  [s3]: { value: 'baz value' },
  [s4]: { value: 'qux value' }
});

// node 中只会打印出对象字面量形式定义的 s1 属性
// 浏览器 f12 中可以打印出 s1，s2，s3 和 s4
console.log(o);

console.log(Object.getOwnPropertyNames(o));

console.log(Object.getOwnPropertySymbols(o));

console.log(Object.getOwnPropertyDescriptors(o));

console.log(Reflect.ownKeys(o));