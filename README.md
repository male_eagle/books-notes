# 介绍

前端书籍的阅读笔记，具体笔记在各自文件夹中的 md 文件、html 文件和 js 文件中。

# Javascript 书籍

《JavaScript语言精粹与编程实践》第 3 版、《Javascript高级程序设计》第 3 版、《JavaScript高级程序设计》第 4 版、《ES6标准入门》第 3 版。

# CSS 书籍

《CSS权威指南》第 4 版、《CSS揭秘》。
